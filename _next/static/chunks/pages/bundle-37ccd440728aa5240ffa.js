_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [53], {
        "+DN6": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = {
                argumentDefinitions: [{
                    defaultValue: null,
                    kind: "LocalArgument",
                    name: "chain"
                }],
                kind: "Fragment",
                metadata: null,
                name: "TradeStation_bundle",
                selections: [{
                    args: [{
                        kind: "Variable",
                        name: "chain",
                        variableName: "chain"
                    }],
                    kind: "FragmentSpread",
                    name: "BidModalContent_bundle"
                }],
                type: "AssetBundleType",
                abstractKey: null,
                hash: "00ba532318f9c4452aceb021f3a96683"
            };
            n.default = t
        },
        "+crw": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "order"
                    }],
                    n = [{
                        alias: null,
                        args: null,
                        concreteType: "MoonpayType",
                        kind: "LinkedField",
                        name: "moonpay",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: [{
                                kind: "Variable",
                                name: "order",
                                variableName: "order"
                            }],
                            concreteType: "MoonpayFiatCheckoutWidgetDataType",
                            kind: "LinkedField",
                            name: "moonpayFiatCheckoutWidgetData",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "url",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "externalTransactionId",
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }];
                return {
                    fragment: {
                        argumentDefinitions: e,
                        kind: "Fragment",
                        metadata: null,
                        name: "MoonPayCheckoutModalQuery",
                        selections: n,
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: e,
                        kind: "Operation",
                        name: "MoonPayCheckoutModalQuery",
                        selections: n
                    },
                    params: {
                        cacheID: "c8c864ccbf11a5e1bf2689040462e40c",
                        id: null,
                        metadata: {},
                        name: "MoonPayCheckoutModalQuery",
                        operationKind: "query",
                        text: "query MoonPayCheckoutModalQuery(\n  $order: OrderV2RelayID!\n) {\n  moonpay {\n    moonpayFiatCheckoutWidgetData(order: $order) {\n      url\n      externalTransactionId\n    }\n  }\n}\n"
                    }
                }
            }();
            t.hash = "fae0baff0614356c0bb881e81ae630e9", n.default = t
        },
        "+u0o": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "archetype"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "bundle"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "includePrivate"
                    }],
                    n = [{
                        kind: "Variable",
                        name: "archetype",
                        variableName: "archetype"
                    }, {
                        kind: "Variable",
                        name: "bundle",
                        variableName: "bundle"
                    }, {
                        kind: "Variable",
                        name: "includePrivate",
                        variableName: "includePrivate"
                    }],
                    a = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "address",
                        storageKey: null
                    },
                    t = {
                        kind: "InlineDataFragmentSpread",
                        name: "wallet_accountKey",
                        selections: [a]
                    },
                    l = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: e,
                        kind: "Fragment",
                        metadata: null,
                        name: "PrivateListingBannerQuery",
                        selections: [{
                            alias: null,
                            args: n,
                            concreteType: "TradeSummaryType",
                            kind: "LinkedField",
                            name: "tradeSummary",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "OrderV2Type",
                                kind: "LinkedField",
                                name: "bestAsk",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "taker",
                                    plural: !1,
                                    selections: [a, {
                                        args: null,
                                        kind: "FragmentSpread",
                                        name: "AccountLink_data"
                                    }, t],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "maker",
                                    plural: !1,
                                    selections: [t],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: e,
                        kind: "Operation",
                        name: "PrivateListingBannerQuery",
                        selections: [{
                            alias: null,
                            args: n,
                            concreteType: "TradeSummaryType",
                            kind: "LinkedField",
                            name: "tradeSummary",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "OrderV2Type",
                                kind: "LinkedField",
                                name: "bestAsk",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "taker",
                                    plural: !1,
                                    selections: [a, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "config",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isCompromised",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "UserType",
                                        kind: "LinkedField",
                                        name: "user",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "publicUsername",
                                            storageKey: null
                                        }, l],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "imageUrl",
                                        storageKey: null
                                    }, l],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "maker",
                                    plural: !1,
                                    selections: [a, l],
                                    storageKey: null
                                }, l],
                                storageKey: null
                            }],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "19574899acbea81a973401bda28aad1b",
                        id: null,
                        metadata: {},
                        name: "PrivateListingBannerQuery",
                        operationKind: "query",
                        text: "query PrivateListingBannerQuery(\n  $archetype: ArchetypeInputType\n  $bundle: BundleSlug\n  $includePrivate: Boolean!\n) {\n  tradeSummary(archetype: $archetype, bundle: $bundle, includePrivate: $includePrivate) {\n    bestAsk {\n      taker {\n        address\n        ...AccountLink_data\n        ...wallet_accountKey\n        id\n      }\n      maker {\n        ...wallet_accountKey\n        id\n      }\n      id\n    }\n  }\n}\n\nfragment AccountLink_data on AccountType {\n  address\n  config\n  isCompromised\n  user {\n    publicUsername\n    id\n  }\n  ...ProfileImage_data\n  ...wallet_accountKey\n  ...accounts_url\n}\n\nfragment ProfileImage_data on AccountType {\n  imageUrl\n  address\n}\n\nfragment accounts_url on AccountType {\n  address\n  user {\n    publicUsername\n    id\n  }\n}\n\nfragment wallet_accountKey on AccountType {\n  address\n}\n"
                    }
                }
            }();
            t.hash = "e715990f4c5b3977e70c77b07823410f", n.default = t
        },
        "/G3T": function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return q
            }));
            var t = a("m6w3"),
                l = a("uEoR"),
                i = a("oA/F"),
                s = (a("mXGw"), a("XIt6")),
                r = a("BDXS"),
                d = a("uq6L"),
                c = a("qBnW"),
                o = a("7yu0"),
                u = a("BOW+"),
                m = 6e4,
                y = 1440,
                p = 43200,
                g = 525600;

            function b(e, n) {
                var a = arguments.length > 2 && void 0 !== arguments[2] ? arguments[2] : {};
                Object(u.a)(2, arguments);
                var t = a.locale || o.a;
                if (!t.formatDistance) throw new RangeError("locale must contain localize.formatDistance property");
                var l = Object(r.a)(e, n);
                if (isNaN(l)) throw new RangeError("Invalid time value");
                var i, b, k = Object(c.a)(a);
                k.addSuffix = Boolean(a.addSuffix), k.comparison = l, l > 0 ? (i = Object(d.a)(n), b = Object(d.a)(e)) : (i = Object(d.a)(e), b = Object(d.a)(n));
                var f, j = null == a.roundingMethod ? "round" : String(a.roundingMethod);
                if ("floor" === j) f = Math.floor;
                else if ("ceil" === j) f = Math.ceil;
                else {
                    if ("round" !== j) throw new RangeError("roundingMethod must be 'floor', 'ceil' or 'round'");
                    f = Math.round
                }
                var h, O = b.getTime() - i.getTime(),
                    v = O / m,
                    x = Object(s.a)(b) - Object(s.a)(i),
                    A = (O - x) / m;
                if ("second" === (h = null == a.unit ? v < 1 ? "second" : v < 60 ? "minute" : v < y ? "hour" : A < p ? "day" : A < g ? "month" : "year" : String(a.unit))) {
                    var T = f(O / 1e3);
                    return t.formatDistance("xSeconds", T, k)
                }
                if ("minute" === h) {
                    var F = f(v);
                    return t.formatDistance("xMinutes", F, k)
                }
                if ("hour" === h) {
                    var S = f(v / 60);
                    return t.formatDistance("xHours", S, k)
                }
                if ("day" === h) {
                    var K = f(A / y);
                    return t.formatDistance("xDays", K, k)
                }
                if ("month" === h) {
                    var C = f(A / p);
                    return 12 === C && "month" !== a.unit ? t.formatDistance("xYears", 1, k) : t.formatDistance("xMonths", C, k)
                }
                if ("year" === h) {
                    var L = f(A / g);
                    return t.formatDistance("xYears", L, k)
                }
                throw new RangeError("unit must be 'second', 'minute', 'hour', 'day', 'month' or 'year'")
            }
            var k = a("wGtP"),
                f = a("3R3r"),
                j = a("UutA"),
                h = a("7ixG"),
                O = a("m5he"),
                v = a("OsKK"),
                x = a("b7Z7"),
                A = a("9E9p"),
                T = a("n0tG"),
                F = a("t3V9"),
                S = a("8BrW"),
                K = a("LjoF"),
                C = a("Z5z1"),
                L = a("dP7h"),
                B = a("oYCi"),
                w = ["confirmationDetails"],
                I = ["confirmationDetails"];

            function P(e, n) {
                var a = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var t = Object.getOwnPropertySymbols(e);
                    n && (t = t.filter((function(n) {
                        return Object.getOwnPropertyDescriptor(e, n).enumerable
                    }))), a.push.apply(a, t)
                }
                return a
            }

            function _(e) {
                for (var n = 1; n < arguments.length; n++) {
                    var a = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? P(Object(a), !0).forEach((function(n) {
                        Object(t.a)(e, n, a[n])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(a)) : P(Object(a)).forEach((function(n) {
                        Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(a, n))
                    }))
                }
                return e
            }
            var Q = "MMM d, y h:mm aa",
                D = Object(j.d)(x.a).withConfig({
                    displayName: "AssetConfirmationItemreact__GridRowSeparator",
                    componentId: "sc-g434db-0"
                })(["&:not(:last-child){grid-column:1 / -1;border-top:1px solid ", ";margin-bottom:16px;}"], (function(e) {
                    return e.theme.colors.border
                })),
                M = function(e) {
                    var n = e.price,
                        a = e.paymentAsset,
                        t = e.isOpen,
                        l = e.toggleIsOpen,
                        i = a.asset.usdSpotPrice;
                    return Object(B.jsxs)(B.Fragment, {
                        children: [Object(B.jsxs)(A.a.Side, {
                            children: [Object(B.jsx)(A.a.Description, {
                                children: "Price"
                            }), Object(B.jsx)(A.a.Title, {
                                display: "flex",
                                justifyContent: "flex-end",
                                children: Object(B.jsx)(L.a, {
                                    paymentAssetDataKey: a.asset,
                                    price: n.toNumber()
                                })
                            }), Object(B.jsxs)(A.a.Description, {
                                children: [i ? "$".concat(Object(K.h)(n.mul(i))) : "$0.00", " ", "USD"]
                            })]
                        }), Object(B.jsx)(A.a.Action, {
                            children: Object(B.jsx)(F.a, {
                                onClick: function(e) {
                                    e.stopPropagation(), e.preventDefault(), l()
                                },
                                children: Object(B.jsx)(O.a, {
                                    color: "gray",
                                    cursor: "pointer",
                                    title: t ? "Collapse" : "Expand",
                                    value: t ? "expand_less" : "expand_more"
                                })
                            })
                        })]
                    })
                },
                E = function(e) {
                    var n = e.duration,
                        a = e.paymentAsset,
                        t = e.endingPrice,
                        l = e.reservedBuyerAddress,
                        i = a.asset,
                        s = i.symbol,
                        r = i.usdSpotPrice;
                    return Object(B.jsx)(A.a.Footer, {
                        borderTop: "none",
                        children: Object(B.jsx)(v.e, {
                            paddingX: "32px",
                            paddingY: ["28px", "12px"],
                            style: {
                                borderLeft: 0,
                                borderRight: 0,
                                borderRadius: 0
                            },
                            width: "100%",
                            children: Object(B.jsxs)(x.a, {
                                display: "grid",
                                gridTemplateColumns: ["1fr", "repeat(2, 1fr)"],
                                children: [Object(B.jsx)(S.a, {
                                    children: Object(B.jsx)(T.a, {
                                        marginTop: [0, "revert"],
                                        variant: "pre-title-small",
                                        children: "Scheduled for"
                                    })
                                }), Object(B.jsxs)(x.a, {
                                    children: [Object(B.jsx)(T.a, {
                                        color: "text.body",
                                        marginBottom: 0,
                                        marginTop: [0, "revert"],
                                        variant: "small-bold",
                                        children: b(n.start, n.end)
                                    }), Object(B.jsxs)(T.a, {
                                        marginTop: 0,
                                        variant: "info",
                                        children: [Object(k.a)(n.start, Q), " -", " ", Object(k.a)(n.end, Q)]
                                    })]
                                }), Object(B.jsx)(D, {}), t && Object(B.jsxs)(B.Fragment, {
                                    children: [Object(B.jsx)(S.a, {
                                        children: Object(B.jsx)(T.a, {
                                            variant: "pre-title-small",
                                            children: "Includes reduced ending price"
                                        })
                                    }), Object(B.jsx)(S.a, {
                                        marginBottom: ["12px", "revert"],
                                        children: Object(B.jsxs)(x.a, {
                                            children: [Object(B.jsxs)(T.a, {
                                                color: "text.body",
                                                display: "inline",
                                                variant: "small-bold",
                                                children: [Object(K.f)(t, s || void 0), " ", s]
                                            }), " ", Object(B.jsxs)(T.a, {
                                                display: "inline",
                                                variant: "info",
                                                children: ["(", r ? "$".concat(Object(K.h)(t.mul(r))) : "$0.00", ")"]
                                            })]
                                        })
                                    }), Object(B.jsx)(D, {})]
                                }), l && Object(B.jsxs)(B.Fragment, {
                                    children: [Object(B.jsx)(S.a, {
                                        children: Object(B.jsx)(T.a, {
                                            variant: "pre-title-small",
                                            children: "Reserved for a specific buyer"
                                        })
                                    }), Object(B.jsx)(S.a, {
                                        alignItems: "flex-start",
                                        marginBottom: ["12px", "revert"],
                                        children: Object(B.jsx)(h.a, {
                                            address: l
                                        })
                                    }), Object(B.jsx)(D, {})]
                                })]
                            })
                        })
                    })
                },
                N = function(e) {
                    var n = e.confirmationDetails,
                        a = Object(i.a)(e, w),
                        t = n.duration,
                        s = n.price,
                        r = n.quantity,
                        d = n.endingPrice,
                        c = n.reservedBuyerAddress,
                        o = n.paymentAsset,
                        u = Object(f.a)(!1),
                        m = Object(l.a)(u, 2),
                        y = m[0],
                        p = m[1];
                    return Object(B.jsx)(C.c, _(_({}, a), {}, {
                        quantity: r,
                        renderExtra: function() {
                            return Object(B.jsx)(M, {
                                isOpen: y,
                                paymentAsset: o,
                                price: s.times(r),
                                toggleIsOpen: p
                            })
                        },
                        renderFooter: function() {
                            return y ? Object(B.jsx)(E, {
                                duration: t,
                                endingPrice: d,
                                paymentAsset: o,
                                reservedBuyerAddress: c
                            }) : null
                        }
                    }))
                },
                V = function(e) {
                    var n = e.confirmationDetails,
                        a = Object(i.a)(e, I),
                        t = n.duration,
                        s = n.price,
                        r = n.endingPrice,
                        d = n.reservedBuyerAddress,
                        c = n.paymentAsset,
                        o = Object(f.a)(!1),
                        u = Object(l.a)(o, 2),
                        m = u[0],
                        y = u[1];
                    return Object(B.jsx)(C.a, _(_({}, a), {}, {
                        renderExtra: function() {
                            return Object(B.jsx)(M, {
                                isOpen: m,
                                paymentAsset: c,
                                price: s,
                                toggleIsOpen: y
                            })
                        },
                        renderFooter: function() {
                            return m ? Object(B.jsx)(E, {
                                duration: t,
                                endingPrice: r,
                                paymentAsset: c,
                                reservedBuyerAddress: d
                            }) : null
                        }
                    }))
                },
                q = function(e) {
                    var n, a = e.assets,
                        t = e.confirmationDetails;
                    if (!a || !t) return null;
                    var l = a[0];
                    return a.length > 1 ? Object(B.jsx)(V, {
                        bundleName: null !== (n = t.bundleName) && void 0 !== n ? n : "No bundle name",
                        confirmationDetails: t,
                        firstAsset: l,
                        numAssets: a.length,
                        style: {
                            padding: "24px"
                        }
                    }) : Object(B.jsx)(N, {
                        asset: l,
                        confirmationDetails: t,
                        style: {
                            padding: "24px"
                        }
                    })
                }
        },
        "/a3s": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "address",
                        storageKey: null
                    },
                    n = [e],
                    a = [e, {
                        args: null,
                        kind: "FragmentSpread",
                        name: "AccountLink_data"
                    }, {
                        kind: "InlineDataFragmentSpread",
                        name: "wallet_accountKey",
                        selections: n
                    }],
                    t = [{
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    l = {
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [e, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "chain",
                            storageKey: null
                        }],
                        storageKey: null
                    },
                    i = [{
                        kind: "Literal",
                        name: "first",
                        value: 30
                    }],
                    s = {
                        args: null,
                        kind: "FragmentSpread",
                        name: "AssetQuantity_data"
                    },
                    r = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    d = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    },
                    c = [{
                        alias: null,
                        args: null,
                        concreteType: "AssetType",
                        kind: "LinkedField",
                        name: "asset",
                        plural: !1,
                        selections: [r],
                        storageKey: null
                    }, d],
                    o = {
                        kind: "InlineDataFragmentSpread",
                        name: "quantity_data",
                        selections: c
                    },
                    u = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    m = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "side",
                        storageKey: null
                    },
                    y = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    },
                    p = [{
                        alias: null,
                        args: null,
                        concreteType: "CollectionType",
                        kind: "LinkedField",
                        name: "collection",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "floorPrice",
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    g = [{
                        args: null,
                        kind: "FragmentSpread",
                        name: "AssetCell_assetBundle"
                    }],
                    b = [{
                        alias: null,
                        args: t,
                        concreteType: "AssetQuantityTypeConnection",
                        kind: "LinkedField",
                        name: "assetQuantities",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetQuantityTypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetQuantityType",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: c,
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: "assetQuantities(first:1)"
                    }];
                return {
                    argumentDefinitions: [{
                        defaultValue: 10,
                        kind: "LocalArgument",
                        name: "count"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "cursor"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "excludeMaker"
                    }, {
                        defaultValue: !1,
                        kind: "LocalArgument",
                        name: "expandedMode"
                    }, {
                        defaultValue: !1,
                        kind: "LocalArgument",
                        name: "filterByOrderRules"
                    }, {
                        defaultValue: !1,
                        kind: "LocalArgument",
                        name: "isBid"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "isExpired"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "isValid"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "maker"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerArchetype"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerAssetBundle"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerAssetIsPayment"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "sortAscending"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "sortBy"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerArchetype"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetBundle"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetCategories"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetCollections"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetIsOwnedBy"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetIsPayment"
                    }],
                    kind: "Fragment",
                    metadata: {
                        connection: [{
                            count: "count",
                            cursor: "cursor",
                            direction: "forward",
                            path: ["orders"]
                        }]
                    },
                    name: "Orders_data",
                    selections: [{
                        alias: "orders",
                        args: [{
                            kind: "Variable",
                            name: "excludeMaker",
                            variableName: "excludeMaker"
                        }, {
                            kind: "Variable",
                            name: "filterByOrderRules",
                            variableName: "filterByOrderRules"
                        }, {
                            kind: "Variable",
                            name: "isExpired",
                            variableName: "isExpired"
                        }, {
                            kind: "Variable",
                            name: "isValid",
                            variableName: "isValid"
                        }, {
                            kind: "Variable",
                            name: "maker",
                            variableName: "maker"
                        }, {
                            kind: "Variable",
                            name: "makerArchetype",
                            variableName: "makerArchetype"
                        }, {
                            kind: "Variable",
                            name: "makerAssetBundle",
                            variableName: "makerAssetBundle"
                        }, {
                            kind: "Variable",
                            name: "makerAssetIsPayment",
                            variableName: "makerAssetIsPayment"
                        }, {
                            kind: "Variable",
                            name: "sortAscending",
                            variableName: "sortAscending"
                        }, {
                            kind: "Variable",
                            name: "sortBy",
                            variableName: "sortBy"
                        }, {
                            kind: "Variable",
                            name: "takerArchetype",
                            variableName: "takerArchetype"
                        }, {
                            kind: "Variable",
                            name: "takerAssetBundle",
                            variableName: "takerAssetBundle"
                        }, {
                            kind: "Variable",
                            name: "takerAssetCategories",
                            variableName: "takerAssetCategories"
                        }, {
                            kind: "Variable",
                            name: "takerAssetCollections",
                            variableName: "takerAssetCollections"
                        }, {
                            kind: "Variable",
                            name: "takerAssetIsOwnedBy",
                            variableName: "takerAssetIsOwnedBy"
                        }, {
                            kind: "Variable",
                            name: "takerAssetIsPayment",
                            variableName: "takerAssetIsPayment"
                        }],
                        concreteType: "OrderV2TypeConnection",
                        kind: "LinkedField",
                        name: "__Orders_orders_connection",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "OrderV2TypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "OrderV2Type",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "closedAt",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isFulfillable",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isValid",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "oldOrder",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "openedAt",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "orderType",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "maker",
                                    plural: !1,
                                    selections: a,
                                    storageKey: null
                                }, {
                                    alias: "makerAsset",
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "makerAssetBundle",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: t,
                                        concreteType: "AssetQuantityTypeConnection",
                                        kind: "LinkedField",
                                        name: "assetQuantities",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityTypeEdge",
                                            kind: "LinkedField",
                                            name: "edges",
                                            plural: !0,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityType",
                                                kind: "LinkedField",
                                                name: "node",
                                                plural: !1,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetType",
                                                    kind: "LinkedField",
                                                    name: "asset",
                                                    plural: !1,
                                                    selections: [l],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: "assetQuantities(first:1)"
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "makerAssetBundle",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: i,
                                        concreteType: "AssetQuantityTypeConnection",
                                        kind: "LinkedField",
                                        name: "assetQuantities",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityTypeEdge",
                                            kind: "LinkedField",
                                            name: "edges",
                                            plural: !0,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityType",
                                                kind: "LinkedField",
                                                name: "node",
                                                plural: !1,
                                                selections: [s, o],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: "assetQuantities(first:30)"
                                    }],
                                    storageKey: null
                                }, u, m, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "taker",
                                    plural: !1,
                                    selections: a,
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "PriceType",
                                    kind: "LinkedField",
                                    name: "perUnitPrice",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "eth",
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "PriceType",
                                    kind: "LinkedField",
                                    name: "price",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "usd",
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "takerAssetBundle",
                                    plural: !1,
                                    selections: [y, {
                                        alias: null,
                                        args: t,
                                        concreteType: "AssetQuantityTypeConnection",
                                        kind: "LinkedField",
                                        name: "assetQuantities",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityTypeEdge",
                                            kind: "LinkedField",
                                            name: "edges",
                                            plural: !0,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityType",
                                                kind: "LinkedField",
                                                name: "node",
                                                plural: !1,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetType",
                                                    kind: "LinkedField",
                                                    name: "asset",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: [{
                                                            kind: "Literal",
                                                            name: "identity",
                                                            value: {}
                                                        }],
                                                        kind: "ScalarField",
                                                        name: "ownedQuantity",
                                                        storageKey: "ownedQuantity(identity:{})"
                                                    }, r, {
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "symbol",
                                                        storageKey: null
                                                    }, u, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetContractType",
                                                        kind: "LinkedField",
                                                        name: "assetContract",
                                                        plural: !1,
                                                        selections: n,
                                                        storageKey: null
                                                    }, {
                                                        kind: "InlineDataFragmentSpread",
                                                        name: "asset_url",
                                                        selections: [l, {
                                                            alias: null,
                                                            args: null,
                                                            kind: "ScalarField",
                                                            name: "tokenId",
                                                            storageKey: null
                                                        }]
                                                    }],
                                                    storageKey: null
                                                }, d, s, o],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: "assetQuantities(first:1)"
                                    }, {
                                        kind: "InlineDataFragmentSpread",
                                        name: "bundle_url",
                                        selections: [y]
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "__typename",
                                    storageKey: null
                                }, {
                                    condition: "isBid",
                                    kind: "Condition",
                                    passingValue: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: null,
                                        kind: "LinkedField",
                                        name: "item",
                                        plural: !1,
                                        selections: [{
                                            kind: "InlineFragment",
                                            selections: p,
                                            type: "AssetType",
                                            abstractKey: null
                                        }, {
                                            kind: "InlineFragment",
                                            selections: p,
                                            type: "AssetBundleType",
                                            abstractKey: null
                                        }],
                                        storageKey: null
                                    }]
                                }, {
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "AskPrice_data"
                                }, {
                                    kind: "InlineDataFragmentSpread",
                                    name: "orderLink_data",
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: i,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [{
                                                            alias: null,
                                                            args: null,
                                                            kind: "ScalarField",
                                                            name: "externalLink",
                                                            storageKey: null
                                                        }, {
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "CollectionType",
                                                            kind: "LinkedField",
                                                            name: "collection",
                                                            plural: !1,
                                                            selections: [{
                                                                alias: null,
                                                                args: null,
                                                                kind: "ScalarField",
                                                                name: "externalUrl",
                                                                storageKey: null
                                                            }],
                                                            storageKey: null
                                                        }],
                                                        storageKey: null
                                                    }],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:30)"
                                        }],
                                        storageKey: null
                                    }]
                                }, {
                                    condition: "expandedMode",
                                    kind: "Condition",
                                    passingValue: !0,
                                    selections: [{
                                        alias: "makerAssetBundleDisplay",
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: g,
                                        storageKey: null
                                    }, {
                                        alias: "takerAssetBundleDisplay",
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "takerAssetBundle",
                                        plural: !1,
                                        selections: g,
                                        storageKey: null
                                    }]
                                }, {
                                    kind: "InlineDataFragmentSpread",
                                    name: "quantity_remaining",
                                    selections: [{
                                        alias: "makerAsset",
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: b,
                                        storageKey: null
                                    }, {
                                        alias: "takerAsset",
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "takerAssetBundle",
                                        plural: !1,
                                        selections: b,
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "remainingQuantity",
                                        storageKey: null
                                    }, m]
                                }],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "cursor",
                                storageKey: null
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "PageInfo",
                            kind: "LinkedField",
                            name: "pageInfo",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "endCursor",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "hasNextPage",
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    type: "Query",
                    abstractKey: null
                }
            }();
            t.hash = "fc576e60e98601ee915c5888b1043c96", n.default = t
        },
        "/xOX": function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return he
            }));
            var t, l, i, s = a("qd51"),
                r = a("etRO"),
                d = a("4jfz"),
                c = a("g2+O"),
                o = a("mHfP"),
                u = a("1U+3"),
                m = a("DY1Z"),
                y = a("m6w3"),
                p = a("/dBk"),
                g = a.n(p),
                b = (a("mXGw"), a("TiKg")),
                k = a.n(b),
                f = a("UutA"),
                j = a("b7Z7"),
                h = a("LoMF"),
                O = a("ocrj"),
                v = a("QrBS"),
                x = a("sX+s"),
                A = a("YO/S"),
                T = a("FS/q"),
                F = a("QYJX"),
                S = a("9va6"),
                K = a("oYCi"),
                C = f.d.ul.attrs({
                    role: "table"
                }).withConfig({
                    displayName: "Tablereact__TableContainer",
                    componentId: "sc-120fhmz-0"
                })(["width:100%;padding:0;margin:0;display:grid;grid-template-columns:", ';[role="cell"],[role="columnheader"]{padding-left:', "px;padding-right:", "px;&:nth-child(", "n){padding-right:", "px;}&:nth-child(", "n + 1){padding-left:", "px;}}"], (function(e) {
                    var n = e.defaultMaxColumnWidth ? "".concat(e.defaultMaxColumnWidth, "px") : "auto",
                        a = e.defaultMinColumnWidth ? "".concat(e.defaultMinColumnWidth, "px") : "auto";
                    return e.minColumnWidths || e.maxColumnWidths ? e.maxColumnWidths ? e.minColumnWidths ? Object(S.range)(e.columns).map((function(t) {
                        var l, i, s, r, d, c;
                        return " minmax(".concat(void 0 === (null === (l = e.minColumnWidths) || void 0 === l ? void 0 : l[t]) ? a : "auto" === (null === (i = e.minColumnWidths) || void 0 === i ? void 0 : i[t]) ? "auto" : "".concat(null === (s = e.minColumnWidths) || void 0 === s ? void 0 : s[t], "px"), ", ").concat(void 0 === (null === (r = e.maxColumnWidths) || void 0 === r ? void 0 : r[t]) ? n : "auto" === (null === (d = e.maxColumnWidths) || void 0 === d ? void 0 : d[t]) ? "auto" : "".concat(null === (c = e.maxColumnWidths) || void 0 === c ? void 0 : c[t], "px"), ")")
                    })) : Object(S.range)(e.columns).map((function(t) {
                        var l, i, s;
                        return " minmax(".concat(a, ", ").concat(void 0 === (null === (l = e.maxColumnWidths) || void 0 === l ? void 0 : l[t]) ? n : "auto" === (null === (i = e.maxColumnWidths) || void 0 === i ? void 0 : i[t]) ? "auto" : "".concat(null === (s = e.maxColumnWidths) || void 0 === s ? void 0 : s[t], "px"), ")")
                    })) : Object(S.range)(e.columns).map((function(t) {
                        var l, i, s;
                        return " minmax(".concat(void 0 === (null === (l = e.minColumnWidths) || void 0 === l ? void 0 : l[t]) ? a : "auto" === (null === (i = e.minColumnWidths) || void 0 === i ? void 0 : i[t]) ? "auto" : "".concat(null === (s = e.minColumnWidths) || void 0 === s ? void 0 : s[t], "px"), ", ").concat(n, ")")
                    })) : "repeat(".concat(e.columns, ", minmax(").concat(a, ", ").concat(n, "))")
                }), (function(e) {
                    var n;
                    return null !== (n = e.horizontalSpacing) && void 0 !== n ? n : 8
                }), (function(e) {
                    var n;
                    return null !== (n = e.horizontalSpacing) && void 0 !== n ? n : 8
                }), (function(e) {
                    return e.columns
                }), (function(e) {
                    return e.horizontalSpacing ? 2 * e.horizontalSpacing : 16
                }), (function(e) {
                    return e.columns
                }), (function(e) {
                    return e.horizontalSpacing ? 2 * e.horizontalSpacing : 16
                })),
                L = f.d.li.attrs({
                    role: "row"
                }).withConfig({
                    displayName: "Tablereact__TableRow",
                    componentId: "sc-120fhmz-1"
                })(["display:contents;"]),
                B = f.d.div.withConfig({
                    displayName: "Tablereact__TableCellContainer",
                    componentId: "sc-120fhmz-2"
                })(["display:flex;align-items:center;padding-top:", ";padding-bottom:", ";background-color:", ";border-top:", ';&[role="columnheader"]{z-index:2;position:sticky;top:0;border-bottom:1px solid ', ";margin-top:-1px;}"], (function(e) {
                    return "body" === e.variant ? "16px" : "4px"
                }), (function(e) {
                    return "body" === e.variant ? "16px" : "4px"
                }), (function(e) {
                    return "body" === e.variant ? e.theme.colors.surface : e.theme.colors.header
                }), (function(e) {
                    return "body" === e.variant ? "1px solid ".concat(e.theme.colors.border) : "none"
                }), (function(e) {
                    return e.theme.colors.border
                })),
                w = function(e) {
                    var n = e.children,
                        a = e.className;
                    return Object(K.jsx)(B, {
                        className: a,
                        role: "columnheader",
                        variant: "header",
                        children: Object(K.jsx)(F.b, {
                            children: n
                        })
                    })
                },
                I = Object.assign((function(e) {
                    var n = e.headers,
                        a = e.minColumnWidths,
                        t = e.maxColumnWidths,
                        l = e.defaultMinColumnWidth,
                        i = e.defaultMaxColumnWidth,
                        s = e.horizontalSpacing,
                        r = e.children,
                        d = e.renderHeader;
                    return Object(K.jsxs)(C, {
                        columns: n.length,
                        defaultMaxColumnWidth: i,
                        defaultMinColumnWidth: l,
                        horizontalSpacing: s,
                        maxColumnWidths: t,
                        minColumnWidths: a,
                        children: [Object(K.jsx)(L, {
                            children: n.map((function(e, n) {
                                return d ? d({
                                    Header: w,
                                    header: e,
                                    index: n
                                }) : Object(K.jsx)(w, {
                                    children: e
                                }, n)
                            }))
                        }), r]
                    })
                }), {
                    Row: L,
                    Cell: function(e) {
                        var n = e.children,
                            a = e.className;
                        return Object(K.jsx)(B, {
                            className: a,
                            role: "cell",
                            variant: "body",
                            children: Object(K.jsx)(F.b, {
                                children: n
                            })
                        })
                    }
                }),
                P = a("n0tG"),
                _ = a("j/Wi"),
                Q = a("ZJLq"),
                D = a("Oe7D"),
                M = a("DqVd"),
                E = Object(M.b)("click counter-offer"),
                N = a("Ujrs"),
                V = a("LsOE"),
                q = a("opVg"),
                R = a("a7GP"),
                U = a("CJkU"),
                H = a("kCmG"),
                $ = a("p+l/"),
                z = a("BmUw"),
                W = a("Z2Bj"),
                Y = a("LjoF"),
                G = a("xpX1"),
                X = a("PAvK"),
                Z = a("7v7j"),
                J = a("Ly9W"),
                ee = a("9gvq"),
                ne = a("C/iq"),
                ae = a("ZmYT"),
                te = a("wwms"),
                le = a("D4YM"),
                ie = a("+n/q"),
                se = a("f/0h"),
                re = a("qXPv"),
                de = a("vxtu"),
                ce = a("uMSw"),
                oe = a("atGD"),
                ue = a("kDvn"),
                me = a("jkGy"),
                ye = a("h64z"),
                pe = a("J+za"),
                ge = a("AvUQ"),
                be = function(e) {
                    var n = e.assetId,
                        a = e.orderId,
                        t = e.trigger,
                        l = e.isDisabled,
                        i = e.onClose,
                        s = e.onPrevious,
                        r = Object(ye.a)().wallet.getActiveAccountKey(),
                        d = Object(pe.a)();
                    return Object(K.jsx)(A.a, {
                        disabled: l,
                        size: "large",
                        trigger: function(e) {
                            return t(d(e))
                        },
                        onClose: i,
                        onPrevious: s,
                        children: function(e) {
                            return r && Object(K.jsx)(ge.a, {
                                variables: {
                                    orderId: a,
                                    asset: n,
                                    identity: {
                                        address: r.address
                                    }
                                },
                                onClose: e
                            })
                        }
                    })
                },
                ke = a("x/dt");

            function fe(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(m.a)(e);
                    if (n) {
                        var l = Object(m.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(u.a)(this, a)
                }
            }
            var je, he;
            ! function(e) {
                e[e.cryptoPrice = 0] = "cryptoPrice", e[e.usdPrice = 1] = "usdPrice", e[e.floorDifference = 2] = "floorDifference", e[e.quantity = 3] = "quantity", e[e.accountLink = 4] = "accountLink", e[e.expiration = 5] = "expiration", e[e.status = 6] = "status", e[e.asset = 7] = "asset", e[e.received = 8] = "received", e[e.actions = 9] = "actions"
            }(je || (je = {})),
            function(e) {
                e.minimal = "minimal", e.full = "full", e.expanded = "expanded"
            }(he || (he = {}));
            var Oe = (i = {}, Object(y.a)(i, he.minimal, [je.cryptoPrice, je.usdPrice, je.floorDifference, je.expiration, je.accountLink, je.actions]), Object(y.a)(i, he.full, [je.cryptoPrice, je.usdPrice, je.quantity, je.floorDifference, je.expiration, je.accountLink, je.actions]), Object(y.a)(i, he.expanded, [je.asset, je.cryptoPrice, je.usdPrice, je.floorDifference, je.accountLink, je.expiration, je.received, je.status, je.actions]), i),
                ve = function(e) {
                    Object(o.a)(a, e);
                    var n = fe(a);

                    function a() {
                        var e;
                        Object(r.a)(this, a);
                        for (var t = arguments.length, l = new Array(t), i = 0; i < t; i++) l[i] = arguments[i];
                        return e = n.call.apply(n, [this].concat(l)), Object(y.a)(Object(c.a)(e), "cancelOrder", function() {
                            var n = Object(s.a)(g.a.mark((function n(a) {
                                var t, l, i;
                                return g.a.wrap((function(n) {
                                    for (;;) switch (n.prev = n.next) {
                                        case 0:
                                            return n.next = 2, Object(te.b)(ee.a.cancelOrderV2(a));
                                        case 2:
                                            n.sent && (t = "ENGLISH" === a.orderType, l = "ASK" === a.side ? "listing" : t ? "bid" : "offer", i = "Your ".concat(l, " was cancelled successfully!"), e.showSuccessMessage(i));
                                        case 4:
                                        case "end":
                                            return n.stop()
                                    }
                                }), n)
                            })));
                            return function(e) {
                                return n.apply(this, arguments)
                            }
                        }()), Object(y.a)(Object(c.a)(e), "renderTakerAction", (function(n) {
                            var a, t, l = n.order,
                                i = n.trigger,
                                s = null !== (a = null === (t = e.context.wallet.activeAccount) || void 0 === t ? void 0 : t.isCompromised) && void 0 !== a && a,
                                r = Object(V.c)(l.takerAssetBundle.assetQuantities),
                                d = null === r || void 0 === r ? void 0 : r.asset,
                                c = Object(W.e)(l.openedAt).local(),
                                o = c.isAfter(k()()),
                                u = o || s;
                            return Object(K.jsx)(de.a, {
                                condition: !u && !!d,
                                wrapper: function(e) {
                                    return d ? Object(K.jsx)(be, {
                                        assetId: d.relayId,
                                        isDisabled: o,
                                        orderId: l.relayId,
                                        trigger: function(n) {
                                            return Object(K.jsx)(j.a, {
                                                onClick: n,
                                                children: e
                                            })
                                        }
                                    }) : Object(K.jsx)(K.Fragment, {})
                                },
                                children: Object(K.jsx)(_.b, {
                                    content: o ? Object(G.a)(c, "ASK" === l.side ? "buy" : "sell") : s ? Q.a : "",
                                    disabled: !u,
                                    children: Object(K.jsx)("span", {
                                        children: i
                                    })
                                })
                            })
                        })), Object(y.a)(Object(c.a)(e), "renderActions", (function(n) {
                            var a, t, l, i = e.props,
                                s = i.hideCta,
                                r = i.collectionSlug,
                                d = i.refetch,
                                c = e.context.wallet;
                            if (s) return null;
                            var o = Object(V.c)(n.takerAssetBundle.assetQuantities),
                                u = "BID" === n.side && o && Object(X.c)(n),
                                m = null === o || void 0 === o ? void 0 : o.asset,
                                y = null === m || void 0 === m ? void 0 : m.decimals,
                                p = c.isActiveAccount(n.maker),
                                g = !n.taker || c.isActiveAccount(n.taker),
                                b = null === (a = Object(V.c)(n.makerAsset.assetQuantities)) || void 0 === a ? void 0 : a.asset.assetContract.chain,
                                f = "ENGLISH" !== n.orderType && (!u || o.asset.ownedQuantity && Object(Y.d)(o.asset.ownedQuantity, y).greaterThanOrEqualTo(Object(z.e)(b) ? Object(Y.d)(1) : u)),
                                T = Object(W.e)(n.openedAt).local().isAfter(k()()),
                                F = null === (t = Object(V.c)(n.makerAsset.assetQuantities)) || void 0 === t ? void 0 : t.asset.assetContract.address,
                                S = n.takerAssetBundle,
                                C = m && "".concat(n.takerAssetBundle.slug ? Object($.a)(S, "sell") : Object(H.e)(m, "sell"), "?taker=").concat(n.maker.address),
                                L = "BID" === n.side && m && (ae.a ? "RINKEBY" === b : "ETHEREUM" === b),
                                B = function() {
                                    return e.renderTakerAction({
                                        order: n,
                                        trigger: Object(K.jsx)(h.c, {
                                            disabled: T,
                                            size: "small",
                                            variant: "secondary",
                                            children: "ASK" === n.side ? "Buy" : "Accept"
                                        })
                                    })
                                };
                            return Object(K.jsx)(v.a, {
                                justifyContent: "flex-end",
                                padding: "8px 0",
                                children: p ? Object(G.c)({
                                    chain: b,
                                    address: F,
                                    slug: r
                                }) && n.isFulfillable ? Object(K.jsx)(A.a, {
                                    trigger: function(e) {
                                        return Object(K.jsx)(h.c, {
                                            size: "small",
                                            variant: "secondary",
                                            onClick: e,
                                            children: "Cancel"
                                        })
                                    },
                                    children: function(e) {
                                        return Object(K.jsx)(ke.a, {
                                            variables: {
                                                orderId: n.relayId,
                                                maker: {
                                                    address: n.maker.address
                                                }
                                            },
                                            onClose: e,
                                            onOrderCancelled: function() {
                                                Object(N.b)(), d()
                                            }
                                        })
                                    }
                                }) : Object(K.jsx)(h.c, {
                                    size: "small",
                                    variant: "secondary",
                                    onClick: function() {
                                        return e.cancelOrder(n)
                                    },
                                    children: "Cancel"
                                }) : f ? g && b ? Object(K.jsx)(ue.a, {
                                    chainIdentifier: b,
                                    children: Object(K.jsx)(oe.a, {
                                        chainIdentifier: b,
                                        collectionSlug: r,
                                        children: L ? Object(K.jsx)(j.a, {
                                            marginLeft: "8px",
                                            children: Object(K.jsx)(x.a, {
                                                greaterThanOrEqual: "md",
                                                children: function(a, t) {
                                                    return t ? Object(K.jsxs)(v.a, {
                                                        children: [Object(K.jsx)(h.c, {
                                                            href: C,
                                                            marginRight: "8px",
                                                            size: "small",
                                                            variant: "tertiary",
                                                            onClick: function() {
                                                                return E()
                                                            },
                                                            children: "Counter"
                                                        }), B()]
                                                    }) : Object(K.jsx)(O.a, {
                                                        appendTo: document.body,
                                                        content: function(a) {
                                                            var t = a.List,
                                                                l = a.Item,
                                                                i = a.close;
                                                            return Object(K.jsxs)(t, {
                                                                children: [e.renderTakerAction({
                                                                    order: n,
                                                                    trigger: Object(K.jsxs)(l, {
                                                                        onClick: i,
                                                                        children: [Object(K.jsx)(l.Avatar, {
                                                                            icon: "task_alt"
                                                                        }), Object(K.jsx)(l.Content, {
                                                                            children: Object(K.jsx)(l.Title, {
                                                                                children: "Accept"
                                                                            })
                                                                        })]
                                                                    })
                                                                }), Object(K.jsxs)(l, {
                                                                    href: C,
                                                                    onClick: function() {
                                                                        E(), i()
                                                                    },
                                                                    children: [Object(K.jsx)(l.Avatar, {
                                                                        icon: "swap_horiz"
                                                                    }), Object(K.jsx)(l.Content, {
                                                                        children: Object(K.jsx)(l.Title, {
                                                                            children: "Counter"
                                                                        })
                                                                    })]
                                                                })]
                                                            })
                                                        },
                                                        lazy: !1,
                                                        children: Object(K.jsx)(h.c, {
                                                            icon: "more_vert",
                                                            marginRight: "4px",
                                                            size: "small",
                                                            variant: "tertiary"
                                                        })
                                                    })
                                                }
                                            })
                                        }) : B()
                                    })
                                }) : Object(K.jsx)(_.b, {
                                    content: null !== (l = n.taker) && void 0 !== l && l.address ? Object(K.jsxs)("div", {
                                        children: [Object(K.jsx)("div", {
                                            className: "Orders--tooltip-header",
                                            children: "Private Listing"
                                        }), Object(K.jsxs)("div", {
                                            children: ["This listing is reserved for ", Object(U.f)(n.taker.address), "."]
                                        })]
                                    }) : "Private listing",
                                    children: Object(K.jsx)("span", {
                                        children: Object(K.jsx)(h.c, {
                                            disabled: !0,
                                            size: "small",
                                            variant: "secondary",
                                            children: "Buy"
                                        })
                                    })
                                }) : null
                            })
                        })), Object(y.a)(Object(c.a)(e), "renderFloorDifference", (function(e, n) {
                            var a = Object(Y.d)(e),
                                t = Object(Y.d)(n.perUnitPrice.eth).div(a).minus(1).times(100);
                            return Object(K.jsx)(v.a, {
                                children: Object(K.jsx)(_.b, {
                                    content: "Collection floor price: ".concat(a.toFixed(2), " ETH"),
                                    children: Object(K.jsx)(v.a, {
                                        cursor: "pointer",
                                        children: Object(K.jsx)(P.a, {
                                            as: "span",
                                            fontSize: "14px",
                                            marginLeft: "4px",
                                            children: t.isZero() ? "At floor" : "".concat(t.abs().toFixed(1), "% ").concat(t.isNegative() ? "below" : "above")
                                        })
                                    })
                                })
                            })
                        })), Object(y.a)(Object(c.a)(e), "getFloorPrice", (function(e) {
                            var n, a, t;
                            return null !== (n = null === (a = e.item) || void 0 === a || null === (t = a.collection) || void 0 === t ? void 0 : t.floorPrice) && void 0 !== n ? n : void 0
                        })), Object(y.a)(Object(c.a)(e), "getRowCellContent", (function(n) {
                            var a = e.props,
                                t = a.side,
                                l = a.isCurrentUser,
                                i = a.variables.takerAssetIsOwnedBy,
                                s = Object(X.c)(n),
                                r = "ask" === t ? n.makerAssetBundleDisplay : n.takerAssetBundleDisplay,
                                d = {
                                    cryptoPrice: void 0,
                                    usdPrice: void 0,
                                    floorDifference: void 0,
                                    quantity: Object(Y.l)(s),
                                    expiration: Object(K.jsx)(P.a, {
                                        as: "span",
                                        fontSize: "14px",
                                        children: n.closedAt ? Object(W.f)(Object(W.e)(n.closedAt)) : Z.a
                                    }),
                                    accountLink: Object(K.jsx)(ie.a, {
                                        dataKey: n.maker,
                                        variant: "no-image"
                                    }),
                                    status: void 0,
                                    asset: Object(K.jsx)(se.a, {
                                        asset: null,
                                        assetBundle: r
                                    }),
                                    actions: e.renderActions(n),
                                    received: Object(K.jsx)(_.b, {
                                        content: Object(W.e)(n.openedAt).local().format("MMMM Do, YYYY h:mm a"),
                                        children: Object(K.jsx)("span", {
                                            children: Object(W.f)(Object(W.e)(n.openedAt))
                                        })
                                    })
                                },
                                c = Object(V.c)(("ask" === t ? n.makerAssetBundle : n.takerAssetBundle).assetQuantities),
                                o = Object(V.c)(("ask" === t ? n.takerAssetBundle : n.makerAssetBundle).assetQuantities);
                            if (!c || !o) throw new Error("Order table row requires values for price and quantity");
                            var u = Object(X.a)(c),
                                m = e.getFloorPrice(n);
                            "bid" === t && void 0 !== m && (d.floorDifference = e.renderFloorDifference(m, n)), d.cryptoPrice = "ask" === t ? Object(K.jsx)(me.a, {
                                data: n,
                                symbolVariant: "both"
                            }) : Object(K.jsx)(re.a, {
                                data: o,
                                mapQuantity: function(e) {
                                    return e.div(u)
                                },
                                symbolVariant: "both"
                            }), d.usdPrice = "ask" === t ? Object(K.jsx)(me.a, {
                                data: n,
                                symbolVariant: "both",
                                variant: "fiat"
                            }) : Object(K.jsx)(P.a, {
                                as: "span",
                                fontSize: "14px",
                                children: Object(Y.g)(u.greaterThanOrEqualTo(1) ? Object(Y.d)(n.price.usd).div(u) : n.price.usd)
                            });
                            var y = l && !i;
                            return d.status = Z.a, y && (d.status = n.isValid ? "Valid" : Object(K.jsx)(_.b, {
                                content: Object(K.jsxs)("div", {
                                    children: ["This offer is invalid until there is at least", Object(K.jsx)(re.a, {
                                        className: "Orders--tooltip-price",
                                        data: o,
                                        isInline: !0,
                                        symbolVariant: "raw"
                                    }), "in your wallet"]
                                }),
                                children: Object(K.jsx)("div", {
                                    className: "Orders--status-text",
                                    children: "Invalid"
                                })
                            })), d
                        })), Object(y.a)(Object(c.a)(e), "getCellClassNames", (function(e) {
                            return Object(y.a)({}, je.actions, "Orders--actions-column")[e]
                        })), Object(y.a)(Object(c.a)(e), "renderRow", (function(n) {
                            try {
                                var a = e.getRowCellContent(n),
                                    t = e.props,
                                    l = t.mode,
                                    i = void 0 === l ? he.minimal : l,
                                    s = t.side;
                                return Object(K.jsx)(I.Row, {
                                    children: Oe[i].map((function(t) {
                                        var l = Object(K.jsx)(K.Fragment, {});
                                        switch (t) {
                                            case je.cryptoPrice:
                                                l = a.cryptoPrice;
                                                break;
                                            case je.usdPrice:
                                                l = a.usdPrice;
                                                break;
                                            case je.floorDifference:
                                                if ("ask" === s) return l;
                                                l = a.floorDifference;
                                                break;
                                            case je.quantity:
                                                l = a.quantity;
                                                break;
                                            case je.expiration:
                                                l = a.expiration;
                                                break;
                                            case je.accountLink:
                                                l = a.accountLink;
                                                break;
                                            case je.actions:
                                                l = a.actions;
                                                break;
                                            case je.status:
                                                l = a.status;
                                                break;
                                            case je.asset:
                                                l = a.asset;
                                                break;
                                            case je.received:
                                                l = a.received;
                                                break;
                                            default:
                                                throw new J.a(t)
                                        }
                                        return Object(K.jsx)(I.Cell, {
                                            className: e.getCellClassNames(t),
                                            children: l
                                        }, "".concat(n.relayId, "-").concat(je[t]))
                                    }))
                                }, n.relayId)
                            } catch (r) {
                                return Object(D.c)(r), Object(K.jsx)(K.Fragment, {})
                            }
                        })), Object(y.a)(Object(c.a)(e), "renderEmptyTable", (function() {
                            var n = e.props.side;
                            return Object(K.jsx)("div", {
                                className: "Orders--empty",
                                children: Object(K.jsxs)("div", {
                                    children: [Object(K.jsx)(ce.a, {
                                        alt: "",
                                        className: "Orders--no-data-img",
                                        height: 100,
                                        url: "bid" === n ? ne.wb : ne.vb
                                    }), Object(K.jsx)("div", {
                                        className: "Orders--no-data-text",
                                        children: "bid" === n ? "No offers yet" : "No listings yet"
                                    })]
                                })
                            })
                        })), Object(y.a)(Object(c.a)(e), "getTableHeaders", (function() {
                            var n = e.props,
                                a = n.side,
                                t = n.isCurrentUser,
                                l = n.variables,
                                i = l.takerAssetIsOwnedBy,
                                s = l.maker,
                                r = ["Price", "USD Price", "Expiration", "From", ""],
                                d = ["Unit Price", "USD Unit Price", "Quantity", "Expiration", "From", ""],
                                c = ["Item", "Unit Price", "USD Unit Price", "From", "Expiration", "bid" === a && s ? "Made" : "Received", t && !i ? "Status" : "", ""];
                            if ("bid" === a) {
                                var o = "Floor Difference";
                                r.splice(2, 0, o), d.splice(3, 0, o), c.splice(3, 0, o)
                            }
                            return {
                                minimal: r,
                                full: d,
                                expanded: c
                            }
                        })), e
                    }
                    return Object(d.a)(a, [{
                        key: "render",
                        value: function() {
                            var e = this,
                                n = this.props,
                                a = n.className,
                                t = n.data,
                                l = n.footer,
                                i = n.mode,
                                s = void 0 === i ? he.minimal : i,
                                r = n.page,
                                d = n.scrollboxClassName,
                                c = Object(V.d)(null === t || void 0 === t ? void 0 : t.orders),
                                o = this.getTableHeaders(),
                                u = t && !c.length ? this.renderEmptyTable() : Object(K.jsxs)(j.a, {
                                    className: d,
                                    maxHeight: "332px",
                                    overflowX: "auto",
                                    children: [Object(K.jsx)(I, {
                                        headers: o[s],
                                        maxColumnWidths: s === he.expanded ? [360, "auto", "auto", "auto", 160, "auto", "auto", "auto", "auto"] : ["auto", "auto", "auto", "auto", 160, "auto"],
                                        renderHeader: function(n) {
                                            var a = n.Header,
                                                t = n.header,
                                                l = n.index;
                                            return Object(K.jsx)(a, {
                                                className: e.getCellClassNames(Oe[s][l]),
                                                children: t
                                            }, "".concat(t || l, "ColumnHeader"))
                                        },
                                        children: c.map((function(n) {
                                            return e.renderRow(n)
                                        }))
                                    }), Object(K.jsx)(T.a, {
                                        intersectionOptions: {
                                            rootMargin: "512px"
                                        },
                                        isFirstPageLoading: !t,
                                        page: r,
                                        size: 10
                                    })]
                                });
                            return Object(K.jsxs)(Ae, {
                                className: a,
                                children: [u, l]
                            })
                        }
                    }]), a
                }(q.a),
                xe = void 0 !== t ? t : t = a("W3fG"),
                Ae = (n.b = Object(R.b)(Object(V.f)(ve, {
                    fragments: {
                        data: void 0 !== l ? l : l = a("/a3s")
                    },
                    query: xe
                }), xe), f.d.div.withConfig({
                    displayName: "Ordersreact__DivContainer",
                    componentId: "sc-1pqzv02-0"
                })([".Orders--empty{align-items:center;display:flex;flex-direction:column;justify-content:center;padding:12px;.Orders--no-data-text{display:flex;font-size:16px;margin-top:4px;justify-content:center;}.Orders--no-data-img{", "}}.Orders--status-text{color:", ";}.Orders--tooltip-price{color:", ";margin:0 0.3em 0 0.15em;}.Orders--actions-column{padding:8px;position:sticky;right:0;}"], Object(le.b)({
                    variants: {
                        dark: {
                            opacity: .5
                        }
                    }
                }), (function(e) {
                    return e.theme.colors.error
                }), (function(e) {
                    return e.theme.colors.fog
                })))
        },
        "1olU": function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return c
            }));
            var t = a("etRO"),
                l = a("mHfP"),
                i = a("1U+3"),
                s = a("DY1Z"),
                r = a("m6w3");

            function d(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(s.a)(e);
                    if (n) {
                        var l = Object(s.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(i.a)(this, a)
                }
            }
            var c = function(e) {
                Object(l.a)(a, e);
                var n = d(a);

                function a() {
                    return Object(t.a)(this, a), n.apply(this, arguments)
                }
                return a
            }(a("opVg").a);
            Object(r.a)(c, "query", void 0)
        },
        25: function(e, n, a) {
            a("xhzY"), e.exports = a("3FDX")
        },
        "3FDX": function(e, n, a) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/bundle", function() {
                return a("XPtY")
            }])
        },
        "3LJ8": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = {
                argumentDefinitions: [{
                    defaultValue: null,
                    kind: "LocalArgument",
                    name: "chain"
                }, {
                    defaultValue: null,
                    kind: "LocalArgument",
                    name: "identity"
                }],
                kind: "Fragment",
                metadata: null,
                name: "TradeStation_archetype",
                selections: [{
                    alias: null,
                    args: null,
                    concreteType: "AssetType",
                    kind: "LinkedField",
                    name: "asset",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "chain",
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: [{
                            kind: "Literal",
                            name: "first",
                            value: 1
                        }],
                        concreteType: "AssetOwnershipTypeConnection",
                        kind: "LinkedField",
                        name: "assetOwners",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetOwnershipTypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetOwnershipType",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "owner",
                                    plural: !1,
                                    selections: [{
                                        kind: "InlineDataFragmentSpread",
                                        name: "wallet_accountKey",
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "address",
                                            storageKey: null
                                        }]
                                    }],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: "assetOwners(first:1)"
                    }, {
                        alias: null,
                        args: null,
                        concreteType: "CollectionType",
                        kind: "LinkedField",
                        name: "collection",
                        plural: !1,
                        selections: [{
                            kind: "InlineDataFragmentSpread",
                            name: "verification_data",
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "isMintable",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "isSafelisted",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "isVerified",
                                storageKey: null
                            }]
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isListable",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    }],
                    storageKey: null
                }, {
                    args: [{
                        kind: "Variable",
                        name: "chain",
                        variableName: "chain"
                    }, {
                        kind: "Variable",
                        name: "identity",
                        variableName: "identity"
                    }],
                    kind: "FragmentSpread",
                    name: "BidModalContent_archetype"
                }],
                type: "ArchetypeType",
                abstractKey: null,
                hash: "c58dca3d60f3e0b6833b8141e753d171"
            };
            n.default = t
        },
        "4QRR": function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return i
            }));
            var t = a("JiVo"),
                l = ["ETH", "WETH", "DAI", "USDC"],
                i = function(e, n) {
                    return Object(t.a)(e).sort((function(e, a) {
                        var t = l.indexOf(n(e)),
                            i = l.indexOf(n(a));
                        return t < 0 && i < 0 ? n(e).localeCompare(n(a)) : t < 0 ? 1 : i < 0 ? -1 : t - i
                    }))
                }
        },
        "5SNe": function(e, n, a) {
            "use strict";
            var t = a("qd51"),
                l = a("etRO"),
                i = a("4jfz"),
                s = a("g2+O"),
                r = a("mHfP"),
                d = a("1U+3"),
                c = a("DY1Z"),
                o = a("m6w3"),
                u = a("/dBk"),
                m = a.n(u),
                y = a("mXGw"),
                p = a.n(y),
                g = a("KsMw"),
                b = a("9va6"),
                k = a("TiKg"),
                f = a.n(k),
                j = a("UutA"),
                h = a("jQgF"),
                O = a("b7Z7"),
                v = a("LoMF"),
                x = a("QrBS"),
                A = Object(j.d)(O.a).withConfig({
                    displayName: "InlineFlexreact__InlineFlex",
                    componentId: "sc-9jbsog-0"
                })(["display:inline-flex;"]),
                T = a("7bY5"),
                F = a("j/Wi"),
                S = a("8BrW"),
                K = a("pAF9"),
                C = a("ZJLq"),
                L = a("u+k6"),
                B = a("DWpj"),
                w = a("LsOE"),
                I = a("Z2Bj"),
                P = a("LjoF"),
                _ = a("xpX1"),
                Q = a("PAvK"),
                D = a("u6YR"),
                M = a("b2pk"),
                E = a("vI8H"),
                N = a("C/iq"),
                V = a("D4YM"),
                q = a("qXPv"),
                R = a("qt8R"),
                U = a("n0tG");

            function H(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(c.a)(e);
                    if (n) {
                        var l = Object(c.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(d.a)(this, a)
                }
            }
            var $ = function(e) {
                    Object(r.a)(a, e);
                    var n = H(a);

                    function a() {
                        var e;
                        Object(l.a)(this, a);
                        for (var t = arguments.length, i = new Array(t), r = 0; r < t; r++) i[r] = arguments[r];
                        return e = n.call.apply(n, [this].concat(i)), Object(o.a)(Object(s.a)(e), "interval", void 0), e
                    }
                    return Object(i.a)(a, [{
                        key: "componentDidMount",
                        value: function() {
                            var e = this;
                            this.interval = window.setInterval((function() {
                                return e.forceUpdate()
                            }), 1e3)
                        }
                    }, {
                        key: "componentWillUnmount",
                        value: function() {
                            this.interval && clearInterval(this.interval)
                        }
                    }, {
                        key: "render",
                        value: function() {
                            var e = this.props.moment;
                            return Object(I.a)(f.a.duration(e.diff(f.a.utc())))
                        }
                    }]), a
                }(p.a.Component),
                z = a("oYCi"),
                W = function(e) {
                    var n = e.prefix,
                        a = void 0 === n ? "" : n,
                        t = e.postfix,
                        l = void 0 === t ? "" : t,
                        i = e.includeDate,
                        s = void 0 === i || i,
                        r = e.endMoment,
                        d = e.includeCountDown,
                        c = void 0 === d || d,
                        o = r ? f()(r).local().startOf("day") : void 0,
                        u = f()().startOf("day"),
                        m = f()(u).add(1, "day"),
                        y = r && s ? "".concat(r.local().format("MMMM D, YYYY [at] h:mma"), " \n  ").concat(f.a.tz(f.a.tz.guess()).zoneAbbr()) : "",
                        p = Object(z.jsx)(U.a, {
                            as: "span",
                            margin: 0,
                            children: c ? "(".concat(y, ")") : y
                        }),
                        g = r && o && c ? o.isSameOrBefore(m) ? Object(z.jsxs)(z.Fragment, {
                            children: [Object(z.jsx)(U.a, {
                                as: "span",
                                color: "inherit",
                                margin: "0",
                                variant: "bold",
                                children: o.isSame(u) ? "today" : "tomorrow"
                            }), "\xa0in\xa0", Object(z.jsx)(U.a, {
                                as: "span",
                                color: "inherit",
                                margin: "0",
                                variant: "bold",
                                children: Object(z.jsx)($, {
                                    moment: r
                                })
                            })]
                        }) : Object(z.jsxs)(z.Fragment, {
                            children: ["\xa0in\xa0", Object(z.jsxs)(U.a, {
                                as: "span",
                                color: "inherit",
                                margin: "0",
                                variant: "bold",
                                children: [f.a.duration(o.diff(u, "days"), "days").asDays(), " ", "days"]
                            })]
                        }) : null,
                        b = Object(z.jsx)(z.Fragment, {
                            children: "\xa0"
                        });
                    return Object(z.jsxs)(O.a, {
                        display: "inline",
                        children: [Object(z.jsx)(U.a, {
                            as: "span",
                            margin: 0,
                            children: a
                        }), a ? b : null, g, g ? b : null, p, b, Object(z.jsx)(U.a, {
                            as: "span",
                            margin: 0,
                            children: l
                        })]
                    })
                },
                Y = a("m5he"),
                G = a("Q5Gx"),
                X = a("BDXS"),
                Z = a("uq6L"),
                J = a("BOW+");

            function ee(e, n) {
                Object(J.a)(2, arguments);
                var a = Object(Z.a)(e),
                    t = Object(Z.a)(n);
                return a.getFullYear() - t.getFullYear()
            }

            function ne(e, n) {
                Object(J.a)(2, arguments);
                var a = Object(Z.a)(e),
                    t = Object(Z.a)(n),
                    l = Object(X.a)(a, t),
                    i = Math.abs(ee(a, t));
                a.setFullYear(1584), t.setFullYear(1584);
                var s = Object(X.a)(a, t) === -l,
                    r = l * (i - Number(s));
                return 0 === r ? 0 : r
            }
            var ae = a("OYLJ"),
                te = a("+SA/"),
                le = a("/DEc"),
                ie = a("ASgk");

            function se(e, n, a) {
                Object(J.a)(2, arguments);
                var t = Object(le.a)(e, n) / te.a;
                return Object(ie.a)(null === a || void 0 === a ? void 0 : a.roundingMethod)(t)
            }
            var re = a("fumj"),
                de = a("W5hp"),
                ce = a("gcqE"),
                oe = a("z2//"),
                ue = a("YYXE"),
                me = a("JS2A");

            function ye(e, n) {
                Object(J.a)(2, arguments);
                var a = Object(ue.a)(n);
                return Object(me.a)(e, -a)
            }

            function pe(e, n) {
                if (Object(J.a)(2, arguments), !n || "object" !== typeof n) return new Date(NaN);
                var a = n.years ? Object(ue.a)(n.years) : 0,
                    t = n.months ? Object(ue.a)(n.months) : 0,
                    l = n.weeks ? Object(ue.a)(n.weeks) : 0,
                    i = n.days ? Object(ue.a)(n.days) : 0,
                    s = n.hours ? Object(ue.a)(n.hours) : 0,
                    r = n.minutes ? Object(ue.a)(n.minutes) : 0,
                    d = n.seconds ? Object(ue.a)(n.seconds) : 0,
                    c = ye(e, t + 12 * a),
                    o = Object(oe.a)(c, i + 7 * l),
                    u = r + 60 * s,
                    m = d + 60 * u,
                    y = 1e3 * m,
                    p = new Date(o.getTime() - y);
                return p
            }

            function ge(e) {
                var n = e.start,
                    a = e.end;
                Object(J.a)(1, arguments);
                var t = Object(Z.a)(n),
                    l = Object(Z.a)(a);
                if (!Object(ce.a)(t)) throw new RangeError("Start Date is invalid");
                if (!Object(ce.a)(l)) throw new RangeError("End Date is invalid");
                var i = {
                        years: 0,
                        months: 0,
                        days: 0,
                        hours: 0,
                        minutes: 0,
                        seconds: 0
                    },
                    s = Object(X.a)(t, l);
                i.years = Math.abs(ne(t, l));
                var r = pe(t, {
                    years: s * i.years
                });
                i.months = Math.abs(Object(ae.a)(r, l));
                var d = pe(r, {
                    months: s * i.months
                });
                i.days = Math.abs(Object(g.a)(d, l));
                var c = pe(d, {
                    days: s * i.days
                });
                i.hours = Math.abs(se(c, l));
                var o = pe(c, {
                    hours: s * i.hours
                });
                i.minutes = Math.abs(Object(re.a)(o, l));
                var u = pe(o, {
                    minutes: s * i.minutes
                });
                return i.seconds = Math.abs(Object(de.a)(u, l)), i
            }
            var be = a("876n"),
                ke = a("rx0e");
            var fe, je, he, Oe = a("d8b/"),
                ve = a("1p8O"),
                xe = function(e) {
                    var n, a, t, l = e.endMoment,
                        i = e.className,
                        s = l ? new Date(l) : new Date,
                        r = Object(y.useState)(new Date),
                        d = r[0],
                        c = r[1],
                        o = Object(y.useState)(ge({
                            start: d,
                            end: s
                        })),
                        u = o[0],
                        m = o[1];
                    Object(Oe.a)((function() {
                        if (Object(be.a)(d, s)) {
                            var e = function(e, n) {
                                if (Object(J.a)(2, arguments), !n || "object" !== typeof n) return new Date(NaN);
                                var a = n.years ? Object(ue.a)(n.years) : 0,
                                    t = n.months ? Object(ue.a)(n.months) : 0,
                                    l = n.weeks ? Object(ue.a)(n.weeks) : 0,
                                    i = n.days ? Object(ue.a)(n.days) : 0,
                                    s = n.hours ? Object(ue.a)(n.hours) : 0,
                                    r = n.minutes ? Object(ue.a)(n.minutes) : 0,
                                    d = n.seconds ? Object(ue.a)(n.seconds) : 0,
                                    c = Object(Z.a)(e),
                                    o = t || a ? Object(me.a)(c, t + 12 * a) : c,
                                    u = i || l ? Object(ke.a)(o, i + 7 * l) : o,
                                    m = 1e3 * (d + 60 * (r + 60 * s));
                                return new Date(u.getTime() + m)
                            }(d, {
                                seconds: 1
                            });
                            c(e), m(ge({
                                start: e,
                                end: s
                            }))
                        }
                    }), 1e3);
                    var p = function(e, n) {
                            for (var a = e.toString().split(""); a.length < n;) a.unshift("0");
                            return a.join("")
                        },
                        b = Object(g.a)(s, d),
                        k = null !== (n = u.hours) && void 0 !== n ? n : 0,
                        f = null !== (a = u.minutes) && void 0 !== a ? a : 0,
                        j = null !== (t = u.seconds) && void 0 !== t ? t : 0,
                        h = b <= 0 && k <= 0;
                    return l ? Object(z.jsx)(Ae, {
                        lessThanOneHourRemains: h,
                        children: Object(z.jsxs)(x.a, {
                            className: i,
                            flexWrap: "wrap",
                            children: [b > 0 ? Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(O.a, {
                                    className: "Timer--numbers",
                                    children: p(b, 2)
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Days"
                                })]
                            }) : null, Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(O.a, {
                                    className: "Timer--numbers",
                                    children: p(k, 2)
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Hours"
                                })]
                            }), Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(O.a, {
                                    className: "Timer--numbers",
                                    children: p(f, 2)
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Minutes"
                                })]
                            }), Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(O.a, {
                                    className: "Timer--numbers",
                                    children: p(j, 2)
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Seconds"
                                })]
                            })]
                        })
                    }) : Object(z.jsx)(Ae, {
                        lessThanOneHourRemains: h,
                        children: Object(z.jsxs)(x.a, {
                            className: i,
                            children: [Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(ve.a.Block, {
                                    height: "24px"
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Days"
                                })]
                            }), Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(ve.a.Block, {
                                    height: "24px"
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Hours"
                                })]
                            }), Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(ve.a.Block, {
                                    height: "24px"
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Minutes"
                                })]
                            }), Object(z.jsxs)(O.a, {
                                className: "Timer--time-unit",
                                children: [Object(z.jsx)(ve.a.Block, {
                                    height: "24px"
                                }), Object(z.jsx)(U.a, {
                                    fontSize: 15,
                                    margin: 0,
                                    children: "Seconds"
                                })]
                            })]
                        })
                    })
                },
                Ae = Object(j.d)(O.a).withConfig({
                    displayName: "Timerreact__StyledContainer",
                    componentId: "sc-rssbju-0"
                })(["font-size:20px;font-weight:500;.Timer--time-unit{:not(:last-child){margin-right:16px;", "}.Timer--numbers{color:", ";}}"], Object(G.e)({
                    phoneM: Object(j.c)(["margin-right:32px;"]),
                    mobile: Object(j.c)(["margin-right:50px;"])
                }), (function(e) {
                    return e.lessThanOneHourRemains ? e.theme.colors.error : e.theme.colors.text.heading
                })),
                Te = a("OsKK"),
                Fe = a("7Smq"),
                Se = a("atGD"),
                Ke = a("kDvn"),
                Ce = a("jkGy"),
                Le = a("AvUQ"),
                Be = a("LDtq"),
                we = a("AsIE");

            function Ie(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(c.a)(e);
                    if (n) {
                        var l = Object(c.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(d.a)(this, a)
                }
            }
            var Pe = function(e) {
                    Object(r.a)(a, e);
                    var n = Ie(a);

                    function a() {
                        var e;
                        Object(l.a)(this, a);
                        for (var t = arguments.length, i = new Array(t), r = 0; r < t; r++) i[r] = arguments[r];
                        return e = n.call.apply(n, [this].concat(i)), Object(o.a)(Object(s.a)(e), "state", {
                            isMoonPayBuyAllowed: void 0,
                            isMoonPayNftAllowed: void 0,
                            isAvailableForFiatCheckout: void 0
                        }), Object(o.a)(Object(s.a)(e), "shouldVerifyCollectionDetails", (function(n) {
                            return !e.props.bundleData && !["verified", "safelisted"].includes(n)
                        })), Object(o.a)(Object(s.a)(e), "renderBidModal", (function(n) {
                            var a = e.props,
                                t = a.data,
                                l = a.archetypeData,
                                i = a.bundleData,
                                s = a.onOrdersChanged;
                            return Object(z.jsx)(Fe.a, {
                                archetypeData: l,
                                bundleData: i,
                                tradeData: t,
                                onClose: n,
                                onOrderCreated: s
                            })
                        })), Object(o.a)(Object(s.a)(e), "renderMakeOfferButton", (function(n) {
                            var a, t, l = n.onClick,
                                i = n.assetRelayId,
                                s = n.isDisabled,
                                r = void 0 !== s && s,
                                d = null !== (a = null === (t = e.context.wallet.activeAccount) || void 0 === t ? void 0 : t.isCompromised) && void 0 !== a && a,
                                c = e.getIsBuyWithCardButtonShown(),
                                o = c ? 0 : "8px";
                            return Object(z.jsx)(F.b, {
                                content: C.a,
                                disabled: !d,
                                children: Object(z.jsx)(A, {
                                    marginLeft: [0, 0, 0, c ? 0 : "8px"],
                                    marginTop: [o, o, o, 0],
                                    width: ["100%", "100%", "100%", c ? "100%" : "50%"],
                                    children: Object(z.jsx)(v.c, {
                                        disabled: r || d,
                                        icon: "local_offer",
                                        variant: "secondary",
                                        width: "100%",
                                        onClick: function(e) {
                                            function n() {
                                                return e.apply(this, arguments)
                                            }
                                            return n.toString = function() {
                                                return e.toString()
                                            }, n
                                        }((function() {
                                            i && Object(L.c)({
                                                assetId: i
                                            }), l()
                                        })),
                                        children: "Make offer"
                                    })
                                })
                            })
                        })), Object(o.a)(Object(s.a)(e), "renderForUnlistedAsset", (function() {
                            var n, a = e.props,
                                t = a.data,
                                l = a.archetypeData,
                                i = a.collectionSlug,
                                s = e.context.wallet,
                                r = null === l || void 0 === l ? void 0 : l.asset;
                            if (!r) return null;
                            if (!r.isListable) return null;
                            var d = Object(w.c)(r.assetOwners);
                            if (!(null === d || void 0 === d || !d.owner) && s.isActiveAccount(d.owner)) return null;
                            var c = Object(w.c)(null === (n = t.bestBid) || void 0 === n ? void 0 : n.makerAssetBundle.assetQuantities),
                                o = Object(M.a)(r.collection);
                            return Object(z.jsx)(De, {
                                children: Object(z.jsx)(Te.e, {
                                    children: Object(z.jsxs)("div", {
                                        className: "TradeStation--main",
                                        children: [c ? Object(z.jsxs)(z.Fragment, {
                                            children: [Object(z.jsx)("div", {
                                                className: "TradeStation--ask-label",
                                                children: "Highest offer"
                                            }), Object(z.jsx)("div", {
                                                className: "TradeStation--price-container",
                                                children: Object(z.jsxs)(z.Fragment, {
                                                    children: [Object(z.jsx)(q.a, {
                                                        className: "TradeStation--price TradeStation--price",
                                                        data: c
                                                    }), Object(z.jsx)(q.a, {
                                                        className: "TradeStation--fiat-price TradeStation--fiat-price",
                                                        data: c,
                                                        secondary: !0,
                                                        variant: "fiat"
                                                    })]
                                                })
                                            })]
                                        }) : null, Object(z.jsx)(Ke.a, {
                                            chainIdentifier: r.assetContract.chain,
                                            children: Object(z.jsx)(Se.a, {
                                                chainIdentifier: r.assetContract.chain,
                                                collectionSlug: i,
                                                children: Object(z.jsx)(we.a, {
                                                    assetId: r.relayId,
                                                    renderMainModal: e.renderBidModal,
                                                    shouldVerifyCollectionDetails: e.shouldVerifyCollectionDetails(o),
                                                    trigger: function(n) {
                                                        return e.renderMakeOfferButton({
                                                            onClick: n,
                                                            assetRelayId: r.relayId
                                                        })
                                                    }
                                                })
                                            })
                                        })]
                                    })
                                })
                            })
                        })), Object(o.a)(Object(s.a)(e), "getIsBuyWithCardButtonShown", (function() {
                            var n = e.props.data.bestAsk,
                                a = null === n || void 0 === n ? void 0 : n.orderType,
                                t = Object(w.c)(null === n || void 0 === n ? void 0 : n.makerAssetBundle.assetQuantities),
                                l = null === t || void 0 === t ? void 0 : t.asset.assetContract.chain;
                            return l && ["MATIC", "MUMBAI"].includes(l) && "ENGLISH" !== a
                        })), e
                    }
                    return Object(i.a)(a, [{
                        key: "componentDidMount",
                        value: function() {
                            var e = Object(t.a)(m.a.mark((function e() {
                                return m.a.wrap((function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, this.getIsMoonPayAllowed();
                                        case 2:
                                            return e.next = 4, this.getIsAvailableForFiatCheckout();
                                        case 4:
                                        case "end":
                                            return e.stop()
                                    }
                                }), e, this)
                            })));
                            return function() {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "getIsAvailableForFiatCheckout",
                        value: function() {
                            var e = Object(t.a)(m.a.mark((function e() {
                                var n, a, t, l, i, s, r, d;
                                return m.a.wrap((function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            if (n = this.props.data.bestAsk) {
                                                e.next = 3;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 3:
                                            if (a = Object(w.c)(n.makerAssetBundle.assetQuantities)) {
                                                e.next = 6;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 6:
                                            if (t = Object(w.c)(n.takerAssetBundle.assetQuantities)) {
                                                e.next = 9;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 9:
                                            if ((l = t.asset.symbol) && K.c.isSupportedSymbol(l)) {
                                                e.next = 12;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 12:
                                            if (i = a.asset.assetContract.chain, K.c.isSupportedChain(i)) {
                                                e.next = 15;
                                                break
                                            }
                                            return e.abrupt("return");
                                        case 15:
                                            return s = Object(Q.b)(n), e.next = 18, K.c.getNftAvailability({
                                                tokenId: a.asset.tokenId,
                                                contractAddress: a.asset.assetContract.address,
                                                listingId: n.relayId,
                                                sellerAddress: n.maker.address,
                                                price: s.toString(),
                                                chain: i
                                            });
                                        case 18:
                                            r = e.sent, d = r.available, this.setState({
                                                isAvailableForFiatCheckout: d
                                            });
                                        case 21:
                                        case "end":
                                            return e.stop()
                                    }
                                }), e, this)
                            })));
                            return function() {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "getIsMoonPayAllowed",
                        value: function() {
                            var e = Object(t.a)(m.a.mark((function e() {
                                var n, a;
                                return m.a.wrap((function(e) {
                                    for (;;) switch (e.prev = e.next) {
                                        case 0:
                                            return e.next = 2, fetch(N.pb);
                                        case 2:
                                            return n = e.sent, e.next = 5, n.json();
                                        case 5:
                                            a = e.sent, this.setState({
                                                isMoonPayBuyAllowed: !!a.isBuyAllowed,
                                                isMoonPayNftAllowed: !!a.isNftAllowed
                                            });
                                        case 7:
                                        case "end":
                                            return e.stop()
                                    }
                                }), e, this)
                            })));
                            return function() {
                                return e.apply(this, arguments)
                            }
                        }()
                    }, {
                        key: "render",
                        value: function() {
                            var e, n, a, l, i = this,
                                s = this.state,
                                r = s.isAvailableForFiatCheckout,
                                d = s.isMoonPayNftAllowed,
                                c = s.isMoonPayBuyAllowed,
                                o = this.props,
                                u = o.data,
                                y = o.bundleData,
                                p = o.collectionSlug,
                                k = this.context.wallet,
                                j = u.bestAsk;
                            if (null === j || void 0 === j || !j.makerAssetBundle) return this.renderForUnlistedAsset();
                            var K = Object(b.first)(Object(w.d)(j.takerAssetBundle.assetQuantities));
                            if (!K) return null;
                            var E = Object(b.first)(Object(w.d)(null === (e = u.bestBid) || void 0 === e ? void 0 : e.makerAssetBundle.assetQuantities)),
                                N = null !== E && void 0 !== E && E.quantity ? Object(P.d)(E.quantity) : null,
                                V = j.englishAuctionReservePrice ? Object(P.d)(j.englishAuctionReservePrice) : null,
                                U = j.dutchAuctionFinalPrice,
                                H = j.orderType,
                                $ = j.closedAt ? Object(I.e)(j.closedAt) : void 0,
                                G = $ ? f.a.duration($.diff(f.a.utc())).asHours() : void 0,
                                X = void 0 !== G && G < 0 && "ENGLISH" === H && !!E,
                                Z = K.quantity ? Object(P.d)(K.quantity) : null,
                                J = Object(w.c)(j.makerAssetBundle.assetQuantities),
                                ee = Object(Q.c)(j),
                                ne = "DUTCH" === H && Z && null !== U && Z.greaterThan(Object(P.d)(U)),
                                ae = "DUTCH" === H && Z && null !== U && Z.lessThan(Object(P.d)(U)),
                                te = j.priceFnEndedAt ? Object(I.e)(j.priceFnEndedAt) : $,
                                le = null === te || void 0 === te ? void 0 : te.isSameOrBefore(f()()),
                                ie = K.asset.symbol,
                                se = "WETH" === ie ? "ETH" : ie,
                                re = V && k.isActiveAccount(j.maker) ? "of ".concat(Object(P.f)(Object(P.d)(V, K.asset.decimals), ie || void 0), " ").concat(se, " ") : "",
                                de = V ? N && N.greaterThanOrEqualTo(V) ? " -- Reserve price ".concat(re, "met!") : " -- Reserve price ".concat(re, "not met.") : "",
                                ce = Object(I.e)(j.openedAt).local(),
                                oe = ce.isAfter(f()()),
                                ue = N && Z && N.greaterThan(Z),
                                me = Object(M.b)(j.makerAssetBundle.assetQuantities),
                                ye = function() {
                                    null !== J && void 0 !== J && J.asset && Object(B.k)(J.asset)
                                },
                                pe = function() {
                                    null !== J && void 0 !== J && J.asset && Object(B.s)(J.asset)
                                },
                                ge = k.isActiveAccount(j.maker),
                                be = null !== (n = null === (a = k.activeAccount) || void 0 === a ? void 0 : a.isCompromised) && void 0 !== n && n,
                                ke = ge || oe || be,
                                fe = null === J || void 0 === J ? void 0 : J.asset.assetContract.chain,
                                je = k.getActiveAccountKey(),
                                he = {
                                    orderId: j.relayId,
                                    asset: K.asset.relayId,
                                    identity: {
                                        address: null === je || void 0 === je ? void 0 : je.address
                                    }
                                },
                                Oe = this.getIsBuyWithCardButtonShown(),
                                ve = "ENGLISH" !== H,
                                Ae = be ? C.a : void 0,
                                Fe = ge ? "You own this item." : void 0,
                                Ie = oe ? Object(_.a)(ce, "ENGLISH" === H ? "bid on" : "buy") : void 0,
                                Pe = null !== (l = null !== Ae && void 0 !== Ae ? Ae : Fe) && void 0 !== l ? l : Ie,
                                Me = function(e) {
                                    return Object(z.jsx)(A, {
                                        width: {
                                            _: "100%",
                                            xl: Oe || !ve ? "100%" : "50%"
                                        },
                                        children: Object(z.jsx)(v.c, {
                                            disabled: ke,
                                            icon: "account_balance_wallet",
                                            width: "100%",
                                            onClick: function(e) {
                                                function n() {
                                                    return e.apply(this, arguments)
                                                }
                                                return n.toString = function() {
                                                    return e.toString()
                                                }, n
                                            }((function() {
                                                "ENGLISH" !== H && J && Object(L.a)({
                                                    assetId: J.asset.relayId,
                                                    assetContractAddress: J.asset.assetContract.address,
                                                    tokenId: J.asset.tokenId
                                                }), e && e()
                                            })),
                                            children: "ENGLISH" === H ? "Place bid" : "Buy ".concat(y ? "bundle" : "now")
                                        })
                                    })
                                },
                                Ee = (this.context.isStaff || h.b) && d && r,
                                Ne = null === J || void 0 === J ? void 0 : J.asset.relayId,
                                Ve = this.shouldVerifyCollectionDetails(me),
                                qe = Object(z.jsxs)("div", {
                                    className: "TradeStation--main",
                                    children: [Object(z.jsxs)("div", {
                                        className: "TradeStation--ask-label",
                                        children: ["ENGLISH" === H ? ue ? "Top bid" : "Minimum bid" : "Current price", de]
                                    }), Object(z.jsxs)("div", {
                                        className: "TradeStation--price-container",
                                        children: [ee.greaterThan(1) ? Object(z.jsx)(F.b, {
                                            content: "Quantity: ".concat(ee),
                                            children: Object(z.jsx)(R.a, {
                                                className: "TradeStation--quantity-badge",
                                                icon: "filter_none",
                                                text: "x".concat(ee)
                                            })
                                        }) : null, "ENGLISH" === H && E && ue ? Object(z.jsxs)(z.Fragment, {
                                            children: [Object(z.jsx)(q.a, {
                                                className: "TradeStation--price",
                                                data: E,
                                                size: 24
                                            }), Object(z.jsx)(q.a, {
                                                className: "TradeStation--fiat-price",
                                                data: E,
                                                secondary: !0,
                                                variant: "fiat"
                                            })]
                                        }) : Object(z.jsxs)(z.Fragment, {
                                            children: [Object(z.jsx)(Ce.a, {
                                                className: "TradeStation--price",
                                                data: j,
                                                size: 24
                                            }), Object(z.jsx)(Ce.a, {
                                                className: "TradeStation--fiat-price",
                                                data: j,
                                                secondary: !0,
                                                size: 24,
                                                variant: "fiat"
                                            })]
                                        }), (ne || ae) && !le || "ENGLISH" === H ? Object(z.jsx)(S.a, {
                                            children: Object(z.jsx)(F.b, {
                                                content: "DUTCH" === H ? Object(z.jsxs)("div", {
                                                    children: [Object(z.jsx)("div", {
                                                        children: ae ? "Price increasing" : "Price declining"
                                                    }), Object(z.jsxs)("div", {
                                                        children: ["The buy-it-now price is", " ", ae ? "increasing" : "declining", " from\xa0", Object(z.jsx)(O.a, {
                                                            display: "inline",
                                                            verticalAlign: "sub",
                                                            children: Object(z.jsx)(q.a, {
                                                                color: "white",
                                                                data: K,
                                                                isInline: !0
                                                            })
                                                        }), "\xa0to\xa0", Object(z.jsx)(O.a, {
                                                            display: "inline",
                                                            verticalAlign: "sub",
                                                            children: Object(z.jsx)(q.a, {
                                                                color: "white",
                                                                data: K,
                                                                isInline: !0,
                                                                mapQuantity: U ? function() {
                                                                    return Object(P.d)(U)
                                                                } : void 0
                                                            })
                                                        }), ", ending", Object(z.jsx)(O.a, {
                                                            display: "inline",
                                                            verticalAlign: "middle",
                                                            children: Object(z.jsx)(W, {
                                                                endMoment: te
                                                            })
                                                        })]
                                                    })]
                                                }) : "The highest bidder will win the item at the end of the auction.",
                                                children: Object(z.jsx)(Y.a, {
                                                    className: Object(D.a)("TradeStation", {
                                                        "price-auction-icon": !0,
                                                        "price-auction-icon-dutch": "DUTCH" === H,
                                                        "price-auction-icon-rising": ae || "ENGLISH" === H
                                                    }),
                                                    cursor: "pointer",
                                                    value: "transit_enterexit"
                                                })
                                            })
                                        }) : null]
                                    }), Object(z.jsx)(O.a, {
                                        display: ["block", "block", "block", Oe ? "block" : "flex"],
                                        maxWidth: ["100%", "100%", "100%", "420px"],
                                        children: Object(z.jsx)(Ke.a, {
                                            chainIdentifier: fe,
                                            overrides: {
                                                unsupportedModalButton: {
                                                    style: {
                                                        width: "100%",
                                                        display: "contents"
                                                    }
                                                }
                                            },
                                            children: Object(z.jsx)(Se.a, {
                                                chainIdentifier: fe,
                                                collectionSlug: p,
                                                children: ke ? Object(z.jsx)(F.b, {
                                                    content: Pe,
                                                    disabled: !ke,
                                                    placement: "right",
                                                    children: Me()
                                                }) : Object(z.jsxs)(z.Fragment, {
                                                    children: [Object(z.jsx)(we.a, {
                                                        assetId: Ne,
                                                        renderMainModal: "ENGLISH" === H ? this.renderBidModal : function(e) {
                                                            return J && je ? Object(z.jsx)(Le.a, {
                                                                variables: he,
                                                                onClose: e
                                                            }) : null
                                                        },
                                                        shouldVerifyCollectionDetails: Ve,
                                                        trigger: Me,
                                                        onClose: ye,
                                                        onPrevious: pe
                                                    }), Oe ? Object(z.jsxs)(_e, {
                                                        marginTop: "8px",
                                                        children: [Object(z.jsx)(O.a, {
                                                            children: Object(z.jsx)(we.a, {
                                                                assetId: null === J || void 0 === J ? void 0 : J.asset.relayId,
                                                                renderMainModal: Ee ? function(e) {
                                                                    return Object(z.jsx)(z.Fragment, {
                                                                        children: J && Object(z.jsx)(Be.MoonPayCheckoutModal, {
                                                                            assetIDs: [J.asset.relayId],
                                                                            order: j.relayId
                                                                        })
                                                                    })
                                                                } : function(e) {
                                                                    return Object(z.jsx)(Be.MoonPayTopupModal, {
                                                                        chain: fe,
                                                                        checkoutVariables: he,
                                                                        fiatValue: K.asset.usdSpotPrice ? Object(P.d)(K.quantity, K.asset.decimals).mul(K.asset.usdSpotPrice) : void 0,
                                                                        symbol: null !== ie && void 0 !== ie ? ie : void 0,
                                                                        onClose: e
                                                                    })
                                                                },
                                                                shouldVerifyCollectionDetails: Ve,
                                                                trigger: function(e) {
                                                                    var n = c || d,
                                                                        a = void 0 === r,
                                                                        l = ke || a || !n,
                                                                        s = !1 === n ? C.b : void 0;
                                                                    return Object(z.jsx)(F.b, {
                                                                        content: null !== Pe && void 0 !== Pe ? Pe : s,
                                                                        disabled: a || !l,
                                                                        children: Object(z.jsx)(O.a, {
                                                                            children: Object(z.jsx)(v.c, {
                                                                                disabled: l,
                                                                                icon: "credit_card",
                                                                                variant: "secondary",
                                                                                width: "100%",
                                                                                onClick: Object(t.a)(m.a.mark((function n() {
                                                                                    return m.a.wrap((function(n) {
                                                                                        for (;;) switch (n.prev = n.next) {
                                                                                            case 0:
                                                                                                if (Object(L.b)({
                                                                                                        assetId: K.asset.relayId
                                                                                                    }), k.activeAccount) {
                                                                                                    n.next = 4;
                                                                                                    break
                                                                                                }
                                                                                                return n.next = 4, i.context.login();
                                                                                            case 4:
                                                                                                e();
                                                                                            case 5:
                                                                                            case "end":
                                                                                                return n.stop()
                                                                                        }
                                                                                    }), n)
                                                                                }))),
                                                                                children: "Buy with card"
                                                                            })
                                                                        })
                                                                    })
                                                                },
                                                                onClose: ye,
                                                                onPrevious: pe
                                                            })
                                                        }), Object(z.jsx)(O.a, {
                                                            children: Object(z.jsx)(we.a, {
                                                                assetId: null === J || void 0 === J ? void 0 : J.asset.relayId,
                                                                renderMainModal: this.renderBidModal,
                                                                shouldVerifyCollectionDetails: Ve,
                                                                trigger: function(e) {
                                                                    return i.renderMakeOfferButton({
                                                                        onClick: e,
                                                                        assetRelayId: null === J || void 0 === J ? void 0 : J.asset.relayId,
                                                                        isDisabled: ke
                                                                    })
                                                                },
                                                                onClose: ye,
                                                                onPrevious: pe
                                                            })
                                                        })]
                                                    }) : ve ? Object(z.jsx)(we.a, {
                                                        assetId: null === J || void 0 === J ? void 0 : J.asset.relayId,
                                                        renderMainModal: this.renderBidModal,
                                                        shouldVerifyCollectionDetails: Ve,
                                                        trigger: function(e) {
                                                            return i.renderMakeOfferButton({
                                                                onClick: e,
                                                                assetRelayId: null === J || void 0 === J ? void 0 : J.asset.relayId,
                                                                isDisabled: ke
                                                            })
                                                        },
                                                        onClose: ye,
                                                        onPrevious: pe
                                                    }) : null]
                                                })
                                            })
                                        })
                                    })]
                                });
                            if (!j.priceFnEndedAt && !$ || le) return Object(z.jsx)(De, {
                                children: Object(z.jsx)(Te.e, {
                                    children: qe
                                })
                            });
                            var Re = te ? Object(g.a)(new Date(te.format()), new Date) : void 0,
                                Ue = void 0 !== Re && Re < 3;
                            return Object(z.jsx)(z.Fragment, {
                                children: Object(z.jsx)(De, {
                                    children: Object(z.jsxs)(Te.e, {
                                        children: [Object(z.jsxs)(Qe, {
                                            className: "TradeStation--header",
                                            children: [Object(z.jsxs)(T.a, {
                                                alignItems: "center",
                                                children: [Object(z.jsxs)(x.a, {
                                                    children: [Ue ? null : Object(z.jsx)(x.a, {
                                                        alignItems: "center",
                                                        marginRight: "4px",
                                                        children: Object(z.jsx)(Y.a, {
                                                            className: "TradeStation--header-icon",
                                                            value: X ? "gavel" : "schedule"
                                                        })
                                                    }), X ? Object(z.jsx)(O.a, {
                                                        children: "Sold! Matching orders on the blockchain."
                                                    }) : Object(z.jsx)(W, {
                                                        endMoment: te,
                                                        includeCountDown: !1,
                                                        includeDate: !0,
                                                        postfix: U ? Object(z.jsxs)(z.Fragment, {
                                                            children: ["\xa0at\xa0", Object(z.jsx)(q.a, {
                                                                className: "TradeStation--header-dutch-final-price",
                                                                data: K,
                                                                mapQuantity: function() {
                                                                    return Object(P.d)(U)
                                                                }
                                                            })]
                                                        }) : void 0,
                                                        prefix: $ ? "Sale ends" : ae ? "Price increase ends" : "Price decline ends"
                                                    })]
                                                }), "DUTCH" === H || "ENGLISH" === H ? Object(z.jsx)(F.b, {
                                                    content: "DUTCH" === H ? Object(z.jsxs)(O.a, {
                                                        children: [ae ? "Increasing" : "Declining", " Price Auction The price of this item", " ", ae ? "increases" : "declines", " over time. It can be bought by anyone at any point during the duration of the auction."]
                                                    }) : Object(z.jsxs)(O.a, {
                                                        children: ["Extending Auction ", Object(z.jsx)("br", {}), "A new highest bid placed under 10 minutes remaining will extend the auction by an additional 10 minutes."]
                                                    }),
                                                    children: Object(z.jsx)(Y.a, {
                                                        className: "TradeStation--header-icon-help",
                                                        cursor: "pointer",
                                                        value: "help_outline"
                                                    })
                                                }) : null]
                                            }), Ue ? Object(z.jsx)(O.a, {
                                                marginTop: "8px",
                                                children: Object(z.jsx)(xe, {
                                                    endMoment: null === te || void 0 === te ? void 0 : te.format()
                                                })
                                            }) : null]
                                        }), qe]
                                    })
                                })
                            })
                        }
                    }]), a
                }(E.b),
                _e = (n.a = Object(w.b)(Pe, {
                    fragments: {
                        archetypeData: void 0 !== fe ? fe : fe = a("3LJ8"),
                        bundleData: void 0 !== je ? je : je = a("+DN6"),
                        data: void 0 !== he ? he : he = a("fnww")
                    }
                }), Object(j.d)(O.a).withConfig({
                    displayName: "TradeStationreact__ButtonGroup",
                    componentId: "sc-o1vm2f-0"
                })(["> *{width:100%;}>:last-child{margin-top:8px;}", ""], Object(G.e)({
                    tabletL: Object(j.c)(["display:flex;> *{width:50%;}>:last-child{margin-top:0px;margin-left:8px;}"])
                }))),
                Qe = Object(j.d)(O.a).withConfig({
                    displayName: "TradeStationreact__TimerContainer",
                    componentId: "sc-o1vm2f-1"
                })(["border-top-right-radius:", ";border-top-left-radius:", ";padding:10px;border-bottom:1px solid ", ";color:", ";font-weight:initial;padding:20px;", " .TradeStation--header-icon-help{&:hover{color:", ";}}.TradeStation--header-dutch-final-price{display:inline-flex;color:inherit;}"], (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), (function(e) {
                    var n = e.theme;
                    return Object(V.b)({
                        variants: {
                            light: {
                                "background-color": n.colors.white
                            },
                            dark: {
                                "background-color": n.colors.onyx
                            }
                        }
                    })
                }), (function(e) {
                    return e.theme.colors.text.heading
                })),
                De = j.d.div.withConfig({
                    displayName: "TradeStationreact__DivContainer",
                    componentId: "sc-o1vm2f-2"
                })([".TradeStation--main{background-color:", ";padding:12px;.TradeStation--ask-label{color:", ";}.TradeStation--price-container{display:flex;flex-wrap:wrap;margin-bottom:8px;.TradeStation--quantity-badge{margin:auto 8px auto 0;}.TradeStation--price{font-size:30px;}.TradeStation--fiat-price{font-size:15px;margin-left:8px;margin-top:15px;}.TradeStation--price-auction-icon{background-color:", ";border-radius:22px;color:", ";height:24px;margin-left:4px;width:24px;&.TradeStation--price-auction-icon-dutch{transform:rotate(270deg);}&.TradeStation--price-auction-icon-rising{transform:rotate(180deg);}}}}.TradeStation--modal{display:inline-block;}", ""], (function(e) {
                    return e.theme.colors.surface
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), (function(e) {
                    return e.theme.colors.white
                }), Object(G.e)({
                    tabletL: Object(j.c)([".TradeStation--main{padding:20px;}"])
                }))
        },
        "7Smq": function(e, n, a) {
            "use strict";
            var t, l, i, s, r = a("JiVo"),
                d = a("qd51"),
                c = a("etRO"),
                o = a("4jfz"),
                u = a("g2+O"),
                m = a("mHfP"),
                y = a("1U+3"),
                p = a("DY1Z"),
                g = a("m6w3"),
                b = a("/dBk"),
                k = a.n(b),
                f = (a("mXGw"), a("TiKg")),
                j = a.n(f),
                h = a("b7Z7"),
                O = a("LoMF"),
                v = a("BWBr"),
                x = a("QrBS"),
                A = a("RKEK"),
                T = a("ZwbU"),
                F = a("6Ojl"),
                S = a("06eW"),
                K = a("j/Wi"),
                C = a("LsOE"),
                L = a("4u0K"),
                B = a("BmUw"),
                w = a("LjoF"),
                I = a("xpX1"),
                P = a("u6YR"),
                _ = a("b2pk"),
                Q = a("DBou"),
                D = a("9gvq"),
                M = a("vI8H"),
                E = a("C/iq"),
                N = a("GEY6"),
                V = a("LweY"),
                q = a("PUKg"),
                R = a("wwms"),
                U = a("Aztv"),
                H = a("WrVu"),
                $ = a("VLAj"),
                z = a("pKap"),
                W = a("xu0C"),
                Y = a("9qRV"),
                G = a("sFAD"),
                X = a("aXrf"),
                Z = a("UutA"),
                J = a("Q5Gx"),
                ee = a("GTDp"),
                ne = a("n0tG"),
                ae = a("4QRR"),
                te = a("oYCi"),
                le = function(e) {
                    var n, l = e.dataKey,
                        i = e.price,
                        s = e.paymentAssetRelayId,
                        r = e.onChange,
                        d = e.showSinglePaymentAsset,
                        c = e.onBlur,
                        o = e.autoFocus,
                        u = Object(X.useFragment)(void 0 !== t ? t : t = a("fKyI"), l),
                        m = u.find((function(e) {
                            return e.relayId === s
                        })),
                        y = null === m || void 0 === m ? void 0 : m.asset.usdSpotPrice;
                    return Object(te.jsx)(ie, {
                        children: Object(te.jsx)(z.a, {
                            autoFocus: o,
                            inputValue: i.toString(),
                            isRequired: !0,
                            maxDecimals: null !== (n = null === m || void 0 === m ? void 0 : m.asset.decimals) && void 0 !== n ? n : 0,
                            placeholder: "Amount",
                            right: Object(te.jsx)(ne.a, {
                                as: "div",
                                className: "PaymentTokenInputV2--price-display PaymentTokenInputV2--input-right",
                                textAlign: "right",
                                variant: "small",
                                children: y && i ? "$".concat(Object(w.h)(Object(w.d)(i).mul(y))) : "$0.00"
                            }),
                            value: i.toString(),
                            onBlur: c,
                            onChange: function(e) {
                                var n = e.value;
                                return void 0 !== n && r({
                                    paymentAssetRelayId: s,
                                    price: n
                                })
                            },
                            children: (d || 1 === u.length) && m ? Object(te.jsx)($.a, {
                                className: "PaymentTokenInputV2--payment-asset PaymentTokenInputV2--input-left",
                                data: m
                            }) : Object(te.jsx)(ee.a, {
                                className: "PaymentTokenInputV2--input-left",
                                getKey: function(e) {
                                    return e.relayId
                                },
                                header: m ? Object(te.jsx)($.a, {
                                    data: m
                                }) : null,
                                isClosedOnSelect: !0,
                                items: Object(ae.a)(u.filter((function(e) {
                                    return e !== m
                                })), (function(e) {
                                    var n;
                                    return null !== (n = e.asset.symbol) && void 0 !== n ? n : ""
                                })),
                                render: function(e) {
                                    return Object(te.jsx)($.a, {
                                        data: e
                                    })
                                },
                                showAllOptions: !0,
                                onItemClick: function(e) {
                                    return r({
                                        paymentAssetRelayId: e.relayId,
                                        price: i
                                    })
                                }
                            })
                        })
                    })
                },
                ie = Z.d.div.withConfig({
                    displayName: "PaymentTokenInputV2react__DivContainer",
                    componentId: "sc-8qscmp-0"
                })([".PaymentTokenInputV2--payment-asset,.PaymentTokenInputV2--price-display{padding:0 12px;}.PaymentTokenInputV2--price-display{overflow:hidden;text-overflow:ellipsis;}", ""], Object(J.e)({
                    small: Object(Z.c)([".PaymentTokenInputV2--input-left{min-width:150px;}.PaymentTokenInputV2--input-right{width:100px;}"])
                })),
                se = a("WAap");

            function re(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(p.a)(e);
                    if (n) {
                        var l = Object(p.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(y.a)(this, a)
                }
            }
            var de = function(e) {
                Object(m.a)(a, e);
                var n = re(a);

                function a() {
                    var e, t, l, i, s, r, o, m, y, p;
                    Object(c.a)(this, a);
                    for (var b = arguments.length, f = new Array(b), h = 0; h < b; h++) f[h] = arguments[h];
                    return p = n.call.apply(n, [this].concat(f)), Object(g.a)(Object(u.a)(p), "now", j()()), Object(g.a)(Object(u.a)(p), "offerExpirationOptions", [{
                        label: "3 days",
                        value: "3d",
                        date: j()(p.now).add(3, "days")
                    }, {
                        label: "7 days",
                        value: "7d",
                        date: j()(p.now).add(1, "week")
                    }, {
                        label: "A month",
                        value: "1m",
                        date: j()(p.now).add(1, "month")
                    }, {
                        label: "Custom date",
                        value: "custom",
                        date: void 0
                    }]), Object(g.a)(Object(u.a)(p), "defaultCustomExpirationDate", j()(p.now).add(30, "minutes")), Object(g.a)(Object(u.a)(p), "state", {
                        balanceSublabel: "Checking balance...",
                        bidExpiration: p.isEnglishAuction() ? j()(null === (e = p.props.tradeData.bestAsk) || void 0 === e ? void 0 : e.closedAt).add(7, "days") : p.offerExpirationOptions[1].date,
                        bidExpirationValue: "7d",
                        paymentAssetRelayId: null !== (t = p.isEnglishAuction() ? null === (l = p.getPaymentAssets()) || void 0 === l || null === (i = l.find((function(e) {
                            var n, a;
                            return e.asset.relayId === (null === (n = Object(C.c)(null === (a = p.props.tradeData.bestAsk) || void 0 === a ? void 0 : a.takerAssetBundle.assetQuantities)) || void 0 === n ? void 0 : n.asset.relayId)
                        }))) || void 0 === i ? void 0 : i.relayId : null === (s = p.getPaymentAssets()) || void 0 === s ? void 0 : s[0].relayId) && void 0 !== t ? t : "",
                        quantity: "1",
                        pricePerUnit: null !== (r = null !== (o = null === (m = p.getMinRaisePrice()) || void 0 === m ? void 0 : m.toString()) && void 0 !== o ? o : null === (y = p.getMinBid()) || void 0 === y ? void 0 : y.toString()) && void 0 !== r ? r : "",
                        isValidPrice: !0,
                        didAcknowledgeReview: !1,
                        didAcknowledgeToS: !1,
                        showAddFunds: !0
                    }), Object(g.a)(Object(u.a)(p), "fetchAndSetBalance", Object(d.a)(k.a.mark((function e() {
                        var n, a, t, l, i, s;
                        return k.a.wrap((function(e) {
                            for (;;) switch (e.prev = e.next) {
                                case 0:
                                    if (n = p.context.wallet, a = p.getCurrentPaymentAsset(), t = null === a || void 0 === a ? void 0 : a.asset.symbol, !(l = null === a || void 0 === a ? void 0 : a.asset.relayId)) {
                                        e.next = 21;
                                        break
                                    }
                                    return e.t0 = w.d, e.next = 8, n.getBaseBalance(l);
                                case 8:
                                    if (e.t2 = i = e.sent, e.t1 = null !== e.t2, !e.t1) {
                                        e.next = 12;
                                        break
                                    }
                                    e.t1 = void 0 !== i;
                                case 12:
                                    if (!e.t1) {
                                        e.next = 16;
                                        break
                                    }
                                    e.t3 = i, e.next = 17;
                                    break;
                                case 16:
                                    e.t3 = 0;
                                case 17:
                                    e.t4 = e.t3, e.t5 = a.asset.decimals, s = (0, e.t0)(e.t4, e.t5), p.setState({
                                        balance: s,
                                        balanceSublabel: "Balance: ".concat(s.toFixed(4, 1).toString(), " ").concat(t)
                                    });
                                case 21:
                                case "end":
                                    return e.stop()
                            }
                        }), e)
                    })))), Object(g.a)(Object(u.a)(p), "validateBalance", (function() {
                        var e = p.getCurrentPaymentAsset(),
                            n = null === e || void 0 === e ? void 0 : e.asset.symbol,
                            a = p.state,
                            t = a.quantity,
                            l = a.pricePerUnit,
                            i = a.balance,
                            s = i && i.lessThan(Object(w.d)(l).times(Object(w.d)(t)));
                        return p.setState({
                            errorText: s ? "Not enough ".concat(n, " to ").concat(p.isEnglishAuction() ? "place bid" : "make offer") : "",
                            showAddFunds: s,
                            isValidPrice: !s
                        }), !s
                    })), Object(g.a)(Object(u.a)(p), "validate", (function() {
                        var e = p.state,
                            n = e.pricePerUnit,
                            a = e.quantity,
                            t = e.bidExpiration,
                            l = Object(w.d)(n),
                            i = Object(w.d)(a);
                        if (!l.greaterThan(0) || !i.greaterThan(0) || !t) return p.setState({
                            errorText: "Please fill out all fields"
                        }), !1;
                        var s = p.getCurrentPaymentAsset(),
                            r = null === s || void 0 === s ? void 0 : s.asset.symbol,
                            d = l.times(i),
                            c = null === s || void 0 === s ? void 0 : s.asset.assetContract.chain,
                            o = p.getMinRaisePrice(),
                            u = p.getMinBid(),
                            m = c && E.mb[c];
                        if (p.isEnglishAuction() && o && d.lessThan(o) && p.setState({
                                errorText: "Place a bid of at least ".concat(o.toString(), " ").concat(r, " to become the highest bidder")
                            }), u && d.lessThan(u)) {
                            var y = p.isEnglishAuction() ? "Offer must be at least the minimum price per unit of ".concat(u.toString(), " ").concat(r) : c && m ? "Offer must be at least the minimum price per unit of $".concat(Object(w.h)(m), " USD (").concat(Object(w.f)(u, null !== r && void 0 !== r ? r : "ETH"), " ").concat(r, ")") : null;
                            if (y) return p.setState({
                                errorText: y,
                                isValidPrice: !1
                            }), !1
                        } else if (!Object(B.e)(c)) return p.validateBalance();
                        return p.setState({
                            errorText: "",
                            isValidPrice: !0
                        }), !0
                    })), Object(g.a)(Object(u.a)(p), "getCurrentPaymentAsset", (function() {
                        var e, n = p.state.paymentAssetRelayId;
                        return null === (e = p.getPaymentAssets()) || void 0 === e ? void 0 : e.find((function(e) {
                            return e.relayId === n
                        }))
                    })), Object(g.a)(Object(u.a)(p), "renderOfferExpirationDropdown", (function() {
                        var e = p.state.bidExpirationValue;
                        return Object(te.jsx)(S.a, {
                            clearable: !1,
                            options: p.offerExpirationOptions,
                            readOnly: !0,
                            style: {
                                width: "176px"
                            },
                            value: e,
                            onSelect: function(e) {
                                var n;
                                e && p.setState({
                                    bidExpirationValue: e.value,
                                    bidExpiration: null !== (n = e.date) && void 0 !== n ? n : p.defaultCustomExpirationDate,
                                    isCustomExpiration: !e.date
                                })
                            }
                        })
                    })), Object(g.a)(Object(u.a)(p), "onOrderCreated", (function() {
                        var e = p.props,
                            n = e.onOrderCreated;
                        (0, e.onClose)(), p.showSuccessMessage("Your offer was submitted successfully!"), null === n || void 0 === n || n()
                    })), Object(g.a)(Object(u.a)(p), "getTotalAmount", (function() {
                        var e = p.state,
                            n = e.pricePerUnit,
                            a = e.quantity;
                        return n && a ? Object(w.d)(n).mul(a).toNumber() : null
                    })), Object(g.a)(Object(u.a)(p), "onSubmit", Object(d.a)(k.a.mark((function e() {
                        var n, a, t, l, i, s, r, d, c, o, u, m, y, g, b, f, j, h, O, v, x, A, T, F, S, K, C, L, B, w, P;
                        return k.a.wrap((function(e) {
                            for (;;) switch (e.prev = e.next) {
                                case 0:
                                    if (t = p.context, l = t.mutate, i = t.wallet, null !== (n = i.activeAccount) && void 0 !== n && null !== (a = n.user) && void 0 !== a && a.hasAffirmativelyAcceptedOpenseaTerms) {
                                        e.next = 4;
                                        break
                                    }
                                    return e.next = 4, p.attempt(Object(I.d)(l));
                                case 4:
                                    if (!p.validate()) {
                                        e.next = 33;
                                        break
                                    }
                                    if (s = p.props, r = s.archetypeData, d = s.bundleData, c = s.tradeData, o = s.onNext, u = s.onClose, m = p.state, y = m.quantity, g = m.bidExpiration, b = m.pricePerUnit, f = null === r || void 0 === r ? void 0 : r.asset.assetContract.address, j = null === r || void 0 === r ? void 0 : r.asset.assetContract.chain, h = p.getCurrentPaymentAsset(), O = p.getTotalAmount(), Object(I.c)({
                                            address: f,
                                            slug: null === r || void 0 === r ? void 0 : r.asset.collection.slug,
                                            chain: j
                                        })) {
                                        e.next = 29;
                                        break
                                    }
                                    if (v = null === r || void 0 === r ? void 0 : r.asset.relayId, !(x = null === d || void 0 === d ? void 0 : d.slug) && !v) {
                                        e.next = 27;
                                        break
                                    }
                                    return T = !!v, e.next = 18, Q.a.fetch(T ? "/assets_by_relay_id?relay_ids=".concat(v) : "/bundle/".concat(x, "/"));
                                case 18:
                                    return F = e.sent, S = T ? F.data.map(N.b)[0] : Object(q.a)(F), K = null === (A = c.bestAsk) || void 0 === A ? void 0 : A.oldOrder, C = K ? JSON.parse(K) : void 0, e.next = 24, Object(R.b)(T ? D.a.placeBuyOrder({
                                        asset: S,
                                        amount: null !== O && void 0 !== O ? O : 0,
                                        quantity: +y,
                                        expirationTime: null === g || void 0 === g ? void 0 : g.unix(),
                                        paymentTokenAddress: null === h || void 0 === h ? void 0 : h.asset.assetContract.address,
                                        currentSellOrder: null !== C && void 0 !== C && C.order_hash ? Object(V.c)(C) : void 0
                                    }) : D.a.placeBundleBuyOrder({
                                        bundle: S,
                                        amount: null !== O && void 0 !== O ? O : 0,
                                        expirationTime: null === g || void 0 === g ? void 0 : g.unix(),
                                        paymentTokenAddress: null === h || void 0 === h ? void 0 : h.asset.assetContract.address,
                                        currentSellOrder: null !== C && void 0 !== C && C.order_hash ? Object(V.c)(C) : void 0
                                    }));
                                case 24:
                                    return (L = e.sent) && p.onOrderCreated(), e.abrupt("return", L);
                                case 27:
                                    e.next = 33;
                                    break;
                                case 29:
                                    return w = null === h || void 0 === h ? void 0 : h.asset.relayId, P = null === r || void 0 === r ? void 0 : r.asset.relayId, w && P && o(Object(te.jsx)(Y.a, {
                                        isEnglishAuction: p.isEnglishAuction(),
                                        mode: "bid",
                                        variables: {
                                            maker: {
                                                address: null === (B = i.getActiveAccountKey()) || void 0 === B ? void 0 : B.address
                                            },
                                            makerAssetQuantities: [{
                                                asset: w,
                                                quantity: Object(I.b)({
                                                    pricePerUnit: b,
                                                    quantity: y,
                                                    decimals: h.asset.decimals
                                                }).toString()
                                            }],
                                            takerAssetQuantities: [{
                                                asset: P,
                                                quantity: y
                                            }],
                                            expiration: null === g || void 0 === g ? void 0 : g.unix(),
                                            useMetaTransactions: Object(I.f)({
                                                chain: j,
                                                address: f,
                                                slug: r.asset.collection.slug
                                            })
                                        },
                                        onEnd: p.onOrderCreated,
                                        onError: function() {
                                            u(), p.showErrorMessage("Something went wrong while creating an offer")
                                        }
                                    })), e.abrupt("return", void 0);
                                case 33:
                                    return e.abrupt("return", void 0);
                                case 34:
                                case "end":
                                    return e.stop()
                            }
                        }), e)
                    })))), p
                }
                return Object(o.a)(a, [{
                    key: "componentDidMount",
                    value: function() {
                        var e = Object(d.a)(k.a.mark((function e() {
                            return k.a.wrap((function(e) {
                                for (;;) switch (e.prev = e.next) {
                                    case 0:
                                        return e.next = 2, this.fetchAndSetBalance();
                                    case 2:
                                    case "end":
                                        return e.stop()
                                }
                            }), e, this)
                        })));
                        return function() {
                            return e.apply(this, arguments)
                        }
                    }()
                }, {
                    key: "getMinBid",
                    value: function() {
                        var e, n, a = this.props.tradeData,
                            t = this.getCurrentPaymentAsset && this.getCurrentPaymentAsset(),
                            l = null === t || void 0 === t ? void 0 : t.asset.assetContract.chain,
                            i = null !== (e = null === t || void 0 === t ? void 0 : t.asset.usdSpotPrice) && void 0 !== e ? e : 1,
                            s = l && E.mb[l],
                            r = s ? Object(w.d)(s).div(i) : null,
                            d = this.isEnglishAuction() ? Object(C.c)(null === (n = a.bestAsk) || void 0 === n ? void 0 : n.takerAssetBundle.assetQuantities) : null;
                        return this.isEnglishAuction() ? d && Object(w.d)(d.quantity, d.asset.decimals) : r
                    }
                }, {
                    key: "getMinRaisePrice",
                    value: function() {
                        var e, n = this.props.tradeData,
                            a = Object(C.c)(null === (e = n.bestBid) || void 0 === e ? void 0 : e.makerAssetBundle.assetQuantities),
                            t = a && Object(w.d)(a.quantity, a.asset.decimals);
                        return this.isEnglishAuction() ? null === t || void 0 === t ? void 0 : t.times(1.05) : null
                    }
                }, {
                    key: "isEnglishAuction",
                    value: function() {
                        var e;
                        return "ENGLISH" === (null === (e = this.props.tradeData.bestAsk) || void 0 === e ? void 0 : e.orderType)
                    }
                }, {
                    key: "getPaymentAssets",
                    value: function() {
                        var e = this.props,
                            n = e.archetypeData,
                            a = e.bundleData;
                        if (n) return n.asset.collection.paymentAssets.filter((function(e) {
                            return !("ETH" === e.asset.symbol && "ETHEREUM" === e.asset.assetContract.chain) && e.asset.assetContract.chain === n.asset.assetContract.chain
                        }));
                        if (a) {
                            var t = Object(C.d)(a.assetQuantities).reduce((function(e, n) {
                                return e.push.apply(e, Object(r.a)(n.asset.collection.paymentAssets.filter((function(e) {
                                    return !("ETH" === e.asset.symbol && "ETHEREUM" === e.asset.assetContract.chain)
                                })))), e
                            }), []);
                            return new L.a((function(e) {
                                return e.asset.symbol
                            }), t).elements
                        }
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e, n, a, t, l = this,
                            i = this.props,
                            s = i.archetypeData,
                            r = i.tradeData,
                            c = i.bundleData,
                            o = i.onNext,
                            u = i.onPrevious,
                            m = i.onClose,
                            y = this.state,
                            p = y.quantity,
                            g = y.pricePerUnit,
                            b = y.showAddFunds,
                            f = y.errorText,
                            F = y.balanceSublabel,
                            S = y.bidExpiration,
                            C = y.isCustomExpiration,
                            L = y.didAcknowledgeReview,
                            B = y.didAcknowledgeToS,
                            I = y.paymentAssetRelayId,
                            Q = y.isValidPrice,
                            D = this.getPaymentAssets(),
                            M = this.getCurrentPaymentAsset(),
                            E = null === M || void 0 === M ? void 0 : M.asset.usdSpotPrice,
                            N = null === M || void 0 === M ? void 0 : M.asset.symbol,
                            V = this.getTotalAmount(),
                            q = E && V ? Object(w.d)(V).times(E) : null,
                            R = !(null === s || void 0 === s || !s.quantity) && "1" !== s.quantity,
                            $ = null !== (e = null === s || void 0 === s ? void 0 : s.quantity) && void 0 !== e ? e : "0",
                            Y = null !== s && void 0 !== s && s.asset.collection ? Object(_.a)(s.asset.collection) : void 0,
                            X = null !== (n = r.bestAsk) && void 0 !== n && n.makerAssetBundle.assetQuantities ? Object(_.b)(r.bestAsk.makerAssetBundle.assetQuantities) : void 0,
                            Z = null !== X && void 0 !== X ? X : Y,
                            J = !["verified", "safelisted"].includes(null !== Z && void 0 !== Z ? Z : "unapproved"),
                            ee = !(null === c || void 0 === c || !c.slug),
                            ne = this.context.wallet.activeAccount,
                            ae = !(null === ne || void 0 === ne || null === (a = ne.user) || void 0 === a || !a.hasAffirmativelyAcceptedOpenseaTerms);
                        return Object(te.jsxs)(te.Fragment, {
                            children: [Object(te.jsx)(T.a.Header, {
                                onPrevious: u,
                                children: Object(te.jsx)(T.a.Title, {
                                    children: this.isEnglishAuction() ? "Place a bid" : "Make an offer"
                                })
                            }), Object(te.jsxs)(T.a.Body, {
                                children: [J && ee && Object(te.jsx)(H.a, {}), R ? Object(te.jsx)(A.a.Control, {
                                    label: "Quantity",
                                    children: Object(te.jsx)(z.a, {
                                        id: "quantity",
                                        inputValue: p,
                                        isRequired: !0,
                                        max: Object(w.d)($),
                                        maxDecimals: null !== (t = s.asset.decimals) && void 0 !== t ? t : 0,
                                        name: "quantity",
                                        placeholder: $,
                                        value: p,
                                        onChange: function(e) {
                                            var n = e.value;
                                            return void 0 !== n && l.setState({
                                                quantity: n
                                            }, l.validate)
                                        }
                                    })
                                }) : null, D ? Object(te.jsx)(A.a.Control, {
                                    captionLeft: R ? "Total amount offered: ".concat(V || 0, " ").concat(N, " ").concat(q ? "($".concat(Object(w.h)(q), ")") : "") : void 0,
                                    captionRight: F || void 0,
                                    className: Object(P.a)("BidModalContent", {
                                        price: R
                                    }),
                                    error: !Q && f,
                                    label: R ? "Price per item" : "Price",
                                    children: Object(te.jsx)(le, {
                                        dataKey: D,
                                        paymentAssetRelayId: I,
                                        price: g,
                                        showSinglePaymentAsset: this.isEnglishAuction(),
                                        onChange: function(e) {
                                            var n = e.paymentAssetRelayId,
                                                a = e.price;
                                            l.setState({
                                                paymentAssetRelayId: n,
                                                pricePerUnit: a.toString()
                                            }, Object(d.a)(k.a.mark((function e() {
                                                return k.a.wrap((function(e) {
                                                    for (;;) switch (e.prev = e.next) {
                                                        case 0:
                                                            return e.next = 2, l.fetchAndSetBalance();
                                                        case 2:
                                                            l.validate();
                                                        case 3:
                                                        case "end":
                                                            return e.stop()
                                                    }
                                                }), e)
                                            }))))
                                        }
                                    })
                                }) : null, this.isEnglishAuction() ? null : Object(te.jsx)(A.a.Control, {
                                    label: "Offer Expiration",
                                    children: Object(te.jsxs)(x.a, {
                                        children: [Object(te.jsx)(h.a, {
                                            marginRight: "8px",
                                            children: this.renderOfferExpirationDropdown()
                                        }), Object(te.jsx)(h.a, {
                                            flexGrow: 1,
                                            minWidth: 0,
                                            children: C ? Object(te.jsx)(v.a, {
                                                max: j()(this.now).add("6", "months"),
                                                min: this.defaultCustomExpirationDate,
                                                value: S,
                                                withTime: !0,
                                                onChange: function(e) {
                                                    return l.setState({
                                                        bidExpiration: e
                                                    })
                                                }
                                            }) : Object(te.jsx)(W.a, {
                                                value: this.state.bidExpiration,
                                                onChange: function(e) {
                                                    return l.setState({
                                                        bidExpiration: e
                                                    })
                                                }
                                            })
                                        })]
                                    })
                                }), (!ae || ee && J) && Object(te.jsx)(U.a, {
                                    hasAcceptedTerms: ae,
                                    isBundle: ee,
                                    isReviewChecked: this.state.didAcknowledgeReview,
                                    isToSChecked: this.state.didAcknowledgeToS,
                                    onReviewChecked: function(e) {
                                        return l.setState({
                                            didAcknowledgeReview: e
                                        })
                                    },
                                    onToSChecked: function(e) {
                                        return l.setState({
                                            didAcknowledgeToS: e
                                        })
                                    }
                                })]
                            }), Object(te.jsxs)(T.a.Footer, {
                                children: [Object(te.jsx)(K.b, {
                                    content: f,
                                    disabled: !f,
                                    children: Object(te.jsx)("div", {
                                        children: Object(te.jsx)(O.c, {
                                            disabled: !Object(w.d)(g).greaterThan(0) || !Object(w.d)(p).greaterThan(0) || !!f || !L && ee && J || !B && !ae,
                                            onClick: this.onSubmit,
                                            children: this.isEnglishAuction() ? "Place Bid" : "Make Offer"
                                        })
                                    })
                                }), b && N ? Object(te.jsx)(h.a, {
                                    marginLeft: "24px",
                                    children: Object(te.jsx)(O.c, {
                                        variant: "secondary",
                                        onClick: function() {
                                            var e;
                                            return o(Object(te.jsx)(G.default, {
                                                assetId: M.asset.relayId,
                                                fundsToAdd: null !== q && void 0 !== q ? q : void 0,
                                                orderId: null === (e = r.bestAsk) || void 0 === e ? void 0 : e.relayId,
                                                variables: {
                                                    symbol: N,
                                                    chain: M.asset.assetContract.chain
                                                },
                                                onClose: m
                                            }))
                                        },
                                        children: "WETH" === N ? "Convert ETH" : "Add Funds"
                                    })
                                }) : null]
                            })]
                        })
                    }
                }]), a
            }(M.b);
            n.a = Object(C.b)(Object(F.c)(Object(se.a)(de)), {
                fragments: {
                    archetypeData: void 0 !== l ? l : l = a("kEUh"),
                    bundleData: void 0 !== i ? i : i = a("dtIP"),
                    tradeData: void 0 !== s ? s : s = a("x1jb")
                }
            })
        },
        "7rjq": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "maker"
                    },
                    n = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "orderId"
                    },
                    a = [{
                        kind: "Variable",
                        name: "order",
                        variableName: "orderId"
                    }],
                    t = [{
                        kind: "Variable",
                        name: "maker",
                        variableName: "maker"
                    }],
                    l = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "side",
                        storageKey: null
                    },
                    i = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "orderType",
                        storageKey: null
                    },
                    s = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "blockExplorerUrl",
                        storageKey: null
                    },
                    r = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "displayName",
                        storageKey: null
                    },
                    d = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "identifier",
                        storageKey: null
                    },
                    c = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "publicRpcUrl",
                        storageKey: null
                    },
                    o = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    },
                    u = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    m = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    },
                    y = {
                        alias: null,
                        args: null,
                        concreteType: "PaymentAssetType",
                        kind: "LinkedField",
                        name: "nativeCurrency",
                        plural: !1,
                        selections: [o, {
                            alias: null,
                            args: null,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "asset",
                            plural: !1,
                            selections: [u, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "name",
                                storageKey: null
                            }, m],
                            storageKey: null
                        }, m],
                        storageKey: null
                    },
                    p = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "networkId",
                        storageKey: null
                    },
                    g = {
                        alias: null,
                        args: null,
                        concreteType: "ChainType",
                        kind: "LinkedField",
                        name: "chain",
                        plural: !1,
                        selections: [s, r, d, c, y, p, m],
                        storageKey: null
                    },
                    b = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "minQuantity",
                        storageKey: null
                    },
                    k = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    f = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "value",
                        storageKey: null
                    },
                    j = [f],
                    h = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "clientMessage",
                        storageKey: null
                    },
                    O = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "clientSignatureStandard",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: [e, n],
                        kind: "Fragment",
                        metadata: null,
                        name: "CancelOrderActionModalQuery",
                        selections: [{
                            alias: null,
                            args: a,
                            concreteType: "OrderV2Type",
                            kind: "LinkedField",
                            name: "order",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: t,
                                concreteType: "ActionDataType",
                                kind: "LinkedField",
                                name: "cancelActions",
                                plural: !1,
                                selections: [{
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "ActionPanelList_data"
                                }],
                                storageKey: null
                            }, l, i],
                            storageKey: null
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [n, e],
                        kind: "Operation",
                        name: "CancelOrderActionModalQuery",
                        selections: [{
                            alias: null,
                            args: a,
                            concreteType: "OrderV2Type",
                            kind: "LinkedField",
                            name: "order",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: t,
                                concreteType: "ActionDataType",
                                kind: "LinkedField",
                                name: "cancelActions",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "ActionType",
                                    kind: "LinkedField",
                                    name: "actions",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "actionType",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AskForDepositType",
                                        kind: "LinkedField",
                                        name: "askForDeposit",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [g, u, o, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "usdSpotPrice",
                                                storageKey: null
                                            }, m],
                                            storageKey: null
                                        }, b],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AskForSwapType",
                                        kind: "LinkedField",
                                        name: "askForSwap",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "fromAsset",
                                            plural: !1,
                                            selections: [g, k, u, o, m],
                                            storageKey: null
                                        }, b, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "maxQuantity",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "toAsset",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "ChainType",
                                                kind: "LinkedField",
                                                name: "chain",
                                                plural: !1,
                                                selections: [d, s, r, c, y, p, m],
                                                storageKey: null
                                            }, k, o, m],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "TransactionSubmissionDataType",
                                        kind: "LinkedField",
                                        name: "transaction",
                                        plural: !1,
                                        selections: [g, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "chainIdentifier",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AddressDataType",
                                            kind: "LinkedField",
                                            name: "source",
                                            plural: !1,
                                            selections: j,
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AddressDataType",
                                            kind: "LinkedField",
                                            name: "destination",
                                            plural: !1,
                                            selections: j,
                                            storageKey: null
                                        }, f, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "data",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "SignAndPostType",
                                        kind: "LinkedField",
                                        name: "signAndPost",
                                        plural: !1,
                                        selections: [g, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "orderData",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "serverSignature",
                                            storageKey: null
                                        }, h, O, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "orderId",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "MetaTransactionDataType",
                                        kind: "LinkedField",
                                        name: "metaTransaction",
                                        plural: !1,
                                        selections: [h, O, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "functionSignature",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "verifyingContract",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: null
                            }, l, i, m],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "6ba306f18d9700adc69f61f857c5823e",
                        id: null,
                        metadata: {},
                        name: "CancelOrderActionModalQuery",
                        operationKind: "query",
                        text: "query CancelOrderActionModalQuery(\n  $orderId: OrderRelayID!\n  $maker: IdentityInputType!\n) {\n  order(order: $orderId) {\n    cancelActions(maker: $maker) {\n      ...ActionPanelList_data\n    }\n    side\n    orderType\n    id\n  }\n}\n\nfragment ActionPanelList_data on ActionDataType {\n  actions {\n    ...ActionPanel_data\n    actionType\n  }\n}\n\nfragment ActionPanel_data on ActionType {\n  actionType\n  askForDeposit {\n    asset {\n      chain {\n        ...chain_data\n        identifier\n        id\n      }\n      decimals\n      symbol\n      usdSpotPrice\n      id\n    }\n    minQuantity\n  }\n  askForSwap {\n    fromAsset {\n      chain {\n        ...chain_data\n        identifier\n        id\n      }\n      relayId\n      decimals\n      symbol\n      id\n    }\n    minQuantity\n    maxQuantity\n    toAsset {\n      chain {\n        identifier\n        ...chain_data\n        id\n      }\n      relayId\n      symbol\n      id\n    }\n  }\n  transaction {\n    chain {\n      ...chain_data\n      identifier\n      id\n    }\n    ...trader_transaction\n  }\n  signAndPost {\n    chain {\n      ...chain_data\n      id\n    }\n    ...trader_sign_and_post\n  }\n  ...trader_meta_transaction\n}\n\nfragment chain_data on ChainType {\n  blockExplorerUrl\n  displayName\n  identifier\n  publicRpcUrl\n  nativeCurrency {\n    symbol\n    asset {\n      decimals\n      name\n      id\n    }\n    id\n  }\n  networkId\n}\n\nfragment trader_meta_transaction on ActionType {\n  metaTransaction {\n    clientMessage\n    clientSignatureStandard\n    functionSignature\n    verifyingContract\n  }\n}\n\nfragment trader_sign_and_post on SignAndPostType {\n  chain {\n    ...chain_data\n    id\n  }\n  orderData\n  serverSignature\n  clientMessage\n  clientSignatureStandard\n  orderId\n}\n\nfragment trader_transaction on TransactionSubmissionDataType {\n  chainIdentifier\n  source {\n    value\n  }\n  destination {\n    value\n  }\n  value\n  data\n}\n"
                    }
                }
            }();
            t.hash = "82db668fa4616493ca9203a2567ba33f", n.default = t
        },
        "8MqD": function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return c
            }));
            var t = a("TiKg"),
                l = a.n(t),
                i = a("C/iq"),
                s = a("Ujrs"),
                r = a("Z2Bj"),
                d = a("Ot2x"),
                c = function(e, n) {
                    Object(d.a)({
                        delay: i.Mb,
                        fn: function() {
                            var a = Object(r.e)(e),
                                t = l.a.duration(a.diff(l()())).asMinutes();
                            t > -1 && t < i.G && (Object(s.b)(), n())
                        }
                    })
                }
        },
        "8koP": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "chain"
                    },
                    n = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "slug"
                    },
                    a = [{
                        kind: "Variable",
                        name: "bundle",
                        variableName: "slug"
                    }],
                    t = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    l = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "name",
                        storageKey: null
                    },
                    i = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    },
                    s = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "description",
                        storageKey: null
                    },
                    r = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "address",
                        storageKey: null
                    },
                    d = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "assetCount",
                        storageKey: null
                    },
                    c = [{
                        kind: "Literal",
                        name: "first",
                        value: 30
                    }],
                    o = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imageUrl",
                        storageKey: null
                    },
                    u = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "tokenId",
                        storageKey: null
                    },
                    m = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "hidden",
                        storageKey: null
                    },
                    y = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "chain",
                        storageKey: null
                    },
                    p = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "blockExplorerLink",
                        storageKey: null
                    },
                    g = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    b = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    },
                    k = [{
                        kind: "Variable",
                        name: "chain",
                        variableName: "chain"
                    }],
                    f = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "orderType",
                        storageKey: null
                    },
                    j = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "closedAt",
                        storageKey: null
                    },
                    h = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    },
                    O = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isMintable",
                        storageKey: null
                    },
                    v = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isSafelisted",
                        storageKey: null
                    },
                    x = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isVerified",
                        storageKey: null
                    },
                    A = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    },
                    T = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "usdSpotPrice",
                        storageKey: null
                    },
                    F = [r, h],
                    S = [{
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    K = [{
                        alias: null,
                        args: S,
                        concreteType: "AssetQuantityTypeConnection",
                        kind: "LinkedField",
                        name: "assetQuantities",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetQuantityTypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetQuantityType",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetType",
                                    kind: "LinkedField",
                                    name: "asset",
                                    plural: !1,
                                    selections: [g, h],
                                    storageKey: null
                                }, b, h],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: "assetQuantities(first:1)"
                    }, h];
                return {
                    fragment: {
                        argumentDefinitions: [e, n],
                        kind: "Fragment",
                        metadata: null,
                        name: "bundleQuery",
                        selections: [{
                            alias: null,
                            args: a,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "bundle",
                            plural: !1,
                            selections: [t, l, i, s, {
                                alias: null,
                                args: null,
                                concreteType: "AccountType",
                                kind: "LinkedField",
                                name: "maker",
                                plural: !1,
                                selections: [r, {
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "AccountLink_data"
                                }],
                                storageKey: null
                            }, d, {
                                alias: null,
                                args: c,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [o, l, u, s, t, {
                                                alias: null,
                                                args: null,
                                                concreteType: "CollectionType",
                                                kind: "LinkedField",
                                                name: "collection",
                                                plural: !1,
                                                selections: [m, s, l, {
                                                    args: null,
                                                    kind: "FragmentSpread",
                                                    name: "CollectionLink_collection"
                                                }],
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetContractType",
                                                kind: "LinkedField",
                                                name: "assetContract",
                                                plural: !1,
                                                selections: [r, y, p],
                                                storageKey: null
                                            }, {
                                                kind: "InlineDataFragmentSpread",
                                                name: "asset_display_name",
                                                selections: [u, l]
                                            }, {
                                                args: null,
                                                kind: "FragmentSpread",
                                                name: "AssetMedia_asset"
                                            }, {
                                                kind: "InlineDataFragmentSpread",
                                                name: "asset_url",
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetContractType",
                                                    kind: "LinkedField",
                                                    name: "assetContract",
                                                    plural: !1,
                                                    selections: [r, y],
                                                    storageKey: null
                                                }, u]
                                            }],
                                            storageKey: null
                                        }, {
                                            kind: "InlineDataFragmentSpread",
                                            name: "quantity_data",
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetType",
                                                kind: "LinkedField",
                                                name: "asset",
                                                plural: !1,
                                                selections: [g],
                                                storageKey: null
                                            }, b]
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:30)"
                            }, {
                                args: k,
                                kind: "FragmentSpread",
                                name: "BidModalContent_bundle"
                            }, {
                                args: k,
                                kind: "FragmentSpread",
                                name: "TradeStation_bundle"
                            }, {
                                kind: "InlineDataFragmentSpread",
                                name: "bundle_url",
                                selections: [i]
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: a,
                            concreteType: "TradeSummaryType",
                            kind: "LinkedField",
                            name: "tradeSummary",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "OrderV2Type",
                                kind: "LinkedField",
                                name: "bestAsk",
                                plural: !1,
                                selections: [f, j, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "maker",
                                    plural: !1,
                                    selections: [{
                                        kind: "InlineDataFragmentSpread",
                                        name: "wallet_accountKey",
                                        selections: [r]
                                    }],
                                    storageKey: null
                                }],
                                storageKey: null
                            }, {
                                args: null,
                                kind: "FragmentSpread",
                                name: "TradeStation_data"
                            }, {
                                args: null,
                                kind: "FragmentSpread",
                                name: "BidModalContent_trade"
                            }],
                            storageKey: null
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [n, e],
                        kind: "Operation",
                        name: "bundleQuery",
                        selections: [{
                            alias: null,
                            args: a,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "bundle",
                            plural: !1,
                            selections: [t, l, i, s, {
                                alias: null,
                                args: null,
                                concreteType: "AccountType",
                                kind: "LinkedField",
                                name: "maker",
                                plural: !1,
                                selections: [r, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "config",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isCompromised",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "UserType",
                                    kind: "LinkedField",
                                    name: "user",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "publicUsername",
                                        storageKey: null
                                    }, h],
                                    storageKey: null
                                }, o, h],
                                storageKey: null
                            }, d, {
                                alias: null,
                                args: c,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [u, l, o, s, t, {
                                                alias: null,
                                                args: null,
                                                concreteType: "CollectionType",
                                                kind: "LinkedField",
                                                name: "collection",
                                                plural: !1,
                                                selections: [m, s, l, i, O, v, x, h, {
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "DisplayDataType",
                                                    kind: "LinkedField",
                                                    name: "displayData",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "cardDisplayStyle",
                                                        storageKey: null
                                                    }],
                                                    storageKey: null
                                                }, {
                                                    alias: null,
                                                    args: k,
                                                    concreteType: "PaymentAssetType",
                                                    kind: "LinkedField",
                                                    name: "paymentAssets",
                                                    plural: !0,
                                                    selections: [t, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [{
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "AssetContractType",
                                                            kind: "LinkedField",
                                                            name: "assetContract",
                                                            plural: !1,
                                                            selections: [r, y, h],
                                                            storageKey: null
                                                        }, g, A, T, t, h, o],
                                                        storageKey: null
                                                    }, h],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetContractType",
                                                kind: "LinkedField",
                                                name: "assetContract",
                                                plural: !1,
                                                selections: [r, y, p, h],
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "animationUrl",
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "backgroundColor",
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "isDelisted",
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "displayImageUrl",
                                                storageKey: null
                                            }, h, g],
                                            storageKey: null
                                        }, b, h],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:30)"
                            }, h],
                            storageKey: null
                        }, {
                            alias: null,
                            args: a,
                            concreteType: "TradeSummaryType",
                            kind: "LinkedField",
                            name: "tradeSummary",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "OrderV2Type",
                                kind: "LinkedField",
                                name: "bestAsk",
                                plural: !1,
                                selections: [f, j, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "maker",
                                    plural: !1,
                                    selections: F,
                                    storageKey: null
                                }, h, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "dutchAuctionFinalPrice",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "openedAt",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "priceFnEndedAt",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "englishAuctionReservePrice",
                                    storageKey: null
                                }, t, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "makerAssetBundle",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: c,
                                        concreteType: "AssetQuantityTypeConnection",
                                        kind: "LinkedField",
                                        name: "assetQuantities",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityTypeEdge",
                                            kind: "LinkedField",
                                            name: "edges",
                                            plural: !0,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityType",
                                                kind: "LinkedField",
                                                name: "node",
                                                plural: !1,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetType",
                                                    kind: "LinkedField",
                                                    name: "asset",
                                                    plural: !1,
                                                    selections: [t, u, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetContractType",
                                                        kind: "LinkedField",
                                                        name: "assetContract",
                                                        plural: !1,
                                                        selections: [y, r, h],
                                                        storageKey: null
                                                    }, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "CollectionType",
                                                        kind: "LinkedField",
                                                        name: "collection",
                                                        plural: !1,
                                                        selections: [i, O, v, x, h, {
                                                            alias: null,
                                                            args: null,
                                                            kind: "ScalarField",
                                                            name: "externalUrl",
                                                            storageKey: null
                                                        }],
                                                        storageKey: null
                                                    }, h, g, {
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "externalLink",
                                                        storageKey: null
                                                    }],
                                                    storageKey: null
                                                }, b, h],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: "assetQuantities(first:30)"
                                    }, h],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "taker",
                                    plural: !1,
                                    selections: F,
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "takerAssetBundle",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: S,
                                        concreteType: "AssetQuantityTypeConnection",
                                        kind: "LinkedField",
                                        name: "assetQuantities",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityTypeEdge",
                                            kind: "LinkedField",
                                            name: "edges",
                                            plural: !0,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityType",
                                                kind: "LinkedField",
                                                name: "node",
                                                plural: !1,
                                                selections: [b, {
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetType",
                                                    kind: "LinkedField",
                                                    name: "asset",
                                                    plural: !1,
                                                    selections: [A, g, t, T, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetContractType",
                                                        kind: "LinkedField",
                                                        name: "assetContract",
                                                        plural: !1,
                                                        selections: [r, h, p, y],
                                                        storageKey: null
                                                    }, u, h, o],
                                                    storageKey: null
                                                }, h],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: "assetQuantities(first:1)"
                                    }, h],
                                    storageKey: null
                                }, {
                                    alias: "makerAsset",
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "makerAssetBundle",
                                    plural: !1,
                                    selections: K,
                                    storageKey: null
                                }, {
                                    alias: "takerAsset",
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "takerAssetBundle",
                                    plural: !1,
                                    selections: K,
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "remainingQuantity",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "side",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isFulfillable",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "oldOrder",
                                    storageKey: null
                                }],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "OrderV2Type",
                                kind: "LinkedField",
                                name: "bestBid",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetBundleType",
                                    kind: "LinkedField",
                                    name: "makerAssetBundle",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: S,
                                        concreteType: "AssetQuantityTypeConnection",
                                        kind: "LinkedField",
                                        name: "assetQuantities",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityTypeEdge",
                                            kind: "LinkedField",
                                            name: "edges",
                                            plural: !0,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityType",
                                                kind: "LinkedField",
                                                name: "node",
                                                plural: !1,
                                                selections: [b, {
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetType",
                                                    kind: "LinkedField",
                                                    name: "asset",
                                                    plural: !1,
                                                    selections: [g, o, A, T, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetContractType",
                                                        kind: "LinkedField",
                                                        name: "assetContract",
                                                        plural: !1,
                                                        selections: [p, y, h],
                                                        storageKey: null
                                                    }, h],
                                                    storageKey: null
                                                }, h],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: "assetQuantities(first:1)"
                                    }, h],
                                    storageKey: null
                                }, h, t],
                                storageKey: null
                            }],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "0cb289fcc1b53b041bdc6f2d65a27338",
                        id: null,
                        metadata: {},
                        name: "bundleQuery",
                        operationKind: "query",
                        text: "query bundleQuery(\n  $slug: BundleSlug!\n  $chain: ChainScalar\n) {\n  bundle(bundle: $slug) {\n    relayId\n    name\n    slug\n    description\n    maker {\n      address\n      ...AccountLink_data\n      id\n    }\n    assetCount\n    assetQuantities(first: 30) {\n      edges {\n        node {\n          asset {\n            ...asset_display_name\n            imageUrl\n            name\n            tokenId\n            description\n            relayId\n            collection {\n              hidden\n              description\n              name\n              ...CollectionLink_collection\n              id\n            }\n            assetContract {\n              address\n              chain\n              blockExplorerLink\n              id\n            }\n            ...AssetMedia_asset\n            ...asset_url\n            id\n          }\n          ...quantity_data\n          id\n        }\n      }\n    }\n    ...BidModalContent_bundle_4iqQ9J\n    ...TradeStation_bundle_4iqQ9J\n    ...bundle_url\n    id\n  }\n  tradeSummary(bundle: $slug) {\n    bestAsk {\n      orderType\n      closedAt\n      maker {\n        ...wallet_accountKey\n        id\n      }\n      id\n    }\n    ...TradeStation_data\n    ...BidModalContent_trade\n  }\n}\n\nfragment AccountLink_data on AccountType {\n  address\n  config\n  isCompromised\n  user {\n    publicUsername\n    id\n  }\n  ...ProfileImage_data\n  ...wallet_accountKey\n  ...accounts_url\n}\n\nfragment AskPrice_data on OrderV2Type {\n  dutchAuctionFinalPrice\n  openedAt\n  priceFnEndedAt\n  makerAssetBundle {\n    assetQuantities(first: 30) {\n      edges {\n        node {\n          ...quantity_data\n          id\n        }\n      }\n    }\n    id\n  }\n  takerAssetBundle {\n    assetQuantities(first: 1) {\n      edges {\n        node {\n          ...AssetQuantity_data\n          id\n        }\n      }\n    }\n    id\n  }\n}\n\nfragment AssetMedia_asset on AssetType {\n  animationUrl\n  backgroundColor\n  collection {\n    displayData {\n      cardDisplayStyle\n    }\n    id\n  }\n  isDelisted\n  imageUrl\n  displayImageUrl\n}\n\nfragment AssetQuantity_data on AssetQuantityType {\n  asset {\n    ...Price_data\n    id\n  }\n  quantity\n}\n\nfragment BidModalContent_bundle_4iqQ9J on AssetBundleType {\n  assetQuantities(first: 30) {\n    edges {\n      node {\n        asset {\n          assetContract {\n            address\n            chain\n            id\n          }\n          decimals\n          relayId\n          collection {\n            slug\n            paymentAssets(chain: $chain) {\n              relayId\n              asset {\n                assetContract {\n                  address\n                  chain\n                  id\n                }\n                decimals\n                symbol\n                usdSpotPrice\n                relayId\n                id\n              }\n              ...PaymentTokenInputV2_data\n              id\n            }\n            id\n          }\n          id\n        }\n        quantity\n        id\n      }\n    }\n  }\n  slug\n}\n\nfragment BidModalContent_trade on TradeSummaryType {\n  bestAsk {\n    closedAt\n    isFulfillable\n    oldOrder\n    orderType\n    relayId\n    makerAssetBundle {\n      assetQuantities(first: 30) {\n        edges {\n          node {\n            asset {\n              collection {\n                ...verification_data\n                id\n              }\n              id\n            }\n            id\n          }\n        }\n      }\n      id\n    }\n    takerAssetBundle {\n      assetQuantities(first: 1) {\n        edges {\n          node {\n            quantity\n            asset {\n              decimals\n              relayId\n              id\n            }\n            id\n          }\n        }\n      }\n      id\n    }\n    id\n  }\n  bestBid {\n    relayId\n    makerAssetBundle {\n      assetQuantities(first: 1) {\n        edges {\n          node {\n            quantity\n            asset {\n              decimals\n              id\n            }\n            ...AssetQuantity_data\n            id\n          }\n        }\n      }\n      id\n    }\n    id\n  }\n}\n\nfragment CollectionLink_collection on CollectionType {\n  name\n  ...collection_url\n  ...verification_data\n}\n\nfragment PaymentAsset_data on PaymentAssetType {\n  asset {\n    assetContract {\n      chain\n      id\n    }\n    imageUrl\n    symbol\n    id\n  }\n}\n\nfragment PaymentTokenInputV2_data on PaymentAssetType {\n  relayId\n  asset {\n    decimals\n    symbol\n    usdSpotPrice\n    id\n  }\n  ...PaymentAsset_data\n}\n\nfragment Price_data on AssetType {\n  decimals\n  imageUrl\n  symbol\n  usdSpotPrice\n  assetContract {\n    blockExplorerLink\n    chain\n    id\n  }\n}\n\nfragment ProfileImage_data on AccountType {\n  imageUrl\n  address\n}\n\nfragment TradeStation_bundle_4iqQ9J on AssetBundleType {\n  ...BidModalContent_bundle_4iqQ9J\n}\n\nfragment TradeStation_data on TradeSummaryType {\n  bestAsk {\n    closedAt\n    dutchAuctionFinalPrice\n    openedAt\n    orderType\n    priceFnEndedAt\n    englishAuctionReservePrice\n    relayId\n    maker {\n      address\n      ...wallet_accountKey\n      id\n    }\n    makerAssetBundle {\n      assetQuantities(first: 30) {\n        edges {\n          node {\n            asset {\n              relayId\n              tokenId\n              assetContract {\n                chain\n                address\n                id\n              }\n              collection {\n                slug\n                ...verification_data\n                id\n              }\n              ...itemEvents_data\n              id\n            }\n            ...quantity_data\n            id\n          }\n        }\n      }\n      id\n    }\n    taker {\n      ...wallet_accountKey\n      id\n    }\n    takerAssetBundle {\n      assetQuantities(first: 1) {\n        edges {\n          node {\n            quantity\n            asset {\n              symbol\n              decimals\n              relayId\n              usdSpotPrice\n              assetContract {\n                address\n                id\n              }\n              tokenId\n              id\n            }\n            ...AssetQuantity_data\n            id\n          }\n        }\n      }\n      id\n    }\n    ...AskPrice_data\n    ...orderLink_data\n    ...quantity_remaining\n    id\n  }\n  bestBid {\n    makerAssetBundle {\n      assetQuantities(first: 1) {\n        edges {\n          node {\n            quantity\n            ...AssetQuantity_data\n            id\n          }\n        }\n      }\n      id\n    }\n    id\n  }\n  ...BidModalContent_trade\n}\n\nfragment accounts_url on AccountType {\n  address\n  user {\n    publicUsername\n    id\n  }\n}\n\nfragment asset_display_name on AssetType {\n  tokenId\n  name\n}\n\nfragment asset_url on AssetType {\n  assetContract {\n    address\n    chain\n    id\n  }\n  tokenId\n}\n\nfragment bundle_url on AssetBundleType {\n  slug\n}\n\nfragment collection_url on CollectionType {\n  slug\n}\n\nfragment itemEvents_data on AssetType {\n  assetContract {\n    address\n    chain\n    id\n  }\n  tokenId\n}\n\nfragment orderLink_data on OrderV2Type {\n  makerAssetBundle {\n    assetQuantities(first: 30) {\n      edges {\n        node {\n          asset {\n            externalLink\n            collection {\n              externalUrl\n              id\n            }\n            id\n          }\n          id\n        }\n      }\n    }\n    id\n  }\n}\n\nfragment quantity_data on AssetQuantityType {\n  asset {\n    decimals\n    id\n  }\n  quantity\n}\n\nfragment quantity_remaining on OrderV2Type {\n  makerAsset: makerAssetBundle {\n    assetQuantities(first: 1) {\n      edges {\n        node {\n          asset {\n            decimals\n            id\n          }\n          quantity\n          id\n        }\n      }\n    }\n    id\n  }\n  takerAsset: takerAssetBundle {\n    assetQuantities(first: 1) {\n      edges {\n        node {\n          asset {\n            decimals\n            id\n          }\n          quantity\n          id\n        }\n      }\n    }\n    id\n  }\n  remainingQuantity\n  side\n}\n\nfragment verification_data on CollectionType {\n  isMintable\n  isSafelisted\n  isVerified\n}\n\nfragment wallet_accountKey on AccountType {\n  address\n}\n"
                    }
                }
            }();
            t.hash = "8beb90d171dd322f939184c4169a1138", n.default = t
        },
        "9qRV": function(e, n, a) {
            "use strict";
            var t, l = a("mXGw"),
                i = a("UutA"),
                s = a("Hgoe"),
                r = a("b7Z7"),
                d = a("gZJk"),
                c = a("ZwbU"),
                o = a("6Ojl"),
                u = a("n0tG"),
                m = a("/G3T"),
                y = a("a7GP"),
                p = a("lxYa"),
                g = a("oYCi");
            n.a = Object(y.b)((function(e) {
                var n, a = e.data,
                    t = e.error,
                    i = e.mode,
                    y = e.isEnglishAuction,
                    k = e.onEnd,
                    f = e.assets,
                    j = e.listingConfirmationDetails,
                    h = e.onError,
                    O = null === a || void 0 === a || null === (n = a.blockchain) || void 0 === n ? void 0 : n.orderCreateActions,
                    v = Object(o.b)().onPrevious;
                if (Object(l.useEffect)((function() {
                        t && h(t)
                    }), [t, h]), !a || !O) return Object(g.jsx)(s.a, {});
                var x = {
                        ask: "Complete your listing",
                        bid: y ? "Place your bid" : "Make your offer"
                    }[i],
                    A = "".concat({
                        ask: "To complete your listing",
                        bid: y ? "To place your bid" : "To make your offer"
                    }[i], ", follow these steps:");
                return Object(g.jsxs)(g.Fragment, {
                    children: [Object(g.jsx)(c.a.Header, {
                        onPrevious: v,
                        children: Object(g.jsx)(c.a.Title, {
                            children: x
                        })
                    }), Object(g.jsxs)(b, {
                        padding: 0,
                        children: [Object(g.jsx)(d.a, {
                            as: "section",
                            children: Object(g.jsx)(m.a, {
                                assets: f,
                                confirmationDetails: j
                            })
                        }), Object(g.jsxs)(r.a, {
                            borderTopColor: "border",
                            borderTopStyle: "solid",
                            borderTopWidth: "1px",
                            padding: "24px",
                            children: [Object(g.jsx)(u.a, {
                                className: "OrderCreateActionModal--text",
                                children: A
                            }), Object(g.jsx)(p.a, {
                                data: O,
                                onEnd: k
                            })]
                        })]
                    })]
                })
            }), void 0 !== t ? t : t = a("wQEM"));
            var b = Object(i.d)(c.a.Body).withConfig({
                displayName: "OrderCreateActionModalreact__StyledBody",
                componentId: "sc-aeqnuv-0"
            })([".OrderCreateActionModal--text{color:", ";}"], (function(e) {
                return e.theme.colors.text.subtle
            }))
        },
        "9zcS": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = {
                kind: "InlineDataFragment",
                name: "utils_PaymentAssetOption",
                hash: "88291269f33380a954fb09e2dd1309be"
            };
            n.default = t
        },
        BDXS: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return i
            }));
            var t = a("uq6L"),
                l = a("BOW+");

            function i(e, n) {
                Object(l.a)(2, arguments);
                var a = Object(t.a)(e),
                    i = Object(t.a)(n),
                    s = a.getTime() - i.getTime();
                return s < 0 ? -1 : s > 0 ? 1 : s
            }
        },
        FrHE: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "archetype"
                    },
                    n = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "bundle"
                    },
                    a = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "chain"
                    },
                    t = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "isBundle"
                    },
                    l = {
                        kind: "Literal",
                        name: "first",
                        value: 20
                    },
                    i = {
                        kind: "Literal",
                        name: "isValid",
                        value: !0
                    },
                    s = {
                        kind: "Literal",
                        name: "maker",
                        value: {}
                    },
                    r = {
                        kind: "Literal",
                        name: "sortAscending",
                        value: !0
                    },
                    d = {
                        kind: "Literal",
                        name: "sortBy",
                        value: "TAKER_ASSETS_USD_PRICE"
                    },
                    c = {
                        kind: "Literal",
                        name: "takerAssetIsPayment",
                        value: !0
                    },
                    o = [l, {
                        kind: "Literal",
                        name: "isExpired",
                        value: !1
                    }, i, s, {
                        kind: "Variable",
                        name: "makerArchetype",
                        variableName: "archetype"
                    }, r, d, c],
                    u = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    m = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "oldOrder",
                        storageKey: null
                    },
                    y = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "englishAuctionReservePrice",
                        storageKey: null
                    },
                    p = [{
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    g = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    b = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    },
                    k = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "displayImageUrl",
                        storageKey: null
                    },
                    f = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "usdSpotPrice",
                        storageKey: null
                    },
                    j = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    },
                    h = [{
                        alias: null,
                        args: null,
                        concreteType: "OrderV2TypeEdge",
                        kind: "LinkedField",
                        name: "edges",
                        plural: !0,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "OrderV2Type",
                            kind: "LinkedField",
                            name: "node",
                            plural: !1,
                            selections: [u, m, y, {
                                alias: null,
                                args: null,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "takerAssetBundle",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: p,
                                    concreteType: "AssetQuantityTypeConnection",
                                    kind: "LinkedField",
                                    name: "assetQuantities",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityTypeEdge",
                                        kind: "LinkedField",
                                        name: "edges",
                                        plural: !0,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityType",
                                            kind: "LinkedField",
                                            name: "node",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetType",
                                                kind: "LinkedField",
                                                name: "asset",
                                                plural: !1,
                                                selections: [g, b, u, {
                                                    kind: "InlineDataFragmentSpread",
                                                    name: "utils_PaymentAssetOption",
                                                    selections: [u, b, k, f]
                                                }],
                                                storageKey: null
                                            }, j],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: "assetQuantities(first:1)"
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    O = [{
                        kind: "Variable",
                        name: "archetype",
                        variableName: "archetype"
                    }],
                    v = {
                        alias: null,
                        args: null,
                        concreteType: "AbleToType",
                        kind: "LinkedField",
                        name: "isEditable",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "value",
                            storageKey: null
                        }],
                        storageKey: null
                    },
                    x = {
                        kind: "Literal",
                        name: "identity",
                        value: {}
                    },
                    A = {
                        alias: null,
                        args: [x],
                        kind: "ScalarField",
                        name: "ownedQuantity",
                        storageKey: "ownedQuantity(identity:{})"
                    },
                    T = {
                        kind: "Variable",
                        name: "chain",
                        variableName: "chain"
                    },
                    F = [l, i, s, {
                        kind: "Variable",
                        name: "makerAssetBundle",
                        variableName: "bundle"
                    }, r, d, c],
                    S = [{
                        kind: "Variable",
                        name: "bundle",
                        variableName: "bundle"
                    }],
                    K = [T],
                    C = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    },
                    L = [{
                        alias: null,
                        args: null,
                        concreteType: "OrderV2TypeEdge",
                        kind: "LinkedField",
                        name: "edges",
                        plural: !0,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "OrderV2Type",
                            kind: "LinkedField",
                            name: "node",
                            plural: !1,
                            selections: [u, m, y, {
                                alias: null,
                                args: null,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "takerAssetBundle",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: p,
                                    concreteType: "AssetQuantityTypeConnection",
                                    kind: "LinkedField",
                                    name: "assetQuantities",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityTypeEdge",
                                        kind: "LinkedField",
                                        name: "edges",
                                        plural: !0,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityType",
                                            kind: "LinkedField",
                                            name: "node",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetType",
                                                kind: "LinkedField",
                                                name: "asset",
                                                plural: !1,
                                                selections: [g, b, u, k, f, C],
                                                storageKey: null
                                            }, j, C],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: "assetQuantities(first:1)"
                                }, C],
                                storageKey: null
                            }, C],
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    B = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "chain",
                        storageKey: null
                    },
                    w = {
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "address",
                            storageKey: null
                        }, B, C],
                        storageKey: null
                    },
                    I = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    },
                    P = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imageUrl",
                        storageKey: null
                    },
                    _ = {
                        alias: null,
                        args: K,
                        concreteType: "PaymentAssetType",
                        kind: "LinkedField",
                        name: "paymentAssets",
                        plural: !0,
                        selections: [u, {
                            alias: null,
                            args: null,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "asset",
                            plural: !1,
                            selections: [w, g, b, f, u, C, P],
                            storageKey: null
                        }, C],
                        storageKey: null
                    },
                    Q = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isMintable",
                        storageKey: null
                    },
                    D = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isSafelisted",
                        storageKey: null
                    },
                    M = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isVerified",
                        storageKey: null
                    },
                    E = [{
                        kind: "Literal",
                        name: "first",
                        value: 30
                    }];
                return {
                    fragment: {
                        argumentDefinitions: [e, n, a, t],
                        kind: "Fragment",
                        metadata: null,
                        name: "OrderManagerQuery",
                        selections: [{
                            condition: "isBundle",
                            kind: "Condition",
                            passingValue: !1,
                            selections: [{
                                alias: null,
                                args: o,
                                concreteType: "OrderV2TypeConnection",
                                kind: "LinkedField",
                                name: "orders",
                                plural: !1,
                                selections: h,
                                storageKey: null
                            }, {
                                alias: null,
                                args: O,
                                concreteType: "ArchetypeType",
                                kind: "LinkedField",
                                name: "archetype",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetType",
                                    kind: "LinkedField",
                                    name: "asset",
                                    plural: !1,
                                    selections: [v],
                                    storageKey: null
                                }, A, j, {
                                    args: [T, x],
                                    kind: "FragmentSpread",
                                    name: "BidModalContent_archetype"
                                }],
                                storageKey: null
                            }, {
                                alias: null,
                                args: O,
                                concreteType: "TradeSummaryType",
                                kind: "LinkedField",
                                name: "tradeSummary",
                                plural: !1,
                                selections: [{
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "BidModalContent_trade"
                                }],
                                storageKey: null
                            }]
                        }, {
                            condition: "isBundle",
                            kind: "Condition",
                            passingValue: !0,
                            selections: [{
                                alias: null,
                                args: F,
                                concreteType: "OrderV2TypeConnection",
                                kind: "LinkedField",
                                name: "orders",
                                plural: !1,
                                selections: h,
                                storageKey: null
                            }, {
                                alias: null,
                                args: S,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "bundle",
                                plural: !1,
                                selections: [{
                                    args: K,
                                    kind: "FragmentSpread",
                                    name: "BidModalContent_bundle"
                                }],
                                storageKey: null
                            }]
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [e, n, t, a],
                        kind: "Operation",
                        name: "OrderManagerQuery",
                        selections: [{
                            condition: "isBundle",
                            kind: "Condition",
                            passingValue: !1,
                            selections: [{
                                alias: null,
                                args: o,
                                concreteType: "OrderV2TypeConnection",
                                kind: "LinkedField",
                                name: "orders",
                                plural: !1,
                                selections: L,
                                storageKey: null
                            }, {
                                alias: null,
                                args: O,
                                concreteType: "ArchetypeType",
                                kind: "LinkedField",
                                name: "archetype",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetType",
                                    kind: "LinkedField",
                                    name: "asset",
                                    plural: !1,
                                    selections: [v, C, w, g, u, {
                                        alias: null,
                                        args: null,
                                        concreteType: "CollectionType",
                                        kind: "LinkedField",
                                        name: "collection",
                                        plural: !1,
                                        selections: [I, _, Q, D, M, C],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }, A, j],
                                storageKey: null
                            }, {
                                alias: null,
                                args: O,
                                concreteType: "TradeSummaryType",
                                kind: "LinkedField",
                                name: "tradeSummary",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "OrderV2Type",
                                    kind: "LinkedField",
                                    name: "bestAsk",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "closedAt",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isFulfillable",
                                        storageKey: null
                                    }, m, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "orderType",
                                        storageKey: null
                                    }, u, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: E,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [{
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "CollectionType",
                                                            kind: "LinkedField",
                                                            name: "collection",
                                                            plural: !1,
                                                            selections: [Q, D, M, C],
                                                            storageKey: null
                                                        }, C],
                                                        storageKey: null
                                                    }, C],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:30)"
                                        }, C],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "takerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: p,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [j, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [g, u, C],
                                                        storageKey: null
                                                    }, C],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:1)"
                                        }, C],
                                        storageKey: null
                                    }, C],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "OrderV2Type",
                                    kind: "LinkedField",
                                    name: "bestBid",
                                    plural: !1,
                                    selections: [u, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: p,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [j, {
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [g, C, P, b, f, {
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "AssetContractType",
                                                            kind: "LinkedField",
                                                            name: "assetContract",
                                                            plural: !1,
                                                            selections: [{
                                                                alias: null,
                                                                args: null,
                                                                kind: "ScalarField",
                                                                name: "blockExplorerLink",
                                                                storageKey: null
                                                            }, B, C],
                                                            storageKey: null
                                                        }],
                                                        storageKey: null
                                                    }, C],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:1)"
                                        }, C],
                                        storageKey: null
                                    }, C],
                                    storageKey: null
                                }],
                                storageKey: null
                            }]
                        }, {
                            condition: "isBundle",
                            kind: "Condition",
                            passingValue: !0,
                            selections: [{
                                alias: null,
                                args: F,
                                concreteType: "OrderV2TypeConnection",
                                kind: "LinkedField",
                                name: "orders",
                                plural: !1,
                                selections: L,
                                storageKey: null
                            }, {
                                alias: null,
                                args: S,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "bundle",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: E,
                                    concreteType: "AssetQuantityTypeConnection",
                                    kind: "LinkedField",
                                    name: "assetQuantities",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityTypeEdge",
                                        kind: "LinkedField",
                                        name: "edges",
                                        plural: !0,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityType",
                                            kind: "LinkedField",
                                            name: "node",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetType",
                                                kind: "LinkedField",
                                                name: "asset",
                                                plural: !1,
                                                selections: [w, g, u, {
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "CollectionType",
                                                    kind: "LinkedField",
                                                    name: "collection",
                                                    plural: !1,
                                                    selections: [I, _, C],
                                                    storageKey: null
                                                }, C],
                                                storageKey: null
                                            }, j, C],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: "assetQuantities(first:30)"
                                }, I, C],
                                storageKey: null
                            }]
                        }]
                    },
                    params: {
                        cacheID: "8e9e192d0d52318bd7f22b89b900de0d",
                        id: null,
                        metadata: {},
                        name: "OrderManagerQuery",
                        operationKind: "query",
                        text: "query OrderManagerQuery(\n  $archetype: ArchetypeInputType!\n  $bundle: BundleSlug\n  $isBundle: Boolean!\n  $chain: ChainScalar\n) {\n  orders(first: 20, isValid: true, isExpired: false, maker: {}, makerArchetype: $archetype, sortAscending: true, sortBy: TAKER_ASSETS_USD_PRICE, takerAssetIsPayment: true) @skip(if: $isBundle) {\n    edges {\n      node {\n        relayId\n        oldOrder\n        englishAuctionReservePrice\n        takerAssetBundle {\n          assetQuantities(first: 1) {\n            edges {\n              node {\n                asset {\n                  decimals\n                  symbol\n                  relayId\n                  ...utils_PaymentAssetOption\n                  id\n                }\n                quantity\n                id\n              }\n            }\n          }\n          id\n        }\n        id\n      }\n    }\n  }\n  archetype(archetype: $archetype) @skip(if: $isBundle) {\n    asset {\n      isEditable {\n        value\n      }\n      id\n    }\n    ownedQuantity(identity: {})\n    quantity\n    ...BidModalContent_archetype_3wquQ2\n  }\n  tradeSummary(archetype: $archetype) @skip(if: $isBundle) {\n    ...BidModalContent_trade\n  }\n  orders(first: 20, isValid: true, maker: {}, makerAssetBundle: $bundle, sortAscending: true, sortBy: TAKER_ASSETS_USD_PRICE, takerAssetIsPayment: true) @include(if: $isBundle) {\n    edges {\n      node {\n        relayId\n        oldOrder\n        englishAuctionReservePrice\n        takerAssetBundle {\n          assetQuantities(first: 1) {\n            edges {\n              node {\n                asset {\n                  decimals\n                  symbol\n                  relayId\n                  ...utils_PaymentAssetOption\n                  id\n                }\n                quantity\n                id\n              }\n            }\n          }\n          id\n        }\n        id\n      }\n    }\n  }\n  bundle(bundle: $bundle) @include(if: $isBundle) {\n    ...BidModalContent_bundle_4iqQ9J\n    id\n  }\n}\n\nfragment AssetQuantity_data on AssetQuantityType {\n  asset {\n    ...Price_data\n    id\n  }\n  quantity\n}\n\nfragment BidModalContent_archetype_3wquQ2 on ArchetypeType {\n  asset {\n    assetContract {\n      address\n      chain\n      id\n    }\n    decimals\n    relayId\n    collection {\n      slug\n      paymentAssets(chain: $chain) {\n        relayId\n        asset {\n          assetContract {\n            address\n            chain\n            id\n          }\n          decimals\n          symbol\n          usdSpotPrice\n          relayId\n          id\n        }\n        ...PaymentTokenInputV2_data\n        id\n      }\n      ...verification_data\n      id\n    }\n    id\n  }\n  quantity\n  ownedQuantity(identity: {})\n}\n\nfragment BidModalContent_bundle_4iqQ9J on AssetBundleType {\n  assetQuantities(first: 30) {\n    edges {\n      node {\n        asset {\n          assetContract {\n            address\n            chain\n            id\n          }\n          decimals\n          relayId\n          collection {\n            slug\n            paymentAssets(chain: $chain) {\n              relayId\n              asset {\n                assetContract {\n                  address\n                  chain\n                  id\n                }\n                decimals\n                symbol\n                usdSpotPrice\n                relayId\n                id\n              }\n              ...PaymentTokenInputV2_data\n              id\n            }\n            id\n          }\n          id\n        }\n        quantity\n        id\n      }\n    }\n  }\n  slug\n}\n\nfragment BidModalContent_trade on TradeSummaryType {\n  bestAsk {\n    closedAt\n    isFulfillable\n    oldOrder\n    orderType\n    relayId\n    makerAssetBundle {\n      assetQuantities(first: 30) {\n        edges {\n          node {\n            asset {\n              collection {\n                ...verification_data\n                id\n              }\n              id\n            }\n            id\n          }\n        }\n      }\n      id\n    }\n    takerAssetBundle {\n      assetQuantities(first: 1) {\n        edges {\n          node {\n            quantity\n            asset {\n              decimals\n              relayId\n              id\n            }\n            id\n          }\n        }\n      }\n      id\n    }\n    id\n  }\n  bestBid {\n    relayId\n    makerAssetBundle {\n      assetQuantities(first: 1) {\n        edges {\n          node {\n            quantity\n            asset {\n              decimals\n              id\n            }\n            ...AssetQuantity_data\n            id\n          }\n        }\n      }\n      id\n    }\n    id\n  }\n}\n\nfragment PaymentAsset_data on PaymentAssetType {\n  asset {\n    assetContract {\n      chain\n      id\n    }\n    imageUrl\n    symbol\n    id\n  }\n}\n\nfragment PaymentTokenInputV2_data on PaymentAssetType {\n  relayId\n  asset {\n    decimals\n    symbol\n    usdSpotPrice\n    id\n  }\n  ...PaymentAsset_data\n}\n\nfragment Price_data on AssetType {\n  decimals\n  imageUrl\n  symbol\n  usdSpotPrice\n  assetContract {\n    blockExplorerLink\n    chain\n    id\n  }\n}\n\nfragment utils_PaymentAssetOption on AssetType {\n  relayId\n  symbol\n  displayImageUrl\n  usdSpotPrice\n}\n\nfragment verification_data on CollectionType {\n  isMintable\n  isSafelisted\n  isVerified\n}\n"
                    }
                }
            }();
            t.hash = "b90abcf650c8cae6279cc0a4045dc9d6", n.default = t
        },
        Gkym: function(e, n, a) {
            "use strict";
            var t = a("uEoR"),
                l = a("mXGw"),
                i = a("vvX8"),
                s = a.n(i),
                r = a("/6Ao"),
                d = a("jQgF"),
                c = a("ocrj"),
                o = a("j/Wi"),
                u = a("pcOi"),
                m = a("Nbt0"),
                y = a("HSVd"),
                p = a("qd51"),
                g = a("/dBk"),
                b = a.n(g);

            function k() {
                return (k = Object(p.a)(b.a.mark((function e() {
                    return b.a.wrap((function(e) {
                        for (;;) switch (e.prev = e.next) {
                            case 0:
                                return window.twttr = function(e, n, a) {
                                    var t, l = e.getElementsByTagName(n)[0],
                                        i = window.twttr || {};
                                    return e.getElementById(a) || ((t = e.createElement(n)).id = a, t.src = "https://platform.twitter.com/widgets.js", l.parentNode.insertBefore(t, l), i._e = [], i.ready = function(e) {
                                        i._e.push(e)
                                    }), i
                                }(document, "script", "twitter-wjs"), e.abrupt("return", new Promise((function(e) {
                                    window.twttr.ready((function(n) {
                                        e(n)
                                    }))
                                })));
                            case 2:
                            case "end":
                                return e.stop()
                        }
                    }), e)
                })))).apply(this, arguments)
            }
            var f = a("C/iq"),
                j = a("oYCi");
            n.a = function(e) {
                var n = e.children,
                    a = e.object,
                    i = e.url,
                    p = e.onEmbed,
                    g = Object(u.a)().origin,
                    b = Object(m.a)().showSuccessMessage,
                    h = Object(r.a)(),
                    O = Object(t.a)(h, 2),
                    v = (O[0], O[1]);
                Object(l.useEffect)((function() {
                    ! function() {
                        k.apply(this, arguments)
                    }()
                }), []);
                var x = function() {
                    return i ? "".concat(g).concat(i) : "".concat(g).concat(y.a.getPathWithMergedQuery())
                };
                return Object(j.jsx)(c.a, {
                    appendTo: d.e ? void 0 : document.body,
                    content: function(e) {
                        var n = e.close,
                            t = e.List,
                            l = e.Item;
                        return Object(j.jsxs)(t, {
                            children: [Object(j.jsxs)(l, {
                                onClick: function() {
                                    v(x()), n(), b("Link copied!")
                                },
                                children: [Object(j.jsx)(l.Avatar, {
                                    src: f.Bb
                                }), Object(j.jsx)(l.Content, {
                                    children: Object(j.jsx)(l.Title, {
                                        children: "Copy Link"
                                    })
                                })]
                            }), Object(j.jsxs)(l, {
                                href: "https://www.facebook.com/sharer/sharer.php?u=".concat(x()),
                                onClick: n,
                                children: [Object(j.jsx)(l.Avatar, {
                                    src: "/static/images/logos/facebook.png"
                                }), Object(j.jsx)(l.Content, {
                                    children: Object(j.jsx)(l.Title, {
                                        children: "Share on Facebook"
                                    })
                                })]
                            }), Object(j.jsxs)(l, {
                                href: "https://twitter.com/intent/tweet?".concat(s.a.stringify({
                                    text: "Check out this ".concat(a, " on OpenSea"),
                                    url: x(),
                                    via: f.Db
                                })),
                                children: [Object(j.jsx)(l.Avatar, {
                                    src: "/static/images/logos/twitter.svg"
                                }), Object(j.jsx)(l.Content, {
                                    children: Object(j.jsx)(l.Title, {
                                        children: "Share to Twitter"
                                    })
                                })]
                            }), p ? Object(j.jsxs)(l, {
                                onClick: function() {
                                    p(), n()
                                },
                                children: [Object(j.jsx)(l.Avatar, {
                                    icon: "code"
                                }), Object(j.jsx)(l.Content, {
                                    children: Object(j.jsx)(l.Title, {
                                        children: "Embed Asset"
                                    })
                                })]
                            }) : null]
                        })
                    },
                    placement: "bottom-end",
                    children: Object(j.jsx)(o.b, {
                        content: "Share",
                        children: n
                    })
                })
            }
        },
        HsWp: function(e, n) {},
        IUmd: function(e, n, a) {
            "use strict";
            var t = a("Q9cQ");
            a.d(n, "a", (function() {
                return t.a
            }))
        },
        KsMw: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return r
            }));
            var t = a("uq6L"),
                l = a("QEM4"),
                i = a("BOW+");

            function s(e, n) {
                var a = e.getFullYear() - n.getFullYear() || e.getMonth() - n.getMonth() || e.getDate() - n.getDate() || e.getHours() - n.getHours() || e.getMinutes() - n.getMinutes() || e.getSeconds() - n.getSeconds() || e.getMilliseconds() - n.getMilliseconds();
                return a < 0 ? -1 : a > 0 ? 1 : a
            }

            function r(e, n) {
                Object(i.a)(2, arguments);
                var a = Object(t.a)(e),
                    r = Object(t.a)(n),
                    d = s(a, r),
                    c = Math.abs(Object(l.a)(a, r));
                a.setDate(a.getDate() - d * c);
                var o = Number(s(a, r) === -d),
                    u = d * (c - o);
                return 0 === u ? 0 : u
            }
        },
        LDtq: function(e, n, a) {
            "use strict";
            var t = a("P1kF");
            a.d(n, "MoonPayCheckoutModal", (function() {
                return t.a
            }));
            var l = a("IUmd");
            a.d(n, "MoonPayTopupModal", (function() {
                return l.a
            }));
            a("HsWp")
        },
        OYLJ: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return o
            }));
            var t = a("uq6L"),
                l = a("BOW+");

            function i(e, n) {
                Object(l.a)(2, arguments);
                var a = Object(t.a)(e),
                    i = Object(t.a)(n),
                    s = a.getFullYear() - i.getFullYear(),
                    r = a.getMonth() - i.getMonth();
                return 12 * s + r
            }
            var s = a("BDXS");

            function r(e) {
                Object(l.a)(1, arguments);
                var n = Object(t.a)(e);
                return n.setHours(23, 59, 59, 999), n
            }

            function d(e) {
                Object(l.a)(1, arguments);
                var n = Object(t.a)(e),
                    a = n.getMonth();
                return n.setFullYear(n.getFullYear(), a + 1, 0), n.setHours(23, 59, 59, 999), n
            }

            function c(e) {
                Object(l.a)(1, arguments);
                var n = Object(t.a)(e);
                return r(n).getTime() === d(n).getTime()
            }

            function o(e, n) {
                Object(l.a)(2, arguments);
                var a, r = Object(t.a)(e),
                    d = Object(t.a)(n),
                    o = Object(s.a)(r, d),
                    u = Math.abs(i(r, d));
                if (u < 1) a = 0;
                else {
                    1 === r.getMonth() && r.getDate() > 27 && r.setDate(30), r.setMonth(r.getMonth() - o * u);
                    var m = Object(s.a)(r, d) === -o;
                    c(Object(t.a)(e)) && 1 === u && 1 === Object(s.a)(e, d) && (m = !1), a = o * (u - Number(m))
                }
                return 0 === a ? 0 : a
            }
        },
        P1kF: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return F
            }));
            var t, l = a("mXGw"),
                i = a("aXrf"),
                s = a("JHWp"),
                r = a("pkFK"),
                d = a("XaKp"),
                c = a("WakB"),
                o = a("b7Z7"),
                u = a("67yl"),
                m = a("QrBS"),
                y = a("ZwbU"),
                p = a("6Ojl"),
                g = a("n0tG"),
                b = a("C/iq"),
                k = a("uEoR"),
                f = a("qd51"),
                j = a("/dBk"),
                h = a.n(j),
                O = a("d8b/"),
                v = a("Oe7D"),
                x = a("2p++"),
                A = a("oYCi"),
                T = function(e) {
                    var n = e.order,
                        d = e.assetIDs,
                        j = Object(p.b)(),
                        T = j.onPrevious,
                        F = j.onReplace,
                        S = Object(i.useLazyLoadQuery)(void 0 !== t ? t : t = a("+crw"), {
                            order: n
                        }, {
                            fetchPolicy: "network-only"
                        });
                    if (!S.moonpay.moonpayFiatCheckoutWidgetData) throw new Error("MoonPay NFT checkout widget data is not defined");
                    var K = S.moonpay.moonpayFiatCheckoutWidgetData,
                        C = K.url,
                        L = function(e) {
                            var n = e.externalTransactionId,
                                a = Object(l.useState)(),
                                t = a[0],
                                i = a[1],
                                s = function() {
                                    var e = Object(f.a)(h.a.mark((function e() {
                                        var a, l, s, r;
                                        return h.a.wrap((function(e) {
                                            for (;;) switch (e.prev = e.next) {
                                                case 0:
                                                    return e.next = 2, fetch(Object(x.b)(n, {
                                                        useNftApiKey: !0
                                                    }));
                                                case 2:
                                                    if ((a = e.sent).ok) {
                                                        e.next = 5;
                                                        break
                                                    }
                                                    return e.abrupt("return");
                                                case 5:
                                                    return e.next = 7, a.json();
                                                case 7:
                                                    (l = e.sent).length > 1 && Object(v.d)(new Error("MoonPay NFT API returned more than one transaction with external id ".concat(n))), s = Object(k.a)(l, 1), (r = s[0].nftTransaction) && r.updatedAt !== (null === t || void 0 === t ? void 0 : t.updatedAt) && i(r);
                                                case 11:
                                                case "end":
                                                    return e.stop()
                                            }
                                        }), e)
                                    })));
                                    return function() {
                                        return e.apply(this, arguments)
                                    }
                                }();
                            return Object(O.a)(s, 3e3), {
                                nftTransaction: t
                            }
                        }({
                            externalTransactionId: K.externalTransactionId
                        }).nftTransaction;
                    return Object(s.a)((function() {
                        null !== L && void 0 !== L && L.transactionHash && F(Object(A.jsx)(c.a, {
                            mode: "bought",
                            transaction: {
                                transactionHash: L.transactionHash
                            },
                            variables: {
                                assetIDs: d
                            }
                        }))
                    }), [L]), Object(A.jsxs)(A.Fragment, {
                        children: [Object(A.jsx)(y.a.Header, {
                            onPrevious: T,
                            children: Object(A.jsx)(y.a.Title, {
                                children: "Checkout with MoonPay"
                            })
                        }), Object(A.jsxs)(y.a.Body, {
                            children: [Object(A.jsx)(o.a, {
                                height: "600px",
                                children: Object(A.jsx)(r.a, {
                                    allow: "accelerometer; autoplay; camera; gyroscope; payment",
                                    url: C
                                })
                            }), Object(A.jsx)(u.a, {
                                marginTop: "24px",
                                children: Object(A.jsxs)(m.a, {
                                    alignItems: "center",
                                    children: [Object(A.jsx)(g.a, {
                                        display: "inline",
                                        variant: "info-bold",
                                        children: "Powered by "
                                    }), Object(A.jsx)(o.a, {
                                        marginLeft: "8px",
                                        children: Object(A.jsx)("img", {
                                            alt: "MoonPay logo",
                                            src: b.qb
                                        })
                                    })]
                                })
                            })]
                        })]
                    })
                },
                F = function(e) {
                    var n = e.order,
                        a = e.assetIDs;
                    return Object(A.jsx)(l.Suspense, {
                        fallback: Object(A.jsx)(d.a, {}),
                        children: Object(A.jsx)(T, {
                            assetIDs: a,
                            order: n
                        })
                    })
                }
        },
        StzY: function(e, n, a) {
            "use strict";
            a.d(n, "b", (function() {
                return s
            })), a.d(n, "d", (function() {
                return r
            })), a.d(n, "a", (function() {
                return d
            })), a.d(n, "c", (function() {
                return c
            }));
            var t = a("Wore"),
                l = a("LsOE"),
                i = function(e) {
                    var n = e.relayId,
                        a = e.model;
                    return "".concat(t.a.getUrl(), "/admin/api/").concat(a, "/").concat(Object(l.g)(n), "/change/")
                },
                s = function(e) {
                    return i({
                        relayId: e,
                        model: "asset"
                    })
                },
                r = function(e) {
                    return i({
                        relayId: e,
                        model: "collection"
                    })
                },
                d = function(e) {
                    return i({
                        relayId: e,
                        model: "account"
                    })
                },
                c = function(e) {
                    return i({
                        relayId: e,
                        model: "assetbundle"
                    })
                }
        },
        UPyn: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: null,
                name: "PriceTag_paymentAsset",
                selections: [{
                    alias: null,
                    args: null,
                    concreteType: "AssetContractType",
                    kind: "LinkedField",
                    name: "assetContract",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "chain",
                        storageKey: null
                    }],
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "symbol",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "imageUrl",
                    storageKey: null
                }],
                type: "AssetType",
                abstractKey: null,
                hash: "03aedbb40b14597054c9490854823d34"
            };
            n.default = t
        },
        UTR7: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return u
            })), a.d(n, "b", (function() {
                return y
            }));
            a("mXGw");
            var t, l = a("vkv6"),
                i = a("QrBS"),
                s = a("D5UM"),
                r = a("y7Mw"),
                d = a("06eW"),
                c = a("LjoF"),
                o = a("oYCi"),
                u = function(e) {
                    var n, a = e.autoFocus,
                        t = e.paymentAssetOptions,
                        u = e.error,
                        m = e.warning,
                        y = e.paymentAssetRelayId,
                        p = e.label,
                        g = e.placeholder,
                        b = void 0 === g ? "Amount" : g,
                        k = e.price,
                        f = e.quantity,
                        j = e.onChange,
                        h = e.onChangePaymentAsset,
                        O = e.name,
                        v = e.id,
                        x = t.find((function(e) {
                            return e.value === y
                        })),
                        A = null === x || void 0 === x ? void 0 : x.usdSpotPrice,
                        T = A && k && !Object(c.d)(k).isNaN() ? Object(c.d)(k).mul(A) : void 0,
                        F = f ? null === T || void 0 === T ? void 0 : T.times(f) : void 0;
                    return Object(o.jsx)(s.a, {
                        captionRight: T && F ? f ? f > 1 ? "".concat(Object(c.g)(F), " Total (").concat(Object(c.g)(T), " each)") : "".concat(Object(c.g)(F), " Total") : void 0 : T ? Object(c.g)(T) : void 0,
                        error: null === u || void 0 === u ? void 0 : u.message,
                        label: p,
                        warning: m,
                        children: Object(o.jsxs)(i.a, {
                            gridColumnGap: "8px",
                            children: [Object(o.jsx)(d.a, {
                                clearable: !1,
                                disabled: t.length <= 1,
                                excludeSelectedOption: !0,
                                options: t,
                                readOnly: !0,
                                startEnhancer: x ? Object(o.jsx)(l.b, {
                                    size: 24,
                                    src: null !== (n = x.displayImageUrl) && void 0 !== n ? n : ""
                                }) : null,
                                style: {
                                    width: "250px"
                                },
                                value: y,
                                onSelect: function(e) {
                                    return (null === e || void 0 === e ? void 0 : e.value) && (null === h || void 0 === h ? void 0 : h(e.value))
                                }
                            }), Object(o.jsx)(r.a, {
                                autoFocus: a,
                                id: v,
                                name: O,
                                placeholder: b,
                                value: k,
                                onChange: function(e) {
                                    return j(e.target.value)
                                }
                            })]
                        })
                    })
                },
                m = a("qPWj"),
                y = function(e) {
                    return e.map((function(e) {
                        var n = e.relayId,
                            a = e.asset;
                        return {
                            relayId: n,
                            asset: p(a)
                        }
                    })).filter((function(e) {
                        return e.asset.symbol
                    })).map((function(e) {
                        var n = e.relayId,
                            a = e.asset,
                            t = a.symbol,
                            l = a.displayImageUrl;
                        return {
                            label: null !== t && void 0 !== t ? t : "",
                            value: n,
                            avatar: {
                                src: null !== l && void 0 !== l ? l : ""
                            },
                            usdSpotPrice: a.usdSpotPrice,
                            displayImageUrl: l
                        }
                    }))
                },
                p = Object(m.a)(void 0 !== t ? t : t = a("9zcS"), (function(e) {
                    return e
                }))
        },
        VLAj: function(e, n, a) {
            "use strict";
            a("mXGw");
            var t, l = a("aXrf"),
                i = a("UutA"),
                s = a("QrBS"),
                r = a("9E9p"),
                d = a("C/iq"),
                c = a("D4YM"),
                o = a("oYCi"),
                u = Object(i.d)(s.a).withConfig({
                    displayName: "PaymentAssetreact__AvatarContainer",
                    componentId: "sc-12mizad-0"
                })(["", ""], (function(e) {
                    return e.lighten && Object(c.b)({
                        variants: {
                            dark: {
                                filter: "brightness(3)"
                            }
                        }
                    })
                }));
            n.a = function(e) {
                var n = e.className,
                    i = e.data,
                    c = e.showChain,
                    m = Object(l.useFragment)(void 0 !== t ? t : t = a("p+t5"), i).asset,
                    y = m.imageUrl,
                    p = m.symbol,
                    g = m.assetContract.chain;
                return Object(o.jsxs)(s.a, {
                    className: n,
                    children: [y ? Object(o.jsx)(u, {
                        alignItems: "center",
                        lighten: "ETH" === p && "ETHEREUM" === g,
                        children: Object(o.jsx)(r.a.Avatar, {
                            alt: "",
                            src: y
                        })
                    }) : null, Object(o.jsxs)(r.a.Content, {
                        children: [Object(o.jsx)(r.a.Title, {
                            children: p
                        }), c ? Object(o.jsx)(r.a.Description, {
                            children: d.o[g]
                        }) : null]
                    })]
                })
            }
        },
        W3fG: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        defaultValue: 10,
                        kind: "LocalArgument",
                        name: "count"
                    },
                    n = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "cursor"
                    },
                    a = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "excludeMaker"
                    },
                    t = {
                        defaultValue: !1,
                        kind: "LocalArgument",
                        name: "expandedMode"
                    },
                    l = {
                        defaultValue: !1,
                        kind: "LocalArgument",
                        name: "filterByOrderRules"
                    },
                    i = {
                        defaultValue: !1,
                        kind: "LocalArgument",
                        name: "isBid"
                    },
                    s = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "isExpired"
                    },
                    r = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "isValid"
                    },
                    d = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "maker"
                    },
                    c = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerArchetype"
                    },
                    o = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerAssetBundle"
                    },
                    u = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerAssetIsPayment"
                    },
                    m = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "sortAscending"
                    },
                    y = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "sortBy"
                    },
                    p = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerArchetype"
                    },
                    g = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetBundle"
                    },
                    b = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetCategories"
                    },
                    k = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetCollections"
                    },
                    f = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetIsOwnedBy"
                    },
                    j = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetIsPayment"
                    },
                    h = {
                        kind: "Variable",
                        name: "excludeMaker",
                        variableName: "excludeMaker"
                    },
                    O = {
                        kind: "Variable",
                        name: "filterByOrderRules",
                        variableName: "filterByOrderRules"
                    },
                    v = {
                        kind: "Variable",
                        name: "isExpired",
                        variableName: "isExpired"
                    },
                    x = {
                        kind: "Variable",
                        name: "isValid",
                        variableName: "isValid"
                    },
                    A = {
                        kind: "Variable",
                        name: "maker",
                        variableName: "maker"
                    },
                    T = {
                        kind: "Variable",
                        name: "makerArchetype",
                        variableName: "makerArchetype"
                    },
                    F = {
                        kind: "Variable",
                        name: "makerAssetBundle",
                        variableName: "makerAssetBundle"
                    },
                    S = {
                        kind: "Variable",
                        name: "makerAssetIsPayment",
                        variableName: "makerAssetIsPayment"
                    },
                    K = {
                        kind: "Variable",
                        name: "sortAscending",
                        variableName: "sortAscending"
                    },
                    C = {
                        kind: "Variable",
                        name: "sortBy",
                        variableName: "sortBy"
                    },
                    L = {
                        kind: "Variable",
                        name: "takerArchetype",
                        variableName: "takerArchetype"
                    },
                    B = {
                        kind: "Variable",
                        name: "takerAssetBundle",
                        variableName: "takerAssetBundle"
                    },
                    w = {
                        kind: "Variable",
                        name: "takerAssetCategories",
                        variableName: "takerAssetCategories"
                    },
                    I = {
                        kind: "Variable",
                        name: "takerAssetCollections",
                        variableName: "takerAssetCollections"
                    },
                    P = {
                        kind: "Variable",
                        name: "takerAssetIsOwnedBy",
                        variableName: "takerAssetIsOwnedBy"
                    },
                    _ = {
                        kind: "Variable",
                        name: "takerAssetIsPayment",
                        variableName: "takerAssetIsPayment"
                    },
                    Q = [{
                        kind: "Variable",
                        name: "after",
                        variableName: "cursor"
                    }, h, O, {
                        kind: "Variable",
                        name: "first",
                        variableName: "count"
                    }, v, x, A, T, F, S, K, C, L, B, w, I, P, _],
                    D = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "address",
                        storageKey: null
                    },
                    M = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    },
                    E = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imageUrl",
                        storageKey: null
                    },
                    N = [D, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "config",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isCompromised",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        concreteType: "UserType",
                        kind: "LinkedField",
                        name: "user",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "publicUsername",
                            storageKey: null
                        }, M],
                        storageKey: null
                    }, E, M],
                    V = [{
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    q = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "chain",
                        storageKey: null
                    },
                    R = {
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [D, q, M],
                        storageKey: null
                    },
                    U = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    H = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    },
                    $ = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    },
                    z = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "usdSpotPrice",
                        storageKey: null
                    },
                    W = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "blockExplorerLink",
                        storageKey: null
                    },
                    Y = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    G = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    },
                    X = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "tokenId",
                        storageKey: null
                    },
                    Z = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "__typename",
                        storageKey: null
                    },
                    J = [{
                        alias: null,
                        args: null,
                        concreteType: "CollectionType",
                        kind: "LinkedField",
                        name: "collection",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "floorPrice",
                            storageKey: null
                        }, M],
                        storageKey: null
                    }],
                    ee = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "name",
                        storageKey: null
                    },
                    ne = [{
                        alias: null,
                        args: [{
                            kind: "Literal",
                            name: "first",
                            value: 2
                        }],
                        concreteType: "AssetQuantityTypeConnection",
                        kind: "LinkedField",
                        name: "assetQuantities",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetQuantityTypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetQuantityType",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetType",
                                    kind: "LinkedField",
                                    name: "asset",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "CollectionType",
                                        kind: "LinkedField",
                                        name: "collection",
                                        plural: !1,
                                        selections: [ee, M, {
                                            alias: null,
                                            args: null,
                                            concreteType: "DisplayDataType",
                                            kind: "LinkedField",
                                            name: "displayData",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "cardDisplayStyle",
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, ee, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "animationUrl",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "backgroundColor",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isDelisted",
                                        storageKey: null
                                    }, E, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "displayImageUrl",
                                        storageKey: null
                                    }, R, X, M],
                                    storageKey: null
                                }, Y, M],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: "assetQuantities(first:2)"
                    }, ee, G, M];
                return {
                    fragment: {
                        argumentDefinitions: [e, n, a, t, l, i, s, r, d, c, o, u, m, y, p, g, b, k, f, j],
                        kind: "Fragment",
                        metadata: null,
                        name: "OrdersQuery",
                        selections: [{
                            args: [{
                                kind: "Variable",
                                name: "count",
                                variableName: "count"
                            }, {
                                kind: "Variable",
                                name: "cursor",
                                variableName: "cursor"
                            }, h, {
                                kind: "Variable",
                                name: "expandedMode",
                                variableName: "expandedMode"
                            }, O, {
                                kind: "Variable",
                                name: "isBid",
                                variableName: "isBid"
                            }, v, x, A, T, F, S, K, C, L, B, w, I, P, _],
                            kind: "FragmentSpread",
                            name: "Orders_data"
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [n, e, a, s, r, d, c, u, p, b, k, f, j, m, y, o, g, t, i, l],
                        kind: "Operation",
                        name: "OrdersQuery",
                        selections: [{
                            alias: null,
                            args: Q,
                            concreteType: "OrderV2TypeConnection",
                            kind: "LinkedField",
                            name: "orders",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "OrderV2TypeEdge",
                                kind: "LinkedField",
                                name: "edges",
                                plural: !0,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "OrderV2Type",
                                    kind: "LinkedField",
                                    name: "node",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "closedAt",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isFulfillable",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isValid",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "oldOrder",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "openedAt",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "orderType",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AccountType",
                                        kind: "LinkedField",
                                        name: "maker",
                                        plural: !1,
                                        selections: N,
                                        storageKey: null
                                    }, {
                                        alias: "makerAsset",
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: V,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [R, M, U],
                                                        storageKey: null
                                                    }, M, H],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:1)"
                                        }, M],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "makerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: [{
                                                kind: "Literal",
                                                name: "first",
                                                value: 30
                                            }],
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [U, E, $, z, {
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "AssetContractType",
                                                            kind: "LinkedField",
                                                            name: "assetContract",
                                                            plural: !1,
                                                            selections: [W, q, M],
                                                            storageKey: null
                                                        }, M, {
                                                            alias: null,
                                                            args: null,
                                                            kind: "ScalarField",
                                                            name: "externalLink",
                                                            storageKey: null
                                                        }, {
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "CollectionType",
                                                            kind: "LinkedField",
                                                            name: "collection",
                                                            plural: !1,
                                                            selections: [{
                                                                alias: null,
                                                                args: null,
                                                                kind: "ScalarField",
                                                                name: "externalUrl",
                                                                storageKey: null
                                                            }, M],
                                                            storageKey: null
                                                        }],
                                                        storageKey: null
                                                    }, H, M],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:30)"
                                        }, M],
                                        storageKey: null
                                    }, Y, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "side",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AccountType",
                                        kind: "LinkedField",
                                        name: "taker",
                                        plural: !1,
                                        selections: N,
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "PriceType",
                                        kind: "LinkedField",
                                        name: "perUnitPrice",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "eth",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "PriceType",
                                        kind: "LinkedField",
                                        name: "price",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "usd",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "takerAssetBundle",
                                        plural: !1,
                                        selections: [G, {
                                            alias: null,
                                            args: V,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [{
                                                            alias: null,
                                                            args: [{
                                                                kind: "Literal",
                                                                name: "identity",
                                                                value: {}
                                                            }],
                                                            kind: "ScalarField",
                                                            name: "ownedQuantity",
                                                            storageKey: "ownedQuantity(identity:{})"
                                                        }, U, $, Y, {
                                                            alias: null,
                                                            args: null,
                                                            concreteType: "AssetContractType",
                                                            kind: "LinkedField",
                                                            name: "assetContract",
                                                            plural: !1,
                                                            selections: [D, M, q, W],
                                                            storageKey: null
                                                        }, X, M, E, z],
                                                        storageKey: null
                                                    }, H, M],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:1)"
                                        }, M],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "dutchAuctionFinalPrice",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "priceFnEndedAt",
                                        storageKey: null
                                    }, {
                                        alias: "takerAsset",
                                        args: null,
                                        concreteType: "AssetBundleType",
                                        kind: "LinkedField",
                                        name: "takerAssetBundle",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: V,
                                            concreteType: "AssetQuantityTypeConnection",
                                            kind: "LinkedField",
                                            name: "assetQuantities",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetQuantityTypeEdge",
                                                kind: "LinkedField",
                                                name: "edges",
                                                plural: !0,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetQuantityType",
                                                    kind: "LinkedField",
                                                    name: "node",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        concreteType: "AssetType",
                                                        kind: "LinkedField",
                                                        name: "asset",
                                                        plural: !1,
                                                        selections: [U, M],
                                                        storageKey: null
                                                    }, H, M],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: "assetQuantities(first:1)"
                                        }, M],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "remainingQuantity",
                                        storageKey: null
                                    }, M, Z, {
                                        condition: "isBid",
                                        kind: "Condition",
                                        passingValue: !0,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: null,
                                            kind: "LinkedField",
                                            name: "item",
                                            plural: !1,
                                            selections: [Z, {
                                                kind: "InlineFragment",
                                                selections: J,
                                                type: "AssetType",
                                                abstractKey: null
                                            }, {
                                                kind: "InlineFragment",
                                                selections: J,
                                                type: "AssetBundleType",
                                                abstractKey: null
                                            }, {
                                                kind: "InlineFragment",
                                                selections: [M],
                                                type: "Node",
                                                abstractKey: "__isNode"
                                            }],
                                            storageKey: null
                                        }]
                                    }, {
                                        condition: "expandedMode",
                                        kind: "Condition",
                                        passingValue: !0,
                                        selections: [{
                                            alias: "makerAssetBundleDisplay",
                                            args: null,
                                            concreteType: "AssetBundleType",
                                            kind: "LinkedField",
                                            name: "makerAssetBundle",
                                            plural: !1,
                                            selections: ne,
                                            storageKey: null
                                        }, {
                                            alias: "takerAssetBundleDisplay",
                                            args: null,
                                            concreteType: "AssetBundleType",
                                            kind: "LinkedField",
                                            name: "takerAssetBundle",
                                            plural: !1,
                                            selections: ne,
                                            storageKey: null
                                        }]
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "cursor",
                                    storageKey: null
                                }],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "PageInfo",
                                kind: "LinkedField",
                                name: "pageInfo",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "endCursor",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "hasNextPage",
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: Q,
                            filters: ["excludeMaker", "isExpired", "isValid", "maker", "makerArchetype", "makerAssetIsPayment", "takerArchetype", "takerAssetCategories", "takerAssetCollections", "takerAssetIsOwnedBy", "takerAssetIsPayment", "sortAscending", "sortBy", "makerAssetBundle", "takerAssetBundle", "filterByOrderRules"],
                            handle: "connection",
                            key: "Orders_orders",
                            kind: "LinkedHandle",
                            name: "orders"
                        }]
                    },
                    params: {
                        cacheID: "396add045b08e73514bb5927b27958ab",
                        id: null,
                        metadata: {},
                        name: "OrdersQuery",
                        operationKind: "query",
                        text: "query OrdersQuery(\n  $cursor: String\n  $count: Int = 10\n  $excludeMaker: IdentityInputType\n  $isExpired: Boolean\n  $isValid: Boolean\n  $maker: IdentityInputType\n  $makerArchetype: ArchetypeInputType\n  $makerAssetIsPayment: Boolean\n  $takerArchetype: ArchetypeInputType\n  $takerAssetCategories: [CollectionSlug!]\n  $takerAssetCollections: [CollectionSlug!]\n  $takerAssetIsOwnedBy: IdentityInputType\n  $takerAssetIsPayment: Boolean\n  $sortAscending: Boolean\n  $sortBy: OrderSortOption\n  $makerAssetBundle: BundleSlug\n  $takerAssetBundle: BundleSlug\n  $expandedMode: Boolean = false\n  $isBid: Boolean = false\n  $filterByOrderRules: Boolean = false\n) {\n  ...Orders_data_hzrfn\n}\n\nfragment AccountLink_data on AccountType {\n  address\n  config\n  isCompromised\n  user {\n    publicUsername\n    id\n  }\n  ...ProfileImage_data\n  ...wallet_accountKey\n  ...accounts_url\n}\n\nfragment AskPrice_data on OrderV2Type {\n  dutchAuctionFinalPrice\n  openedAt\n  priceFnEndedAt\n  makerAssetBundle {\n    assetQuantities(first: 30) {\n      edges {\n        node {\n          ...quantity_data\n          id\n        }\n      }\n    }\n    id\n  }\n  takerAssetBundle {\n    assetQuantities(first: 1) {\n      edges {\n        node {\n          ...AssetQuantity_data\n          id\n        }\n      }\n    }\n    id\n  }\n}\n\nfragment AssetCell_assetBundle on AssetBundleType {\n  assetQuantities(first: 2) {\n    edges {\n      node {\n        asset {\n          collection {\n            name\n            id\n          }\n          name\n          ...AssetMedia_asset\n          ...asset_url\n          id\n        }\n        relayId\n        id\n      }\n    }\n  }\n  name\n  slug\n}\n\nfragment AssetMedia_asset on AssetType {\n  animationUrl\n  backgroundColor\n  collection {\n    displayData {\n      cardDisplayStyle\n    }\n    id\n  }\n  isDelisted\n  imageUrl\n  displayImageUrl\n}\n\nfragment AssetQuantity_data on AssetQuantityType {\n  asset {\n    ...Price_data\n    id\n  }\n  quantity\n}\n\nfragment Orders_data_hzrfn on Query {\n  orders(after: $cursor, excludeMaker: $excludeMaker, first: $count, isExpired: $isExpired, isValid: $isValid, maker: $maker, makerArchetype: $makerArchetype, makerAssetIsPayment: $makerAssetIsPayment, takerArchetype: $takerArchetype, takerAssetCategories: $takerAssetCategories, takerAssetCollections: $takerAssetCollections, takerAssetIsOwnedBy: $takerAssetIsOwnedBy, takerAssetIsPayment: $takerAssetIsPayment, sortAscending: $sortAscending, sortBy: $sortBy, makerAssetBundle: $makerAssetBundle, takerAssetBundle: $takerAssetBundle, filterByOrderRules: $filterByOrderRules) {\n    edges {\n      node {\n        closedAt\n        isFulfillable\n        isValid\n        oldOrder\n        openedAt\n        orderType\n        maker {\n          address\n          ...AccountLink_data\n          ...wallet_accountKey\n          id\n        }\n        makerAsset: makerAssetBundle {\n          assetQuantities(first: 1) {\n            edges {\n              node {\n                asset {\n                  assetContract {\n                    address\n                    chain\n                    id\n                  }\n                  id\n                }\n                id\n              }\n            }\n          }\n          id\n        }\n        makerAssetBundle {\n          assetQuantities(first: 30) {\n            edges {\n              node {\n                ...AssetQuantity_data\n                ...quantity_data\n                id\n              }\n            }\n          }\n          id\n        }\n        relayId\n        side\n        taker {\n          address\n          ...AccountLink_data\n          ...wallet_accountKey\n          id\n        }\n        perUnitPrice {\n          eth\n        }\n        price {\n          usd\n        }\n        item @include(if: $isBid) {\n          __typename\n          ... on AssetType {\n            collection {\n              floorPrice\n              id\n            }\n          }\n          ... on AssetBundleType {\n            collection {\n              floorPrice\n              id\n            }\n          }\n          ... on Node {\n            __isNode: __typename\n            id\n          }\n        }\n        takerAssetBundle {\n          slug\n          ...bundle_url\n          assetQuantities(first: 1) {\n            edges {\n              node {\n                asset {\n                  ownedQuantity(identity: {})\n                  decimals\n                  symbol\n                  relayId\n                  assetContract {\n                    address\n                    id\n                  }\n                  ...asset_url\n                  id\n                }\n                quantity\n                ...AssetQuantity_data\n                ...quantity_data\n                id\n              }\n            }\n          }\n          id\n        }\n        ...AskPrice_data\n        ...orderLink_data\n        makerAssetBundleDisplay: makerAssetBundle @include(if: $expandedMode) {\n          ...AssetCell_assetBundle\n          id\n        }\n        takerAssetBundleDisplay: takerAssetBundle @include(if: $expandedMode) {\n          ...AssetCell_assetBundle\n          id\n        }\n        ...quantity_remaining\n        id\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n\nfragment Price_data on AssetType {\n  decimals\n  imageUrl\n  symbol\n  usdSpotPrice\n  assetContract {\n    blockExplorerLink\n    chain\n    id\n  }\n}\n\nfragment ProfileImage_data on AccountType {\n  imageUrl\n  address\n}\n\nfragment accounts_url on AccountType {\n  address\n  user {\n    publicUsername\n    id\n  }\n}\n\nfragment asset_url on AssetType {\n  assetContract {\n    address\n    chain\n    id\n  }\n  tokenId\n}\n\nfragment bundle_url on AssetBundleType {\n  slug\n}\n\nfragment orderLink_data on OrderV2Type {\n  makerAssetBundle {\n    assetQuantities(first: 30) {\n      edges {\n        node {\n          asset {\n            externalLink\n            collection {\n              externalUrl\n              id\n            }\n            id\n          }\n          id\n        }\n      }\n    }\n    id\n  }\n}\n\nfragment quantity_data on AssetQuantityType {\n  asset {\n    decimals\n    id\n  }\n  quantity\n}\n\nfragment quantity_remaining on OrderV2Type {\n  makerAsset: makerAssetBundle {\n    assetQuantities(first: 1) {\n      edges {\n        node {\n          asset {\n            decimals\n            id\n          }\n          quantity\n          id\n        }\n      }\n    }\n    id\n  }\n  takerAsset: takerAssetBundle {\n    assetQuantities(first: 1) {\n      edges {\n        node {\n          asset {\n            decimals\n            id\n          }\n          quantity\n          id\n        }\n      }\n    }\n    id\n  }\n  remainingQuantity\n  side\n}\n\nfragment wallet_accountKey on AccountType {\n  address\n}\n"
                    }
                }
            }();
            t.hash = "89d267a229e476d28463c9a854b5e993", n.default = t
        },
        W5hp: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return s
            }));
            var t = a("/DEc"),
                l = a("BOW+"),
                i = a("ASgk");

            function s(e, n, a) {
                Object(l.a)(2, arguments);
                var s = Object(t.a)(e, n) / 1e3;
                return Object(i.a)(null === a || void 0 === a ? void 0 : a.roundingMethod)(s)
            }
        },
        WWKX: function(e, n, a) {
            "use strict";
            var t, l = a("etRO"),
                i = a("4jfz"),
                s = a("mHfP"),
                r = a("1U+3"),
                d = a("DY1Z"),
                c = (a("mXGw"), a("UutA")),
                o = a("b7Z7"),
                u = a("+n/q"),
                m = a("opVg"),
                y = a("a7GP"),
                p = c.d.div.withConfig({
                    displayName: "NoticeBannerreact__NoticeBanner",
                    componentId: "sc-fj2tmr-0"
                })(["align-items:center;background:", ";color:white;display:flex;font-size:14px;justify-content:center;padding:10px 20px;position:relative;text-align:center;white-space:initial;width:100%;"], (function(e) {
                    return e.theme.colors.primary
                })),
                g = a("oYCi");

            function b(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(d.a)(e);
                    if (n) {
                        var l = Object(d.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(r.a)(this, a)
                }
            }
            var k = function(e) {
                    Object(s.a)(a, e);
                    var n = b(a);

                    function a() {
                        return Object(l.a)(this, a), n.apply(this, arguments)
                    }
                    return Object(i.a)(a, [{
                        key: "render",
                        value: function() {
                            var e, n, a = this.props.data,
                                t = this.context.wallet,
                                l = null === a || void 0 === a || null === (e = a.tradeSummary.bestAsk) || void 0 === e ? void 0 : e.maker,
                                i = l && t.isActiveAccount(l),
                                s = null === a || void 0 === a || null === (n = a.tradeSummary.bestAsk) || void 0 === n ? void 0 : n.taker,
                                r = i || s && t.isActiveAccount(s);
                            if (!a || !s || !r) return null;
                            var d = Object(g.jsx)(o.a, {
                                display: "inline-block",
                                children: Object(g.jsx)(u.a, {
                                    className: "PrivateListingBanner--account-link",
                                    dataKey: s,
                                    mode: "light"
                                })
                            });
                            return Object(g.jsx)(f, {
                                children: Object(g.jsx)(p, {
                                    children: Object(g.jsx)("div", {
                                        className: "PrivateListingBanner--content",
                                        children: i ? Object(g.jsxs)("div", {
                                            className: "PrivateListingBanner--identity-sentence",
                                            children: ["This is a private listing that you made for ", d, "."]
                                        }) : t.isActiveAccount(s) ? "This listing is reserved for you!" : null
                                    })
                                })
                            })
                        }
                    }]), a
                }(m.a),
                f = (n.a = Object(y.b)(k, void 0 !== t ? t : t = a("+u0o")), c.d.div.withConfig({
                    displayName: "PrivateListingBannerreact__DivContainer",
                    componentId: "sc-1i2raxq-0"
                })(["width:100%;.PrivateListingBanner--content{align-items:center;display:flex;flex-wrap:wrap;font-size:16px;justify-content:center;width:100%;.PrivateListingBanner--identity-sentence{margin-right:4px;.PrivateListingBanner--account-link{margin-left:4px;padding:6px 0;vertical-align:bottom;}}}"]))
        },
        XPtY: function(e, n, a) {
            "use strict";
            a.r(n), a.d(n, "default", (function() {
                return pe
            }));
            var t, l = a("qd51"),
                i = a("etRO"),
                s = a("4jfz"),
                r = a("g2+O"),
                d = a("mHfP"),
                c = a("1U+3"),
                o = a("DY1Z"),
                u = a("m6w3"),
                m = a("/dBk"),
                y = a.n(m),
                p = a("mXGw"),
                g = a("8Jek"),
                b = a.n(g),
                k = a("AfLb"),
                f = a("/sTg"),
                j = a("UutA"),
                h = a("+n/q"),
                O = a("2A7z"),
                v = a("wcUd"),
                x = a("qt8R"),
                A = a("1spp"),
                T = a("vxtu"),
                F = a("qymy"),
                S = a("Q5Gx"),
                K = a("Gkym"),
                C = a("h6Qh"),
                L = a("/mO9"),
                B = a("OsKK"),
                w = a("i0w7"),
                I = a("WWKX"),
                P = a("7Smq"),
                _ = a("atGD"),
                Q = a("kDvn"),
                D = a("fmwF"),
                M = a("/xOX"),
                E = a("5SNe"),
                N = a("TGkK"),
                V = a("b7Z7"),
                q = a("LoMF"),
                R = a("QrBS"),
                U = a("azPg"),
                H = a("QCNz"),
                $ = a("sX+s"),
                z = a("YO/S"),
                W = a("7bY5"),
                Y = a("n0tG"),
                G = a("j/Wi"),
                X = a("SMcu"),
                Z = a("Ujrs"),
                J = a("LsOE"),
                ee = a("1olU"),
                ne = a("StzY"),
                ae = a("kCmG"),
                te = a("8MqD"),
                le = a("p+l/"),
                ie = a("LjoF"),
                se = a("PAvK"),
                re = a("u6YR"),
                de = a("B6yL"),
                ce = a("heV+"),
                oe = a("K7R9"),
                ue = a("C/iq"),
                me = a("oYCi");

            function ye(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(o.a)(e);
                    if (n) {
                        var l = Object(o.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(c.a)(this, a)
                }
            }
            var pe = function(e) {
                Object(d.a)(a, e);
                var n = ye(a);

                function a() {
                    var e;
                    Object(i.a)(this, a);
                    for (var t = arguments.length, l = new Array(t), s = 0; s < t; s++) l[s] = arguments[s];
                    return e = n.call.apply(n, [this].concat(l)), Object(u.a)(Object(r.a)(e), "state", {
                        currentItem: 0,
                        isEnglishAuctionPollingChecked: !1
                    }), Object(u.a)(Object(r.a)(e), "renderOrderManager", (function() {
                        var n = e.props,
                            a = n.refetch,
                            t = n.variables.slug,
                            l = n.data;
                        return e.isViewerOwner() && null !== l && void 0 !== l && l.bundle && t ? Object(me.jsx)(D.a, {
                            sellRoute: Object(le.a)(l.bundle, "sell"),
                            variables: {
                                archetype: {
                                    tokenId: "",
                                    assetContractAddress: k.NULL_ADDRESS
                                },
                                isBundle: !0,
                                bundle: t
                            },
                            onOrdersChanged: function() {
                                Object(Z.b)(), a()
                            }
                        }) : null
                    })), Object(u.a)(Object(r.a)(e), "renderCarousel", (function() {
                        var n = e.props.data,
                            a = null === n || void 0 === n ? void 0 : n.bundle;
                        return a ? Object(me.jsx)(ge, {
                            bundle: a
                        }) : null
                    })), e
                }
                return Object(s.a)(a, [{
                    key: "componentDidUpdate",
                    value: function(e) {
                        var n, a;
                        if (e.data !== this.props.data && (null === (n = e.data) || void 0 === n ? void 0 : n.tradeSummary.bestAsk) !== (null === (a = this.props.data) || void 0 === a ? void 0 : a.tradeSummary.bestAsk) && !this.state.isEnglishAuctionPollingChecked) {
                            this.setState({
                                isEnglishAuctionPollingChecked: !0
                            });
                            var t = this.props,
                                l = t.data,
                                i = t.refetch,
                                s = null === l || void 0 === l ? void 0 : l.tradeSummary.bestAsk;
                            "ENGLISH" === (null === s || void 0 === s ? void 0 : s.orderType) && s.closedAt && Object(te.a)(s.closedAt, i)
                        }
                    }
                }, {
                    key: "isViewerOwner",
                    value: function() {
                        var e, n = this.props.data,
                            a = this.context.wallet,
                            t = null === n || void 0 === n || null === (e = n.tradeSummary.bestAsk) || void 0 === e ? void 0 : e.maker;
                        return t && a.isActiveAccount(t)
                    }
                }, {
                    key: "renderPrivateListingBanner",
                    value: function() {
                        var e = this.props.variables.slug;
                        return Object(me.jsx)(I.a, {
                            variables: {
                                bundle: e,
                                includePrivate: !0
                            }
                        })
                    }
                }, {
                    key: "renderItems",
                    value: function() {
                        var e = this.props.data,
                            n = null === e || void 0 === e ? void 0 : e.bundle;
                        if (!n) return null;
                        var a = Object(J.d)(n.assetQuantities);
                        return Object(me.jsx)(B.e, {
                            className: "Bundle--summary-frame",
                            children: Object(me.jsx)(B.c, {
                                children: Object(me.jsx)(w.a, {
                                    icon: "view_module",
                                    isContentPadded: !1,
                                    mode: "start-open",
                                    title: "".concat(a.length, " Items"),
                                    children: Object(me.jsx)("ul", {
                                        className: "Bundle--items-list",
                                        children: a.map((function(e) {
                                            var n = e.asset,
                                                a = e,
                                                t = n.collection,
                                                l = Object(ae.e)(n);
                                            return Object(me.jsx)(F.a, {
                                                href: l,
                                                target: "_blank",
                                                children: Object(me.jsxs)("li", {
                                                    className: "Bundle--items-list-item",
                                                    children: [Object(me.jsx)("div", {
                                                        className: "Bundle--image-frame",
                                                        children: Object(me.jsx)(O.a, {
                                                            asset: n,
                                                            size: 76,
                                                            title: "BundleItemImage"
                                                        })
                                                    }), Object(me.jsxs)("div", {
                                                        className: "Bundle--items-list-description",
                                                        children: [Object(me.jsx)("div", {
                                                            className: "Bundle--items-list-collection-description",
                                                            children: Object(me.jsx)(v.a, {
                                                                collection: t,
                                                                isSmall: !0
                                                            })
                                                        }), Object(me.jsx)(Y.a, {
                                                            className: "Bundle--items-list-name",
                                                            children: Object(ae.a)(n)
                                                        })]
                                                    }), Object(me.jsx)("div", {
                                                        className: "Bundle--items-list-quantity-container",
                                                        children: Object(me.jsx)(Y.a, {
                                                            className: "Bundle--items-list-quantity",
                                                            variant: "small",
                                                            children: "x" + Object(ie.l)(Object(se.a)(a))
                                                        })
                                                    })]
                                                })
                                            }, n.relayId)
                                        }))
                                    })
                                })
                            })
                        })
                    }
                }, {
                    key: "renderSummary",
                    value: function() {
                        var e = this.props.data,
                            n = null === e || void 0 === e ? void 0 : e.bundle;
                        if (!n) return null;
                        var a = n.description,
                            t = n.maker;
                        return Object(me.jsx)(B.e, {
                            className: "Bundle--summary-frame",
                            children: Object(me.jsx)(B.c, {
                                children: a || t ? Object(me.jsxs)(w.a, {
                                    bodyClassName: "Bundle--description",
                                    icon: "subject",
                                    maxHeight: 200,
                                    mode: "always-open",
                                    title: "Bundle Description",
                                    children: [t && Object(me.jsx)("section", {
                                        className: "Bundle--creator",
                                        children: Object(me.jsx)(h.a, {
                                            dataKey: t,
                                            isCreator: !0,
                                            variant: "no-image"
                                        })
                                    }), a && Object(me.jsx)(H.a, {
                                        children: a
                                    })]
                                }) : null
                            })
                        })
                    }
                }, {
                    key: "renderHeader",
                    value: function() {
                        var e = this.props.data,
                            n = null === e || void 0 === e ? void 0 : e.bundle;
                        return n ? Object(me.jsxs)(W.a, {
                            className: "Bundle--header",
                            children: [Object(me.jsx)(Y.a, {
                                className: "Bundle--header-text",
                                variant: "h2",
                                children: n.name
                            }), Object(me.jsxs)(R.a, {
                                height: "50px",
                                children: [Object(me.jsx)(L.a, {
                                    flags: ["staff"],
                                    children: Object(me.jsx)(V.a, {
                                        marginRight: "8px",
                                        children: Object(me.jsx)(G.b, {
                                            content: "Django Admin",
                                            children: Object(me.jsx)(q.c, {
                                                href: Object(ne.c)(n.relayId),
                                                icon: "vpn_key",
                                                variant: "tertiary"
                                            })
                                        })
                                    })
                                }), Object(me.jsx)(K.a, {
                                    object: "bundle",
                                    url: "/bundles/".concat(n.slug),
                                    children: Object(me.jsx)(q.c, {
                                        "aria-label": "Share",
                                        icon: "share",
                                        variant: "tertiary"
                                    })
                                })]
                            })]
                        }) : null
                    }
                }, {
                    key: "renderOrders",
                    value: function() {
                        var e, n = this.props,
                            a = n.data,
                            t = n.refetch,
                            l = n.variables.slug,
                            i = this.context,
                            s = i.wallet,
                            r = i.login,
                            d = Object(J.c)(null === a || void 0 === a || null === (e = a.bundle) || void 0 === e ? void 0 : e.assetQuantities),
                            c = null === d || void 0 === d ? void 0 : d.asset.assetContract.chain;
                        if (!l || null === a || void 0 === a || !a.bundle) return null;
                        var o;
                        return Object(me.jsxs)(me.Fragment, {
                            children: [Object(me.jsx)(w.a, {
                                className: "Bundle--orders",
                                icon: "local_offer",
                                isContentPadded: !1,
                                mode: "start-closed",
                                title: "Listings",
                                children: Object(me.jsx)(M.b, {
                                    mode: M.a.minimal,
                                    side: "ask",
                                    variables: {
                                        isExpired: !1,
                                        isValid: !0,
                                        makerAssetBundle: l,
                                        takerAssetIsPayment: !0,
                                        sortAscending: !0,
                                        sortBy: "TAKER_ASSETS_USD_PRICE"
                                    }
                                })
                            }), Object(me.jsx)(w.a, {
                                className: "Bundle--orders",
                                icon: "toc",
                                isContentPadded: !1,
                                mode: "start-open",
                                title: "Offers",
                                children: Object(me.jsx)(M.b, {
                                    footer: this.isViewerOwner() ? null : Object(me.jsx)("footer", {
                                        className: "Bundle--orders-footer",
                                        children: Object(me.jsx)(Q.a, {
                                            chainIdentifier: c,
                                            children: Object(me.jsx)(_.a, {
                                                chainIdentifier: c,
                                                children: (o = "Make Offer", Object(me.jsx)(T.a, {
                                                    condition: !!s.getActiveAccountKey(),
                                                    wrapper: function(e) {
                                                        return Object(me.jsx)(z.a, {
                                                            trigger: function(n) {
                                                                return Object(me.jsx)(V.a, {
                                                                    onClick: n,
                                                                    children: e
                                                                })
                                                            },
                                                            children: function(e) {
                                                                return Object(me.jsx)(P.a, {
                                                                    archetypeData: null,
                                                                    bundleData: a.bundle,
                                                                    tradeData: a.tradeSummary,
                                                                    onClose: e,
                                                                    onOrderCreated: function() {
                                                                        Object(Z.b)(), t()
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    },
                                                    children: Object(me.jsx)(q.c, {
                                                        variant: "secondary",
                                                        onClick: function() {
                                                            return !s.getActiveAccountKey() && r()
                                                        },
                                                        children: o
                                                    })
                                                }))
                                            })
                                        })
                                    }),
                                    mode: M.a.minimal,
                                    side: "bid",
                                    variables: {
                                        isExpired: !1,
                                        isValid: !0,
                                        takerAssetBundle: l,
                                        makerAssetIsPayment: !0,
                                        sortBy: "MAKER_ASSETS_USD_PRICE",
                                        isBid: !0
                                    }
                                })
                            })]
                        })
                    }
                }, {
                    key: "renderTradeStation",
                    value: function() {
                        var e = this.props,
                            n = e.refetch,
                            a = e.data;
                        return a ? Object(me.jsx)("div", {
                            className: "Bundle--trade-station-container",
                            children: Object(me.jsx)(E.a, {
                                archetypeData: null,
                                bundleData: a.bundle,
                                data: a.tradeSummary,
                                onOrdersChanged: n
                            })
                        }) : null
                    }
                }, {
                    key: "renderEventHistory",
                    value: function() {
                        var e = this.props.variables.slug;
                        return Object(me.jsx)(C.a, {
                            className: "Bundle--event-frame",
                            mode: "nonfungible",
                            showFilters: !0,
                            variables: {
                                bundle: e
                            }
                        })
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e, n, a, t = this,
                            l = this.props.data,
                            i = Object(J.c)(null === l || void 0 === l || null === (e = l.bundle) || void 0 === e ? void 0 : e.assetQuantities),
                            s = null === i || void 0 === i ? void 0 : i.asset;
                        return Object(me.jsxs)(N.a, {
                            children: [s ? Object(me.jsx)(X.a, {
                                description: (null === (n = s.description) || void 0 === n ? void 0 : n.substring(0, ue.Rb)) || s.collection.description || "View item history and listings",
                                image: s.imageUrl ? Object(de.l)(s.imageUrl) : void 0,
                                title: "".concat(null !== (a = s.name) && void 0 !== a ? a : s.tokenId, " - ").concat(s.collection.name, " | OpenSea")
                            }) : null, this.renderPrivateListingBanner(), this.renderOrderManager(), Object(me.jsxs)(be, {
                                children: [Object(me.jsx)($.a, {
                                    greaterThanOrEqual: "lg",
                                    children: function(e, n) {
                                        return n && Object(me.jsxs)("div", {
                                            className: b()("Bundle--wrapper", e),
                                            children: [Object(me.jsxs)("div", {
                                                className: "Bundle--summary",
                                                children: [t.renderCarousel(), t.renderSummary()]
                                            }), Object(me.jsxs)("div", {
                                                className: "Bundle--main",
                                                children: [t.renderHeader(), t.renderTradeStation(), t.renderOrders(), t.renderItems()]
                                            }), t.renderEventHistory()]
                                        })
                                    }
                                }), Object(me.jsx)($.a, {
                                    lessThan: "lg",
                                    children: function(e, n) {
                                        return n && Object(me.jsxs)("span", {
                                            className: b()("Bundle--wrapper", e),
                                            children: [Object(me.jsxs)("div", {
                                                className: "Bundle--summary",
                                                children: [t.renderHeader(), t.renderCarousel(), t.renderTradeStation(), t.renderItems(), t.renderOrders(), t.renderSummary()]
                                            }), t.renderEventHistory()]
                                        })
                                    }
                                })]
                            })]
                        })
                    }
                }]), a
            }(ee.a);
            Object(u.a)(pe, "query", void 0 !== t ? t : t = a("8koP")), Object(u.a)(pe, "getInitialProps", ce.a.nextParser({
                bundleSlug: ce.a.string,
                chainIdentifier: ce.a.Optional(ce.a.ChainIdentifier)
            }, function() {
                var e = Object(l.a)(y.a.mark((function e(n) {
                    var a, t;
                    return y.a.wrap((function(e) {
                        for (;;) switch (e.prev = e.next) {
                            case 0:
                                return a = n.bundleSlug, t = n.chainIdentifier, e.abrupt("return", {
                                    variables: {
                                        slug: a,
                                        chain: t
                                    }
                                });
                            case 2:
                            case "end":
                                return e.stop()
                        }
                    }), e)
                })));
                return function(n) {
                    return e.apply(this, arguments)
                }
            }()));
            var ge = function(e) {
                    var n = e.bundle,
                        a = Object(p.useRef)(null),
                        t = Object(p.useState)(0),
                        l = t[0],
                        i = t[1],
                        s = Object(J.d)(n.assetQuantities),
                        r = s.length - 1;
                    return Object(f.a)("ArrowRight", (function() {
                        var e;
                        return null === (e = a.current) || void 0 === e ? void 0 : e.slickGoTo(l === r ? 0 : l + 1)
                    })), Object(f.a)("ArrowLeft", (function() {
                        var e;
                        return null === (e = a.current) || void 0 === e ? void 0 : e.slickGoTo(0 === l ? r : l - 1)
                    })), Object(me.jsxs)(B.e, {
                        className: "Bundle--carousel-container",
                        children: [Object(me.jsx)(A.a, {
                            beforeChange: function(e, n) {
                                i(n)
                            },
                            customPaging: function(e) {
                                var n = e === l;
                                return s[e] ? Object(me.jsx)("div", {
                                    className: Object(re.a)("Bundle", {
                                        "image-frame": !n,
                                        "selected-frame": n
                                    }),
                                    children: Object(me.jsx)(O.a, {
                                        asset: s[e].asset,
                                        className: "Bundle--asset-media",
                                        size: 76,
                                        title: "BundleThumbnail"
                                    })
                                }) : Object(me.jsx)(me.Fragment, {})
                            },
                            dotType: "image",
                            dots: !0,
                            ref: a,
                            children: s.map((function(e) {
                                return Object(me.jsx)("div", {
                                    className: "Bundle--main-image",
                                    children: Object(me.jsx)(U.a, {
                                        trigger: function(n) {
                                            return Object(me.jsx)(V.a, {
                                                onClick: n,
                                                children: Object(me.jsx)(O.a, {
                                                    asset: e.asset,
                                                    autoPlay: !0,
                                                    className: "Bundle--main-carousel-image",
                                                    isMuted: !0,
                                                    showModel: !0,
                                                    size: 992
                                                })
                                            })
                                        },
                                        children: Object(me.jsx)(O.a, {
                                            asset: e.asset,
                                            autoPlay: !0,
                                            showControls: !0,
                                            showModel: !0
                                        })
                                    })
                                }, e.asset.relayId)
                            }))
                        }), Object(me.jsx)(x.a, {
                            className: "Bundle--pill",
                            icon: "view_module",
                            text: "".concat(l + 1, " of ").concat(s.length)
                        })]
                    })
                },
                be = j.d.div.withConfig({
                    displayName: "bundle__DivContainer",
                    componentId: "sc-1psat62-0"
                })(["padding:8px 0px 30px 0px;display:flex;justify-content:center;width:100%;flex-wrap:wrap;.Bundle--wrapper{display:flex;flex-direction:column;max-width:600px;width:100%;margin:0 10px;justify-content:center;.Bundle--trade-station-container{padding:0 8px;}.Bundle--loader-container{align-items:center;width:100%;display:flex;justify-content:center;min-height:calc(100vh - ", ");svg{margin-top:-", ";}}.Bundle--image-frame{border:1px solid ", ";border-radius:4px;height:78px;width:78px;}.Bundle--summary{align-self:center;max-width:100%;.Bundle--asset-media{border-radius:initial;}.Bundle--carousel-container{position:relative;margin:0px 8px 20px 8px;.Bundle--selected-frame{border:4px solid ", ";border-radius:4px;height:82px;width:82px;}.Bundle--pill{position:absolute;top:12px;right:12px;min-width:70px;}.Bundle--main-image{cursor:pointer;.Bundle--main-carousel-image{height:calc(100vw - 22px);max-height:578px;width:100%;}}}.Bundle--summary-frame{margin:20px 8px 0px 8px;.Bundle--description{font-size:14px;.Bundle--creator{align-items:center;color:", ";display:flex;height:32px;}*{font-size:14px;overflow:hidden;text-overflow:ellipsis;}}}}.Bundle--items-list{overflow-y:auto;max-height:324px;margin:0;.Bundle--items-list-item{height:108px;border-bottom:1px solid ", ";padding:20px 10px;display:flex;align-items:center;&:hover{box-shadow:", ";}.Bundle--items-list-description{margin:10px 20px;max-width:calc(100vw - 180px);display:flex;flex-direction:column;flex-grow:1;.Bundle--items-list-collection-description{display:flex;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;}.Bundle--items-list-name{overflow:hidden;white-space:nowrap;text-overflow:ellipsis;color:", ";margin-top:5px;}}.Bundle--items-list-quantity-container{display:flex;.Bundle--items-list-quantity{align-self:center;}}}}.Bundle--main{align-self:center;max-width:100%;width:100%;.Bundle--summary-frame{margin:20px 10px 20px 0;}}.Bundle--header{flex-wrap:wrap;padding:0 8px 0 8px;.Bundle--header-text{font-size:30px;margin-top:10px;max-width:calc(100% - 58px);overflow:hidden;text-overflow:ellipsis;}}.Bundle--orders{margin-top:20px;padding:0 8px;.Bundle--orders-footer{border-top:1px solid ", ";padding:10px;}}.Bundle--event-frame{padding:0px 8px;width:100%;margin-top:20px;}}", ""], oe.c, oe.c, (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.primary
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.shadows.default
                }), (function(e) {
                    return e.theme.colors.text.body
                }), (function(e) {
                    return e.theme.colors.border
                }), Object(S.e)({
                    tabletL: Object(j.c)([".Bundle--wrapper{flex-direction:row;width:1280px;max-width:100%;flex-wrap:wrap;margin:0;.Bundle--trade-station-container{padding:0 10px 0 0;}.Bundle--summary{align-self:auto;max-width:550px;flex:3 0;.Bundle--summary-frame{margin:20px;}.Bundle--carousel-container{margin:20px;.Bundle--main-image{.Bundle--main-carousel-image{height:516px;}}}}.Bundle--items-list{.Bundle--items-list-item{.Bundle--items-list-description{max-width:540px;}}}.Bundle--main{align-self:auto;flex:4 0;}.Bundle--header{margin-top:20px;padding:0 10px 0 0;}.Bundle--orders{padding:0 10px 0 0;}.Bundle--event-frame{padding:0 10px 0 20px;margin-top:0;}}"])
                }))
        },
        Z5z1: function(e, n, a) {
            "use strict";
            a.d(n, "c", (function() {
                return v
            })), a.d(n, "a", (function() {
                return A
            })), a.d(n, "b", (function() {
                return T
            }));
            var t = a("m6w3"),
                l = a("oA/F"),
                i = (a("mXGw"), a("UutA")),
                s = a("2A7z"),
                r = a("m5he"),
                d = a("b7Z7"),
                c = a("9E9p"),
                o = a("7bY5"),
                u = a("j/Wi"),
                m = a("t3V9"),
                y = a("kCmG"),
                p = a("7v7j"),
                g = a("tQfM"),
                b = a("oYCi"),
                k = ["asset", "renderExtra", "renderFooter", "quantity"],
                f = ["onRemove"];

            function j(e, n) {
                var a = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var t = Object.getOwnPropertySymbols(e);
                    n && (t = t.filter((function(n) {
                        return Object.getOwnPropertyDescriptor(e, n).enumerable
                    }))), a.push.apply(a, t)
                }
                return a
            }

            function h(e) {
                for (var n = 1; n < arguments.length; n++) {
                    var a = null != arguments[n] ? arguments[n] : {};
                    n % 2 ? j(Object(a), !0).forEach((function(n) {
                        Object(t.a)(e, n, a[n])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(a)) : j(Object(a)).forEach((function(n) {
                        Object.defineProperty(e, n, Object.getOwnPropertyDescriptor(a, n))
                    }))
                }
                return e
            }
            var O = 48,
                v = function(e) {
                    var n = e.asset,
                        a = e.renderExtra,
                        t = e.renderFooter,
                        i = e.quantity,
                        r = Object(l.a)(e, k);
                    return Object(b.jsxs)(c.a, h(h({
                        flexDirection: "column"
                    }, r), {}, {
                        children: [Object(b.jsxs)(o.a, {
                            children: [Object(b.jsx)(c.a.Avatar, {
                                borderRadius: g.d.default,
                                size: O,
                                children: Object(b.jsx)(s.a, {
                                    asset: n,
                                    size: O
                                }, n.relayId)
                            }), Object(b.jsxs)(c.a.Content, {
                                children: [Object(b.jsx)(c.a.Description, {
                                    children: n.collection.name
                                }), Object(b.jsx)(c.a.Title, {
                                    children: Object(y.a)(n)
                                }), i && Object(b.jsx)(c.a.Description, {
                                    children: "Quantity: ".concat(i)
                                })]
                            }), null === a || void 0 === a ? void 0 : a(n)]
                        }), null === t || void 0 === t ? void 0 : t(n)]
                    }))
                },
                x = Object(i.d)(d.a).withConfig({
                    displayName: "AssetItemreact__AssetBundleMediaContainer",
                    componentId: "sc-m00yup-0"
                })(["position:relative;.SellHeader--asset{height:48px;width:48px;background:", ";border:1px solid ", ";border-radius:", ";position:relative;z-index:1;}.SellHeader--skeleton-asset{position:absolute;background:", ";height:", "px;width:", "px;border:1px solid ", ";border-radius:", ";top:-4px;right:-4px;z-index:0;}"], (function(e) {
                    return e.theme.colors.card
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.gray
                }), O, O, (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.borderRadius.default
                })),
                A = function(e) {
                    var n = e.firstAsset,
                        a = e.numAssets,
                        t = e.bundleName,
                        l = e.style,
                        i = e.renderExtra,
                        r = e.renderFooter;
                    return Object(b.jsxs)(c.a, {
                        flexDirection: "column",
                        style: l,
                        children: [Object(b.jsxs)(o.a, {
                            children: [Object(b.jsx)(c.a.Avatar, {
                                borderRadius: g.d.default,
                                size: O,
                                style: {
                                    overflow: "visible"
                                },
                                children: Object(b.jsxs)(x, {
                                    children: [Object(b.jsx)(d.a, {
                                        children: Object(b.jsx)(s.a, {
                                            asset: n,
                                            className: "SellHeader--asset",
                                            size: O
                                        }, n.relayId)
                                    }), a > 1 && Object(b.jsx)(d.a, {
                                        className: "SellHeader--skeleton-asset"
                                    })]
                                })
                            }), Object(b.jsxs)(c.a.Content, {
                                children: [Object(b.jsx)(c.a.Description, {
                                    children: "".concat(a, " ").concat(Object(p.l)("item", a))
                                }), Object(b.jsx)(c.a.Title, {
                                    children: t
                                })]
                            }), null === i || void 0 === i ? void 0 : i(n)]
                        }), null === r || void 0 === r ? void 0 : r(n)]
                    })
                },
                T = function(e) {
                    var n = e.onRemove,
                        a = Object(l.a)(e, f);
                    return Object(b.jsx)(v, h(h({}, a), {}, {
                        renderExtra: function(e) {
                            return n && Object(b.jsx)(c.a.Action, {
                                children: Object(b.jsx)(u.b, {
                                    content: "Remove",
                                    children: Object(b.jsx)(m.a, {
                                        onClick: function(a) {
                                            a.stopPropagation(), a.preventDefault(), n(e.relayId)
                                        },
                                        children: Object(b.jsx)(r.a, {
                                            color: "gray",
                                            cursor: "pointer",
                                            value: "remove"
                                        })
                                    })
                                })
                            })
                        }
                    }))
                }
        },
        ZJLq: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return t
            })), a.d(n, "b", (function() {
                return l
            }));
            var t = "Action disabled due to account being compromised",
                l = "Not available in your region"
        },
        dP7h: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return y
            }));
            a("mXGw");
            var t, l = a("aXrf"),
                i = a("QYJX"),
                s = a("vkv6"),
                r = a("QrBS"),
                d = a("j/Wi"),
                c = a("BmUw"),
                o = a("LjoF"),
                u = a("C/iq"),
                m = a("oYCi"),
                y = function(e) {
                    var n = e.price,
                        y = e.paymentAssetDataKey,
                        p = Object(l.useFragment)(void 0 !== t ? t : t = a("UPyn"), y),
                        g = p.assetContract.chain,
                        b = p.symbol,
                        k = p.imageUrl;
                    return Object(m.jsxs)(r.a, {
                        fontSize: "14px",
                        justifyContent: "flex-end",
                        children: [b ? Object(m.jsx)(d.b, {
                            content: b ? "".concat(b).concat(Object(c.e)(g) ? " on ".concat(u.o[g]) : "") : "Unknown currency",
                            children: Object(m.jsx)(r.a, {
                                alignItems: "center",
                                cursor: "pointer",
                                marginRight: "4px",
                                children: Object(m.jsx)(s.b, {
                                    size: 16,
                                    src: null !== k && void 0 !== k ? k : ""
                                })
                            })
                        }) : null, " ", Object(m.jsx)(i.b, {
                            children: Object(o.f)(null !== n && void 0 !== n ? n : 0, b || void 0)
                        })]
                    })
                }
        },
        dtIP: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "address",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "chain",
                            storageKey: null
                        }],
                        storageKey: null
                    },
                    n = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    a = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    t = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    };
                return {
                    argumentDefinitions: [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "chain"
                    }],
                    kind: "Fragment",
                    metadata: null,
                    name: "BidModalContent_bundle",
                    selections: [{
                        alias: null,
                        args: [{
                            kind: "Literal",
                            name: "first",
                            value: 30
                        }],
                        concreteType: "AssetQuantityTypeConnection",
                        kind: "LinkedField",
                        name: "assetQuantities",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetQuantityTypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetQuantityType",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetType",
                                    kind: "LinkedField",
                                    name: "asset",
                                    plural: !1,
                                    selections: [e, n, a, {
                                        alias: null,
                                        args: null,
                                        concreteType: "CollectionType",
                                        kind: "LinkedField",
                                        name: "collection",
                                        plural: !1,
                                        selections: [t, {
                                            alias: null,
                                            args: [{
                                                kind: "Variable",
                                                name: "chain",
                                                variableName: "chain"
                                            }],
                                            concreteType: "PaymentAssetType",
                                            kind: "LinkedField",
                                            name: "paymentAssets",
                                            plural: !0,
                                            selections: [a, {
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetType",
                                                kind: "LinkedField",
                                                name: "asset",
                                                plural: !1,
                                                selections: [e, n, {
                                                    alias: null,
                                                    args: null,
                                                    kind: "ScalarField",
                                                    name: "symbol",
                                                    storageKey: null
                                                }, {
                                                    alias: null,
                                                    args: null,
                                                    kind: "ScalarField",
                                                    name: "usdSpotPrice",
                                                    storageKey: null
                                                }, a],
                                                storageKey: null
                                            }, {
                                                args: null,
                                                kind: "FragmentSpread",
                                                name: "PaymentTokenInputV2_data"
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "quantity",
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: "assetQuantities(first:30)"
                    }, t],
                    type: "AssetBundleType",
                    abstractKey: null
                }
            }();
            t.hash = "4bc87bb580ed8b2e4352552c73d01ff0", n.default = t
        },
        fKyI: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: {
                    plural: !0
                },
                name: "PaymentTokenInputV2_data",
                selections: [{
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "relayId",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    concreteType: "AssetType",
                    kind: "LinkedField",
                    name: "asset",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "usdSpotPrice",
                        storageKey: null
                    }],
                    storageKey: null
                }, {
                    args: null,
                    kind: "FragmentSpread",
                    name: "PaymentAsset_data"
                }],
                type: "PaymentAssetType",
                abstractKey: null,
                hash: "c3451c847568e62b5b14792be081099c"
            };
            n.default = t
        },
        fmwF: function(e, n, a) {
            "use strict";
            var t, l = a("qd51"),
                i = a("etRO"),
                s = a("4jfz"),
                r = a("g2+O"),
                d = a("mHfP"),
                c = a("1U+3"),
                o = a("DY1Z"),
                u = a("m6w3"),
                m = a("/dBk"),
                y = a.n(m),
                p = (a("mXGw"), a("UutA")),
                g = a("UTR7"),
                b = a("LoMF"),
                k = a("ZwbU"),
                f = a("YO/S"),
                j = a("j/Wi"),
                h = a("ZJLq"),
                O = a("LsOE"),
                v = a("opVg"),
                x = a("a7GP"),
                A = a("4u0K"),
                T = a("BmUw"),
                F = a("LjoF"),
                S = a("xpX1"),
                K = a("7v7j"),
                C = a("9gvq"),
                L = a("gCP0"),
                B = a("LweY"),
                w = a("wwms"),
                I = a("Q5Gx"),
                P = a("atGD"),
                _ = a("kDvn"),
                Q = a("x/dt"),
                D = a("1hf2"),
                M = a("m5he"),
                E = a("qymy"),
                N = a("b7Z7"),
                V = a("QrBS"),
                q = a("n0tG"),
                R = a("8BrW"),
                U = a("p6pn"),
                H = a("oYCi"),
                $ = function(e) {
                    var n = e.errorMessage,
                        a = e.isOpen,
                        t = e.isSubmitting,
                        l = e.placeholder,
                        i = e.onClose,
                        s = e.onSubmit,
                        r = e.paymentAssetOption,
                        d = Object(U.a)({
                            mode: "onChange"
                        }),
                        c = d.handleSubmit,
                        o = d.formState,
                        u = o.errors,
                        m = o.isValid,
                        y = d.control;
                    return Object(H.jsx)(k.a, {
                        isOpen: a,
                        onClose: i,
                        children: Object(H.jsxs)(R.a, {
                            as: "form",
                            onSubmit: c(s),
                            children: [Object(H.jsx)(k.a.Header, {
                                children: Object(H.jsx)(k.a.Title, {
                                    children: "Lower the listing price"
                                })
                            }), Object(H.jsxs)(k.a.Body, {
                                children: [Object(H.jsx)(D.a, {
                                    control: y,
                                    name: "newAmount",
                                    render: function(e) {
                                        var n = e.field;
                                        return Object(H.jsx)(g.a, {
                                            autoFocus: !0,
                                            error: u[n.name],
                                            id: n.name,
                                            name: n.name,
                                            paymentAssetOptions: [r],
                                            paymentAssetRelayId: r.value,
                                            placeholder: l,
                                            price: n.value,
                                            onChange: n.onChange
                                        })
                                    }
                                }), n && Object(H.jsxs)(H.Fragment, {
                                    children: [Object(H.jsx)("br", {}), Object(H.jsx)("p", {
                                        className: "OrderManager--modal-error-text",
                                        children: n
                                    })]
                                }), Object(H.jsxs)(V.a, {
                                    marginBottom: "16px",
                                    marginTop: "24px",
                                    children: [Object(H.jsx)(M.a, {
                                        color: "gray",
                                        value: "warning_amber"
                                    }), Object(H.jsxs)(q.a, {
                                        as: "span",
                                        marginLeft: "8px",
                                        children: ["You must pay an additional gas fee if you want to cancel this listing at a later point.", " ", Object(H.jsx)(E.a, {
                                            href: "https://support.opensea.io/hc/articles/4410153816723",
                                            target: "_blank",
                                            children: "Learn more"
                                        })]
                                    })]
                                })]
                            }), Object(H.jsxs)(k.a.Footer, {
                                children: [Object(H.jsx)(b.c, {
                                    variant: "tertiary",
                                    onClick: i,
                                    children: "Never mind"
                                }), Object(H.jsx)(N.a, {
                                    marginLeft: "8px",
                                    children: Object(H.jsx)(b.c, {
                                        disabled: !m || t,
                                        isLoading: t,
                                        type: "submit",
                                        children: "Set new price"
                                    })
                                })]
                            })]
                        })
                    })
                };

            function z(e) {
                var n = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var a, t = Object(o.a)(e);
                    if (n) {
                        var l = Object(o.a)(this).constructor;
                        a = Reflect.construct(t, arguments, l)
                    } else a = t.apply(this, arguments);
                    return Object(c.a)(this, a)
                }
            }
            var W = 10,
                Y = function(e) {
                    Object(d.a)(a, e);
                    var n = z(a);

                    function a() {
                        var e;
                        Object(i.a)(this, a);
                        for (var t = arguments.length, s = new Array(t), d = 0; d < t; d++) s[d] = arguments[d];
                        return e = n.call.apply(n, [this].concat(s)), Object(u.a)(Object(r.a)(e), "state", {
                            legacyOrderCancelModalOpen: !1,
                            priceDropModalOpen: !1,
                            loading: !1
                        }), Object(u.a)(Object(r.a)(e), "onCancel", Object(l.a)(y.a.mark((function n() {
                            var a, t;
                            return y.a.wrap((function(n) {
                                for (;;) switch (n.prev = n.next) {
                                    case 0:
                                        return a = e.props.onOrdersChanged, t = e.getMakerOrders().map((function(e) {
                                            return e.oldOrder
                                        })).filter((function(e) {
                                            return null !== e
                                        })).map((function(e) {
                                            return Object(B.c)(JSON.parse(e))
                                        })), n.prev = 2, e.setState({
                                            loading: !0
                                        }), n.next = 6, Promise.all(t.map((function(e) {
                                            return Object(w.b)(C.a.cancelOrder(e))
                                        })));
                                    case 6:
                                        e.showSuccessMessage("Your listing was cancelled successfully!"), a(), e.setState({
                                            legacyOrderCancelModalOpen: !1
                                        }), n.next = 13;
                                        break;
                                    case 11:
                                        n.prev = 11, n.t0 = n.catch(2);
                                    case 13:
                                        return n.prev = 13, e.setState({
                                            loading: !1
                                        }), n.finish(13);
                                    case 16:
                                    case "end":
                                        return n.stop()
                                }
                            }), n, null, [
                                [2, 11, 13, 16]
                            ])
                        })))), Object(u.a)(Object(r.a)(e), "onPriceDrop", function() {
                            var n = Object(l.a)(y.a.mark((function n(a) {
                                var t, l, i, s, r, d, c, o, u, m, p, g, b, k, f;
                                return y.a.wrap((function(n) {
                                    for (;;) switch (n.prev = n.next) {
                                        case 0:
                                            if (t = a.newAmount, l = e.props, i = l.data, s = l.onOrdersChanged, r = Object(O.d)(null === i || void 0 === i ? void 0 : i.orders)[0], d = Object(O.d)(r.takerAssetBundle.assetQuantities)[0], c = Object(F.d)(d.quantity, d.asset.decimals), null !== (o = r.oldOrder ? Object(B.c)(JSON.parse(r.oldOrder)) : void 0) && void 0 !== o && o.asset || null !== o && void 0 !== o && o.assetBundle) {
                                                n.next = 10;
                                                break
                                            }
                                            return u = "Values missing.", e.setState({
                                                errorMessage: u
                                            }), n.abrupt("return");
                                        case 10:
                                            if ((m = Object(F.d)(t || c)).isNaN() && (u = "The new sale price must be a number."), m.lessThanOrEqualTo(0) && !window.confirm("Are you sure you want to make this free?") && (u = "Please try again with a positive price."), m.greaterThanOrEqualTo(c) && (u = "The new sale price must be lower than the current price. If you need to set a higher price, cancel the listing and re-list."), !u) {
                                                n.next = 17;
                                                break
                                            }
                                            return e.setState({
                                                errorMessage: u
                                            }), n.abrupt("return");
                                        case 17:
                                            if (p = +o.makerReferrerFee, g = o.waitingForBestCounterOrder ? +o.listingTime : +o.expirationTime, b = r.englishAuctionReservePrice ? +Object(F.d)(r.englishAuctionReservePrice, d.asset.decimals) : void 0, n.prev = 20, e.setState({
                                                    loading: !0,
                                                    errorMessage: void 0
                                                }), k = o.asset, !o.assetBundle) {
                                                n.next = 30;
                                                break
                                            }
                                            return f = o.assetOrBundle, n.next = 28, Object(w.b)(C.a.placeBundleSellOrder({
                                                bundleName: f.name,
                                                bundleDescription: f.description,
                                                bundleExternalLink: f.externalLink,
                                                assets: f.assets,
                                                collection: f.collection,
                                                startAmount: +m,
                                                endAmount: +m,
                                                expirationTime: g,
                                                paymentTokenAddress: o.paymentToken,
                                                buyerAddress: o.taker,
                                                extraBountyBasisPoints: p,
                                                waitForHighestBid: o.waitingForBestCounterOrder,
                                                reservePrice: b
                                            }));
                                        case 28:
                                            n.next = 33;
                                            break;
                                        case 30:
                                            if (!k) {
                                                n.next = 33;
                                                break
                                            }
                                            return n.next = 33, Object(w.b)(C.a.placeSellOrder({
                                                asset: k,
                                                startAmount: +m,
                                                endAmount: +m,
                                                quantity: +Object(F.d)(o.quantity, k.decimals),
                                                expirationTime: g,
                                                paymentTokenAddress: o.paymentToken,
                                                buyerAddress: o.taker,
                                                extraBountyBasisPoints: p,
                                                waitForHighestBid: o.waitingForBestCounterOrder,
                                                reservePrice: b
                                            }));
                                        case 33:
                                            s(), e.showSuccessMessage("Price successfully lowered"), e.setState({
                                                priceDropModalOpen: !1
                                            }), n.next = 41;
                                            break;
                                        case 38:
                                            n.prev = 38, n.t0 = n.catch(20), e.setState({
                                                errorMessage: n.t0.toString()
                                            });
                                        case 41:
                                            return n.prev = 41, e.setState({
                                                loading: !1
                                            }), n.finish(41);
                                        case 44:
                                        case "end":
                                            return n.stop()
                                    }
                                }), n, null, [
                                    [20, 38, 41, 44]
                                ])
                            })));
                            return function(e) {
                                return n.apply(this, arguments)
                            }
                        }()), Object(u.a)(Object(r.a)(e), "renderCancelModal", (function() {
                            var n = e.props.data,
                                a = e.state,
                                t = a.legacyOrderCancelModalOpen,
                                l = a.loading,
                                i = Object(O.d)(null === n || void 0 === n ? void 0 : n.orders).length,
                                s = function() {
                                    return e.setState({
                                        legacyOrderCancelModalOpen: !1
                                    })
                                };
                            return Object(H.jsxs)(k.a, {
                                isOpen: t,
                                onClose: s,
                                children: [Object(H.jsx)(k.a.Header, {
                                    children: Object(H.jsxs)(k.a.Title, {
                                        children: ["Are you sure you want to cancel your", " ", Object(K.l)("listing", i), "?"]
                                    })
                                }), Object(H.jsxs)(k.a.Body, {
                                    children: [i > 1 ? Object(H.jsxs)("p", {
                                        children: ["Canceling your listings will unpublish them on OpenSea and requires transactions to make sure they will never be fulfillable.", i >= W ? " You can cancel up to ".concat(W, " at a time.") : ""]
                                    }) : Object(H.jsx)("p", {
                                        children: "Canceling your listing will unpublish this sale from OpenSea and requires a transaction to make sure it will never be fulfillable."
                                    }), i > 1 && Object(H.jsxs)("small", {
                                        className: "OrderManager--note",
                                        children: ["This will cancel your", " ", i >= W ? "lowest " : "", Object(O.d)(null === n || void 0 === n ? void 0 : n.orders).length, " orders."]
                                    })]
                                }), Object(H.jsxs)(k.a.Footer, {
                                    children: [Object(H.jsx)(b.c, {
                                        variant: "tertiary",
                                        onClick: s,
                                        children: "Never mind"
                                    }), Object(H.jsxs)(b.c, {
                                        disabled: l,
                                        isLoading: l,
                                        marginLeft: "24px",
                                        onClick: e.onCancel,
                                        children: ["Cancel ", Object(K.l)("listing", i)]
                                    })]
                                })]
                            }, "CancelModal")
                        })), Object(u.a)(Object(r.a)(e), "renderPriceDropModal", (function() {
                            var n, a = e.props.data,
                                t = e.state,
                                l = t.priceDropModalOpen,
                                i = t.errorMessage,
                                s = t.loading,
                                r = null === (n = Object(O.d)(null === a || void 0 === a ? void 0 : a.orders)[0]) || void 0 === n ? void 0 : n.takerAssetBundle.assetQuantities,
                                d = Object(O.c)(r);
                            if (!d) return null;
                            var c = Object(g.b)([{
                                    relayId: d.asset.relayId,
                                    asset: d.asset
                                }])[0],
                                o = Object(F.f)(Object(F.d)(d.quantity, d.asset.decimals), d.asset.symbol || void 0);
                            return Object(H.jsx)($, {
                                errorMessage: i,
                                isOpen: l,
                                isSubmitting: s,
                                paymentAssetOption: c,
                                placeholder: o,
                                onClose: function() {
                                    return e.setState({
                                        priceDropModalOpen: !1
                                    })
                                },
                                onSubmit: e.onPriceDrop
                            })
                        })), Object(u.a)(Object(r.a)(e), "renderEditButton", (function() {
                            var n = e.props,
                                a = n.variables,
                                t = a.archetype,
                                l = t.assetContractAddress,
                                i = t.tokenId,
                                s = a.chain,
                                r = n.collectionSlug,
                                d = e.isFungible() ? "You do not own the entire item supply." : "You are not the creator of this item.";
                            return Object(H.jsx)(j.b, {
                                content: d,
                                disabled: e.canEdit(),
                                children: Object(H.jsx)("span", {
                                    children: Object(H.jsx)(b.c, {
                                        className: "OrderManager--second-button",
                                        disabled: !e.canEdit(),
                                        href: "/collection/".concat(r, "/asset/").concat(Object(T.a)(s)).concat(l, "/").concat(i, "/edit"),
                                        size: b.a.medium,
                                        variant: "secondary",
                                        width: "162px",
                                        children: "Edit"
                                    })
                                })
                            })
                        })), Object(u.a)(Object(r.a)(e), "renderSellButton", (function() {
                            var n, a, t = e.props.sellRoute,
                                l = null !== (n = null === (a = e.context.wallet.activeAccount) || void 0 === a ? void 0 : a.isCompromised) && void 0 !== n && n;
                            return Object(H.jsx)(j.b, {
                                content: h.a,
                                disabled: !l,
                                children: Object(H.jsx)("span", {
                                    children: Object(H.jsx)(b.c, {
                                        className: "OrderManager--second-button",
                                        disabled: !e.ownsEnoughToSell() || l,
                                        href: t,
                                        size: b.a.medium,
                                        width: "162px",
                                        children: "Sell"
                                    })
                                })
                            })
                        })), Object(u.a)(Object(r.a)(e), "renderCancelButton", (function() {
                            var n = e.props,
                                a = n.data,
                                t = n.variables,
                                l = t.archetype.assetContractAddress,
                                i = t.chain,
                                s = t.isBundle,
                                r = n.collectionSlug,
                                d = n.onOrdersChanged,
                                c = e.context.wallet,
                                o = Object(O.d)(null === a || void 0 === a ? void 0 : a.orders),
                                u = Object(A.c)(o),
                                m = Object(S.c)({
                                    chain: i,
                                    address: l,
                                    slug: r
                                });
                            return Object(H.jsx)(f.a, {
                                size: "large",
                                trigger: function(n) {
                                    return Object(H.jsxs)(H.Fragment, {
                                        children: [Object(H.jsxs)(b.c, {
                                            className: "OrderManager--second-button",
                                            minWidth: "162px",
                                            size: b.a.medium,
                                            variant: m ? "primary" : "secondary",
                                            onClick: function() {
                                                m ? n() : e.setState({
                                                    legacyOrderCancelModalOpen: !0
                                                })
                                            },
                                            children: ["Cancel ", Object(K.l)("listing", o.length)]
                                        }), m || s ? null : Object(H.jsx)(b.c, {
                                            minWidth: "162px",
                                            size: b.a.medium,
                                            onClick: function() {
                                                return e.setState({
                                                    priceDropModalOpen: !0
                                                })
                                            },
                                            children: "Lower price"
                                        })]
                                    })
                                },
                                children: function(e) {
                                    return Object(H.jsx)(Q.a, {
                                        variables: {
                                            orderId: u.relayId,
                                            maker: {
                                                address: c.address
                                            }
                                        },
                                        onClose: e,
                                        onOrderCancelled: d
                                    })
                                }
                            })
                        })), Object(u.a)(Object(r.a)(e), "isFungible", (function() {
                            var n, a = e.props.data;
                            return null != (null === a || void 0 === a || null === (n = a.archetype) || void 0 === n ? void 0 : n.quantity) && +Object(F.d)(a.archetype.quantity) > 1
                        })), Object(u.a)(Object(r.a)(e), "ownsEnoughToSell", (function() {
                            var n, a, t = e.props.data;
                            return 0 !== +Object(F.d)(null !== (n = null === t || void 0 === t || null === (a = t.archetype) || void 0 === a ? void 0 : a.ownedQuantity) && void 0 !== n ? n : 0) && !(!e.isFungible() && e.hasOpenOrders())
                        })), Object(u.a)(Object(r.a)(e), "hasOpenOrders", (function() {
                            return e.getMakerOrders().length > 0
                        })), Object(u.a)(Object(r.a)(e), "getMakerOrders", (function() {
                            var n = e.props.data;
                            return Object(O.d)(null === n || void 0 === n ? void 0 : n.orders)
                        })), Object(u.a)(Object(r.a)(e), "canEdit", (function() {
                            var n, a = e.props.data;
                            return null != (null === a || void 0 === a || null === (n = a.archetype) || void 0 === n ? void 0 : n.asset.isEditable.value) && Boolean(a.archetype.asset.isEditable.value)
                        })), e
                    }
                    return Object(s.a)(a, [{
                        key: "render",
                        value: function() {
                            var e = this.props,
                                n = e.className,
                                a = e.variables.chain,
                                t = e.collectionSlug,
                                l = this.ownsEnoughToSell() || this.isFungible(),
                                i = this.canEdit() || this.isFungible(),
                                s = !this.isFungible() && this.hasOpenOrders();
                            return Object(H.jsxs)(G, {
                                className: n,
                                children: [Object(H.jsx)("div", {
                                    className: "OrderManager--cta-container",
                                    children: Object(H.jsx)(_.a, {
                                        chainIdentifier: a,
                                        children: Object(H.jsxs)(P.a, {
                                            chainIdentifier: a,
                                            collectionSlug: t,
                                            children: [i ? this.renderEditButton() : null, s ? this.renderCancelButton() : null, l ? this.renderSellButton() : null]
                                        })
                                    })
                                }), this.renderCancelModal(), this.renderPriceDropModal()]
                            })
                        }
                    }]), a
                }(v.a),
                G = (n.a = Object(x.b)(Y, void 0 !== t ? t : t = a("FrHE")), p.d.div.withConfig({
                    displayName: "OrderManagerreact__DivContainer",
                    componentId: "sc-rw3i3h-0"
                })(["bottom:0px;position:fixed;top:auto;width:100%;right:0px;padding:10px 20px;z-index:", ";background-color:", ";border-bottom:1px solid ", ";display:flex;.OrderManager--cta-container{align-items:center;display:flex;flex:1 0;justify-content:flex-end;.OrderManager--second-button{margin-right:10px;}}.Modal{.OrderManager--cta-container{margin:10px 0px 20px;justify-content:center;.OrderManager--cta{padding:18px;}}.OrderManager--modal-error-text{color:", ";overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}}.OrderManager--note{text-align:center;display:block;opacity:0.5;font-size:13px;}.OrderManager--loader{display:flex;justify-content:center;margin-top:32px;}", ""], L.a.MANAGER_BAR, (function(e) {
                    return e.theme.colors.surface
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.error
                }), Object(I.e)({
                    tabletL: Object(p.c)(["position:sticky;top:72px;height:70px;.OrderManager--cta-container{max-width:1280px;margin:auto;padding:0 20px;}"])
                })))
        },
        fnww: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    n = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "address",
                        storageKey: null
                    },
                    a = [n],
                    t = {
                        kind: "InlineDataFragmentSpread",
                        name: "wallet_accountKey",
                        selections: a
                    },
                    l = [{
                        kind: "Literal",
                        name: "first",
                        value: 30
                    }],
                    i = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "tokenId",
                        storageKey: null
                    },
                    s = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "chain",
                        storageKey: null
                    },
                    r = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    d = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    },
                    c = [{
                        alias: null,
                        args: null,
                        concreteType: "AssetType",
                        kind: "LinkedField",
                        name: "asset",
                        plural: !1,
                        selections: [r],
                        storageKey: null
                    }, d],
                    o = [{
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    u = {
                        args: null,
                        kind: "FragmentSpread",
                        name: "AssetQuantity_data"
                    },
                    m = [{
                        alias: null,
                        args: o,
                        concreteType: "AssetQuantityTypeConnection",
                        kind: "LinkedField",
                        name: "assetQuantities",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetQuantityTypeEdge",
                            kind: "LinkedField",
                            name: "edges",
                            plural: !0,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetQuantityType",
                                kind: "LinkedField",
                                name: "node",
                                plural: !1,
                                selections: c,
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: "assetQuantities(first:1)"
                    }];
                return {
                    argumentDefinitions: [],
                    kind: "Fragment",
                    metadata: null,
                    name: "TradeStation_data",
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "OrderV2Type",
                        kind: "LinkedField",
                        name: "bestAsk",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "closedAt",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "dutchAuctionFinalPrice",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "openedAt",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "orderType",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "priceFnEndedAt",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "englishAuctionReservePrice",
                            storageKey: null
                        }, e, {
                            alias: null,
                            args: null,
                            concreteType: "AccountType",
                            kind: "LinkedField",
                            name: "maker",
                            plural: !1,
                            selections: [n, t],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "makerAssetBundle",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: l,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [e, i, {
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetContractType",
                                                kind: "LinkedField",
                                                name: "assetContract",
                                                plural: !1,
                                                selections: [s, n],
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                concreteType: "CollectionType",
                                                kind: "LinkedField",
                                                name: "collection",
                                                plural: !1,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    kind: "ScalarField",
                                                    name: "slug",
                                                    storageKey: null
                                                }, {
                                                    kind: "InlineDataFragmentSpread",
                                                    name: "verification_data",
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "isMintable",
                                                        storageKey: null
                                                    }, {
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "isSafelisted",
                                                        storageKey: null
                                                    }, {
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "isVerified",
                                                        storageKey: null
                                                    }]
                                                }],
                                                storageKey: null
                                            }, {
                                                kind: "InlineDataFragmentSpread",
                                                name: "itemEvents_data",
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "AssetContractType",
                                                    kind: "LinkedField",
                                                    name: "assetContract",
                                                    plural: !1,
                                                    selections: [n, s],
                                                    storageKey: null
                                                }, i]
                                            }],
                                            storageKey: null
                                        }, {
                                            kind: "InlineDataFragmentSpread",
                                            name: "quantity_data",
                                            selections: c
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:30)"
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "AccountType",
                            kind: "LinkedField",
                            name: "taker",
                            plural: !1,
                            selections: [t],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "takerAssetBundle",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: o,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [d, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "symbol",
                                                storageKey: null
                                            }, r, e, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "usdSpotPrice",
                                                storageKey: null
                                            }, {
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetContractType",
                                                kind: "LinkedField",
                                                name: "assetContract",
                                                plural: !1,
                                                selections: a,
                                                storageKey: null
                                            }, i],
                                            storageKey: null
                                        }, u],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:1)"
                            }],
                            storageKey: null
                        }, {
                            args: null,
                            kind: "FragmentSpread",
                            name: "AskPrice_data"
                        }, {
                            kind: "InlineDataFragmentSpread",
                            name: "orderLink_data",
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "makerAssetBundle",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: l,
                                    concreteType: "AssetQuantityTypeConnection",
                                    kind: "LinkedField",
                                    name: "assetQuantities",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityTypeEdge",
                                        kind: "LinkedField",
                                        name: "edges",
                                        plural: !0,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetQuantityType",
                                            kind: "LinkedField",
                                            name: "node",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "AssetType",
                                                kind: "LinkedField",
                                                name: "asset",
                                                plural: !1,
                                                selections: [{
                                                    alias: null,
                                                    args: null,
                                                    kind: "ScalarField",
                                                    name: "externalLink",
                                                    storageKey: null
                                                }, {
                                                    alias: null,
                                                    args: null,
                                                    concreteType: "CollectionType",
                                                    kind: "LinkedField",
                                                    name: "collection",
                                                    plural: !1,
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "externalUrl",
                                                        storageKey: null
                                                    }],
                                                    storageKey: null
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: "assetQuantities(first:30)"
                                }],
                                storageKey: null
                            }]
                        }, {
                            kind: "InlineDataFragmentSpread",
                            name: "quantity_remaining",
                            selections: [{
                                alias: "makerAsset",
                                args: null,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "makerAssetBundle",
                                plural: !1,
                                selections: m,
                                storageKey: null
                            }, {
                                alias: "takerAsset",
                                args: null,
                                concreteType: "AssetBundleType",
                                kind: "LinkedField",
                                name: "takerAssetBundle",
                                plural: !1,
                                selections: m,
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "remainingQuantity",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "side",
                                storageKey: null
                            }]
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        concreteType: "OrderV2Type",
                        kind: "LinkedField",
                        name: "bestBid",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "makerAssetBundle",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: o,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [d, u],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:1)"
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        args: null,
                        kind: "FragmentSpread",
                        name: "BidModalContent_trade"
                    }],
                    type: "TradeSummaryType",
                    abstractKey: null
                }
            }();
            t.hash = "7aadc3d84e102de8cb77e1cc0f5cc756", n.default = t
        },
        kEUh: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "address",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "chain",
                            storageKey: null
                        }],
                        storageKey: null
                    },
                    n = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    a = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    };
                return {
                    argumentDefinitions: [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "chain"
                    }, {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "identity"
                    }],
                    kind: "Fragment",
                    metadata: null,
                    name: "BidModalContent_archetype",
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "AssetType",
                        kind: "LinkedField",
                        name: "asset",
                        plural: !1,
                        selections: [e, n, a, {
                            alias: null,
                            args: null,
                            concreteType: "CollectionType",
                            kind: "LinkedField",
                            name: "collection",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "slug",
                                storageKey: null
                            }, {
                                alias: null,
                                args: [{
                                    kind: "Variable",
                                    name: "chain",
                                    variableName: "chain"
                                }],
                                concreteType: "PaymentAssetType",
                                kind: "LinkedField",
                                name: "paymentAssets",
                                plural: !0,
                                selections: [a, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetType",
                                    kind: "LinkedField",
                                    name: "asset",
                                    plural: !1,
                                    selections: [e, n, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "symbol",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "usdSpotPrice",
                                        storageKey: null
                                    }, a],
                                    storageKey: null
                                }, {
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "PaymentTokenInputV2_data"
                                }],
                                storageKey: null
                            }, {
                                kind: "InlineDataFragmentSpread",
                                name: "verification_data",
                                selections: [{
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isMintable",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isSafelisted",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "isVerified",
                                    storageKey: null
                                }]
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    }, {
                        alias: null,
                        args: [{
                            kind: "Variable",
                            name: "identity",
                            variableName: "identity"
                        }],
                        kind: "ScalarField",
                        name: "ownedQuantity",
                        storageKey: null
                    }],
                    type: "ArchetypeType",
                    abstractKey: null
                }
            }();
            t.hash = "dc8f8a487826d001c86e9a6cdb809572", n.default = t
        },
        "p+t5": function(e, n, a) {
            "use strict";
            a.r(n);
            var t = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: null,
                name: "PaymentAsset_data",
                selections: [{
                    alias: null,
                    args: null,
                    concreteType: "AssetType",
                    kind: "LinkedField",
                    name: "asset",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "chain",
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imageUrl",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    }],
                    storageKey: null
                }],
                type: "PaymentAssetType",
                abstractKey: null,
                hash: "9693ea45cbc1276e29837dbd5d9d8a1e"
            };
            n.default = t
        },
        qBnW: function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return l
            }));
            var t = a("iJ6s");

            function l(e) {
                return Object(t.a)({}, e)
            }
        },
        wQEM: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "expiration"
                    },
                    n = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "maker"
                    },
                    a = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "makerAssetQuantities"
                    },
                    t = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "openedAt"
                    },
                    l = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "taker"
                    },
                    i = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "takerAssetQuantities"
                    },
                    s = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "useMetaTransactions"
                    },
                    r = [{
                        kind: "Variable",
                        name: "expiration",
                        variableName: "expiration"
                    }, {
                        kind: "Variable",
                        name: "maker",
                        variableName: "maker"
                    }, {
                        kind: "Variable",
                        name: "makerAssets",
                        variableName: "makerAssetQuantities"
                    }, {
                        kind: "Variable",
                        name: "openedAt",
                        variableName: "openedAt"
                    }, {
                        kind: "Variable",
                        name: "taker",
                        variableName: "taker"
                    }, {
                        kind: "Variable",
                        name: "takerAssets",
                        variableName: "takerAssetQuantities"
                    }, {
                        kind: "Variable",
                        name: "useMetaTransactions",
                        variableName: "useMetaTransactions"
                    }],
                    d = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "blockExplorerUrl",
                        storageKey: null
                    },
                    c = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "displayName",
                        storageKey: null
                    },
                    o = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "identifier",
                        storageKey: null
                    },
                    u = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "publicRpcUrl",
                        storageKey: null
                    },
                    m = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "symbol",
                        storageKey: null
                    },
                    y = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    },
                    p = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    },
                    g = {
                        alias: null,
                        args: null,
                        concreteType: "PaymentAssetType",
                        kind: "LinkedField",
                        name: "nativeCurrency",
                        plural: !1,
                        selections: [m, {
                            alias: null,
                            args: null,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "asset",
                            plural: !1,
                            selections: [y, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "name",
                                storageKey: null
                            }, p],
                            storageKey: null
                        }, p],
                        storageKey: null
                    },
                    b = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "networkId",
                        storageKey: null
                    },
                    k = {
                        alias: null,
                        args: null,
                        concreteType: "ChainType",
                        kind: "LinkedField",
                        name: "chain",
                        plural: !1,
                        selections: [d, c, o, u, g, b, p],
                        storageKey: null
                    },
                    f = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "minQuantity",
                        storageKey: null
                    },
                    j = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    h = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "value",
                        storageKey: null
                    },
                    O = [h],
                    v = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "clientMessage",
                        storageKey: null
                    },
                    x = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "clientSignatureStandard",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: [e, n, a, t, l, i, s],
                        kind: "Fragment",
                        metadata: null,
                        name: "OrderCreateActionModalQuery",
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "BlockchainType",
                            kind: "LinkedField",
                            name: "blockchain",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: r,
                                concreteType: "ActionDataType",
                                kind: "LinkedField",
                                name: "orderCreateActions",
                                plural: !1,
                                selections: [{
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "ActionPanelList_data"
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [n, a, l, i, e, s, t],
                        kind: "Operation",
                        name: "OrderCreateActionModalQuery",
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "BlockchainType",
                            kind: "LinkedField",
                            name: "blockchain",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: r,
                                concreteType: "ActionDataType",
                                kind: "LinkedField",
                                name: "orderCreateActions",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "ActionType",
                                    kind: "LinkedField",
                                    name: "actions",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "actionType",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AskForDepositType",
                                        kind: "LinkedField",
                                        name: "askForDeposit",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [k, y, m, {
                                                alias: null,
                                                args: null,
                                                kind: "ScalarField",
                                                name: "usdSpotPrice",
                                                storageKey: null
                                            }, p],
                                            storageKey: null
                                        }, f],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "AskForSwapType",
                                        kind: "LinkedField",
                                        name: "askForSwap",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "fromAsset",
                                            plural: !1,
                                            selections: [k, j, y, m, p],
                                            storageKey: null
                                        }, f, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "maxQuantity",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "toAsset",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "ChainType",
                                                kind: "LinkedField",
                                                name: "chain",
                                                plural: !1,
                                                selections: [o, d, c, u, g, b, p],
                                                storageKey: null
                                            }, j, m, p],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "TransactionSubmissionDataType",
                                        kind: "LinkedField",
                                        name: "transaction",
                                        plural: !1,
                                        selections: [k, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "chainIdentifier",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AddressDataType",
                                            kind: "LinkedField",
                                            name: "source",
                                            plural: !1,
                                            selections: O,
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AddressDataType",
                                            kind: "LinkedField",
                                            name: "destination",
                                            plural: !1,
                                            selections: O,
                                            storageKey: null
                                        }, h, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "data",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "SignAndPostType",
                                        kind: "LinkedField",
                                        name: "signAndPost",
                                        plural: !1,
                                        selections: [k, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "orderData",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "serverSignature",
                                            storageKey: null
                                        }, v, x, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "orderId",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "MetaTransactionDataType",
                                        kind: "LinkedField",
                                        name: "metaTransaction",
                                        plural: !1,
                                        selections: [v, x, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "functionSignature",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "verifyingContract",
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "e79b37b25a3f84faacd0ed5ae45d1e6e",
                        id: null,
                        metadata: {},
                        name: "OrderCreateActionModalQuery",
                        operationKind: "query",
                        text: "query OrderCreateActionModalQuery(\n  $maker: IdentityInputType!\n  $makerAssetQuantities: [AssetQuantityInputType!]!\n  $taker: IdentityInputType\n  $takerAssetQuantities: [AssetQuantityInputType!]!\n  $expiration: Int\n  $useMetaTransactions: Boolean\n  $openedAt: DateTime\n) {\n  blockchain {\n    orderCreateActions(maker: $maker, makerAssets: $makerAssetQuantities, taker: $taker, takerAssets: $takerAssetQuantities, expiration: $expiration, useMetaTransactions: $useMetaTransactions, openedAt: $openedAt) {\n      ...ActionPanelList_data\n    }\n  }\n}\n\nfragment ActionPanelList_data on ActionDataType {\n  actions {\n    ...ActionPanel_data\n    actionType\n  }\n}\n\nfragment ActionPanel_data on ActionType {\n  actionType\n  askForDeposit {\n    asset {\n      chain {\n        ...chain_data\n        identifier\n        id\n      }\n      decimals\n      symbol\n      usdSpotPrice\n      id\n    }\n    minQuantity\n  }\n  askForSwap {\n    fromAsset {\n      chain {\n        ...chain_data\n        identifier\n        id\n      }\n      relayId\n      decimals\n      symbol\n      id\n    }\n    minQuantity\n    maxQuantity\n    toAsset {\n      chain {\n        identifier\n        ...chain_data\n        id\n      }\n      relayId\n      symbol\n      id\n    }\n  }\n  transaction {\n    chain {\n      ...chain_data\n      identifier\n      id\n    }\n    ...trader_transaction\n  }\n  signAndPost {\n    chain {\n      ...chain_data\n      id\n    }\n    ...trader_sign_and_post\n  }\n  ...trader_meta_transaction\n}\n\nfragment chain_data on ChainType {\n  blockExplorerUrl\n  displayName\n  identifier\n  publicRpcUrl\n  nativeCurrency {\n    symbol\n    asset {\n      decimals\n      name\n      id\n    }\n    id\n  }\n  networkId\n}\n\nfragment trader_meta_transaction on ActionType {\n  metaTransaction {\n    clientMessage\n    clientSignatureStandard\n    functionSignature\n    verifyingContract\n  }\n}\n\nfragment trader_sign_and_post on SignAndPostType {\n  chain {\n    ...chain_data\n    id\n  }\n  orderData\n  serverSignature\n  clientMessage\n  clientSignatureStandard\n  orderId\n}\n\nfragment trader_transaction on TransactionSubmissionDataType {\n  chainIdentifier\n  source {\n    value\n  }\n  destination {\n    value\n  }\n  value\n  data\n}\n"
                    }
                }
            }();
            t.hash = "1a016b54779649e036ce16e68fe7413e", n.default = t
        },
        "x/dt": function(e, n, a) {
            "use strict";
            var t, l = a("mXGw"),
                i = a("Hgoe"),
                s = a("LoMF"),
                r = a("ZwbU"),
                d = a("6Ojl"),
                c = a("Nbt0"),
                o = a("a7GP"),
                u = a("lxYa"),
                m = a("oYCi");
            n.a = Object(o.b)((function(e) {
                var n = e.data,
                    a = e.onClose,
                    t = e.onOrderCancelled,
                    o = Object(d.b)().onPrevious,
                    y = Object(c.a)().showSuccessMessage,
                    p = Object(l.useState)(!1),
                    g = p[0],
                    b = p[1];
                if (!n) return Object(m.jsx)(i.a, {});
                var k = n.order,
                    f = k.cancelActions,
                    j = k.orderType,
                    h = "ASK" === k.side ? "listing" : "ENGLISH" === j ? "bid" : "offer";
                return Object(m.jsx)(m.Fragment, {
                    children: g ? Object(m.jsxs)(m.Fragment, {
                        children: [Object(m.jsx)(r.a.Header, {
                            onPrevious: function() {
                                return b(!1)
                            },
                            children: Object(m.jsxs)(r.a.Title, {
                                children: ["Cancel your ", h]
                            })
                        }), Object(m.jsx)(r.a.Body, {
                            children: Object(m.jsx)(u.a, {
                                data: f,
                                onEnd: function() {
                                    var e = "Your ".concat(h, " was cancelled successfully!");
                                    a(), y(e), null === t || void 0 === t || t()
                                }
                            })
                        })]
                    }) : Object(m.jsxs)(m.Fragment, {
                        children: [Object(m.jsx)(r.a.Header, {
                            onPrevious: o,
                            children: Object(m.jsxs)(r.a.Title, {
                                children: ["Are you sure you want to cancel your ", h, "?"]
                            })
                        }), Object(m.jsx)(r.a.Footer, {
                            children: Object(m.jsx)(s.c, {
                                onClick: function() {
                                    return b(!0)
                                },
                                children: "Confirm"
                            })
                        })]
                    })
                })
            }), void 0 !== t ? t : t = a("7rjq"))
        },
        x1jb: function(e, n, a) {
            "use strict";
            a.r(n);
            var t = function() {
                var e = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "relayId",
                        storageKey: null
                    },
                    n = [{
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    a = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "quantity",
                        storageKey: null
                    },
                    t = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "decimals",
                        storageKey: null
                    };
                return {
                    argumentDefinitions: [],
                    kind: "Fragment",
                    metadata: null,
                    name: "BidModalContent_trade",
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "OrderV2Type",
                        kind: "LinkedField",
                        name: "bestAsk",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "closedAt",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "isFulfillable",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "oldOrder",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "orderType",
                            storageKey: null
                        }, e, {
                            alias: null,
                            args: null,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "makerAssetBundle",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: [{
                                    kind: "Literal",
                                    name: "first",
                                    value: 30
                                }],
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [{
                                                alias: null,
                                                args: null,
                                                concreteType: "CollectionType",
                                                kind: "LinkedField",
                                                name: "collection",
                                                plural: !1,
                                                selections: [{
                                                    kind: "InlineDataFragmentSpread",
                                                    name: "verification_data",
                                                    selections: [{
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "isMintable",
                                                        storageKey: null
                                                    }, {
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "isSafelisted",
                                                        storageKey: null
                                                    }, {
                                                        alias: null,
                                                        args: null,
                                                        kind: "ScalarField",
                                                        name: "isVerified",
                                                        storageKey: null
                                                    }]
                                                }],
                                                storageKey: null
                                            }],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:30)"
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "takerAssetBundle",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: n,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [a, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [t, e],
                                            storageKey: null
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:1)"
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        concreteType: "OrderV2Type",
                        kind: "LinkedField",
                        name: "bestBid",
                        plural: !1,
                        selections: [e, {
                            alias: null,
                            args: null,
                            concreteType: "AssetBundleType",
                            kind: "LinkedField",
                            name: "makerAssetBundle",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: n,
                                concreteType: "AssetQuantityTypeConnection",
                                kind: "LinkedField",
                                name: "assetQuantities",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetQuantityTypeEdge",
                                    kind: "LinkedField",
                                    name: "edges",
                                    plural: !0,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetQuantityType",
                                        kind: "LinkedField",
                                        name: "node",
                                        plural: !1,
                                        selections: [a, {
                                            alias: null,
                                            args: null,
                                            concreteType: "AssetType",
                                            kind: "LinkedField",
                                            name: "asset",
                                            plural: !1,
                                            selections: [t],
                                            storageKey: null
                                        }, {
                                            args: null,
                                            kind: "FragmentSpread",
                                            name: "AssetQuantity_data"
                                        }],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: "assetQuantities(first:1)"
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    type: "TradeSummaryType",
                    abstractKey: null
                }
            }();
            t.hash = "1f90e5660f5f518a932319414598ec49", n.default = t
        },
        "z2//": function(e, n, a) {
            "use strict";
            a.d(n, "a", (function() {
                return s
            }));
            var t = a("YYXE"),
                l = a("rx0e"),
                i = a("BOW+");

            function s(e, n) {
                Object(i.a)(2, arguments);
                var a = Object(t.a)(n);
                return Object(l.a)(e, -a)
            }
        }
    },
    [
        [25, 2, 1, 6, 4, 3, 7, 9, 0, 5, 8, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22, 26, 27, 29]
    ]
]);
//# sourceMappingURL=bundle-37ccd440728aa5240ffa.js.map