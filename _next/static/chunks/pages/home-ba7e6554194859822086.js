_N_E = (window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [69], {
        "2A7z": function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return F
            }));
            var n, r = a("m6w3"),
                i = a("uEoR"),
                l = a("mXGw"),
                o = a("Ld9l"),
                s = a.n(o),
                c = a("aXrf"),
                d = a("UutA"),
                u = a("m5he"),
                m = a("uMSw"),
                h = a("b7Z7"),
                p = a("67yl"),
                g = a("5apE"),
                b = a("n0tG"),
                j = a("eV01"),
                x = a("u6YR"),
                y = a("B6yL"),
                f = a("D4YM"),
                O = a("C/iq"),
                w = a("oYCi");

            function v(e, t) {
                var a = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var n = Object.getOwnPropertySymbols(e);
                    t && (n = n.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), a.push.apply(a, n)
                }
                return a
            }

            function k(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var a = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? v(Object(a), !0).forEach((function(t) {
                        Object(r.a)(e, t, a[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(a)) : v(Object(a)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(a, t))
                    }))
                }
                return e
            }
            var C = s()((function() {
                    return Promise.all([a.e(33), a.e(37)]).then(a.bind(null, "PHJS")).then((function(e) {
                        return e.ModelScene
                    }))
                }), {
                    ssr: !1,
                    loadableGenerated: {
                        webpack: function() {
                            return ["PHJS"]
                        },
                        modules: ["../components/assets/AssetMedia/AssetMedia.react.tsx -> components/viz/ModelScene.react"]
                    }
                }),
                F = function(e) {
                    var t, r, o, s = e.asset,
                        d = e.autoPlay,
                        f = e.useCustomPlayButton,
                        v = e.isMuted,
                        F = e.showModel,
                        N = e.showControls,
                        A = e.mediaStyles,
                        D = e.size,
                        K = e.alt,
                        L = e.className,
                        M = e.title,
                        U = e.width,
                        E = Object(l.useState)(!1),
                        B = E[0],
                        R = E[1],
                        P = Object(l.useRef)(null),
                        I = Object(g.b)().theme,
                        V = Object(l.useRef)(null),
                        z = Object(j.a)(V),
                        q = Object(i.a)(z, 1)[0],
                        H = Object(c.useFragment)(void 0 !== n ? n : n = a("DEU0"), s),
                        G = H.backgroundColor ? "#".concat(H.backgroundColor) : void 0,
                        Y = function() {
                            return !d && f ? Object(w.jsx)(u.a, {
                                className: "AssetMedia--play-icon",
                                size: 24,
                                value: B ? "pause" : "play_arrow",
                                onClick: function(e) {
                                    var t, a;
                                    e.preventDefault(), B ? null === (t = P.current) || void 0 === t || t.pause() : null === (a = P.current) || void 0 === a || a.play(), R((function(e) {
                                        return !e
                                    }))
                                }
                            }) : null
                        },
                        Q = function(e) {
                            var t, a = e.animationUrl,
                                n = e.imagePreviewUrl,
                                r = e.cardDisplayStyle,
                                i = d || B || !n;
                            return Object(w.jsx)("div", {
                                className: "AssetMedia--animation",
                                children: Object(y.j)(a) ? F ? Object(w.jsx)(h.a, {
                                    minHeight: "500px",
                                    width: "100%",
                                    children: Object(w.jsx)(C, {
                                        backgroundColor: G,
                                        url: a
                                    })
                                }) : n ? W(n, r) : null : Object(y.i)(a) ? Object(w.jsxs)(p.a, {
                                    className: "AssetMedia--playback-wrapper",
                                    style: {
                                        backgroundColor: G
                                    },
                                    onContextMenu: function(e) {
                                        return e.preventDefault()
                                    },
                                    children: [i ? Object(w.jsx)("video", {
                                        autoPlay: d || B,
                                        className: "AssetMedia--video",
                                        controls: N,
                                        controlsList: "nodownload",
                                        loop: !0,
                                        muted: v,
                                        playsInline: !0,
                                        poster: n,
                                        preload: "metadata",
                                        ref: P,
                                        style: A,
                                        children: Object(w.jsx)("source", {
                                            src: "".concat(a, "#t=0.001"),
                                            type: "video/".concat(Object(y.b)(a))
                                        })
                                    }) : n ? W(n, r) : null, Object(w.jsx)("div", {
                                        children: Object(y.j)(a) || !Object(y.i)(a) && !Object(y.k)(a) ? null : Y()
                                    })]
                                }) : Object(w.jsx)(w.Fragment, {
                                    children: i ? Object(w.jsx)("iframe", {
                                        allow: "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture",
                                        allowFullScreen: !0,
                                        frameBorder: "0",
                                        height: "100%",
                                        sandbox: "".concat(Object(y.g)(a) ? "allow-same-origin " : "", "allow-scripts"),
                                        src: (t = a, t.replace("youtu.be/", "www.youtube.com/watch?v=").replace(/\/watch\?v=([A-Za-z0-9_-]+\??)/, "/embed/$1?autoplay=1&controls=0&loop=1&playlist=$1&")),
                                        style: {
                                            minHeight: "500px"
                                        },
                                        width: "100%"
                                    }) : n ? W(n, r) : null
                                })
                            })
                        },
                        W = function(e, t) {
                            var a = "COVER" === t ? "cover" : "CONTAIN" === t ? "contain" : void 0;
                            return Object(w.jsx)(m.a, {
                                alt: K,
                                className: Object(x.a)("AssetMedia", {
                                    img: !0,
                                    invert: "dark" === I && e === O.Kb
                                }),
                                fade: !0,
                                imgStyle: k({
                                    width: "cover" === a ? "100%" : "auto",
                                    height: "cover" === a ? "100%" : "auto",
                                    maxWidth: "100%",
                                    maxHeight: "100%"
                                }, A),
                                isSpinnerShown: !0,
                                size: D,
                                sizing: a,
                                url: e,
                                width: U
                            })
                        },
                        X = H.animationUrl,
                        J = H.collection,
                        Z = H.displayImageUrl,
                        $ = H.imageUrl,
                        ee = null !== (t = null === (r = J.displayData) || void 0 === r ? void 0 : r.cardDisplayStyle) && void 0 !== t ? t : "CONTAIN",
                        te = X && !Object(y.d)(X) ? X : Z && Object(y.i)(Z) ? Z : void 0,
                        ae = X && Object(y.e)(X) ? X : void 0,
                        ne = X && Object(y.d)(X) ? X : Z || O.Kb;
                    return H.isDelisted ? Object(w.jsx)(m.a, {
                        alt: "Delisted item image",
                        className: Object(x.a)("AssetMedia", {
                            img: !0,
                            invert: "dark" === I
                        }),
                        fade: !0,
                        imgStyle: {
                            width: "100%",
                            height: "100%",
                            maxWidth: "100%",
                            maxHeight: "100%"
                        },
                        size: D,
                        sizing: "cover",
                        url: O.C,
                        width: U
                    }) : (o = ae && ne ? function(e, t, a) {
                        return Object(w.jsx)("div", {
                            className: "AssetMedia--animation",
                            children: Object(w.jsxs)("div", {
                                className: "AssetMedia--playback-wrapper",
                                style: {
                                    backgroundColor: G
                                },
                                onContextMenu: function(e) {
                                    return e.preventDefault()
                                },
                                children: [W(t, a), Object(w.jsx)("audio", {
                                    autoPlay: d,
                                    className: "AssetMedia--audio",
                                    controls: N,
                                    controlsList: "nodownload",
                                    loop: !0,
                                    muted: v,
                                    preload: "none",
                                    ref: P,
                                    src: e
                                }), Y()]
                            })
                        })
                    }(ae, ne, ee) : Object(y.i)(ne) ? Q({
                        animationUrl: ne,
                        cardDisplayStyle: ee
                    }) : te ? Q({
                        animationUrl: te,
                        imagePreviewUrl: ne,
                        cardDisplayStyle: ee
                    }) : !$ && Z ? function(e, t) {
                        var a = "COVER" === t ? "cover" : "CONTAIN" === t ? "contain" : void 0,
                            n = Object(y.m)(ne, {
                                freezeAnimation: !0
                            }),
                            r = {
                                backgroundImage: "url(".concat(n, ")")
                            },
                            i = q < 50 ? 35 : q < 130 ? 60 : q < 300 ? 85 : 150,
                            l = q > 130;
                        return Object(w.jsxs)(S, {
                            ref: V,
                            children: [Object(w.jsx)(_, {
                                style: r
                            }), Object(w.jsxs)(p.a, {
                                height: "100%",
                                position: "absolute",
                                children: [Object(w.jsx)(m.a, {
                                    alt: K,
                                    className: Object(x.a)("AssetMedia", {
                                        "placeholder-img": !0
                                    }),
                                    fade: !0,
                                    imgStyle: {
                                        position: "absolute",
                                        top: "0",
                                        left: "0",
                                        maxWidth: "none",
                                        objectFit: "cover"
                                    },
                                    isSpinnerShown: !0,
                                    size: i,
                                    sizing: a,
                                    url: e,
                                    variant: "round"
                                }), l && Object(w.jsx)(b.a, {
                                    className: "AssetMedia--text",
                                    textAlign: "center",
                                    variant: "info",
                                    children: "Content not available yet"
                                })]
                            })]
                        })
                    }(ne, ee) : W(ne, ee), Object(w.jsx)(T, {
                        className: L,
                        title: M,
                        children: Object(w.jsx)(p.a, {
                            borderRadius: "inherit",
                            height: "100%",
                            minHeight: "inherit",
                            position: "relative",
                            style: {
                                backgroundColor: G
                            },
                            width: "100%",
                            children: o
                        })
                    }))
                },
                _ = d.d.div.withConfig({
                    displayName: "AssetMediareact__PlaceholderBackground",
                    componentId: "sc-1v86bfg-0"
                })(["background-size:cover;background-position:center;background-repeat:none;border-radius:inherit;width:100%;height:0px;padding:50% 0;filter:blur(30px);mask:linear-gradient( 0deg,rgba(53,56,64,1) 0%,rgba(53,56,64,0.4) 100% );overflow:hidden;"]),
                S = Object(d.d)(p.a).withConfig({
                    displayName: "AssetMediareact__PlaceholderContainer",
                    componentId: "sc-1v86bfg-1"
                })(["position:relative;width:100%;background-color:", ";border-radius:inherit;.AssetMedia--text{color:", ";width:100%;text-shadow:0px 4px 4px rgba(0,0,0,0.25);padding:0 10px;}.AssetMedia--placeholder-img{position:relative;overflow:hidden;> img{border-radius:inherit;}}"], (function(e) {
                    return e.theme.colors.background
                }), (function(e) {
                    return e.theme.colors.white
                })),
                T = d.d.div.withConfig({
                    displayName: "AssetMediareact__DivContainer",
                    componentId: "sc-1v86bfg-2"
                })(["min-height:inherit;border-radius:inherit;height:100%;width:100%;.AssetMedia--animation{position:relative;min-height:inherit;max-height:100%;height:100%;width:100%;display:flex;flex-direction:column;justify-content:center;align-items:center;border-radius:inherit;.AssetMedia--playback-wrapper{height:100%;max-width:100%;width:100%;position:relative;justify-content:center;border-radius:inherit;.AssetMedia--audio{width:100%;outline:none;}.AssetMedia--video{height:100%;width:100%;}> img{border-radius:inherit;}}}.AssetMedia--play-icon,.AssetMedia--play-shadow{position:absolute;}.AssetMedia--play-icon{align-items:center;border-radius:50%;border:1px solid ", ";display:flex;height:32px;justify-content:center;bottom:8px;right:8px;width:32px;z-index:1;", " &:hover{box-shadow:", ";}}.AssetMedia--play-shadow{width:26px;height:26px;right:11px;bottom:11px;border-radius:50%;box-shadow:", ";pointer-events:none;}.AssetMedia--img{border-radius:inherit;> img{border-radius:inherit;}}.AssetMedia--invert{filter:grayscale(1) invert(1);}"], (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return Object(f.b)({
                        variants: {
                            light: {
                                color: e.theme.colors.gray,
                                backgroundColor: e.theme.colors.white,
                                "&:hover": {
                                    color: e.theme.colors.oil
                                }
                            },
                            dark: {
                                color: e.theme.colors.fog,
                                backgroundColor: e.theme.colors.oil,
                                "&:hover": {
                                    color: e.theme.colors.white,
                                    backgroundColor: e.theme.colors.ash
                                }
                            }
                        }
                    })
                }), (function(e) {
                    return e.theme.shadows.default
                }), (function(e) {
                    return e.theme.shadows.default
                }))
        },
        "3+lx": function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return c
            }));
            a("mXGw");
            var n = a("UutA"),
                r = a("vkv6"),
                i = a("67yl"),
                l = a("j/Wi"),
                o = a("D4YM"),
                s = a("oYCi"),
                c = function(e) {
                    var t = e.size,
                        a = void 0 === t ? 14 : t;
                    return Object(s.jsx)(l.b, {
                        content: "ETH",
                        children: Object(s.jsx)(i.a, {
                            cursor: "pointer",
                            children: Object(s.jsx)(d, {
                                alt: "Ether symbol",
                                size: a
                            })
                        })
                    })
                },
                d = Object(n.d)(r.b).withConfig({
                    displayName: "EthLogoreact__EthAvatar",
                    componentId: "sc-bgaajd-0"
                })(["", ""], Object(o.b)({
                    variants: {
                        dark: {
                            filter: "brightness(3)"
                        }
                    }
                }));
            d.defaultProps = {
                src: "https://storage.opensea.io/files/6f8e2979d428180222796ff4a33ab929.svg"
            }
        },
        41: function(e, t, a) {
            a("xhzY"), e.exports = a("MFWt")
        },
        "4TjO": function(e, t, a) {
            "use strict";
            a.r(t);
            var n = function() {
                var e = [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "sortBy"
                    }],
                    t = {
                        kind: "Variable",
                        name: "sortBy",
                        variableName: "sortBy"
                    },
                    a = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: e,
                        kind: "Fragment",
                        metadata: null,
                        name: "TopCollectionsLazyQuery",
                        selections: [{
                            args: [t],
                            kind: "FragmentSpread",
                            name: "TopCollections_data"
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: e,
                        kind: "Operation",
                        name: "TopCollectionsLazyQuery",
                        selections: [{
                            alias: null,
                            args: [{
                                kind: "Literal",
                                name: "first",
                                value: 15
                            }, {
                                kind: "Literal",
                                name: "includeHidden",
                                value: !0
                            }, {
                                kind: "Literal",
                                name: "sortAscending",
                                value: !1
                            }, t],
                            concreteType: "CollectionTypeConnection",
                            kind: "LinkedField",
                            name: "collections",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "CollectionTypeEdge",
                                kind: "LinkedField",
                                name: "edges",
                                plural: !0,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "CollectionType",
                                    kind: "LinkedField",
                                    name: "node",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "slug",
                                        storageKey: null
                                    }, a, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "name",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "logo",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "createdDate",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isVerified",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "CollectionStatsType",
                                        kind: "LinkedField",
                                        name: "stats",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "oneDayChange",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "oneDayVolume",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "sevenDayChange",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "sevenDayVolume",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "thirtyDayChange",
                                            storageKey: null
                                        }, {
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "thirtyDayVolume",
                                            storageKey: null
                                        }, a],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "5e5f8c491992a14df6af16447cc89f90",
                        id: null,
                        metadata: {},
                        name: "TopCollectionsLazyQuery",
                        operationKind: "query",
                        text: "query TopCollectionsLazyQuery(\n  $sortBy: CollectionSort\n) {\n  ...TopCollections_data_34jhwD\n}\n\nfragment TopCollections_data_34jhwD on Query {\n  collections(first: 15, sortBy: $sortBy, sortAscending: false, includeHidden: true) {\n    edges {\n      node {\n        slug\n        id\n        name\n        logo\n        createdDate\n        isVerified\n        stats {\n          oneDayChange\n          oneDayVolume\n          sevenDayChange\n          sevenDayVolume\n          thirtyDayChange\n          thirtyDayVolume\n          id\n        }\n      }\n    }\n  }\n}\n"
                    }
                }
            }();
            n.hash = "54074de55447e194b4a0d497071360dc", t.default = n
        },
        "4iFg": function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return s
            }));
            a("mXGw");
            var n = a("3+lx"),
                r = a("QYJX"),
                i = a("3uPl"),
                l = a("QrBS"),
                o = a("oYCi"),
                s = function(e) {
                    var t = e.value,
                        a = e.textVariant,
                        s = e.color,
                        c = e.fontSize,
                        d = !(void 0 === t || "0" === t);
                    return Object(o.jsxs)(l.a, {
                        alignItems: "center",
                        children: [d && Object(o.jsx)(l.a, {
                            alignItems: "center",
                            display: "inline-flex",
                            height: "22px",
                            marginRight: "4px",
                            children: Object(o.jsx)(n.a, {})
                        }), Object(o.jsx)(i.a.StatText, {
                            as: "span",
                            color: s,
                            fontSize: c,
                            variant: null !== a && void 0 !== a ? a : "small",
                            children: Object(o.jsx)(r.b, {
                                children: d ? t : "---"
                            })
                        })]
                    })
                }
        },
        "9U3r": function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return p
            })), a.d(t, "b", (function() {
                return g
            }));
            a("mXGw");
            var n = a("UutA"),
                r = a("Q5Gx"),
                i = a("z1wA"),
                l = a("b7Z7"),
                o = a("LoMF"),
                s = a("67yl"),
                c = a("QrBS"),
                d = a("n0tG"),
                u = a("C/iq"),
                m = a("D4YM"),
                h = a("oYCi"),
                p = "meetopensea",
                g = function(e) {
                    var t = e.showButton;
                    return Object(h.jsx)(b, {
                        id: p,
                        children: Object(h.jsx)(j, {
                            children: Object(h.jsxs)(x, {
                                children: [Object(h.jsx)(y, {
                                    children: "Meet OpenSea"
                                }), Object(h.jsx)(f, {
                                    children: "The NFT marketplace with everything for everyone"
                                }), Object(h.jsxs)(O, {
                                    children: [Object(h.jsx)(i.a, {
                                        title: "Meet OpenSea",
                                        url: "https://www.youtube.com/watch?v=gfGuPd1CELo&cc_load_policy=3"
                                    }), Object(h.jsx)(d.a, {
                                        textAlign: "center",
                                        variant: "info",
                                        children: "Fiat on-ramp and profile customization is coming soon."
                                    })]
                                }), t ? Object(h.jsx)(w, {
                                    children: Object(h.jsx)(o.c, {
                                        className: "MeetOpenSea--button",
                                        href: "/explore-collections",
                                        children: "Explore the marketplace"
                                    })
                                }) : null]
                            })
                        })
                    })
                },
                b = Object(n.d)(l.a).withConfig({
                    displayName: "MeetOpenSeareact__AnchorContainer",
                    componentId: "sc-b32ykt-0"
                })(["padding-top:40px;width:100%;"]),
                j = Object(n.d)(s.a).withConfig({
                    displayName: "MeetOpenSeareact__Section",
                    componentId: "sc-b32ykt-1"
                })(["width:100%;padding-top:40px;padding-bottom:40px;", " ", ";"], Object(r.e)({
                    medium: Object(n.c)(["padding-bottom:0;"])
                }), (function(e) {
                    var t = e.theme;
                    return Object(m.b)({
                        variants: {
                            light: {
                                "background-color": t.colors.lightMarina
                            },
                            dark: {
                                "background-color": t.colors.onyx
                            }
                        }
                    })
                })),
                x = Object(n.d)(l.a).withConfig({
                    displayName: "MeetOpenSeareact__Container",
                    componentId: "sc-b32ykt-2"
                })(["max-width:", ";width:100%;"], (function(e) {
                    return e.theme.maxWidth.largePadding
                })),
                y = Object(n.d)(d.a).attrs({
                    variant: "h2"
                }).withConfig({
                    displayName: "MeetOpenSeareact__Title",
                    componentId: "sc-b32ykt-3"
                })(["font-size:32px;margin-bottom:8px;margin-top:0;text-align:center;width:100%;", ""], Object(r.e)({
                    phoneXl: Object(n.c)(["font-size:40px;"])
                })),
                f = Object(n.d)(d.a).withConfig({
                    displayName: "MeetOpenSeareact__Subtitle",
                    componentId: "sc-b32ykt-4"
                })(["text-align:center;margin-bottom:40px;", ""], Object(r.e)({
                    medium: Object(n.c)(["margin-bottom:20px;"])
                })),
                O = Object(n.d)(l.a).withConfig({
                    displayName: "MeetOpenSeareact__VideoContainer",
                    componentId: "sc-b32ykt-5"
                })(["", ""], Object(r.e)({
                    medium: Object(n.c)(["background:url(", ") center / contain no-repeat;padding:40px 100px 80px;"], u.jc)
                })),
                w = Object(n.d)(c.a).withConfig({
                    displayName: "MeetOpenSeareact__ButtonContainer",
                    componentId: "sc-b32ykt-6"
                })(["justify-content:center;margin-bottom:0;margin-top:40px;.MeetOpenSea--button{text-align:center;}", ""], Object(r.e)({
                    medium: Object(n.c)(["margin-bottom:80px;margin-top:0;"])
                }))
        },
        Aujd: function(e, t, a) {
            "use strict";
            a.r(t);
            var n = function() {
                var e = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imageUrl",
                        storageKey: null
                    },
                    t = [{
                        alias: null,
                        args: null,
                        concreteType: "AssetContractType",
                        kind: "LinkedField",
                        name: "assetContract",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "address",
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "chain",
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "tokenId",
                        storageKey: null
                    }];
                return {
                    argumentDefinitions: [],
                    kind: "Fragment",
                    metadata: null,
                    name: "Featured_data",
                    selections: [{
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "name",
                        storageKey: null
                    }, {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imagePreviewUrl",
                        storageKey: null
                    }, e, {
                        alias: null,
                        args: null,
                        concreteType: "AccountType",
                        kind: "LinkedField",
                        name: "creator",
                        plural: !1,
                        selections: [e, {
                            alias: null,
                            args: null,
                            concreteType: "UserType",
                            kind: "LinkedField",
                            name: "user",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "publicUsername",
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }, {
                        kind: "InlineDataFragmentSpread",
                        name: "asset_url",
                        selections: t
                    }, {
                        args: null,
                        kind: "FragmentSpread",
                        name: "AssetMedia_asset"
                    }, {
                        kind: "InlineDataFragmentSpread",
                        name: "itemEvents_data",
                        selections: t
                    }],
                    type: "AssetType",
                    abstractKey: null
                }
            }();
            n.hash = "ae43f6107aabc2c2064a6d9b29d8c9f4", t.default = n
        },
        DEU0: function(e, t, a) {
            "use strict";
            a.r(t);
            var n = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: null,
                name: "AssetMedia_asset",
                selections: [{
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "animationUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "backgroundColor",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    concreteType: "CollectionType",
                    kind: "LinkedField",
                    name: "collection",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "DisplayDataType",
                        kind: "LinkedField",
                        name: "displayData",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "cardDisplayStyle",
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "isDelisted",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "imageUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "displayImageUrl",
                    storageKey: null
                }],
                type: "AssetType",
                abstractKey: null,
                hash: "a6846ccbe6f7f6f19a41fe8169ee4b20"
            };
            t.default = n
        },
        DQ1a: function(e, t, a) {
            "use strict";
            a.r(t);
            var n = {
                argumentDefinitions: [{
                    defaultValue: 15,
                    kind: "LocalArgument",
                    name: "count"
                }, {
                    defaultValue: !0,
                    kind: "LocalArgument",
                    name: "includeHidden"
                }, {
                    defaultValue: !1,
                    kind: "LocalArgument",
                    name: "sortAscending"
                }, {
                    defaultValue: "SEVEN_DAY_VOLUME",
                    kind: "LocalArgument",
                    name: "sortBy"
                }],
                kind: "Fragment",
                metadata: null,
                name: "TopCollections_data",
                selections: [{
                    alias: null,
                    args: [{
                        kind: "Variable",
                        name: "first",
                        variableName: "count"
                    }, {
                        kind: "Variable",
                        name: "includeHidden",
                        variableName: "includeHidden"
                    }, {
                        kind: "Variable",
                        name: "sortAscending",
                        variableName: "sortAscending"
                    }, {
                        kind: "Variable",
                        name: "sortBy",
                        variableName: "sortBy"
                    }],
                    concreteType: "CollectionTypeConnection",
                    kind: "LinkedField",
                    name: "collections",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "CollectionTypeEdge",
                        kind: "LinkedField",
                        name: "edges",
                        plural: !0,
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "CollectionType",
                            kind: "LinkedField",
                            name: "node",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "slug",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "id",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "name",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "logo",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "createdDate",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "isVerified",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "CollectionStatsType",
                                kind: "LinkedField",
                                name: "stats",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "oneDayChange",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "oneDayVolume",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "sevenDayChange",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "sevenDayVolume",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "thirtyDayChange",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "thirtyDayVolume",
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    storageKey: null
                }],
                type: "Query",
                abstractKey: null,
                hash: "8cbe5710781782282aa635e240326cf3"
            };
            t.default = n
        },
        HAVD: function(e, t, a) {
            "use strict";
            (function(e) {
                const a = {},
                    n = t => "undefined" !== typeof self && self && t in self ? self : "undefined" !== typeof window && window && t in window ? window : "undefined" !== typeof e && e && t in e ? e : "undefined" !== typeof globalThis && globalThis ? globalThis : void 0,
                    r = ["Headers", "Request", "Response", "ReadableStream", "fetch", "AbortController", "FormData"];
                for (const t of r) Object.defineProperty(a, t, {
                    get() {
                        const e = n(t),
                            a = e && e[t];
                        return "function" === typeof a ? a.bind(e) : a
                    }
                });
                const i = e => null !== e && "object" === typeof e,
                    l = "function" === typeof a.AbortController,
                    o = "function" === typeof a.ReadableStream,
                    s = "function" === typeof a.FormData,
                    c = (e, t) => {
                        const n = new a.Headers(e || {}),
                            r = t instanceof a.Headers,
                            i = new a.Headers(t || {});
                        for (const [a, l] of i) r && "undefined" === l || void 0 === l ? n.delete(a) : n.set(a, l);
                        return n
                    },
                    d = (...e) => {
                        let t = {},
                            a = {};
                        for (const n of e) {
                            if (Array.isArray(n)) Array.isArray(t) || (t = []), t = [...t, ...n];
                            else if (i(n)) {
                                for (let [e, a] of Object.entries(n)) i(a) && e in t && (a = d(t[e], a)), t = { ...t,
                                    [e]: a
                                };
                                i(n.headers) && (a = c(a, n.headers))
                            }
                            t.headers = a
                        }
                        return t
                    },
                    u = ["get", "post", "put", "patch", "head", "delete"],
                    m = {
                        json: "application/json",
                        text: "text/*",
                        formData: "multipart/form-data",
                        arrayBuffer: "*/*",
                        blob: "*/*"
                    },
                    h = [413, 429, 503],
                    p = Symbol("stop");
                class g extends Error {
                    constructor(e) {
                        super(e.statusText || String(0 === e.status || e.status ? e.status : "Unknown response error")), this.name = "HTTPError", this.response = e
                    }
                }
                class b extends Error {
                    constructor(e) {
                        super("Request timed out"), this.name = "TimeoutError", this.request = e
                    }
                }
                const j = e => new Promise((t => setTimeout(t, e))),
                    x = e => u.includes(e) ? e.toUpperCase() : e,
                    y = {
                        limit: 2,
                        methods: ["get", "put", "head", "delete", "options", "trace"],
                        statusCodes: [408, 413, 429, 500, 502, 503, 504],
                        afterStatusCodes: h
                    },
                    f = (e = {}) => {
                        if ("number" === typeof e) return { ...y,
                            limit: e
                        };
                        if (e.methods && !Array.isArray(e.methods)) throw new Error("retry.methods must be an array");
                        if (e.statusCodes && !Array.isArray(e.statusCodes)) throw new Error("retry.statusCodes must be an array");
                        return { ...y,
                            ...e,
                            afterStatusCodes: h
                        }
                    },
                    O = 2147483647;
                class w {
                    constructor(e, t = {}) {
                        if (this._retryCount = 0, this._input = e, this._options = {
                                credentials: this._input.credentials || "same-origin",
                                ...t,
                                headers: c(this._input.headers, t.headers),
                                hooks: d({
                                    beforeRequest: [],
                                    beforeRetry: [],
                                    afterResponse: []
                                }, t.hooks),
                                method: x(t.method || this._input.method),
                                prefixUrl: String(t.prefixUrl || ""),
                                retry: f(t.retry),
                                throwHttpErrors: !1 !== t.throwHttpErrors,
                                timeout: "undefined" === typeof t.timeout ? 1e4 : t.timeout,
                                fetch: t.fetch || a.fetch
                            }, "string" !== typeof this._input && !(this._input instanceof URL || this._input instanceof a.Request)) throw new TypeError("`input` must be a string, URL, or Request");
                        if (this._options.prefixUrl && "string" === typeof this._input) {
                            if (this._input.startsWith("/")) throw new Error("`input` must not begin with a slash when using `prefixUrl`");
                            this._options.prefixUrl.endsWith("/") || (this._options.prefixUrl += "/"), this._input = this._options.prefixUrl + this._input
                        }
                        if (l && (this.abortController = new a.AbortController, this._options.signal && this._options.signal.addEventListener("abort", (() => {
                                this.abortController.abort()
                            })), this._options.signal = this.abortController.signal), this.request = new a.Request(this._input, this._options), this._options.searchParams) {
                            const e = "?" + new URLSearchParams(this._options.searchParams).toString(),
                                t = this.request.url.replace(/(?:\?.*?)?(?=#|$)/, e);
                            !(s && this._options.body instanceof a.FormData || this._options.body instanceof URLSearchParams) || this._options.headers && this._options.headers["content-type"] || this.request.headers.delete("content-type"), this.request = new a.Request(new a.Request(t, this.request), this._options)
                        }
                        void 0 !== this._options.json && (this._options.body = JSON.stringify(this._options.json), this.request.headers.set("content-type", "application/json"), this.request = new a.Request(this.request, {
                            body: this._options.body
                        }));
                        const n = async () => {
                                if (this._options.timeout > O) throw new RangeError("The `timeout` option cannot be greater than 2147483647");
                                await j(1);
                                let e = await this._fetch();
                                for (const t of this._options.hooks.afterResponse) {
                                    const n = await t(this.request, this._options, this._decorateResponse(e.clone()));
                                    n instanceof a.Response && (e = n)
                                }
                                if (this._decorateResponse(e), !e.ok && this._options.throwHttpErrors) throw new g(e);
                                if (this._options.onDownloadProgress) {
                                    if ("function" !== typeof this._options.onDownloadProgress) throw new TypeError("The `onDownloadProgress` option must be a function");
                                    if (!o) throw new Error("Streams are not supported in your environment. `ReadableStream` is missing.");
                                    return this._stream(e.clone(), this._options.onDownloadProgress)
                                }
                                return e
                            },
                            r = this._options.retry.methods.includes(this.request.method.toLowerCase()) ? this._retry(n) : n();
                        for (const [a, i] of Object.entries(m)) r[a] = async () => {
                            this.request.headers.set("accept", this.request.headers.get("accept") || i);
                            const e = (await r).clone();
                            if ("json" === a) {
                                if (204 === e.status) return "";
                                if (t.parseJson) return t.parseJson(await e.text())
                            }
                            return e[a]()
                        };
                        return r
                    }
                    _calculateRetryDelay(e) {
                        if (this._retryCount++, this._retryCount < this._options.retry.limit && !(e instanceof b)) {
                            if (e instanceof g) {
                                if (!this._options.retry.statusCodes.includes(e.response.status)) return 0;
                                const t = e.response.headers.get("Retry-After");
                                if (t && this._options.retry.afterStatusCodes.includes(e.response.status)) {
                                    let e = Number(t);
                                    return Number.isNaN(e) ? e = Date.parse(t) - Date.now() : e *= 1e3, "undefined" !== typeof this._options.retry.maxRetryAfter && e > this._options.retry.maxRetryAfter ? 0 : e
                                }
                                if (413 === e.response.status) return 0
                            }
                            return .3 * 2 ** (this._retryCount - 1) * 1e3
                        }
                        return 0
                    }
                    _decorateResponse(e) {
                        return this._options.parseJson && (e.json = async () => this._options.parseJson(await e.text())), e
                    }
                    async _retry(e) {
                        try {
                            return await e()
                        } catch (t) {
                            const a = Math.min(this._calculateRetryDelay(t), O);
                            if (0 !== a && this._retryCount > 0) {
                                await j(a);
                                for (const e of this._options.hooks.beforeRetry) {
                                    if (await e({
                                            request: this.request,
                                            options: this._options,
                                            error: t,
                                            retryCount: this._retryCount
                                        }) === p) return
                                }
                                return this._retry(e)
                            }
                            if (this._options.throwHttpErrors) throw t
                        }
                    }
                    async _fetch() {
                        for (const n of this._options.hooks.beforeRequest) {
                            const e = await n(this.request, this._options);
                            if (e instanceof Request) {
                                this.request = e;
                                break
                            }
                            if (e instanceof Response) return e
                        }
                        return !1 === this._options.timeout ? this._options.fetch(this.request.clone()) : (e = this.request.clone(), t = this.abortController, a = this._options, new Promise(((n, r) => {
                            const i = setTimeout((() => {
                                t && t.abort(), r(new b(e))
                            }), a.timeout);
                            a.fetch(e).then(n).catch(r).then((() => {
                                clearTimeout(i)
                            }))
                        })));
                        var e, t, a
                    }
                    _stream(e, t) {
                        const n = Number(e.headers.get("content-length")) || 0;
                        let r = 0;
                        return new a.Response(new a.ReadableStream({
                            start(a) {
                                const i = e.body.getReader();
                                t && t({
                                    percent: 0,
                                    transferredBytes: 0,
                                    totalBytes: n
                                }, new Uint8Array), async function e() {
                                    const {
                                        done: l,
                                        value: o
                                    } = await i.read();
                                    if (l) a.close();
                                    else {
                                        if (t) {
                                            r += o.byteLength;
                                            t({
                                                percent: 0 === n ? 0 : r / n,
                                                transferredBytes: r,
                                                totalBytes: n
                                            }, o)
                                        }
                                        a.enqueue(o), e()
                                    }
                                }()
                            }
                        }))
                    }
                }
                const v = (...e) => {
                        for (const t of e)
                            if ((!i(t) || Array.isArray(t)) && "undefined" !== typeof t) throw new TypeError("The `options` argument must be an object");
                        return d({}, ...e)
                    },
                    k = e => {
                        const t = (t, a) => new w(t, v(e, a));
                        for (const a of u) t[a] = (t, n) => new w(t, v(e, n, {
                            method: a
                        }));
                        return t.HTTPError = g, t.TimeoutError = b, t.create = e => k(v(e)), t.extend = t => k(v(e, t)), t.stop = p, t
                    };
                t.a = k()
            }).call(this, a("bqPV"))
        },
        KI7k: function(e, t, a) {
            "use strict";
            a.r(t);
            var n, r, i, l, o, s = a("mXGw"),
                c = a("UutA"),
                d = a("h7iG"),
                u = a("Ojv9"),
                m = a("JiVo"),
                h = a("xiTr"),
                p = a("bK7F"),
                g = a("Q5Gx"),
                b = a("b7Z7"),
                j = a("67yl"),
                x = a("QrBS"),
                y = a("n0tG"),
                f = a("XHwO"),
                O = a("C/iq"),
                w = a("oYCi"),
                v = function() {
                    var e = Object(m.a)(O.n.map((function(e) {
                        return {
                            image: e.slug,
                            title: e.name,
                            url: "/collection/".concat(e.slug),
                            text: e.text
                        }
                    })));
                    return Object(w.jsx)(f.a.Provider, {
                        value: {
                            eventSource: "BrowseCategories"
                        },
                        children: Object(w.jsx)(k, {
                            children: Object(w.jsxs)(p.a, {
                                children: [Object(w.jsx)(x.a, {
                                    textAlign: "center",
                                    children: Object(w.jsx)(y.a, {
                                        as: "h2",
                                        margin: "0",
                                        variant: "h3",
                                        children: "Browse by category"
                                    })
                                }), Object(w.jsx)(b.a, {
                                    className: "BrowseCategories--card-container",
                                    children: e.map((function(e) {
                                        return Object(w.jsx)(h.a, {
                                            containerClassName: "BrowseCategories--card",
                                            href: e.url,
                                            imageUrl: "/static/images/categories/".concat(e.image, ".png"),
                                            children: Object(w.jsx)(x.a, {
                                                alignItems: "center",
                                                height: 40,
                                                justifyContent: "center",
                                                children: Object(w.jsx)(y.a, {
                                                    as: "span",
                                                    className: "BrowseCategories--title",
                                                    variant: "h4",
                                                    children: e.title
                                                })
                                            })
                                        }, e.image)
                                    }))
                                })]
                            })
                        })
                    })
                },
                k = Object(c.d)(j.a).withConfig({
                    displayName: "BrowseCategoriesreact__Container",
                    componentId: "sc-1c6tvgt-0"
                })(["flex-direction:column;align-items:center;margin-bottom:80px;margin-top:40px;.BrowseCategories--card{place-self:center;transition:transform 0.1s ease-out;border-radius:", ";&:hover{box-shadow:", ";}}.BrowseCategories--card-container{display:grid;justify-content:center;grid-gap:15px;margin-top:50px;width:100%;padding:0 20px;", "}.BrowseCategories--title{color:", ";}"], (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.shadows.default
                }), Object(g.e)({
                    extraLarge: Object(c.c)(["grid-template-columns:repeat(", ",340px);grid-template-rows:repeat( ", ",282px );"], 3, Math.ceil(O.n.length / 3)),
                    tabletS: Object(c.c)(["padding:0 30px;grid-template-columns:repeat(", ",182px);grid-template-rows:repeat( ", ",156px );"], 2, Math.ceil(O.n.length / 2)),
                    phoneXs: Object(c.c)(["grid-template-columns:340px;grid-template-rows:repeat(", ",282px);"], Math.ceil(O.n.length))
                }), (function(e) {
                    return e.theme.colors.text.body
                })),
                C = a("HAVD"),
                F = a("B5kz"),
                _ = function() {
                    var e = Object(F.useQuery)("posts", (function() {
                        return T()
                    })).data;
                    return e ? Object(w.jsx)(S, {
                        children: Object(w.jsxs)(x.a, {
                            className: "FromBlog--main",
                            children: [Object(w.jsx)(y.a, {
                                as: "h2",
                                className: "FromBlog--title",
                                variant: "h3",
                                children: "Resources for getting started"
                            }), Object(w.jsx)(p.b, {
                                children: e.map((function(e) {
                                    return Object(w.jsx)(h.a, {
                                        className: "FromBlog--card",
                                        containerClassName: "FromBlog--card-container",
                                        href: e.link,
                                        imageHeight: 265,
                                        imageUrl: e.image,
                                        children: Object(w.jsx)(y.a, {
                                            as: "div",
                                            className: "FromBlog--card-title",
                                            dangerouslySetInnerHTML: {
                                                __html: e.title
                                            },
                                            variant: "h4"
                                        })
                                    }, e.link)
                                }))
                            })]
                        })
                    }) : Object(w.jsx)(x.a, {
                        minHeight: "470px"
                    })
                },
                S = Object(c.d)(x.a).attrs({
                    as: "section"
                }).withConfig({
                    displayName: "FromBlogreact__Container",
                    componentId: "sc-zaoynb-0"
                })(["flex-direction:column;align-items:center;margin-top:40px;margin-bottom:120px;.FromBlog--main{flex-direction:column;align-items:center;width:100%;.FromBlog--title{margin-bottom:48px;text-align:center;}.Carousel--left-arrow{left:-8px;top:247px;}.Carousel--right-arrow{right:-19px;top:247px;}.FromBlog--card-container{padding:0;", "}.FromBlog--card{", "}.FromBlog--card-title{color:", ";font-weight:600;font-size:16px;margin:10px 20px 10px 16px;height:50px;overflow:hidden;display:-webkit-box;-webkit-line-clamp:2;-webkit-box-orient:vertical;}}"], Object(g.e)({
                    small: Object(c.c)(["padding:2%;margin:0 10px;"]),
                    medium: Object(c.c)(["padding:1.3%;"])
                }), Object(g.e)({
                    small: Object(c.c)(["width:98%;"])
                }), (function(e) {
                    return e.theme.colors.text.body
                })),
                T = function() {
                    return C.a.get("https://opensea.io/blog/wp-json/wp/v2/posts?_fields=title,jetpack_featured_media_url,link&categories=86,152&per_page=6&order_date&exclude=5616").json().then((function(e) {
                        return e.map((function(e) {
                            return {
                                title: e.title.rendered,
                                link: e.link,
                                image: e.jetpack_featured_media_url
                            }
                        }))
                    }))
                },
                N = a("JAph"),
                A = a.n(N),
                D = a("qymy"),
                K = a("D4YM"),
                L = function() {
                    var e = [{
                        image: "wallet",
                        title: "Set up your wallet",
                        text: Object(w.jsxs)(y.a, {
                            marginTop: "4px",
                            children: ["Once you\u2019ve set up your wallet of choice, connect it to OpenSea by clicking the wallet icon in the top right corner. Learn about the", " ", Object(w.jsx)(D.a, {
                                className: "GettingStarted--link",
                                href: "https://openseahelp.zendesk.com/hc/en-us/articles/1500007978402-Wallets-supported-by-OpenSea",
                                children: "wallets we support"
                            }), "."]
                        })
                    }, {
                        image: "collection",
                        title: "Create your collection",
                        text: Object(w.jsxs)(y.a, {
                            marginTop: "4px",
                            children: ["Click", " ", Object(w.jsx)(D.a, {
                                className: "GettingStarted--link",
                                href: "/collections",
                                children: "My Collections"
                            }), " ", "and set up your collection. Add social links, a description, profile & banner images, and set a secondary sales fee."]
                        })
                    }, {
                        image: "nft",
                        title: "Add your NFTs",
                        text: Object(w.jsx)(y.a, {
                            marginTop: "4px",
                            children: "Upload your work (image, video, audio, or 3D art), add a title and description, and customize your NFTs with properties, stats, and unlockable content."
                        })
                    }, {
                        image: "sale",
                        title: "List them for sale",
                        text: Object(w.jsx)(y.a, {
                            marginTop: "4px",
                            children: "Choose between auctions, fixed-price listings, and declining-price listings. You choose how you want to sell your NFTs, and we help you sell them!"
                        })
                    }];
                    return Object(w.jsx)(f.a.Provider, {
                        value: {
                            eventSource: "GettingStarted"
                        },
                        children: Object(w.jsx)(U, {
                            children: Object(w.jsxs)(p.a, {
                                textAlign: "center",
                                children: [Object(w.jsx)(y.a, {
                                    as: "h2",
                                    variant: "h3",
                                    children: "Create and sell your NFTs"
                                }), Object(w.jsx)(x.a, {
                                    className: "GettingStarted--step-container",
                                    children: e.map((function(e) {
                                        return Object(w.jsx)(M, {
                                            imageUrl: "/static/images/icons/".concat(e.image, ".svg"),
                                            text: e.text,
                                            title: e.title
                                        }, e.image)
                                    }))
                                })]
                            })
                        })
                    })
                },
                M = function(e) {
                    var t = e.imageUrl,
                        a = e.title,
                        n = e.text;
                    return Object(w.jsxs)(j.a, {
                        className: "GettingStarted--step",
                        children: [Object(w.jsx)(A.a, {
                            alt: a,
                            height: 40,
                            src: t,
                            unoptimized: !0,
                            width: 40
                        }), Object(w.jsx)(y.a, {
                            marginBottom: "4px",
                            variant: "bold",
                            children: a
                        }), n]
                    })
                },
                U = Object(c.d)(j.a).withConfig({
                    displayName: "GettingStartedreact__Container",
                    componentId: "sc-ukft4p-0"
                })(["width:100%;padding-top:40px;padding-bottom:40px;", " .GettingStarted--step-container{flex-wrap:wrap;justify-content:space-around;margin:40px 0 20px 0;width:100%;.GettingStarted--step{max-width:100%;margin-bottom:20px;margin-right:20px;margin-left:20px;padding:0 20px;justify-content:flex-start;", " .GettingStarted--link{font-weight:600;color:", ";&:hover{color:", ";}}}}"], (function(e) {
                    return Object(K.b)({
                        variants: {
                            light: {
                                backgroundColor: e.theme.colors.cloud
                            },
                            dark: {
                                backgroundColor: e.theme.colors.withOpacity.oil
                            }
                        }
                    })
                }), Object(g.e)({
                    small: Object(c.c)(["max-width:240px;padding:0px;"]),
                    medium: Object(c.c)(["max-width:300px;"]),
                    large: Object(c.c)(["max-width:360px;"]),
                    extraLarge: Object(c.c)(["max-width:260px;"])
                }), (function(e) {
                    return e.theme.colors.primary
                }), (function(e) {
                    return e.theme.colors.darkSeaBlue
                })),
                E = a("9U3r"),
                B = a("TiKg"),
                R = a.n(B),
                P = a("aXrf"),
                I = a("bHgl"),
                V = Object(c.d)(x.a).attrs({
                    as: "section"
                }).withConfig({
                    displayName: "Promotionsreact__Container",
                    componentId: "sc-wzaqja-0"
                })(["width:100%;flex-direction:column;align-items:center;margin-top:40px;margin-bottom:120px;.Carousel--left-arrow{left:-17px;top:62.5%;}.Carousel--right-arrow{right:-17px;top:62.5%;}", ""], Object(g.e)({
                    extraLarge: Object(c.c)([".Carousel--left-arrow{top:63%;}.Carousel--right-arrow{top:63%;}"])
                })),
                z = function(e) {
                    var t = e.data,
                        r = Object(P.useFragment)(void 0 !== n ? n : n = a("iCgP"), t),
                        i = (null !== r && void 0 !== r ? r : []).filter((function(e) {
                            return Boolean(e.saleStartTime)
                        }));
                    if (0 === i.length) return null;
                    var l = R()();
                    return Object(w.jsxs)(V, {
                        children: [Object(w.jsx)(y.a, {
                            as: "h2",
                            marginBottom: "8px",
                            textAlign: "center",
                            variant: "h3",
                            children: "Notable Drops"
                        }), Object(w.jsx)(p.b, {
                            slidesNumber: i.length,
                            children: i.map((function(e) {
                                return Object(w.jsx)(I.b, {
                                    now: l,
                                    promotion: e
                                }, e.promoCardLink)
                            }))
                        })]
                    })
                },
                q = a("veNq"),
                H = a("4iFg"),
                G = a("m5he"),
                Y = a("QYJX"),
                Q = a("LoMF"),
                W = a("ocrj"),
                X = a("dI2x"),
                J = a("lqpq"),
                Z = a("9E9p"),
                $ = a("sX+s"),
                ee = a("1p8O"),
                te = a("LsOE"),
                ae = a("kCmG"),
                ne = a("LjoF"),
                re = a("tQfM"),
                ie = a("jQgF"),
                le = [{
                    label: "last 24 hours",
                    value: "ONE_DAY_VOLUME"
                }, {
                    label: "last 7 days",
                    value: "SEVEN_DAY_VOLUME"
                }, {
                    label: "last 30 days",
                    value: "THIRTY_DAY_VOLUME"
                }],
                oe = function() {
                    var e = Object(s.useState)(le[1]),
                        t = e[0],
                        a = e[1];
                    return Object(w.jsx)(he, {
                        children: Object(w.jsxs)(pe, {
                            children: [Object(w.jsx)(j.a, {
                                alignItems: "center",
                                className: "title",
                                flexDirection: "row",
                                marginBottom: "56px",
                                textAlign: "center",
                                children: Object(w.jsxs)(y.a, {
                                    as: "h2",
                                    margin: "0px",
                                    variant: "h3",
                                    children: ["Top collections over", Object(w.jsx)(W.a, {
                                        content: function(e) {
                                            var n = e.close,
                                                r = e.List,
                                                i = e.Item;
                                            return Object(w.jsx)(r, {
                                                children: le.map((function(e) {
                                                    return Object(w.jsx)(i, {
                                                        onClick: function() {
                                                            e.value !== t.value && a(e), n()
                                                        },
                                                        children: Object(w.jsx)(i.Content, {
                                                            children: Object(w.jsx)(x.a, {
                                                                alignItems: "center",
                                                                children: Object(w.jsx)(i.Title, {
                                                                    children: e.label
                                                                })
                                                            })
                                                        })
                                                    }, e.value)
                                                }))
                                            })
                                        },
                                        children: Object(w.jsxs)(x.a, {
                                            className: "TopCollections--dropdown",
                                            children: [Object(w.jsx)(y.a, {
                                                className: "TopCollections--category",
                                                fontSize: 24,
                                                margin: "0",
                                                textAlign: "center",
                                                variant: "faux-link",
                                                whiteSpace: "nowrap",
                                                children: t.label
                                            }), Object(w.jsx)(G.a, {
                                                className: "TopCollections--icon",
                                                color: "blue",
                                                value: "expand_more"
                                            })]
                                        })
                                    })]
                                })
                            }), ie.e ? Object(w.jsx)(ue, {}) : Object(w.jsx)(s.Suspense, {
                                fallback: Object(w.jsx)(ue, {}),
                                children: Object(w.jsx)(se, {
                                    sortBy: t.value
                                })
                            }), Object(w.jsx)(Q.c, {
                                href: "/rankings",
                                margin: "40px auto 0px",
                                width: "fit-content",
                                children: "Go to Rankings"
                            })]
                        })
                    })
                },
                se = function(e) {
                    var t = e.sortBy,
                        n = Object(P.useLazyLoadQuery)(void 0 !== r ? r : r = a("4TjO"), {
                            sortBy: t
                        });
                    return Object(w.jsx)(ce, {
                        dataKey: n,
                        sortBy: t
                    })
                },
                ce = function(e) {
                    var t = e.dataKey,
                        n = e.sortBy,
                        r = Object(P.useFragment)(void 0 !== i ? i : i = a("DQ1a"), t),
                        l = Object(te.d)(null === r || void 0 === r ? void 0 : r.collections).map((function(e, t) {
                            var a = e.name,
                                r = e.logo,
                                i = e.id,
                                l = e.slug,
                                o = e.isVerified,
                                s = Object(ae.f)(new Date(e.createdDate)),
                                c = "ONE_DAY_VOLUME" === n && e.stats.oneDayChange ? Object(ne.d)(e.stats.oneDayChange) : "SEVEN_DAY_VOLUME" === n && e.stats.sevenDayChange ? Object(ne.d)(e.stats.sevenDayChange) : "THIRTY_DAY_VOLUME" === n && e.stats.thirtyDayChange ? Object(ne.d)(e.stats.thirtyDayChange) : Object(ne.d)(0),
                                d = c.greaterThan(0),
                                u = c.times(100).toFixed(2).toLocaleString(),
                                m = "ONE_DAY_VOLUME" === n && e.stats.oneDayVolume ? Object(ne.j)(e.stats.oneDayVolume, 2) : "SEVEN_DAY_VOLUME" === n && e.stats.sevenDayVolume ? Object(ne.j)(e.stats.sevenDayVolume, 2) : "THIRTY_DAY_VOLUME" === n && e.stats.thirtyDayVolume ? Object(ne.j)(e.stats.thirtyDayVolume, 2) : "\uff0d";
                            return Object(w.jsxs)(Z.a, {
                                className: "TopCollections--item",
                                height: 88,
                                href: "/collection/".concat(l),
                                children: [Object(w.jsx)(j.a, {
                                    marginRight: "8px",
                                    children: Object(w.jsx)(y.a, {
                                        as: "span",
                                        fontSize: "16px",
                                        textAlign: "right",
                                        variant: "h4",
                                        children: Object(w.jsx)(Y.b, {
                                            children: t + 1
                                        })
                                    })
                                }), Object(w.jsx)(q.a, {
                                    isNew: s,
                                    isVerified: o,
                                    name: a,
                                    url: r
                                }), Object(w.jsxs)(Z.a.Content, {
                                    children: [Object(w.jsx)(Z.a.Title, {
                                        width: "100%",
                                        children: Object(w.jsx)(Y.b, {
                                            children: a
                                        })
                                    }), Object(w.jsx)(Z.a.Description, {
                                        children: Object(w.jsx)(H.a, {
                                            color: re.b.gray,
                                            fontSize: "14px",
                                            value: m
                                        })
                                    })]
                                }), Object(w.jsx)(Z.a.Side, {
                                    children: Object(w.jsx)(x.a, {
                                        children: c.equals(0) ? Object(w.jsx)(X.e, {
                                            color: re.b.gray,
                                            margin: "0",
                                            children: "\uff0d"
                                        }) : Object(w.jsx)(X.e, {
                                            as: "span",
                                            color: d ? "seaGrass" : "coral",
                                            fontWeight: 400,
                                            children: Object(w.jsx)(Y.b, {
                                                children: "".concat(d ? "+" : "").concat(u, "%")
                                            })
                                        })
                                    })
                                })]
                            }, i)
                        }));
                    return de(l)
                },
                de = function(e) {
                    return Object(w.jsxs)(pe, {
                        children: [Object(w.jsx)($.a, {
                            greaterThanOrEqual: "xl",
                            children: Object(w.jsxs)(x.a, {
                                children: [Object(w.jsx)(J.a, {
                                    marginRight: "44px",
                                    minWidth: "30%",
                                    children: e.slice(0, 5)
                                }), Object(w.jsx)(J.a, {
                                    marginRight: "44px",
                                    minWidth: "30%",
                                    children: e.slice(5, 10)
                                }), Object(w.jsx)(J.a, {
                                    marginRight: "44px",
                                    minWidth: "30%",
                                    children: e.slice(10, 15)
                                })]
                            })
                        }), Object(w.jsx)($.a, {
                            lessThan: "xl",
                            children: Object(w.jsx)(j.a, {
                                children: Object(w.jsx)(J.a, {
                                    maxWidth: "440px",
                                    width: "100%",
                                    children: e
                                })
                            })
                        })]
                    })
                },
                ue = function() {
                    var e = me();
                    return de(e)
                },
                me = function() {
                    return new Array(15).fill(null).map((function(e, t) {
                        return Object(w.jsxs)(Z.a, {
                            className: "TopCollections--item",
                            height: 88,
                            width: "100%",
                            children: [Object(w.jsx)(j.a, {
                                marginRight: "8px",
                                children: Object(w.jsx)(ee.a.Line, {
                                    width: "16px"
                                })
                            }), Object(w.jsx)(ee.a.Circle, {
                                height: "50px",
                                width: "50px"
                            }), Object(w.jsxs)(Z.a.Content, {
                                children: [Object(w.jsx)(ee.a.Row, {
                                    height: "100%",
                                    width: "100%",
                                    children: Object(w.jsx)(ee.a.Line, {})
                                }), Object(w.jsx)(ee.a.Row, {
                                    height: "100%",
                                    width: "100%",
                                    children: Object(w.jsx)(ee.a.Line, {})
                                })]
                            }), Object(w.jsx)(Z.a.Side, {
                                children: Object(w.jsx)(ee.a.Line, {
                                    width: "64px"
                                })
                            })]
                        }, t)
                    }))
                },
                he = Object(c.d)(b.a).withConfig({
                    displayName: "TopCollectionsreact__Section",
                    componentId: "sc-1oks2s3-0"
                })(["padding:0;width:100%;max-width:1280px;margin-left:auto;margin-right:auto;", ""], Object(g.e)({
                    extraLarge: Object(c.c)(["padding:40px;"])
                })),
                pe = Object(c.d)(J.a).attrs({
                    as: "section"
                }).withConfig({
                    displayName: "TopCollectionsreact__StyledContainer",
                    componentId: "sc-1oks2s3-1"
                })(["width:100%;.TopCollections--item{border-top:1px solid transparent;border-right:1px solid transparent;border-left:1px solid transparent;border-bottom:", ";&:hover{border-radius:", ";border:", ";}}.TopCollections--dropdown{display:inline-flex;align-items:center;cursor:pointer;justify-content:center;}.TopCollections--dropdown{display:inline-flex;align-items:center;cursor:pointer;justify-content:center;margin-left:8px;}"], (function(e) {
                    return "1px solid ".concat(e.theme.colors.border)
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return "1px solid ".concat(e.theme.colors.border)
                })),
                ge = a("uiiN"),
                be = a("TGkK"),
                je = a("ZwbU"),
                xe = a("bbd2"),
                ye = a("Ld9l"),
                fe = a.n(ye),
                Oe = a("2A7z"),
                we = a("nuco"),
                ve = a("j/Wi"),
                ke = a("t3V9"),
                Ce = a("7Yyi"),
                Fe = a("DWpj"),
                _e = "Featured",
                Se = fe()((function() {
                    return a.e(95).then(a.bind(null, "psbw")).then((function(e) {
                        return e.GetFeaturedModal
                    }))
                }), {
                    loadableGenerated: {
                        webpack: function() {
                            return ["psbw"]
                        },
                        modules: ["../features/home/components/Featured/Featured.react.tsx -> features/home/components/GetFeaturedModal"]
                    }
                }),
                Te = function(e) {
                    var t, n = e.data,
                        r = Object(P.useFragment)(void 0 !== l ? l : l = a("Aujd"), n),
                        i = {
                            backgroundImage: "url(".concat((null === r || void 0 === r ? void 0 : r.imagePreviewUrl) || (null === r || void 0 === r ? void 0 : r.imageUrl), ")")
                        },
                        o = function() {
                            return Object(w.jsx)(je.a, {
                                trigger: function(e) {
                                    return Object(w.jsx)(ke.a, {
                                        "aria-label": "Get featured",
                                        onClick: function(t) {
                                            t.preventDefault(), e(), Object(Ce.a)()
                                        },
                                        children: Object(w.jsx)(ve.b, {
                                            content: "Get featured on the homepage",
                                            children: Object(w.jsx)(G.a, {
                                                color: "gray",
                                                cursor: "pointer",
                                                value: "info",
                                                variant: "outlined"
                                            })
                                        })
                                    })
                                },
                                children: Object(w.jsx)(Se, {})
                            })
                        },
                        s = function() {
                            return Object(w.jsx)(D.a, {
                                className: "Featured--learn-more",
                                href: "/#".concat(E.a),
                                children: Object(w.jsxs)(x.a, {
                                    className: "Featured--learn-more-container",
                                    children: [Object(w.jsx)(G.a, {
                                        className: "Featured--learn-more-icon",
                                        color: "blue",
                                        value: "play_circle_filled"
                                    }), Object(w.jsx)(y.a, {
                                        className: "Featured--learn-more-text",
                                        children: "Learn more about OpenSea"
                                    })]
                                })
                            })
                        };
                    return Object(w.jsx)(w.Fragment, {
                        children: Object(w.jsxs)(Ae, {
                            children: [Object(w.jsx)(b.a, {
                                className: "Featured--background-container",
                                children: Object(w.jsx)(Ne, {
                                    style: i
                                })
                            }), Object(w.jsxs)(x.a, {
                                className: "Featured--container",
                                children: [Object(w.jsxs)(x.a, {
                                    className: "Featured--title",
                                    children: [Object(w.jsx)(y.a, {
                                        className: "Featured--header",
                                        variant: "h1",
                                        children: O.U
                                    }), Object(w.jsx)(y.a, {
                                        as: "span",
                                        className: "Featured--subheader",
                                        variant: "subtitle",
                                        children: O.V
                                    }), Object(w.jsxs)(x.a, {
                                        className: "Featured--button-container",
                                        children: [Object(w.jsx)(b.a, {
                                            marginRight: "20px",
                                            children: Object(w.jsx)(Q.c, {
                                                className: "Featured--button",
                                                eventSource: _e,
                                                href: "/explore-collections",
                                                children: "Explore"
                                            })
                                        }), Object(w.jsx)(Q.c, {
                                            className: "Featured--button",
                                            eventSource: _e,
                                            href: "/asset/create",
                                            variant: "secondary",
                                            children: "Create"
                                        })]
                                    }), Object(w.jsx)($.a, {
                                        greaterThanOrEqual: "lg",
                                        children: function(e, t) {
                                            return t && Object(w.jsx)(x.a, {
                                                alignItems: "flex-end",
                                                className: e,
                                                height: "100%",
                                                children: Object(w.jsx)(s, {})
                                            })
                                        }
                                    })]
                                }), Object(w.jsx)(x.a, {
                                    className: "Featured--image",
                                    children: r ? Object(w.jsx)(x.a, {
                                        as: "article",
                                        className: "Featured--image-card",
                                        children: Object(w.jsxs)(D.a, {
                                            className: "Featured--image-link",
                                            href: Object(ae.e)(r),
                                            onClick: function() {
                                                var e, t;
                                                return Object(Fe.j)(r, {
                                                    assetName: r.name,
                                                    creatorUsername: null === (e = r.creator) || void 0 === e || null === (t = e.user) || void 0 === t ? void 0 : t.publicUsername,
                                                    link: Object(ae.e)(r)
                                                })
                                            },
                                            children: [Object(w.jsx)(Oe.a, {
                                                alt: "",
                                                asset: r,
                                                autoPlay: !0,
                                                className: "Featured--image-media",
                                                isMuted: !0,
                                                mediaStyles: {
                                                    objectFit: "cover",
                                                    width: "100%",
                                                    height: "100%",
                                                    borderRadius: "inherit",
                                                    borderBottomLeftRadius: 0,
                                                    borderBottomRightRadius: 0
                                                },
                                                size: 550
                                            }), r.creator && Object(w.jsxs)(Z.a, {
                                                as: "footer",
                                                className: "Featured--image-text-area",
                                                children: [Object(w.jsx)(Z.a.Avatar, {
                                                    alt: "Featured creator",
                                                    className: "Featured--image-avatar",
                                                    outline: 0,
                                                    size: 40,
                                                    src: r.creator.imageUrl
                                                }), Object(w.jsxs)(Z.a.Content, {
                                                    className: "Featured--image-content",
                                                    children: [Object(w.jsx)(Z.a.Title, {
                                                        children: r.name
                                                    }), Object(w.jsx)(Z.a.Description, {
                                                        className: "Featured--image-creator",
                                                        fontSize: 14,
                                                        children: null === (t = r.creator.user) || void 0 === t ? void 0 : t.publicUsername
                                                    })]
                                                }), Object(w.jsx)(Z.a.Side, {
                                                    children: Object(w.jsx)(o, {})
                                                })]
                                            })]
                                        })
                                    }) : Object(w.jsxs)(ee.a, {
                                        className: "Featured--skeleton",
                                        children: [Object(w.jsxs)(ee.a.Row, {
                                            className: "Featured--skeleton-row",
                                            children: [Object(w.jsx)(ee.a.Block, {
                                                className: "Featured--skeleton-block"
                                            }), Object(w.jsx)(ee.a.Block, {
                                                className: "Featured--skeleton-block",
                                                direction: "rtl"
                                            })]
                                        }), Object(w.jsx)(ee.a.Row, {
                                            children: Object(w.jsxs)(we.a, {
                                                children: [Object(w.jsx)(we.a.Avatar, {}), Object(w.jsxs)(we.a.Content, {
                                                    children: [Object(w.jsx)(we.a.Title, {}), Object(w.jsx)(we.a.Description, {})]
                                                }), Object(w.jsxs)(we.a.Side, {
                                                    children: [Object(w.jsx)(we.a.Title, {}), Object(w.jsx)(we.a.Description, {})]
                                                })]
                                            })
                                        })]
                                    })
                                }), Object(w.jsx)($.a, {
                                    lessThan: "lg",
                                    children: function(e, t) {
                                        return t && Object(w.jsx)(x.a, {
                                            className: e,
                                            justifyContent: "center",
                                            width: "100%",
                                            children: Object(w.jsx)(s, {})
                                        })
                                    }
                                })]
                            })]
                        })
                    })
                },
                Ne = Object(c.d)(x.a).withConfig({
                    displayName: "Featuredreact__Background",
                    componentId: "sc-vt8n24-0"
                })(["height:780px;background-size:cover;background-color:", ";background-position:center;opacity:0.3;filter:blur(8px);-webkit-filter:blur(8px);mask:linear-gradient(#fff,transparent);-webkit-mask:linear-gradient(#fff,transparent);", ";"], (function(e) {
                    return e.theme.colors.background
                }), Object(g.e)({
                    large: Object(c.c)(["height:586px;"])
                })),
                Ae = Object(c.d)(x.a).withConfig({
                    displayName: "Featuredreact__Container",
                    componentId: "sc-vt8n24-1"
                })(["height:780px;", " .Featured--background-container{width:100%;position:absolute;overflow:hidden;}.Featured--learn-more{z-index:2;cursor:pointer;.Featured--learn-more-container{align-items:center;height:30px;&:hover{color:", ";}.Featured--learn-more-icon{color:inherit;}.Featured--learn-more-text{margin-left:8px;font-weight:600;color:", ";&:hover{color:inherit;}}}}.Featured--container{margin:0 auto;max-width:", ";width:100%;flex-wrap:wrap;&:hover{box-shadow:10px;}.Featured--title{flex-direction:column;align-items:center;width:100%;padding:30px 20px 20px 20px;", " .Featured--header{margin:0;font-size:28px;text-align:center;z-index:2;max-width:330px;", "}.Featured--subheader{margin-top:20px;max-width:400px;text-align:center;z-index:2;font-size:18px;", " ", "}.Featured--button-container{margin-top:20px;z-index:2;", " .Featured--button{width:120px;", "}}}.Featured--image{flex-direction:column;align-items:center;width:100%;padding-top:0;", " .AssetMedia--img{border-bottom-left-radius:0;border-bottom-right-radius:0;}.Featured--skeleton{max-width:355px;", " .Featured--skeleton-row{border-top-left-radius:10px;border-top-right-radius:10px;margin-bottom:-10px;.Featured--skeleton-block{height:300px;", "}}}.Featured--image-card{width:100%;background-color:", ";flex-direction:column;border-radius:10px;z-index:2;max-width:355px;box-shadow:0px 0px 10px 0px ", ";", " &:hover{transition:box-shadow 0.3s ease-in;box-shadow:0px 0px 50px 0px ", ";}.Image--image{border-bottom-left-radius:0 !important;border-bottom-right-radius:0 !important;}.Featured--image-text-area{border:none;&:hover{box-shadow:none;border-bottom-left-radius:10px;border-bottom-right-radius:10px;}.Featured--image-avatar{object-fit:cover;}.Featured--image-creator{color:", ";&:hover{color:", ";}}}.Featured--image-link{border-radius:10px;.Featured--image-media{height:80vw;z-index:2;border-bottom:1px solid ", ";", "}}}}}"], Object(g.e)({
                    large: Object(c.c)(["height:586px;"])
                }), (function(e) {
                    return e.theme.colors.darkSeaBlue
                }), (function(e) {
                    return e.theme.colors.primary
                }), (function(e) {
                    return e.theme.maxWidth.smallPadding
                }), Object(g.e)({
                    large: Object(c.c)(["width:50%;padding:110px 20px 44px 30px;align-items:flex-start;"])
                }), Object(g.e)({
                    small: Object(c.c)(["font-size:32px;max-width:", "px;"], 550),
                    large: Object(c.c)(["text-align:left;max-width:100%;margin-right:10px;"]),
                    extraLarge: Object(c.c)(["font-size:45px;"])
                }), (function(e) {
                    return Object(K.b)({
                        variants: {
                            light: {
                                color: "".concat(e.theme.colors.oil, ";")
                            },
                            dark: {
                                color: "".concat(e.theme.colors.text.subtle, ";")
                            }
                        }
                    })
                }), Object(g.e)({
                    large: Object(c.c)(["font-size:24px;text-align:left;"])
                }), Object(g.e)({
                    large: Object(c.c)(["margin-top:40px;"])
                }), Object(g.e)({
                    large: Object(c.c)(["width:167px;"])
                }), Object(g.e)({
                    large: Object(c.c)(["width:50%;padding:40px 0px;align-items:flex-end;"])
                }), Object(g.e)({
                    large: Object(c.c)(["max-width:550px;"])
                }), Object(g.e)({
                    large: Object(c.c)(["height:420px;"])
                }), (function(e) {
                    return e.theme.colors.surface
                }), (function(e) {
                    return e.theme.colors.withOpacity.charcoal.light
                }), Object(g.e)({
                    large: Object(c.c)(["max-width:550px;margin-right:30px;"])
                }), (function(e) {
                    return e.theme.colors.withOpacity.charcoal.light
                }), (function(e) {
                    return e.theme.colors.primary
                }), (function(e) {
                    return e.theme.colors.darkSeaBlue
                }), (function(e) {
                    return e.theme.colors.border
                }), Object(g.e)({
                    phoneL: Object(c.c)(["height:300px;"]),
                    large: Object(c.c)(["height:420px;"])
                })),
                De = a("h64z"),
                Ke = a("0c5R"),
                Le = a("O4Bb"),
                Me = a("HSVd"),
                Ue = a("heV+"),
                Ee = void 0 !== o ? o : o = a("eqlB"),
                Be = function(e) {
                    var t, a, n, r = e.data,
                        i = Object(De.a)().announcementBanner;
                    Object(Ke.a)((function() {
                        Object(Le.k)()
                    }));
                    var l = Ue.a.parse({
                            show_delisted_notice: Ue.a.Optional(Ue.a.boolean)
                        }).show_delisted_notice,
                        o = null === i || void 0 === i ? void 0 : i.announcementBanner,
                        s = "HOMEPAGE_ONLY" == (null === o || void 0 === o ? void 0 : o.displayMode);
                    return Object(w.jsxs)(w.Fragment, {
                        children: [Object(w.jsx)(Re, {}), s && Object(w.jsx)(xe.a, {
                            ctaText: o.ctaText,
                            heading: o.heading,
                            headingMobile: o.headingMobile,
                            relayId: o.relayId,
                            text: o.text,
                            url: null !== (t = o.url) && void 0 !== t ? t : ""
                        }), Object(w.jsxs)(be.a, {
                            isLanding: !0,
                            isPageAnnouncementShown: s,
                            children: [Object(w.jsx)(Te, {
                                data: null !== (a = null === r || void 0 === r ? void 0 : r.featuredAsset) && void 0 !== a ? a : null
                            }), Object(w.jsx)(z, {
                                data: null !== (n = null === r || void 0 === r ? void 0 : r.promotions) && void 0 !== n ? n : null
                            }), Object(w.jsx)(oe, {}), Object(w.jsx)(d.a, {}), Object(w.jsx)(L, {}), Object(w.jsx)(_, {}), Object(w.jsx)(v, {}), Object(w.jsx)(E.b, {
                                showButton: !0
                            }), Object(w.jsx)(u.a, {}), l && Object(w.jsx)(je.a, {
                                isOpen: !0,
                                children: Object(w.jsx)(ge.a, {
                                    variant: "home",
                                    onClose: function() {
                                        return Me.a.push("/")
                                    }
                                })
                            })]
                        })]
                    })
                };
            Be.query = Ee, Be.getInitialProps = function() {
                return {
                    cacheMaxAge: 300,
                    variables: {}
                }
            };
            var Re = Object(c.b)(["html{scroll-behavior:smooth;}"]);
            t.default = Be
        },
        MFWt: function(e, t, a) {
            (window.__NEXT_P = window.__NEXT_P || []).push(["/home", function() {
                return a("KI7k")
            }])
        },
        eqlB: function(e, t, a) {
            "use strict";
            a.r(t);
            var n = function() {
                var e = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    },
                    t = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "imageUrl",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: [],
                        kind: "Fragment",
                        metadata: null,
                        name: "HomePageQuery",
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "PromotionType",
                            kind: "LinkedField",
                            name: "promotions",
                            plural: !0,
                            selections: [{
                                args: null,
                                kind: "FragmentSpread",
                                name: "Promotions_promotions"
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "featuredAsset",
                            plural: !1,
                            selections: [{
                                args: null,
                                kind: "FragmentSpread",
                                name: "Featured_data"
                            }],
                            storageKey: null
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [],
                        kind: "Operation",
                        name: "HomePageQuery",
                        selections: [{
                            alias: null,
                            args: null,
                            concreteType: "PromotionType",
                            kind: "LinkedField",
                            name: "promotions",
                            plural: !0,
                            selections: [e, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "promoCardImg",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "promoCardLink",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "promoHeader",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "cardColor",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "promoSubtitle",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "saleStartTime",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "saleEndTime",
                                storageKey: null
                            }],
                            storageKey: null
                        }, {
                            alias: null,
                            args: null,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "featuredAsset",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "name",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "imagePreviewUrl",
                                storageKey: null
                            }, t, {
                                alias: null,
                                args: null,
                                concreteType: "AccountType",
                                kind: "LinkedField",
                                name: "creator",
                                plural: !1,
                                selections: [t, {
                                    alias: null,
                                    args: null,
                                    concreteType: "UserType",
                                    kind: "LinkedField",
                                    name: "user",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "publicUsername",
                                        storageKey: null
                                    }, e],
                                    storageKey: null
                                }, e],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "AssetContractType",
                                kind: "LinkedField",
                                name: "assetContract",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "address",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "chain",
                                    storageKey: null
                                }, e],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "tokenId",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "animationUrl",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "backgroundColor",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "CollectionType",
                                kind: "LinkedField",
                                name: "collection",
                                plural: !1,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "DisplayDataType",
                                    kind: "LinkedField",
                                    name: "displayData",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "cardDisplayStyle",
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }, e],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "isDelisted",
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                kind: "ScalarField",
                                name: "displayImageUrl",
                                storageKey: null
                            }, e],
                            storageKey: null
                        }]
                    },
                    params: {
                        cacheID: "d4bf5ba145ffbbbac72b4b4ee4f41f34",
                        id: null,
                        metadata: {},
                        name: "HomePageQuery",
                        operationKind: "query",
                        text: "query HomePageQuery {\n  promotions {\n    ...Promotions_promotions\n    id\n  }\n  featuredAsset {\n    ...Featured_data\n    id\n  }\n}\n\nfragment AssetMedia_asset on AssetType {\n  animationUrl\n  backgroundColor\n  collection {\n    displayData {\n      cardDisplayStyle\n    }\n    id\n  }\n  isDelisted\n  imageUrl\n  displayImageUrl\n}\n\nfragment Featured_data on AssetType {\n  name\n  imagePreviewUrl\n  imageUrl\n  creator {\n    imageUrl\n    user {\n      publicUsername\n      id\n    }\n    id\n  }\n  ...asset_url\n  ...AssetMedia_asset\n  ...itemEvents_data\n}\n\nfragment Promotions_promotions on PromotionType {\n  id\n  promoCardImg\n  promoCardLink\n  promoHeader\n  cardColor\n  promoSubtitle\n  saleStartTime\n  saleEndTime\n}\n\nfragment asset_url on AssetType {\n  assetContract {\n    address\n    chain\n    id\n  }\n  tokenId\n}\n\nfragment itemEvents_data on AssetType {\n  assetContract {\n    address\n    chain\n    id\n  }\n  tokenId\n}\n"
                    }
                }
            }();
            n.hash = "7d0de3c5c67b1b686c74d048303e38e2", t.default = n
        },
        iCgP: function(e, t, a) {
            "use strict";
            a.r(t);
            var n = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: {
                    plural: !0
                },
                name: "Promotions_promotions",
                selections: [{
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "id",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "promoCardImg",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "promoCardLink",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "promoHeader",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "cardColor",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "promoSubtitle",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "saleStartTime",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "saleEndTime",
                    storageKey: null
                }],
                type: "PromotionType",
                abstractKey: null,
                hash: "732ba31c4732a35904b9bc354a9b5b6a"
            };
            t.default = n
        },
        uiiN: function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return c
            }));
            a("mXGw");
            var n = a("qymy"),
                r = a("LoMF"),
                i = a("ZwbU"),
                l = a("n0tG"),
                o = a("C/iq"),
                s = a("oYCi"),
                c = function(e) {
                    var t = e.onClose,
                        a = e.variant;
                    return Object(s.jsxs)(s.Fragment, {
                        children: [Object(s.jsx)(i.a.Header, {
                            children: Object(s.jsxs)(i.a.Title, {
                                children: ["account" === a ? "This item" : "The item you tried to visit", " ", "is no longer available on OpenSea"]
                            })
                        }), Object(s.jsx)(i.a.Body, {
                            children: Object(s.jsxs)("div", {
                                className: "DelistedNotice--content",
                                children: [Object(s.jsxs)(l.a, {
                                    children: ["account" === a ? "This item" : "The item you tried to visit", " ", "is no longer available on OpenSea. It will not be visible or accessible to anyone browsing the marketplace", "account" === a ? " or your profile" : "", "."]
                                }), Object(s.jsxs)(l.a, {
                                    children: ["To learn more about why", " ", "account" === a ? "this item" : "the item you tried to visit", " ", "is no longer available on OpenSea, read", " ", Object(s.jsx)(n.a, {
                                        href: "https://openseahelp.zendesk.com/hc/en-us/articles/1500010625362",
                                        children: "our Help Center guide on this topic"
                                    }), ". If you have questions or concerns regarding this action, contact the OpenSea team ", Object(s.jsx)(n.a, {
                                        href: o.Vb,
                                        children: "here"
                                    }), "."]
                                })]
                            })
                        }), Object(s.jsx)(i.a.Footer, {
                            children: Object(s.jsx)(r.c, {
                                variant: "secondary",
                                onClick: t,
                                children: "Close"
                            })
                        })]
                    })
                }
        },
        veNq: function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return m
            }));
            a("mXGw");
            var n = a("UutA"),
                r = a("uMSw"),
                i = a("Vy0S"),
                l = a("b7Z7"),
                o = a("QrBS"),
                s = a("j/Wi"),
                c = a("oYCi"),
                d = Object(n.d)(l.a).withConfig({
                    displayName: "CollectionAvatarreact__NewIndicator",
                    componentId: "sc-3iovjc-0"
                })(["background-color:", ";border-radius:", ";height:10px;min-width:10px;position:absolute;right:20.5%;top:0%;border:1px solid ", ";"], (function(e) {
                    return e.theme.colors.seaGrass
                }), (function(e) {
                    return e.theme.radii.circle
                }), (function(e) {
                    return e.theme.colors.border
                })),
                u = Object(n.d)(l.a).withConfig({
                    displayName: "CollectionAvatarreact__VerifiedIndicator",
                    componentId: "sc-3iovjc-1"
                })(["height:10px;min-width:10px;position:absolute;right:12.5%;bottom:16%;"]),
                m = function(e) {
                    var t = e.url,
                        a = e.isNew,
                        n = e.isVerified,
                        m = e.name;
                    return Object(c.jsxs)(l.a, {
                        position: "relative",
                        children: [Object(c.jsx)(o.a, {
                            border: "1px solid",
                            borderColor: "border",
                            borderRadius: "circle",
                            marginX: "8px",
                            children: Object(c.jsx)(r.a, {
                                alt: m ? "".concat(m, " profile image") : "",
                                size: 50,
                                sizing: "cover",
                                url: null !== t && void 0 !== t ? t : void 0,
                                variant: "round"
                            })
                        }), a && Object(c.jsx)(s.b, {
                            content: "New",
                            children: Object(c.jsx)(d, {})
                        }), n && Object(c.jsx)(s.b, {
                            content: "Verified Collection",
                            children: Object(c.jsx)(u, {
                                children: Object(c.jsx)(i.a, {
                                    size: "small"
                                })
                            })
                        })]
                    })
                }
        },
        z1wA: function(e, t, a) {
            "use strict";
            a.d(t, "a", (function() {
                return o
            }));
            a("mXGw");
            var n = a("UutA"),
                r = a("b7Z7"),
                i = a("B6yL"),
                l = a("oYCi"),
                o = function(e) {
                    var t = e.url,
                        a = e.autoplay,
                        n = e.loop,
                        r = e.showControls,
                        o = e.title;
                    if (!Object(i.k)(t)) return null;
                    var c = t.replace("youtu.be/", "www.youtube.com/watch?v=").replace(/\/(watch\?v=|embed\/)([A-Za-z0-9_-]+\??)/, "/embed/$2?playlist=$2"),
                        d = new URLSearchParams({
                            autoplay: a ? "1" : "0",
                            controls: r ? "1" : "0",
                            loop: n ? "1" : "0",
                            modestbranding: "1",
                            rel: "0"
                        }),
                        u = "".concat(c, "&").concat(d);
                    return Object(l.jsx)(s, {
                        children: Object(l.jsx)("iframe", {
                            allow: "accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture",
                            allowFullScreen: !0,
                            frameBorder: "0",
                            height: "315",
                            sandbox: "allow-popups allow-same-origin allow-scripts allow-presentation",
                            src: u,
                            title: o,
                            width: "560"
                        })
                    })
                },
                s = Object(n.d)(r.a).withConfig({
                    displayName: "YouTubeVideoreact__Container",
                    componentId: "sc-1e7ikaq-0"
                })(["height:0;padding-bottom:56.25%;position:relative;iframe,object,embed{height:100%;left:0;position:absolute;top:0;width:100%;}"])
        }
    },
    [
        [41, 2, 1, 6, 4, 3, 7, 9, 0, 5, 8, 10, 11, 12, 13, 16, 17, 20, 28, 32]
    ]
]);
//# sourceMappingURL=home-ba7e6554194859822086.js.map