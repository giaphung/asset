(window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [21], {
        "06eW": function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return T
            }));
            var a = n("m6w3"),
                r = n("uEoR"),
                l = n("mXGw"),
                i = n("K2DV"),
                c = n("9va6"),
                o = n("/sTg"),
                s = n("5bJd"),
                u = n("UutA"),
                d = n("ocrj"),
                p = n("dA/+"),
                m = n("m5he"),
                b = n("fEtS"),
                j = n("Ly9W"),
                f = n("67yl"),
                g = n("y7Mw"),
                O = n("9E9p"),
                h = n("nuco"),
                y = n("nB74"),
                v = n("t3V9"),
                x = n("X9C2"),
                w = n("8BrW"),
                k = n("oYCi");

            function C(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function S(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? C(Object(n), !0).forEach((function(t) {
                        Object(a.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : C(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }
            var T = function(e) {
                    var t = e.autoComplete,
                        n = e.disabled,
                        a = e.placeholder,
                        u = e.options,
                        g = e.onSelect,
                        x = e.renderItem,
                        C = e.value,
                        T = e.startEnhancer,
                        D = e.clearable,
                        M = void 0 === D || D,
                        I = e.searchFilter,
                        K = void 0 === I ? function(e, t) {
                            var n;
                            return !!(e.label.toLowerCase().includes(t) || null !== (n = e.description) && void 0 !== n && n.toLowerCase().includes(t))
                        } : I,
                        P = e.readOnly,
                        _ = e.emptyText,
                        L = e.autoFocus,
                        N = e.variant,
                        E = void 0 === N ? "search" : N,
                        U = e.name,
                        R = e.id,
                        B = e.isLoading,
                        G = void 0 !== B && B,
                        V = e.onChange,
                        q = e.style,
                        z = e.excludeSelectedOption,
                        Y = void 0 !== z && z,
                        X = e.maxHeight,
                        W = e.matcher,
                        H = void 0 === W ? function(e, t) {
                            return e.value === t
                        } : W,
                        Z = e.onOpenChange,
                        Q = e.overrides,
                        J = Object(l.useRef)(!1),
                        $ = Object(l.useRef)(null),
                        ee = Object(l.useState)(""),
                        te = ee[0],
                        ne = ee[1],
                        ae = te.toLowerCase(),
                        re = Object(i.a)($),
                        le = Object(r.a)(re, 1)[0],
                        ie = Object(l.useRef)(null),
                        ce = Object(p.a)(),
                        oe = ce.isOpen,
                        se = ce.open,
                        ue = ce.close,
                        de = ce.setIsOpen,
                        pe = Object(l.useMemo)((function() {
                            return u.find((function(e) {
                                return H(e, C)
                            }))
                        }), [u, C, H]),
                        me = function(e) {
                            var t, n;
                            return Object(b.a)(null !== (t = null === (n = ie.current) || void 0 === n ? void 0 : n.closest("[data-tippy-root]")) && void 0 !== t ? t : null, e)
                        };
                    Object(o.a)("Escape", oe ? ue : void 0), Object(s.a)($, (function(e) {
                        me(e.target) || ue()
                    }));
                    var be = Object(l.useMemo)((function() {
                        var e = P || "item" === E ? u : u.filter((function(e) {
                            return K(e, ae)
                        }));
                        return Y ? e.filter((function(e) {
                            return !H(e, C)
                        })) : e
                    }), [ae, u, Y, C, E, P, K, H]);
                    Object(l.useEffect)((function() {
                        null === Z || void 0 === Z || Z(oe)
                    }), [oe]), Object(l.useEffect)((function() {
                        var e, t;
                        ne(null !== (e = null !== (t = null === pe || void 0 === pe ? void 0 : pe.label) && void 0 !== t ? t : C) && void 0 !== e ? e : "")
                    }), [pe, C]);
                    var je = function(e) {
                            var t;
                            if (x) return x(e);
                            var n = e.Item,
                                a = e.item;
                            return Object(k.jsxs)(n, {
                                onBlur: function(e) {
                                    me(e.relatedTarget) || ue()
                                },
                                onClick: function() {
                                    g(a), a.value === C && a.label !== te && ne(a.label), ue()
                                },
                                children: [a.avatar && Object(k.jsx)(n.Avatar, S({}, a.avatar)), Object(k.jsxs)(n.Content, {
                                    children: [Object(k.jsx)(n.Title, {
                                        children: a.label
                                    }), a.description && Object(k.jsx)(n.Description, {
                                        children: a.description
                                    })]
                                })]
                            }, null !== (t = a.key) && void 0 !== t ? t : a.value)
                        },
                        fe = function(e) {
                            var t = !0 === e ? {
                                title: !0
                            } : e;
                            return Object(c.range)(0, t.count || 3).map((function(e) {
                                return Object(k.jsxs)(h.a, {
                                    "aria-label": "loading option",
                                    children: [t.avatar && Object(k.jsx)(h.a.Avatar, {}), Object(k.jsxs)(h.a.Content, {
                                        children: [t.title && Object(k.jsx)(h.a.Title, {}), t.description && Object(k.jsx)(h.a.Description, {})]
                                    })]
                                }, e)
                            }))
                        },
                        ge = Object(k.jsx)(m.a, {
                            "aria-label": "Show more",
                            color: "gray",
                            cursor: "pointer",
                            value: "keyboard_arrow_down"
                        }),
                        Oe = {
                            onClick: function() {
                                return de((function(e) {
                                    return !e
                                }))
                            },
                            onFocus: function() {
                                J.current || se(), J.current = !1
                            },
                            onMouseDown: function() {
                                J.current = !0
                            }
                        };
                    return Object(k.jsx)(d.a, S(S(S({
                        disabled: n,
                        visible: oe
                    }, 0 !== be.length || G ? {
                        content: function(e) {
                            var t = e.List,
                                n = e.Item,
                                a = e.close;
                            return Object(k.jsxs)(t, {
                                ref: ie,
                                children: [be.map((function(e) {
                                    return je({
                                        Item: n,
                                        item: e,
                                        close: a
                                    })
                                })), G ? fe(G) : null]
                            })
                        }
                    } : {
                        content: function() {
                            return Object(k.jsx)(f.a, {
                                padding: "32px",
                                children: _ || "No results"
                            })
                        }
                    }), {}, {
                        maxHeight: X,
                        minWidth: le,
                        offset: [0, 0]
                    }, null === Q || void 0 === Q ? void 0 : Q.Dropdown.props), {}, {
                        children: function() {
                            switch (E) {
                                case "search":
                                    return Object(k.jsx)(F, S({
                                        autoComplete: t,
                                        autoFocus: L,
                                        clearOnEscape: !0,
                                        clearable: M,
                                        cursor: P ? "pointer" : void 0,
                                        disabled: n,
                                        endEnhancer: G ? Object(k.jsx)(w.a, {
                                            marginLeft: "12px",
                                            children: Object(k.jsx)(y.a, {
                                                size: "small"
                                            })
                                        }) : Object(k.jsx)(w.a, {
                                            marginLeft: "12px",
                                            children: ge
                                        }),
                                        id: R,
                                        name: U,
                                        placeholder: a,
                                        readOnly: P,
                                        ref: $,
                                        startEnhancer: T ? Object(k.jsx)(w.a, {
                                            marginRight: "12px",
                                            children: T
                                        }) : void 0,
                                        style: q,
                                        value: te,
                                        onBlur: function(e) {
                                            J.current || me(e.relatedTarget) || (u.some((function(e) {
                                                return e.label === te
                                            })) || (C ? g(void 0) : ne("")), Object(b.b)($, e.relatedTarget) || ue())
                                        },
                                        onChange: function(e) {
                                            ne(e.currentTarget.value), !e.currentTarget.value && C && g(void 0), null === V || void 0 === V || V(e.currentTarget.value), se()
                                        }
                                    }, Oe));
                                case "item":
                                    return function() {
                                        var e, t;
                                        return Object(k.jsxs)(A, S(S(S({
                                            as: v.a,
                                            disabled: n,
                                            ref: $
                                        }, Oe), null === Q || void 0 === Q ? void 0 : Q.ContentItem.props), {}, {
                                            children: [T ? Object(k.jsx)(w.a, {
                                                marginRight: "12px",
                                                children: T
                                            }) : void 0, Object(k.jsx)("input", {
                                                placeholder: a,
                                                type: "hidden",
                                                value: te
                                            }), (null === pe || void 0 === pe ? void 0 : pe.avatar) && Object(k.jsx)(O.a.Avatar, S({}, pe.avatar)), Object(k.jsxs)(O.a.Content, {
                                                children: [Object(k.jsx)(O.a.Title, {
                                                    children: null !== (e = null !== (t = null === pe || void 0 === pe ? void 0 : pe.label) && void 0 !== t ? t : C) && void 0 !== e ? e : a
                                                }), (null === pe || void 0 === pe ? void 0 : pe.description) && Object(k.jsx)(O.a.Description, {
                                                    children: pe.description
                                                })]
                                            }), Object(k.jsx)(O.a.Side, {
                                                children: ge
                                            })]
                                        }))
                                    }();
                                default:
                                    throw new j.a(E)
                            }
                        }()
                    }))
                },
                D = Object(u.c)(["i{color:", ';}&:hover{i[aria-label="Show more"]{color:', ";}}"], (function(e) {
                    return e.theme.colors.gray
                }), (function(e) {
                    return e.theme.colors.darkGray
                })),
                F = Object(u.d)(g.a).withConfig({
                    displayName: "Selectreact__SelectInput",
                    componentId: "sc-1shssly-0"
                })(["", " ", ""], D, x.b),
                A = Object(u.d)(O.a).withConfig({
                    displayName: "Selectreact__SelectItem",
                    componentId: "sc-1shssly-1"
                })(["", " border-radius:", ";"], D, (function(e) {
                    return e.theme.borderRadius.default
                }))
        },
        "3+lx": function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return s
            }));
            n("mXGw");
            var a = n("UutA"),
                r = n("vkv6"),
                l = n("67yl"),
                i = n("j/Wi"),
                c = n("D4YM"),
                o = n("oYCi"),
                s = function(e) {
                    var t = e.size,
                        n = void 0 === t ? 14 : t;
                    return Object(o.jsx)(i.b, {
                        content: "ETH",
                        children: Object(o.jsx)(l.a, {
                            cursor: "pointer",
                            children: Object(o.jsx)(u, {
                                alt: "Ether symbol",
                                size: n
                            })
                        })
                    })
                },
                u = Object(a.d)(r.b).withConfig({
                    displayName: "EthLogoreact__EthAvatar",
                    componentId: "sc-bgaajd-0"
                })(["", ""], Object(c.b)({
                    variants: {
                        dark: {
                            filter: "brightness(3)"
                        }
                    }
                }));
            u.defaultProps = {
                src: "https://storage.opensea.io/files/6f8e2979d428180222796ff4a33ab929.svg"
            }
        },
        AsIE: function(e, t, n) {
            "use strict";
            n.d(t, "b", (function() {
                return se
            })), n.d(t, "a", (function() {
                return ue
            }));
            var a, r = n("m6w3"),
                l = n("oA/F"),
                i = n("mXGw"),
                c = n("9va6"),
                o = n("JAph"),
                s = n.n(o),
                u = n("aXrf"),
                d = n("UutA"),
                p = n("b7Z7"),
                m = n("67yl"),
                b = n("IOvR"),
                j = n("QrBS"),
                f = n("ZwbU"),
                g = n("6Ojl"),
                O = n("7//c"),
                h = n("1p8O"),
                y = n("n0tG"),
                v = n("t3V9"),
                x = n("dA/+"),
                w = n("0c5R"),
                k = n("XHwO"),
                C = n("DWpj"),
                S = n("LsOE"),
                T = n("CJkU"),
                D = n("Z2Bj"),
                F = n("LjoF"),
                A = n("7v7j"),
                M = n("C/iq"),
                I = n("+n/q"),
                K = n("NXiZ"),
                P = n("3+lx"),
                _ = n("m5he"),
                L = n("qymy"),
                N = n("Q5Gx"),
                E = n("/Zf5"),
                U = n("Kqrw"),
                R = n("oYCi");

            function B(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function G(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? B(Object(n), !0).forEach((function(t) {
                        Object(r.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : B(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }
            var V, q = function(e) {
                    var t = e.collectionKey,
                        r = e.emptyPlaceholder,
                        l = Object(u.useFragment)(void 0 !== a ? a : a = n("sFUV"), t),
                        i = l.discordUrl,
                        c = l.externalUrl,
                        o = l.instagramUsername,
                        s = l.mediumUsername,
                        d = l.telegramUrl,
                        p = l.twitterUsername,
                        m = l.connectedTwitterUsername,
                        b = {
                            fill: "seaBlue",
                            height: 24,
                            width: 24,
                            withHoverChange: !0
                        },
                        j = !!m,
                        f = j ? m : p,
                        g = [{
                            tooltip: "".concat(j ? "Connected " : "", "Twitter"),
                            url: f && Object(U.a)("twitter", f),
                            icon: Object(E.a)(G({
                                name: "twitter"
                            }, b))
                        }, {
                            tooltip: "Discord",
                            url: i,
                            icon: Object(E.a)(G({
                                name: "discord"
                            }, b))
                        }, {
                            tooltip: "Website",
                            url: c,
                            icon: Object(E.a)(G({
                                name: "website"
                            }, b))
                        }, {
                            tooltip: "Instagram",
                            url: o && Object(U.a)("instagram", o),
                            icon: Object(E.a)(G({
                                name: "instagram"
                            }, b))
                        }, {
                            tooltip: "Medium",
                            url: s && Object(U.a)("medium", s),
                            icon: Object(E.a)(G({
                                name: "medium"
                            }, b))
                        }, {
                            tooltip: "Telegram",
                            url: d,
                            icon: Object(E.a)(G({
                                name: "telegram"
                            }, b))
                        }].filter((function(e) {
                            return e.url
                        }));
                    return g.length ? Object(R.jsx)(z, {
                        children: g.map((function(e) {
                            return e.url && Object(R.jsx)(Y, {
                                children: Object(R.jsx)(O.b, {
                                    content: function() {
                                        return e.tooltip
                                    },
                                    children: Object(R.jsx)(L.a, {
                                        href: e.url,
                                        children: e.icon
                                    })
                                })
                            }, e.tooltip)
                        }))
                    }) : Object(R.jsx)(R.Fragment, {
                        children: r
                    })
                },
                z = d.d.ul.withConfig({
                    displayName: "SocialLinksreact__SocialLinkList",
                    componentId: "sc-10hw19o-0"
                })(["display:flex;flex-wrap:wrap;list-style-type:none;margin:-8px 0 0;padding:0;"]),
                Y = d.d.li.withConfig({
                    displayName: "SocialLinksreact__SocialLink",
                    componentId: "sc-10hw19o-1"
                })(["margin-right:16px;margin-top:8px;"]),
                X = "ReviewCollectionDetailsModal",
                W = function(e) {
                    var t, a, r = e.assetId,
                        l = e.hasConfirmedDetails,
                        i = Object(x.a)(!1),
                        c = i.isOpen,
                        o = i.toggle,
                        s = Object(u.useLazyLoadQuery)(void 0 !== V ? V : V = n("XuKg"), {
                            asset: r
                        }),
                        d = s.asset,
                        m = s.ethPaymentAssets,
                        b = d.assetContract,
                        f = b.address,
                        g = b.blockExplorerLink,
                        h = d.collection,
                        v = h.createdDate,
                        M = h.isMintable,
                        _ = h.name,
                        N = h.owner,
                        E = h.slug,
                        U = h.stats;
                    Object(w.a)((function() {
                        return Object(C.r)(d, {
                                collectionSlug: E
                            }),
                            function() {
                                l || Object(C.l)(d, {
                                    collectionSlug: E
                                })
                            }
                    }));
                    var B = null !== (t = Object(S.c)(m)) && void 0 !== t ? t : null,
                        G = U.totalSales,
                        z = U.totalSupply,
                        Y = U.totalVolume,
                        W = /( {2})|[-_~|]/.test(_),
                        H = [{
                            type: "Collection name",
                            info: {
                                heading: Object(R.jsxs)(j.a, {
                                    children: [Object(R.jsx)(L.a, {
                                        href: "/collection/".concat(E),
                                        target: "_blank",
                                        children: _
                                    }), M && Object(R.jsx)(K.a, {
                                        size: "small",
                                        verificationStatus: "mintable"
                                    })]
                                }),
                                warningText: W && "Name includes special characters or irregular spacing"
                            }
                        }, {
                            type: "Creator",
                            info: {
                                heading: N && Object(R.jsx)(I.a, {
                                    dataKey: N,
                                    target: "_blank",
                                    variant: "no-image"
                                }),
                                subheading: N && "(member since ".concat(Object(D.e)(N.createdDate).format("MMM D, YYYY"), ")")
                            }
                        }, {
                            type: "Total sales",
                            info: {
                                heading: "".concat(Object(F.f)(G), " ").concat(Object(A.l)("sale", G))
                            }
                        }, {
                            type: "Social links",
                            info: {
                                heading: Object(R.jsx)(q, {
                                    collectionKey: d.collection,
                                    emptyPlaceholder: "Not specified"
                                })
                            }
                        }, {
                            type: "Contract address",
                            info: {
                                heading: Object(R.jsx)(L.a, {
                                    href: g,
                                    target: "_blank",
                                    children: Object(T.f)(f)
                                })
                            }
                        }, {
                            type: "Total volume",
                            info: {
                                heading: Y ? Object(R.jsxs)(j.a, {
                                    alignItems: "center",
                                    children: [Object(R.jsx)(P.a, {
                                        size: 18
                                    }), Object(R.jsx)(p.a, {
                                        as: "span",
                                        marginLeft: "4px",
                                        children: Object(F.j)(Y, Y < 10 ? 3 : 0)
                                    })]
                                }) : "---",
                                subheading: null !== B && void 0 !== B && null !== (a = B.asset) && void 0 !== a && a.usdSpotPrice && Y ? "($".concat(Object(F.h)(Object(F.d)(Y).mul(B.asset.usdSpotPrice)), ")") : void 0
                            }
                        }, {
                            type: "Total items",
                            info: {
                                heading: "".concat(Object(F.f)(z), " ").concat(Object(A.l)("item", z))
                            }
                        }, {
                            type: "Created date",
                            info: {
                                heading: Object(D.f)(Object(D.e)(v))
                            }
                        }],
                        J = c ? H : H.slice(0, 4);
                    return Object(R.jsx)(k.a.Provider, {
                        value: {
                            eventSource: X
                        },
                        children: Object(R.jsxs)("div", {
                            className: "CollectionDetailsModal--content",
                            children: [Object(R.jsx)("ul", {
                                className: "CollectionDetailsModal--details",
                                children: J.map((function(e) {
                                    var t = e.type,
                                        n = e.info,
                                        a = n.subheading && Object(R.jsx)(Q, {
                                            children: n.subheading
                                        });
                                    return Object(R.jsx)("li", {
                                        className: "CollectionDetailsModal--detail",
                                        children: Object(R.jsxs)(j.a, {
                                            className: "CollectionDetailsModal--detail-content",
                                            children: [Object(R.jsx)(y.a, {
                                                as: "span",
                                                className: "CollectionDetailsModal--detail-type",
                                                children: t
                                            }), Object(R.jsxs)(j.a, {
                                                alignItems: "center",
                                                flexWrap: "wrap",
                                                children: [n.warningText && Object(R.jsx)(O.b, {
                                                    content: function() {
                                                        return n.warningText
                                                    },
                                                    children: Object(R.jsx)(te, {
                                                        color: "yellow",
                                                        cursor: "pointer",
                                                        value: "report_problem"
                                                    })
                                                }), Object(R.jsx)(Z, {
                                                    marginLeft: n.warningText ? "8px" : "0",
                                                    children: n.heading
                                                }), a]
                                            })]
                                        })
                                    }, t)
                                }))
                            }), Object(R.jsx)(ee, {
                                onClick: function() {
                                    c || Object(C.v)(d, {
                                        collectionSlug: E
                                    }), o()
                                },
                                children: Object(R.jsxs)(y.a, {
                                    color: "",
                                    textAlign: "center",
                                    variant: "bold",
                                    children: ["Show ", c ? "less" : "more"]
                                })
                            })]
                        })
                    })
                },
                H = function() {
                    return Object(R.jsx)(h.a, {
                        marginTop: "36px",
                        children: Object(c.times)(4, (function(e) {
                            return Object(R.jsxs)(h.a.Row, {
                                className: "CollectionDetailsModal--detail CollectionDetailsModal--detail-content",
                                children: [Object(R.jsx)(h.a.Line, {
                                    className: "CollectionDetailsModal--detail-type"
                                }), Object(R.jsx)(h.a.Line, {
                                    width: "100%"
                                })]
                            }, e)
                        }))
                    })
                },
                Z = Object(d.d)(y.a).attrs({
                    as: "span",
                    variant: "bold"
                }).withConfig({
                    displayName: "CollectionDetailsModalreact__MainText",
                    componentId: "sc-62xqrp-0"
                })(["margin-right:8px;max-width:100%;"]),
                Q = Object(d.d)(y.a).attrs({
                    as: "span"
                }).withConfig({
                    displayName: "CollectionDetailsModalreact__AccompanyingText",
                    componentId: "sc-62xqrp-1"
                })(["color:", ";font-size:14px;"], (function(e) {
                    return e.theme.colors.text.subtle
                })),
                J = Object(d.d)(f.a.Body).withConfig({
                    displayName: "CollectionDetailsModalreact__StyledBody",
                    componentId: "sc-62xqrp-2"
                })(["padding-bottom:16px;.CollectionDetailsModal--content{border:1px solid ", ";border-radius:", ";margin-top:24px;}.CollectionDetailsModal--details{list-style:none;margin:0;padding:0;}.CollectionDetailsModal--detail{&:not(:last-child){border-bottom:1px solid ", ";}}.CollectionDetailsModal--detail-content{align-items:center;padding:16px;font-size:16px;}.CollectionDetailsModal--detail-type{color:", ";flex:none;line-height:24px;margin-right:16px;width:80px;}", " .CollectionDetailsModal--details-header{color:", ";}"], (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), Object(N.e)({
                    tabletS: Object(d.c)(["padding:16px 24px;.CollectionDetailsModal--detail-content{padding:16px 24px;}.CollectionDetailsModal--detail-type{margin-right:70px;width:142px;}"])
                }), (function(e) {
                    return e.theme.colors.text.heading
                })),
                $ = Object(d.d)(m.a).withConfig({
                    displayName: "CollectionDetailsModalreact__FooterContent",
                    componentId: "sc-62xqrp-3"
                })([".CollectionDetailsModal--confirmation-label{color:", ";}"], (function(e) {
                    return e.theme.colors.text.heading
                })),
                ee = Object(d.d)(v.a).withConfig({
                    displayName: "CollectionDetailsModalreact__ShowDetailsButton",
                    componentId: "sc-62xqrp-4"
                })(["border-top:1px solid ", ";color:", ";justify-content:center;width:100%;"], (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.subtle
                })),
                te = Object(d.d)(_.a).withConfig({
                    displayName: "CollectionDetailsModalreact__VerticalAlignedIcon",
                    componentId: "sc-62xqrp-5"
                })(["vertical-align:middle;"]),
                ne = function(e) {
                    var t = e.assetId,
                        n = e.renderNextModal,
                        a = Object(i.useState)(!1),
                        r = a[0],
                        l = a[1],
                        c = Object(g.b)().onNext;
                    return Object(R.jsxs)(k.a.Provider, {
                        value: {
                            eventSource: X
                        },
                        children: [Object(R.jsx)(f.a.Header, {
                            children: Object(R.jsx)(f.a.Title, {
                                children: "This is an unreviewed collection"
                            })
                        }), Object(R.jsxs)(J, {
                            children: [Object(R.jsx)(m.a, {
                                height: "114px",
                                marginBottom: "40px",
                                position: "relative",
                                width: "100%",
                                children: Object(R.jsx)(s.a, {
                                    alt: "",
                                    layout: "fill",
                                    objectFit: "contain",
                                    priority: !0,
                                    src: "".concat(M.Sb, "/review-collection.png"),
                                    unoptimized: !0
                                })
                            }), Object(R.jsx)(j.a, {
                                alignItems: "center",
                                justifyContent: "center",
                                children: Object(R.jsxs)(y.a, {
                                    as: "div",
                                    className: "CollectionDetailsModal--details-header",
                                    children: ["Review this information to ensure it\u2019s what you want to buy.", " ", Object(R.jsx)(O.b, {
                                        content: function() {
                                            return Object(R.jsxs)(R.Fragment, {
                                                children: ["Learn more about protecting yourself from scams", " ", Object(R.jsx)(L.a, {
                                                    href: "https://opensea.io/blog/guides/how-to-safely-purchase-nfts-on-opensea/",
                                                    children: "here"
                                                }), "."]
                                            })
                                        },
                                        children: Object(R.jsx)(te, {
                                            color: "gray",
                                            cursor: "pointer",
                                            value: "info",
                                            variant: "outlined"
                                        })
                                    })]
                                })
                            }), Object(R.jsx)(i.Suspense, {
                                fallback: Object(R.jsx)(H, {}),
                                children: Object(R.jsx)(W, {
                                    assetId: t,
                                    hasConfirmedDetails: r
                                })
                            })]
                        }), Object(R.jsx)(f.a.Footer, {
                            children: Object(R.jsx)($, {
                                paddingX: {
                                    _: "0",
                                    md: "24px"
                                },
                                children: Object(R.jsxs)(j.a, {
                                    alignItems: "center",
                                    children: [Object(R.jsx)(b.a, {
                                        autoFocus: !0,
                                        checked: r,
                                        id: "review-confirmation",
                                        onChange: function() {
                                            return l(!0), void c(n())
                                        }
                                    }), Object(R.jsx)(p.a, {
                                        marginLeft: "8px",
                                        children: Object(R.jsx)(y.a, {
                                            as: "label",
                                            className: "CollectionDetailsModal--confirmation-label",
                                            htmlFor: "review-confirmation",
                                            children: "I understand that OpenSea has not reviewed this collection and blockchain transactions are irreversible."
                                        })
                                    })]
                                })
                            })
                        })]
                    })
                },
                ae = n("YO/S"),
                re = n("h64z"),
                le = n("J+za"),
                ie = ["assetId", "trigger", "shouldVerifyCollectionDetails", "renderMainModal"];

            function ce(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function oe(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? ce(Object(n), !0).forEach((function(t) {
                        Object(r.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : ce(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }
            var se = function(e) {
                    var t = e.assetId,
                        n = e.shouldVerifyCollectionDetails,
                        a = e.renderMainModal,
                        r = e.onClose;
                    return n && t ? Object(R.jsx)(ne, {
                        assetId: t,
                        renderNextModal: function() {
                            return a(r)
                        }
                    }) : a(r)
                },
                ue = function(e) {
                    var t = e.assetId,
                        n = e.trigger,
                        a = e.shouldVerifyCollectionDetails,
                        r = e.renderMainModal,
                        i = Object(l.a)(e, ie),
                        c = Object(re.a)().wallet.getActiveAccountKey(),
                        o = Object(le.a)();
                    return Object(R.jsx)(ae.a, oe(oe({
                        size: "large",
                        trigger: function(e) {
                            return n(o(e))
                        }
                    }, i), {}, {
                        children: function(e) {
                            return c && Object(R.jsx)(se, {
                                assetId: t,
                                renderMainModal: r,
                                shouldVerifyCollectionDetails: a,
                                onClose: e
                            })
                        }
                    }))
                }
        },
        "J+za": function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return c
            }));
            var a = n("qd51"),
                r = n("/dBk"),
                l = n.n(r),
                i = n("h64z"),
                c = function() {
                    var e = Object(i.a)(),
                        t = e.wallet,
                        n = e.updateContext,
                        r = t.getActiveAccountKey();
                    return function(e) {
                        return Object(a.a)(l.a.mark((function t() {
                            return l.a.wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        r || n({
                                            isWalletSidebarOpen: !0
                                        }), r && e();
                                    case 2:
                                    case "end":
                                        return t.stop()
                                }
                            }), t)
                        })))
                    }
                }
        },
        Kqrw: function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return l
            }));
            var a = n("7v7j"),
                r = n("Ly9W"),
                l = function(e, t) {
                    switch (e) {
                        case "github":
                            return "https://github.com/".concat(t);
                        case "instagram":
                            return "https://instagram.com/".concat(Object(a.m)(t));
                        case "linkedin":
                            return "https://linkedin.com/in/".concat(t);
                        case "medium":
                            return "https://www.medium.com/@".concat(t);
                        case "twitter":
                            return "https://twitter.com/".concat(t);
                        default:
                            throw new r.a(e)
                    }
                }
        },
        XuKg: function(e, t, n) {
            "use strict";
            n.r(t);
            var a = function() {
                var e = [{
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "asset"
                    }],
                    t = [{
                        kind: "Variable",
                        name: "asset",
                        variableName: "asset"
                    }],
                    n = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "address",
                        storageKey: null
                    },
                    a = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "blockExplorerLink",
                        storageKey: null
                    },
                    r = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "name",
                        storageKey: null
                    },
                    l = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "createdDate",
                        storageKey: null
                    },
                    i = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "slug",
                        storageKey: null
                    },
                    c = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "displayName",
                        storageKey: null
                    },
                    o = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "totalVolume",
                        storageKey: null
                    },
                    s = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "totalSales",
                        storageKey: null
                    },
                    u = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "totalSupply",
                        storageKey: null
                    },
                    d = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "isMintable",
                        storageKey: null
                    },
                    p = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "chain",
                        storageKey: null
                    },
                    m = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "tokenId",
                        storageKey: null
                    },
                    b = [{
                        kind: "Literal",
                        name: "asset_Symbol_Iexact",
                        value: "ETH"
                    }, {
                        kind: "Literal",
                        name: "first",
                        value: 1
                    }],
                    j = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "usdSpotPrice",
                        storageKey: null
                    },
                    f = {
                        alias: null,
                        args: null,
                        kind: "ScalarField",
                        name: "id",
                        storageKey: null
                    };
                return {
                    fragment: {
                        argumentDefinitions: e,
                        kind: "Fragment",
                        metadata: null,
                        name: "CollectionDetailsModalQuery",
                        selections: [{
                            alias: null,
                            args: t,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "asset",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetContractType",
                                kind: "LinkedField",
                                name: "assetContract",
                                plural: !1,
                                selections: [n, a],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "CollectionType",
                                kind: "LinkedField",
                                name: "collection",
                                plural: !1,
                                selections: [r, l, i, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "owner",
                                    plural: !1,
                                    selections: [n, l, c, {
                                        args: null,
                                        kind: "FragmentSpread",
                                        name: "AccountLink_data"
                                    }],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "CollectionStatsType",
                                    kind: "LinkedField",
                                    name: "stats",
                                    plural: !1,
                                    selections: [o, s, u],
                                    storageKey: null
                                }, d, {
                                    args: null,
                                    kind: "FragmentSpread",
                                    name: "SocialLinks_collection"
                                }],
                                storageKey: null
                            }, {
                                kind: "InlineDataFragmentSpread",
                                name: "itemEvents_data",
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "AssetContractType",
                                    kind: "LinkedField",
                                    name: "assetContract",
                                    plural: !1,
                                    selections: [n, p],
                                    storageKey: null
                                }, m]
                            }],
                            storageKey: null
                        }, {
                            alias: "ethPaymentAssets",
                            args: b,
                            concreteType: "PaymentAssetTypeConnection",
                            kind: "LinkedField",
                            name: "paymentAssets",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "PaymentAssetTypeEdge",
                                kind: "LinkedField",
                                name: "edges",
                                plural: !0,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "PaymentAssetType",
                                    kind: "LinkedField",
                                    name: "node",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetType",
                                        kind: "LinkedField",
                                        name: "asset",
                                        plural: !1,
                                        selections: [j],
                                        storageKey: null
                                    }],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: 'paymentAssets(asset_Symbol_Iexact:"ETH",first:1)'
                        }],
                        type: "Query",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: e,
                        kind: "Operation",
                        name: "CollectionDetailsModalQuery",
                        selections: [{
                            alias: null,
                            args: t,
                            concreteType: "AssetType",
                            kind: "LinkedField",
                            name: "asset",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "AssetContractType",
                                kind: "LinkedField",
                                name: "assetContract",
                                plural: !1,
                                selections: [n, a, f, p],
                                storageKey: null
                            }, {
                                alias: null,
                                args: null,
                                concreteType: "CollectionType",
                                kind: "LinkedField",
                                name: "collection",
                                plural: !1,
                                selections: [r, l, i, {
                                    alias: null,
                                    args: null,
                                    concreteType: "AccountType",
                                    kind: "LinkedField",
                                    name: "owner",
                                    plural: !1,
                                    selections: [n, l, c, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "config",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "isCompromised",
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        concreteType: "UserType",
                                        kind: "LinkedField",
                                        name: "user",
                                        plural: !1,
                                        selections: [{
                                            alias: null,
                                            args: null,
                                            kind: "ScalarField",
                                            name: "publicUsername",
                                            storageKey: null
                                        }, f],
                                        storageKey: null
                                    }, {
                                        alias: null,
                                        args: null,
                                        kind: "ScalarField",
                                        name: "imageUrl",
                                        storageKey: null
                                    }, f],
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    concreteType: "CollectionStatsType",
                                    kind: "LinkedField",
                                    name: "stats",
                                    plural: !1,
                                    selections: [o, s, u, f],
                                    storageKey: null
                                }, d, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "discordUrl",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "externalUrl",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "instagramUsername",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "mediumUsername",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "telegramUrl",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "twitterUsername",
                                    storageKey: null
                                }, {
                                    alias: null,
                                    args: null,
                                    kind: "ScalarField",
                                    name: "connectedTwitterUsername",
                                    storageKey: null
                                }, f],
                                storageKey: null
                            }, m, f],
                            storageKey: null
                        }, {
                            alias: "ethPaymentAssets",
                            args: b,
                            concreteType: "PaymentAssetTypeConnection",
                            kind: "LinkedField",
                            name: "paymentAssets",
                            plural: !1,
                            selections: [{
                                alias: null,
                                args: null,
                                concreteType: "PaymentAssetTypeEdge",
                                kind: "LinkedField",
                                name: "edges",
                                plural: !0,
                                selections: [{
                                    alias: null,
                                    args: null,
                                    concreteType: "PaymentAssetType",
                                    kind: "LinkedField",
                                    name: "node",
                                    plural: !1,
                                    selections: [{
                                        alias: null,
                                        args: null,
                                        concreteType: "AssetType",
                                        kind: "LinkedField",
                                        name: "asset",
                                        plural: !1,
                                        selections: [j, f],
                                        storageKey: null
                                    }, f],
                                    storageKey: null
                                }],
                                storageKey: null
                            }],
                            storageKey: 'paymentAssets(asset_Symbol_Iexact:"ETH",first:1)'
                        }]
                    },
                    params: {
                        cacheID: "f15f91ff67c74b0b831211a4c0649941",
                        id: null,
                        metadata: {},
                        name: "CollectionDetailsModalQuery",
                        operationKind: "query",
                        text: 'query CollectionDetailsModalQuery(\n  $asset: AssetRelayID!\n) {\n  asset(asset: $asset) {\n    assetContract {\n      address\n      blockExplorerLink\n      id\n    }\n    collection {\n      name\n      createdDate\n      slug\n      owner {\n        address\n        createdDate\n        displayName\n        ...AccountLink_data\n        id\n      }\n      stats {\n        totalVolume\n        totalSales\n        totalSupply\n        id\n      }\n      isMintable\n      ...SocialLinks_collection\n      id\n    }\n    ...itemEvents_data\n    id\n  }\n  ethPaymentAssets: paymentAssets(asset_Symbol_Iexact: "ETH", first: 1) {\n    edges {\n      node {\n        asset {\n          usdSpotPrice\n          id\n        }\n        id\n      }\n    }\n  }\n}\n\nfragment AccountLink_data on AccountType {\n  address\n  config\n  isCompromised\n  user {\n    publicUsername\n    id\n  }\n  ...ProfileImage_data\n  ...wallet_accountKey\n  ...accounts_url\n}\n\nfragment ProfileImage_data on AccountType {\n  imageUrl\n  address\n}\n\nfragment SocialLinks_collection on CollectionType {\n  discordUrl\n  externalUrl\n  instagramUsername\n  mediumUsername\n  telegramUrl\n  twitterUsername\n  connectedTwitterUsername\n}\n\nfragment accounts_url on AccountType {\n  address\n  user {\n    publicUsername\n    id\n  }\n}\n\nfragment itemEvents_data on AssetType {\n  assetContract {\n    address\n    chain\n    id\n  }\n  tokenId\n}\n\nfragment wallet_accountKey on AccountType {\n  address\n}\n'
                    }
                }
            }();
            a.hash = "7c08318ca3553cde33dea6ca42dec96f", t.default = a
        },
        atGD: function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return I
            }));
            var a = n("qd51"),
                r = n("etRO"),
                l = n("4jfz"),
                i = n("g2+O"),
                c = n("mHfP"),
                o = n("1U+3"),
                s = n("DY1Z"),
                u = n("m6w3"),
                d = n("/dBk"),
                p = n.n(d),
                m = n("mXGw"),
                b = n.n(m),
                j = n("b7Z7"),
                f = n("LoMF"),
                g = n("ZwbU"),
                O = n("n0tG"),
                h = n("vI8H"),
                y = n("C/iq"),
                v = n("xpX1"),
                x = n("tQfM"),
                w = n("m5he"),
                k = n("qymy"),
                C = n("/K/p"),
                S = n("oYCi");

            function T(e) {
                var t = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var n, a = Object(s.a)(e);
                    if (t) {
                        var r = Object(s.a)(this).constructor;
                        n = Reflect.construct(a, arguments, r)
                    } else n = a.apply(this, arguments);
                    return Object(o.a)(this, n)
                }
            }
            var D, F = function(e) {
                Object(c.a)(n, e);
                var t = T(n);

                function n() {
                    return Object(r.a)(this, n), t.apply(this, arguments)
                }
                return Object(l.a)(n, [{
                    key: "render",
                    value: function() {
                        var e = this.props,
                            t = e.className,
                            n = e.inputClassName,
                            a = e.onChange,
                            r = e.placeholder,
                            l = e.value,
                            i = e.inputValue,
                            c = e.containerClassName;
                        return Object(S.jsx)(C.a, {
                            className: t,
                            containerClassName: c,
                            errorInfo: "Invalid email.",
                            inputClassName: n,
                            inputValue: i,
                            placeholder: r,
                            resolve: function(e) {
                                return t = e, new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(t) ? e : void 0;
                                var t
                            },
                            value: l,
                            onChange: a
                        })
                    }
                }]), n
            }(b.a.Component);

            function A(e) {
                var t = function() {
                    if ("undefined" === typeof Reflect || !Reflect.construct) return !1;
                    if (Reflect.construct.sham) return !1;
                    if ("function" === typeof Proxy) return !0;
                    try {
                        return Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], (function() {}))), !0
                    } catch (e) {
                        return !1
                    }
                }();
                return function() {
                    var n, a = Object(s.a)(e);
                    if (t) {
                        var r = Object(s.a)(this).constructor;
                        n = Reflect.construct(a, arguments, r)
                    } else n = a.apply(this, arguments);
                    return Object(o.a)(this, n)
                }
            }
            var M = {
                    MATIC: "MATIC_TRADING",
                    MUMBAI: "MATIC_TRADING",
                    BSC: "BSC_TRADING",
                    BSC_TESTNET: "BSC_TRADING",
                    KLAYTN: "KLAYTN_TRADING",
                    BAOBAB: "KLAYTN_TRADING",
                    XDAI: "XDAI_TRADING"
                },
                I = function(e) {
                    Object(c.a)(o, e);
                    var t = A(o);

                    function o() {
                        var e;
                        Object(r.a)(this, o);
                        for (var l = arguments.length, c = new Array(l), s = 0; s < l; s++) c[s] = arguments[s];
                        return e = t.call.apply(t, [this].concat(c)), Object(u.a)(Object(i.a)(e), "state", {
                            email: "",
                            emailInput: "",
                            message: "",
                            subscribed: !1
                        }), Object(u.a)(Object(i.a)(e), "handleSubmit", Object(a.a)(p.a.mark((function t() {
                            var a, r, l, i;
                            return p.a.wrap((function(t) {
                                for (;;) switch (t.prev = t.next) {
                                    case 0:
                                        if (a = e.props.chainIdentifier, r = e.state.email, l = a && M[a]) {
                                            t.next = 5;
                                            break
                                        }
                                        throw new Error('No feature corresponds to the chain identifier "'.concat(a, '".'));
                                    case 5:
                                        if (!r) {
                                            t.next = 17;
                                            break
                                        }
                                        return i = e.context.mutate, e.setState({
                                            message: "Joining..."
                                        }), t.prev = 8, t.next = 11, i(void 0 !== D ? D : D = n("yu4c"), {
                                            feature: l,
                                            email: r
                                        });
                                    case 11:
                                        e.setState({
                                            message: "You have been added to the waitlist",
                                            subscribed: !0
                                        }), t.next = 17;
                                        break;
                                    case 14:
                                        t.prev = 14, t.t0 = t.catch(8), e.setState({
                                            message: t.t0.message
                                        });
                                    case 17:
                                    case "end":
                                        return t.stop()
                                }
                            }), t, null, [
                                [8, 14]
                            ])
                        })))), Object(u.a)(Object(i.a)(e), "renderModal", (function() {
                            var t = e.props.chainIdentifier,
                                n = e.state,
                                a = n.email,
                                r = n.emailInput,
                                l = n.message,
                                i = n.subscribed;
                            if (!t) return null;
                            var c = y.o[t];
                            return Object(S.jsxs)(S.Fragment, {
                                children: [Object(S.jsxs)(g.a.Body, {
                                    children: [Object(S.jsx)(w.a, {
                                        style: {
                                            color: x.b.seaBlue,
                                            fontSize: "94px"
                                        },
                                        value: "directions_boat"
                                    }), Object(S.jsxs)(O.a, {
                                        variant: "h3",
                                        children: [c, " trading coming soon!"]
                                    }), Object(S.jsxs)(O.a, {
                                        children: ["Enter your email below to get notified when trading for ", c, " ", "assets is available."]
                                    }), "MATIC" === t && Object(S.jsxs)(O.a, {
                                        children: ["If you'd like to experience an alpha trading experience, check out", " ", Object(S.jsx)(k.a, {
                                            href: "https://matic.opensea.io",
                                            children: "matic.opensea.io"
                                        }), "."]
                                    }), Object(S.jsx)(F, {
                                        className: "MultiChainTradingGate--input",
                                        inputValue: r,
                                        placeholder: "boaty@mcboat.com",
                                        value: a,
                                        onChange: function(t) {
                                            var n = t.value,
                                                a = t.inputValue;
                                            return e.setState({
                                                email: null !== n && void 0 !== n ? n : "",
                                                emailInput: a,
                                                message: ""
                                            })
                                        }
                                    }), Object(S.jsx)(O.a, {
                                        children: l
                                    })]
                                }), Object(S.jsx)(g.a.Footer, {
                                    children: Object(S.jsx)(f.c, {
                                        disabled: i,
                                        variant: "primary",
                                        onClick: e.handleSubmit,
                                        children: "Keep Me Posted"
                                    })
                                })]
                            })
                        })), e
                    }
                    return Object(l.a)(o, [{
                        key: "render",
                        value: function() {
                            var e = this.props,
                                t = e.chainIdentifier,
                                n = e.children,
                                a = e.collectionSlug,
                                r = e.isFungible;
                            return t && Object(v.e)(t, a, r) ? Object(S.jsx)(g.a, {
                                trigger: function(e) {
                                    return Object(S.jsx)(j.a, {
                                        onClickCapture: function(t) {
                                            t.stopPropagation(), t.preventDefault(), e()
                                        },
                                        children: n
                                    })
                                },
                                children: this.renderModal
                            }) : n
                        }
                    }]), o
                }(h.b)
        },
        fEtS: function(e, t, n) {
            "use strict";
            n.d(t, "b", (function() {
                return a
            })), n.d(t, "a", (function() {
                return r
            }));
            var a = function(e, t) {
                    return r(e.current, t)
                },
                r = function(e, t) {
                    return null === e || void 0 === e ? void 0 : e.contains(t)
                }
        },
        kDvn: function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return b
            }));
            var a = n("qd51"),
                r = n("/dBk"),
                l = n.n(r),
                i = (n("mXGw"), n("jg/+")),
                c = n("b7Z7"),
                o = n("LoMF"),
                s = n("ZwbU"),
                u = n("n0tG"),
                d = n("h64z"),
                p = n("C/iq"),
                m = n("oYCi"),
                b = function(e) {
                    var t = e.chainIdentifier,
                        n = e.children,
                        r = e.overrides,
                        b = Object(d.a)(),
                        j = b.wallet,
                        f = b.updateContext,
                        g = b.isEmbedded,
                        O = b.login,
                        h = j.getActiveAccountKey(),
                        y = Object(i.c)().chain,
                        v = Object(m.jsx)(s.a, {
                            trigger: function(e) {
                                var t;
                                return Object(m.jsx)(c.a, {
                                    style: null === r || void 0 === r || null === (t = r.unsupportedModalButton) || void 0 === t ? void 0 : t.style,
                                    onClickCapture: function() {
                                        var t = Object(a.a)(l.a.mark((function t(n) {
                                            return l.a.wrap((function(t) {
                                                for (;;) switch (t.prev = t.next) {
                                                    case 0:
                                                        if (n.stopPropagation(), n.preventDefault(), h) {
                                                            t.next = 10;
                                                            break
                                                        }
                                                        if (!g) {
                                                            t.next = 8;
                                                            break
                                                        }
                                                        return t.next = 6, O();
                                                    case 6:
                                                        t.next = 9;
                                                        break;
                                                    case 8:
                                                        f({
                                                            isWalletSidebarOpen: !0
                                                        });
                                                    case 9:
                                                        return t.abrupt("return");
                                                    case 10:
                                                        e();
                                                    case 11:
                                                    case "end":
                                                        return t.stop()
                                                }
                                            }), t)
                                        })));
                                        return function(e) {
                                            return t.apply(this, arguments)
                                        }
                                    }(),
                                    children: n
                                })
                            },
                            children: function(e) {
                                return function(e) {
                                    if (!t) return null;
                                    var n = p.o[t];
                                    return Object(m.jsxs)(m.Fragment, {
                                        children: [Object(m.jsx)(s.a.Header, {
                                            children: Object(m.jsxs)(s.a.Title, {
                                                children: ["Please switch to a wallet that supports ", n, " network"]
                                            })
                                        }), Object(m.jsx)(s.a.Body, {
                                            children: Object(m.jsxs)(u.a, {
                                                variant: "body",
                                                children: ["In order to trade items, connect to a ", n, " network wallet. Please lock your current wallet and connect with a wallet that supports ", n, " network."]
                                            })
                                        }), Object(m.jsx)(s.a.Footer, {
                                            children: Object(m.jsx)(o.c, {
                                                onClick: function() {
                                                    f({
                                                        isWalletSidebarOpen: !0
                                                    }), e()
                                                },
                                                children: "Connect Wallet"
                                            })
                                        })]
                                    })
                                }(e)
                            }
                        });
                    return t ? h && y ? "MATIC" === t ? "ETHEREUM" === y || "MATIC" === y ? Object(m.jsx)(m.Fragment, {
                        children: n
                    }) : v : y === t ? Object(m.jsx)(m.Fragment, {
                        children: n
                    }) : v : v : Object(m.jsx)(m.Fragment, {
                        children: n
                    })
                }
        },
        p6pn: function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return d
            }));
            var a = n("m6w3"),
                r = n("oA/F"),
                l = n("mXGw"),
                i = n("1hf2"),
                c = ["register"],
                o = ["ref"];

            function s(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function u(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? s(Object(n), !0).forEach((function(t) {
                        Object(a.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : s(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }
            var d = function() {
                var e = i.f.apply(void 0, arguments),
                    t = e.register,
                    n = Object(r.a)(e, c),
                    a = Object(l.useCallback)((function() {
                        var e = t.apply(void 0, arguments),
                            n = e.ref,
                            a = Object(r.a)(e, o);
                        return u({
                            inputRef: n,
                            id: a.name
                        }, a)
                    }), [t]);
                return u(u({}, n), {}, {
                    register: a
                })
            }
        },
        qt8R: function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return O
            }));
            var a = n("m6w3"),
                r = n("oA/F"),
                l = n("mXGw"),
                i = n("UutA"),
                c = n("FbDh"),
                o = n("b7Z7"),
                s = n("n0tG"),
                u = n("D4YM"),
                d = n("m5he"),
                p = n("uMSw"),
                m = n("oYCi"),
                b = ["text", "textVariant", "icon", "iconSize", "className", "variant", "imageUrl"];

            function j(e, t) {
                var n = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var a = Object.getOwnPropertySymbols(e);
                    t && (a = a.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), n.push.apply(n, a)
                }
                return n
            }

            function f(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? j(Object(n), !0).forEach((function(t) {
                        Object(a.a)(e, t, n[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(n)) : j(Object(n)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(n, t))
                    }))
                }
                return e
            }
            var g = {
                    PRIMARY: "primary",
                    SECONDARY: "secondary",
                    TERTIARY: "tertiary",
                    SUCCESS: "success",
                    WARNING: "warning",
                    ERROR: "error"
                },
                O = Object(l.forwardRef)((function(e, t) {
                    var n = e.text,
                        a = e.textVariant,
                        l = void 0 === a ? "small" : a,
                        i = e.icon,
                        c = e.iconSize,
                        o = void 0 === c ? 14 : c,
                        u = e.className,
                        j = e.variant,
                        g = e.imageUrl,
                        O = Object(r.a)(e, b);
                    return Object(m.jsxs)(h, f(f({
                        className: u,
                        ref: t,
                        variant: j
                    }, O), {}, {
                        children: [g ? Object(m.jsx)(p.a, {
                            alt: "",
                            size: o,
                            url: g
                        }) : null, i ? Object(m.jsx)(d.a, {
                            className: "Badge--icon",
                            size: o,
                            value: i
                        }) : null, n ? Object(m.jsx)(s.a, {
                            as: "span",
                            className: "Badge--text",
                            variant: l,
                            children: n
                        }) : null]
                    }))
                })),
                h = Object(i.d)(o.a).withConfig({
                    displayName: "Badgereact__DivContainer",
                    componentId: "sc-hnc3y2-0"
                })(["width:fit-content;border-radius:4px;padding:6px;display:flex;align-items:center;justify-content:space-around;background-color:", ";color:", ";.Badge--icon{margin-right:4px;}.Badge--text{", "}", ""], (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return e.theme.colors.text.subtle
                }), (function(e) {
                    return Object(c.n)({
                        variants: Object(u.a)(g, (function(t) {
                            return {
                                color: e.theme.colors.text.on[t]
                            }
                        }))
                    })
                }), (function(e) {
                    return Object(c.n)({
                        variants: Object(u.a)(g, (function(t) {
                            return {
                                backgroundColor: e.theme.colors[t],
                                color: e.theme.colors.text.on[t]
                            }
                        }))
                    })
                }))
        },
        rx0e: function(e, t, n) {
            "use strict";
            n.d(t, "a", (function() {
                return i
            }));
            var a = n("YYXE"),
                r = n("uq6L"),
                l = n("BOW+");

            function i(e, t) {
                Object(l.a)(2, arguments);
                var n = Object(r.a)(e),
                    i = Object(a.a)(t);
                return isNaN(i) ? new Date(NaN) : i ? (n.setDate(n.getDate() + i), n) : n
            }
        },
        sFUV: function(e, t, n) {
            "use strict";
            n.r(t);
            var a = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: null,
                name: "SocialLinks_collection",
                selections: [{
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "discordUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "externalUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "instagramUsername",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "mediumUsername",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "telegramUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "twitterUsername",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "connectedTwitterUsername",
                    storageKey: null
                }],
                type: "CollectionType",
                abstractKey: null,
                hash: "c21146c278207f1de9e5cb5dd6fc2413"
            };
            t.default = a
        },
        yu4c: function(e, t, n) {
            "use strict";
            n.r(t);
            var a = function() {
                var e = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "email"
                    },
                    t = {
                        defaultValue: null,
                        kind: "LocalArgument",
                        name: "feature"
                    },
                    n = [{
                        alias: null,
                        args: null,
                        concreteType: "WaitlistMutationType",
                        kind: "LinkedField",
                        name: "waitlist",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: [{
                                kind: "Variable",
                                name: "email",
                                variableName: "email"
                            }, {
                                kind: "Variable",
                                name: "feature",
                                variableName: "feature"
                            }],
                            kind: "ScalarField",
                            name: "join",
                            storageKey: null
                        }],
                        storageKey: null
                    }];
                return {
                    fragment: {
                        argumentDefinitions: [e, t],
                        kind: "Fragment",
                        metadata: null,
                        name: "MultiChainTradingGateMutation",
                        selections: n,
                        type: "Mutation",
                        abstractKey: null
                    },
                    kind: "Request",
                    operation: {
                        argumentDefinitions: [t, e],
                        kind: "Operation",
                        name: "MultiChainTradingGateMutation",
                        selections: n
                    },
                    params: {
                        cacheID: "10c6c3aa43928cc33946472bc492aa3a",
                        id: null,
                        metadata: {},
                        name: "MultiChainTradingGateMutation",
                        operationKind: "mutation",
                        text: "mutation MultiChainTradingGateMutation(\n  $feature: Feature!\n  $email: String!\n) {\n  waitlist {\n    join(feature: $feature, email: $email)\n  }\n}\n"
                    }
                }
            }();
            a.hash = "5e7e149b4e6e0d5f4fc6d677eac009bd", t.default = a
        }
    }
]);
//# sourceMappingURL=7c64e57289bb8c3caa70709b45d911f94e347953.8374f17099662b8839b2.js.map