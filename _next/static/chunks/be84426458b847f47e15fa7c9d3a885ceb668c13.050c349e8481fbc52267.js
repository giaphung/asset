(window.webpackJsonp_N_E = window.webpackJsonp_N_E || []).push([
    [14], {
        "2A7z": function(e, t, i) {
            "use strict";
            i.d(t, "a", (function() {
                return A
            }));
            var r, a = i("m6w3"),
                n = i("uEoR"),
                o = i("mXGw"),
                l = i("Ld9l"),
                s = i.n(l),
                c = i("aXrf"),
                d = i("UutA"),
                u = i("m5he"),
                h = i("uMSw"),
                b = i("b7Z7"),
                p = i("67yl"),
                m = i("5apE"),
                g = i("n0tG"),
                f = i("eV01"),
                j = i("u6YR"),
                v = i("B6yL"),
                w = i("D4YM"),
                x = i("C/iq"),
                y = i("oYCi");

            function O(e, t) {
                var i = Object.keys(e);
                if (Object.getOwnPropertySymbols) {
                    var r = Object.getOwnPropertySymbols(e);
                    t && (r = r.filter((function(t) {
                        return Object.getOwnPropertyDescriptor(e, t).enumerable
                    }))), i.push.apply(i, r)
                }
                return i
            }

            function k(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var i = null != arguments[t] ? arguments[t] : {};
                    t % 2 ? O(Object(i), !0).forEach((function(t) {
                        Object(a.a)(e, t, i[t])
                    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(i)) : O(Object(i)).forEach((function(t) {
                        Object.defineProperty(e, t, Object.getOwnPropertyDescriptor(i, t))
                    }))
                }
                return e
            }
            var C = s()((function() {
                    return Promise.all([i.e(33), i.e(37)]).then(i.bind(null, "PHJS")).then((function(e) {
                        return e.ModelScene
                    }))
                }), {
                    ssr: !1,
                    loadableGenerated: {
                        webpack: function() {
                            return ["PHJS"]
                        },
                        modules: ["../components/assets/AssetMedia/AssetMedia.react.tsx -> components/viz/ModelScene.react"]
                    }
                }),
                A = function(e) {
                    var t, a, l, s = e.asset,
                        d = e.autoPlay,
                        w = e.useCustomPlayButton,
                        O = e.isMuted,
                        A = e.showModel,
                        D = e.showControls,
                        P = e.mediaStyles,
                        R = e.size,
                        _ = e.alt,
                        F = e.className,
                        I = e.title,
                        U = e.width,
                        z = Object(o.useState)(!1),
                        K = z[0],
                        H = z[1],
                        E = Object(o.useRef)(null),
                        T = Object(m.b)().theme,
                        Y = Object(o.useRef)(null),
                        G = Object(f.a)(Y),
                        L = Object(n.a)(G, 1)[0],
                        X = Object(c.useFragment)(void 0 !== r ? r : r = i("DEU0"), s),
                        B = X.backgroundColor ? "#".concat(X.backgroundColor) : void 0,
                        J = function() {
                            return !d && w ? Object(y.jsx)(u.a, {
                                className: "AssetMedia--play-icon",
                                size: 24,
                                value: K ? "pause" : "play_arrow",
                                onClick: function(e) {
                                    var t, i;
                                    e.preventDefault(), K ? null === (t = E.current) || void 0 === t || t.pause() : null === (i = E.current) || void 0 === i || i.play(), H((function(e) {
                                        return !e
                                    }))
                                }
                            }) : null
                        },
                        W = function(e) {
                            var t, i = e.animationUrl,
                                r = e.imagePreviewUrl,
                                a = e.cardDisplayStyle,
                                n = d || K || !r;
                            return Object(y.jsx)("div", {
                                className: "AssetMedia--animation",
                                children: Object(v.j)(i) ? A ? Object(y.jsx)(b.a, {
                                    minHeight: "500px",
                                    width: "100%",
                                    children: Object(y.jsx)(C, {
                                        backgroundColor: B,
                                        url: i
                                    })
                                }) : r ? $(r, a) : null : Object(v.i)(i) ? Object(y.jsxs)(p.a, {
                                    className: "AssetMedia--playback-wrapper",
                                    style: {
                                        backgroundColor: B
                                    },
                                    onContextMenu: function(e) {
                                        return e.preventDefault()
                                    },
                                    children: [n ? Object(y.jsx)("video", {
                                        autoPlay: d || K,
                                        className: "AssetMedia--video",
                                        controls: D,
                                        controlsList: "nodownload",
                                        loop: !0,
                                        muted: O,
                                        playsInline: !0,
                                        poster: r,
                                        preload: "metadata",
                                        ref: E,
                                        style: P,
                                        children: Object(y.jsx)("source", {
                                            src: "".concat(i, "#t=0.001"),
                                            type: "video/".concat(Object(v.b)(i))
                                        })
                                    }) : r ? $(r, a) : null, Object(y.jsx)("div", {
                                        children: Object(v.j)(i) || !Object(v.i)(i) && !Object(v.k)(i) ? null : J()
                                    })]
                                }) : Object(y.jsx)(y.Fragment, {
                                    children: n ? Object(y.jsx)("iframe", {
                                        allow: "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture",
                                        allowFullScreen: !0,
                                        frameBorder: "0",
                                        height: "100%",
                                        sandbox: "".concat(Object(v.g)(i) ? "allow-same-origin " : "", "allow-scripts"),
                                        src: (t = i, t.replace("youtu.be/", "www.youtube.com/watch?v=").replace(/\/watch\?v=([A-Za-z0-9_-]+\??)/, "/embed/$1?autoplay=1&controls=0&loop=1&playlist=$1&")),
                                        style: {
                                            minHeight: "500px"
                                        },
                                        width: "100%"
                                    }) : r ? $(r, a) : null
                                })
                            })
                        },
                        $ = function(e, t) {
                            var i = "COVER" === t ? "cover" : "CONTAIN" === t ? "contain" : void 0;
                            return Object(y.jsx)(h.a, {
                                alt: _,
                                className: Object(j.a)("AssetMedia", {
                                    img: !0,
                                    invert: "dark" === T && e === x.Kb
                                }),
                                fade: !0,
                                imgStyle: k({
                                    width: "cover" === i ? "100%" : "auto",
                                    height: "cover" === i ? "100%" : "auto",
                                    maxWidth: "100%",
                                    maxHeight: "100%"
                                }, P),
                                isSpinnerShown: !0,
                                size: R,
                                sizing: i,
                                url: e,
                                width: U
                            })
                        },
                        V = X.animationUrl,
                        Z = X.collection,
                        Q = X.displayImageUrl,
                        q = X.imageUrl,
                        ee = null !== (t = null === (a = Z.displayData) || void 0 === a ? void 0 : a.cardDisplayStyle) && void 0 !== t ? t : "CONTAIN",
                        te = V && !Object(v.d)(V) ? V : Q && Object(v.i)(Q) ? Q : void 0,
                        ie = V && Object(v.e)(V) ? V : void 0,
                        re = V && Object(v.d)(V) ? V : Q || x.Kb;
                    return X.isDelisted ? Object(y.jsx)(h.a, {
                        alt: "Delisted item image",
                        className: Object(j.a)("AssetMedia", {
                            img: !0,
                            invert: "dark" === T
                        }),
                        fade: !0,
                        imgStyle: {
                            width: "100%",
                            height: "100%",
                            maxWidth: "100%",
                            maxHeight: "100%"
                        },
                        size: R,
                        sizing: "cover",
                        url: x.C,
                        width: U
                    }) : (l = ie && re ? function(e, t, i) {
                        return Object(y.jsx)("div", {
                            className: "AssetMedia--animation",
                            children: Object(y.jsxs)("div", {
                                className: "AssetMedia--playback-wrapper",
                                style: {
                                    backgroundColor: B
                                },
                                onContextMenu: function(e) {
                                    return e.preventDefault()
                                },
                                children: [$(t, i), Object(y.jsx)("audio", {
                                    autoPlay: d,
                                    className: "AssetMedia--audio",
                                    controls: D,
                                    controlsList: "nodownload",
                                    loop: !0,
                                    muted: O,
                                    preload: "none",
                                    ref: E,
                                    src: e
                                }), J()]
                            })
                        })
                    }(ie, re, ee) : Object(v.i)(re) ? W({
                        animationUrl: re,
                        cardDisplayStyle: ee
                    }) : te ? W({
                        animationUrl: te,
                        imagePreviewUrl: re,
                        cardDisplayStyle: ee
                    }) : !q && Q ? function(e, t) {
                        var i = "COVER" === t ? "cover" : "CONTAIN" === t ? "contain" : void 0,
                            r = Object(v.m)(re, {
                                freezeAnimation: !0
                            }),
                            a = {
                                backgroundImage: "url(".concat(r, ")")
                            },
                            n = L < 50 ? 35 : L < 130 ? 60 : L < 300 ? 85 : 150,
                            o = L > 130;
                        return Object(y.jsxs)(M, {
                            ref: Y,
                            children: [Object(y.jsx)(N, {
                                style: a
                            }), Object(y.jsxs)(p.a, {
                                height: "100%",
                                position: "absolute",
                                children: [Object(y.jsx)(h.a, {
                                    alt: _,
                                    className: Object(j.a)("AssetMedia", {
                                        "placeholder-img": !0
                                    }),
                                    fade: !0,
                                    imgStyle: {
                                        position: "absolute",
                                        top: "0",
                                        left: "0",
                                        maxWidth: "none",
                                        objectFit: "cover"
                                    },
                                    isSpinnerShown: !0,
                                    size: n,
                                    sizing: i,
                                    url: e,
                                    variant: "round"
                                }), o && Object(y.jsx)(g.a, {
                                    className: "AssetMedia--text",
                                    textAlign: "center",
                                    variant: "info",
                                    children: "Content not available yet"
                                })]
                            })]
                        })
                    }(re, ee) : $(re, ee), Object(y.jsx)(S, {
                        className: F,
                        title: I,
                        children: Object(y.jsx)(p.a, {
                            borderRadius: "inherit",
                            height: "100%",
                            minHeight: "inherit",
                            position: "relative",
                            style: {
                                backgroundColor: B
                            },
                            width: "100%",
                            children: l
                        })
                    }))
                },
                N = d.d.div.withConfig({
                    displayName: "AssetMediareact__PlaceholderBackground",
                    componentId: "sc-1v86bfg-0"
                })(["background-size:cover;background-position:center;background-repeat:none;border-radius:inherit;width:100%;height:0px;padding:50% 0;filter:blur(30px);mask:linear-gradient( 0deg,rgba(53,56,64,1) 0%,rgba(53,56,64,0.4) 100% );overflow:hidden;"]),
                M = Object(d.d)(p.a).withConfig({
                    displayName: "AssetMediareact__PlaceholderContainer",
                    componentId: "sc-1v86bfg-1"
                })(["position:relative;width:100%;background-color:", ";border-radius:inherit;.AssetMedia--text{color:", ";width:100%;text-shadow:0px 4px 4px rgba(0,0,0,0.25);padding:0 10px;}.AssetMedia--placeholder-img{position:relative;overflow:hidden;> img{border-radius:inherit;}}"], (function(e) {
                    return e.theme.colors.background
                }), (function(e) {
                    return e.theme.colors.white
                })),
                S = d.d.div.withConfig({
                    displayName: "AssetMediareact__DivContainer",
                    componentId: "sc-1v86bfg-2"
                })(["min-height:inherit;border-radius:inherit;height:100%;width:100%;.AssetMedia--animation{position:relative;min-height:inherit;max-height:100%;height:100%;width:100%;display:flex;flex-direction:column;justify-content:center;align-items:center;border-radius:inherit;.AssetMedia--playback-wrapper{height:100%;max-width:100%;width:100%;position:relative;justify-content:center;border-radius:inherit;.AssetMedia--audio{width:100%;outline:none;}.AssetMedia--video{height:100%;width:100%;}> img{border-radius:inherit;}}}.AssetMedia--play-icon,.AssetMedia--play-shadow{position:absolute;}.AssetMedia--play-icon{align-items:center;border-radius:50%;border:1px solid ", ";display:flex;height:32px;justify-content:center;bottom:8px;right:8px;width:32px;z-index:1;", " &:hover{box-shadow:", ";}}.AssetMedia--play-shadow{width:26px;height:26px;right:11px;bottom:11px;border-radius:50%;box-shadow:", ";pointer-events:none;}.AssetMedia--img{border-radius:inherit;> img{border-radius:inherit;}}.AssetMedia--invert{filter:grayscale(1) invert(1);}"], (function(e) {
                    return e.theme.colors.border
                }), (function(e) {
                    return Object(w.b)({
                        variants: {
                            light: {
                                color: e.theme.colors.gray,
                                backgroundColor: e.theme.colors.white,
                                "&:hover": {
                                    color: e.theme.colors.oil
                                }
                            },
                            dark: {
                                color: e.theme.colors.fog,
                                backgroundColor: e.theme.colors.oil,
                                "&:hover": {
                                    color: e.theme.colors.white,
                                    backgroundColor: e.theme.colors.ash
                                }
                            }
                        }
                    })
                }), (function(e) {
                    return e.theme.shadows.default
                }), (function(e) {
                    return e.theme.shadows.default
                }))
        },
        DEU0: function(e, t, i) {
            "use strict";
            i.r(t);
            var r = {
                argumentDefinitions: [],
                kind: "Fragment",
                metadata: null,
                name: "AssetMedia_asset",
                selections: [{
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "animationUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "backgroundColor",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    concreteType: "CollectionType",
                    kind: "LinkedField",
                    name: "collection",
                    plural: !1,
                    selections: [{
                        alias: null,
                        args: null,
                        concreteType: "DisplayDataType",
                        kind: "LinkedField",
                        name: "displayData",
                        plural: !1,
                        selections: [{
                            alias: null,
                            args: null,
                            kind: "ScalarField",
                            name: "cardDisplayStyle",
                            storageKey: null
                        }],
                        storageKey: null
                    }],
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "isDelisted",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "imageUrl",
                    storageKey: null
                }, {
                    alias: null,
                    args: null,
                    kind: "ScalarField",
                    name: "displayImageUrl",
                    storageKey: null
                }],
                type: "AssetType",
                abstractKey: null,
                hash: "a6846ccbe6f7f6f19a41fe8169ee4b20"
            };
            t.default = r
        },
        OsKK: function(e, t, i) {
            "use strict";
            i.d(t, "d", (function() {
                return c
            })), i.d(t, "c", (function() {
                return u
            })), i.d(t, "a", (function() {
                return h
            })), i.d(t, "b", (function() {
                return b
            }));
            var r = i("mXGw"),
                a = i.n(r),
                n = i("UutA"),
                o = i("b7Z7"),
                l = i("oYCi"),
                s = Object(n.d)(o.a).attrs((function(e) {
                    var t;
                    return {
                        as: null !== (t = e.as) && void 0 !== t ? t : "section"
                    }
                })).withConfig({
                    displayName: "Framereact__Frame",
                    componentId: "sc-139h1ex-0"
                })(["border-radius:", ";border:1px solid ", ";overflow:hidden;"], (function(e) {
                    return e.theme.borderRadius.default
                }), (function(e) {
                    return e.theme.colors.border
                })),
                c = Object(n.d)(s).withConfig({
                    displayName: "Framereact__InputFrame",
                    componentId: "sc-139h1ex-1"
                })([":focus-within{border-color:", ";}"], (function(e) {
                    return e.theme.colors.seaBlue
                }));
            t.e = s;
            var d = a.a.createContext({}),
                u = Object(r.forwardRef)((function(e, t) {
                    var i = e.children,
                        r = e.className;
                    return Object(l.jsx)(d.Provider, {
                        value: {
                            isFramed: !0
                        },
                        children: Object(l.jsx)("div", {
                            className: r,
                            ref: t,
                            children: i
                        })
                    })
                })),
                h = function(e) {
                    var t = e.children,
                        i = e.className;
                    return Object(l.jsx)(d.Provider, {
                        value: {
                            isFramed: !1
                        },
                        children: Object(l.jsx)("div", {
                            className: i,
                            children: t
                        })
                    })
                },
                b = d.Consumer
        },
        QYJX: function(e, t, i) {
            "use strict";
            i.d(t, "a", (function() {
                return c
            }));
            var r = i("uEoR"),
                a = i("mXGw"),
                n = i("UutA"),
                o = i("j/Wi"),
                l = i("eV01"),
                s = i("oYCi");
            t.b = function(e) {
                var t = e.children,
                    i = e.className,
                    n = e.as,
                    d = e.lines,
                    u = Object(a.useRef)(null),
                    h = Object(l.a)(u),
                    b = Object(r.a)(h, 2),
                    p = b[0],
                    m = b[1],
                    g = u.current && (u.current.scrollHeight > m || u.current.scrollWidth > p);
                return Object(s.jsx)(o.b, {
                    content: t,
                    disabled: !g,
                    children: Object(s.jsx)(c, {
                        $lines: d,
                        as: n,
                        className: i,
                        ref: u,
                        children: t
                    })
                })
            };
            var c = n.d.div.withConfig({
                displayName: "Overflowreact__OverflowContainer",
                componentId: "sc-10mm0lu-0"
            })(["width:100%;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;", ""], (function(e) {
                return e.$lines && Object(n.c)(["display:-webkit-box;-webkit-line-clamp:", ";-webkit-box-orient:vertical;white-space:normal;"], e.$lines)
            }))
        },
        d6H9: function(e, t, i) {
            "use strict";
            i("mXGw");
            var r = i("UutA"),
                a = i("4u0K"),
                n = i("u6YR"),
                o = i("Q5Gx"),
                l = i("oYCi");
            t.a = function(e) {
                var t = e.children,
                    i = e.className,
                    r = e.columnIndexClassName,
                    o = e.isHeader,
                    c = e.spaced;
                return Object(l.jsx)(s, {
                    className: Object(n.a)("Row", {
                        isHeader: o
                    }, i),
                    role: "row",
                    children: Object(a.e)(t, (function(e, t) {
                        return void 0 === e ? null : Object(l.jsx)("div", {
                            className: Object(n.a)("Row", {
                                cell: !0,
                                cellIsSpaced: c
                            }, null === r || void 0 === r ? void 0 : r[t]),
                            children: e
                        })
                    }))
                })
            };
            var s = r.d.div.withConfig({
                displayName: "Rowreact__DivContainer",
                componentId: "sc-amt98e-0"
            })(["display:flex;&:last-child{.Row--cell{border-bottom:none;}}.Row--cell{align-items:center;border-bottom:1px solid ", ";display:flex;flex:1 0 100px;overflow-x:auto;padding:16px 4px;&:first-child{padding-left:16px;}&:last-child{padding-right:16px;}}&.Row--isHeader{position:sticky;top:0;z-index:1;.Row--cell{background-color:", ";color:", ";padding-bottom:4px;padding-top:4px;}}.Row--cellIsSpaced{flex-basis:150px;}", ""], (function(e) {
                return e.theme.colors.border
            }), (function(e) {
                return e.theme.colors.header
            }), (function(e) {
                return e.theme.colors.text.heading
            }), Object(o.e)({
                mobile: Object(r.c)([".Row--cellIsSpaced{flex-basis:100px;}"])
            }))
        }
    }
]);
//# sourceMappingURL=be84426458b847f47e15fa7c9d3a885ceb668c13.050c349e8481fbc52267.js.map